//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DMSDATABASE
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbPasswordResetMst
    {
        public int PKResetCodeId { get; set; }
        public string Usercode { get; set; }
        public string UserRole { get; set; }
        public string ResetCode { get; set; }
        public Nullable<bool> IsUsed { get; set; }
        public Nullable<System.DateTime> UsedOn { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
