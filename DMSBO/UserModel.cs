﻿using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class UserModel
    {
        public IEnumerable<DMSBO.UserManagementModel> userManagementModel { get; set; }
        public int DepotId{get;set;}
        public string Name { get; set; }
        public string RoleCode { get; set; }
        public string Region { get; set; }
        public UserEnumration ActiveStatus { get; set; }
    }
}