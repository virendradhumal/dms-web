﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
  public  class DSRProductReport
    {

        public string DSRNo{get;set;}
        public DateTime DSRDate { get; set;}
        public string RegionCode { get; set; }
        public string DepotName { get; set; }
        public int DepotId { get; set; }  
        public string ItemCode{get;set;}
        public string ItemDescription{get;set;}
        public decimal UnitPriceExclusive{get;set;}
        public decimal UnitPriceInclusive{get;set;}
        public decimal VATAmount{get;set;}
        public int SaleQuantity{get;set;}
        public decimal SalePriceExclusive{get;set;}
        public decimal SalePriceInclusive{get;set;}
        public string DSRStatus{get;set;}
    }
}
