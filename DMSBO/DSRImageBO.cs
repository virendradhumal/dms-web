﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSRImageBO
    {
        public string ImageKey { get; set; }
        public string DSRImage { get; set; }
    }
}
