﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class FilterDepots
    {
        public string DepotName{get;set;}
        public int? Active{get;set;}
    }
}
