﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DMSBO
{
    public class CreateIDNBO
    {
        public int DepotId { get; set; }
        public string IDNNo { get; set; }
        public int IDNId { get; set; }
        public decimal IDNTotal { get; set; }
        public string Pathname { get; set; }
        public HttpPostedFileBase IDNPhoto { get; set; }
        public int OrderId { get; set; }
    }
}
