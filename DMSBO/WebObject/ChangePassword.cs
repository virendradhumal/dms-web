﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    /// <summary>
    /// Modal for Reseting password
    /// </summary>
    /// 
    [Serializable]
    public class ChangePassword
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string key { get; set; }
        [Required(ErrorMessage = "Please enter new password.")]
        [DataMember]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "New password and confirm password do not match."), Required(ErrorMessage = "Please enter confirm password.")]
        [DataMember]
        public string ConfirmPassword { get; set; }
        [DataMember]
        [Required(ErrorMessage="Please enter security code.")]
        public string CapthaValue { get; set; }
    }
}
