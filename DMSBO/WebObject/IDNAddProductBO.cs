﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DMSBO
{
    public class IDNAddProductBO
    {

        public string DepotName { get; set; }
        public int DepotId { get; set; }
        public string IDNNo { get; set; }
        public int IDNId { get; set; }
        public decimal TotalIDNAmount { get; set; }
        public string Path { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public DateTime IDNDate { get; set; }
        public HttpPostedFileBase GRRPhoto { get; set; }
        public string PathName { get; set; }
        public string GRRNo { get; set; }
        public string Status { get; set; }
        public string StatusGroup { get; set; }
        public IEnumerable<ProductIDNBO> productIDNBO { get; set; }
    }
}
