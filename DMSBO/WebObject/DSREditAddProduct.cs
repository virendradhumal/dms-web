﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSREditAddProduct
    {
        public int? OrderId { get; set; }
        public string ProductCode { get; set; }
        public int? Quantity { get; set; }
        public decimal? Rate { get; set; }
        public string Username { get; set; }
    }
}
