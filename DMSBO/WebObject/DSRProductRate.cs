﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSRProductRate
    {
        public decimal? Rate { get; set; }
        public string ProductCode { get; set; }
    }
}
