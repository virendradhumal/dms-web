﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class ProductIDNBO
    {
        public int IDNDId { get; set; }
        public int IDNDetailId { get; set; }
        public string DepotName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal Rate { get; set; }
        public int Quantity { get; set; }
        public Decimal Total { get; set; }
        public int SQuantity { get; set; }
        public int RQuantity { get; set; }
        public int EQuantity { get; set; }
    }
}
