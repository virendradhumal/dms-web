﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
   public class OrderAndDetail
    {
        public OrderManageModal orderManageModal;
        public IEnumerable<OrderDetailModal> orderDetailModal;
    }
}
