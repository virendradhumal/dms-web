﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class BankAccountBO
    {
        [DataMember]
        public string AccountNo { get; set; }
    }
}
