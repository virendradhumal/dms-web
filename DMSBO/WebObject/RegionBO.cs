﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class RegionBO
    {
       public string RegionCode { get; set; }
       public string RegionName { get; set; }
    }
}
