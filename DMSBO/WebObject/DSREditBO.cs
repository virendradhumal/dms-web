﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSREditBO
    {
        public IEnumerable<DMSBO.OrderManageModal> orderManageModal { get; set; }
        public int? DepotId { get; set; }
        public int? DSRId { get; set; }
        public string DSRNo { get; set; }
        public string DepotName { get; set; }
        public DateTime? DSRDate { get; set; }
        public Decimal DSRTotal { get; set; }
        public Decimal DSRDiscount { get; set; }
        public decimal DSRTotalSale { get; set; }
        public string DSRImage { get; set; }
        public string PCVNo{get;set;}
          public string PCSNo{get;set;}                                                     
           public decimal PCSAmount{get;set;}                                                 
        public string Status { get; set; }
        public string StatusGroup { get; set; }
        public IEnumerable<DSRImageBO> Images { get;set; }
    }
}
