﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class DepotBO
    {
        public int DepotId {get;set;}
        public string DepotName{get;set;}
    }
}
