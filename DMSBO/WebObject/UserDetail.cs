﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    /// <summary>
    /// Modal is used for Login password
    /// </summary>
    [Serializable]
    public class UserDetail
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string CapthaValue { get; set; }
        [DataMember]
        public bool Remember { get; set; }

    }
}
