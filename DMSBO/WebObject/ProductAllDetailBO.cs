﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    [DataContract]
    public class ProductAllDetailBO
    {
        [DataMember]
        public int PKProductId { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Description2 { get; set; }
        [DataMember]
        public string ProductGroup { get; set; }
        [DataMember]
        public string ProductGroupDescription { get; set; }
        [DataMember]
        public Nullable<decimal> PackSize { get; set; }
        [DataMember]
        public string PackDescription { get; set; }
        [DataMember]
        public Nullable<int> TaxType { get; set; }
        [DataMember]
        public Nullable<int> ActiveStatus { get; set; }
        [DataMember]
        public decimal DiscountValue { get; set; }
        [DataMember]
        public string DiscountType { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public decimal DiscAmount { get; set; }
        [DataMember]
        public decimal NetAmount { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public decimal Rate { get; set; }
        [DataMember]
        public decimal TaxAmount { get; set; }
    }
}
