﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
   public class OrderDetailModal
    {
        public int OrderId { get; set; }
        public int OrderDetailId { get; set; }
       public string ProductCode { get; set; }
       public string ProductName { get; set; }
       public int quantity { get; set; }
       public decimal rate { get; set; }
       public decimal taxamount { get; set; }
       public int total { get; set; }
 
    }


    }