﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{

    [Serializable]
    public class BankNameBO
    {
        [DataMember]
        public string BankName { get; set; }
        [DataMember]
        public int BankId { get; set; }
    }
}
