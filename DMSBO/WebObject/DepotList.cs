﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class DepotList
    {
        public int DepotId { get; set; }
        public string DepotName { get; set; }
        public string Region { get; set; }
        public string RegionCode { get; set; }
        public int Active { get; set; }
    }
}
