﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    [DataContract]
    public class OrderAllDetailBO
    {
        [DataMember]
        public int PKOrderDetailsMst { get; set; }
        [DataMember]
        public Nullable<int> FKORderId { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public Nullable<int> Quantity { get; set; }
        [DataMember]
        public Nullable<decimal> Rate { get; set; }
        [DataMember]
        public Nullable<decimal> TaxValue { get; set; }
        [DataMember]
        public string DiscountType { get; set; }
        [DataMember]
        public Nullable<decimal> DiscountValue { get; set; }
        [DataMember]
        public Nullable<decimal> Amount { get; set; }
        [DataMember]
        public Nullable<decimal> DiscAmount { get; set; }
        [DataMember]
        public Nullable<decimal> NetAmount { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedOn { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
    }
}
