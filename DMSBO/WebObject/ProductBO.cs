﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class ProductBO
    {
        public int PKProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description2 { get; set; }
        public string ProductGroup { get; set; }
        public string ProductGroupDescription { get; set; }
        public Nullable<decimal> PackSize { get; set; }
        public string PackDescription { get; set; }
        public Nullable<int> TaxType { get; set; }
        public Nullable<int> ActiveStatus { get; set; }
        public Nullable<int> DeleteStatus { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
