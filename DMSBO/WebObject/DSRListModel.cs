﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSRListModel
    {
     //   Required fields are Depot name / DSR Date / DSR no / Total Value ( Incluasive ) / Total Value (Exclusive ) / Vat amount / PCV no /PCV amount / PSC no / PSC amount / DSR Net Value / DSR Status.
        public int DSRId { get; set; }
        public string DepotName { get; set; }
        public string RegionCode { get; set; }
        public int DepotId { get; set; }
        public string DSRNo { get; set; }
        public DateTime DSRDate { get; set; }
        public Decimal DSRTotal { get; set; }
        public Decimal DSRDiscount { get; set; }
        public decimal DSRTotalSale { get; set; }
        public string DSRImage { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public IEnumerable<DSRImageBO> Images { get; set; }
        public decimal TotalValueInclusive { get; set; }
        public decimal TotalValueExclusive { get; set; }
        public string PCVNo { get; set; }
        public Decimal PCVAmount { get; set; }
        public string PCSNo { get; set; }
        public   decimal PCSAmount { get; set; }
        public decimal VatAmount { get; set; }
        public decimal DSRNetValue { get; set; }
    }
}
