﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class FilterBank
    {
       public string BankName { get; set; }
       public string AccountNo { get; set; }
       public int? Active { get; set; }
   }
}
