﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class BankBO
    {
          [DataMember]
        public int PKBankId { get; set; }
         [DataMember]
        public string BankName { get; set; }
          [DataMember]
        public string AccountNumber { get; set; }
          [DataMember]
        public string BankBranch { get; set; }
          [DataMember]
        public string RegionCodes { get; set; }
        [DataMember]
        public Nullable<int> ActiveStatus { get; set; }
          [DataMember]
        public Nullable<int> DeleteStatus { get; set; }
         [DataMember]
        public Nullable<System.DateTime> CreatedOn { get; set; }
          [DataMember]
        public string CreatedBy { get; set; }
          [DataMember]
        public Nullable<System.DateTime> ModifiedOn { get; set; }
          [DataMember]
        public string ModifiedBy { get; set; }
    }
}
