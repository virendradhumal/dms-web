﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class FilterOrders
    {
        public string OrderNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? DepotId { get; set; }
        public int? StatusId { get; set; }
    }
}
