﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class FilterIDN : IValidatableObject
    {
        public int? SourceDepotId { get; set; }
        public int? DestinationDepotId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Status { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (FromDate > ToDate)
            {
                yield return
                  new ValidationResult(errorMessage: "ToDate must be greater than FromDate",
                                       memberNames: new[] { "EndDate" });
            }
        }
    }
}
