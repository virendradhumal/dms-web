﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class BankTransactionBO
    {
            public int PKTransactionId { get; set; }
            public Nullable<int> FKDepotId { get; set; }
            public String  DepotName  { get; set; }
            public Nullable<int> FKDSRId { get; set; }
            public string DSRNo { get; set; }
            public Nullable<System.DateTime> TransactionDate { get; set; }
            public Nullable<decimal> TransactionAmount { get; set; }
            public Nullable<int> StatusId { get; set; }
            public string Status { get; set; }
            public string StatusGroup { get; set; }
            public string TransactionImage { get; set; }
            public Nullable<System.DateTime> CreatedOn { get; set; }
            public string CreatedBy { get; set; }
            public string RegionCode { get; set; }
            public string BankName { get; set; }
            public string BankBranch { get; set; }
            public string Path { get; set; }
            public int BankId { get; set; }
            public string Username { get; set; }
            public long TRNDate { get; set; }
            public string TransactionDateMillisecond { get; set; }
            public decimal DSRAmount { get; set; }
            public int DelayInDays { get; set; }
            public DateTime DSRDate { get; set; }
    }
}
