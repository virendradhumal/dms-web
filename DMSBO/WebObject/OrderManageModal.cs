﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    public class OrderManageModal
    {
        public int OrderId { get; set; }
        public string OrderNo { get; set; }
        public int? DepotId { get; set; }
        public string DepoName { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal OrderAmount { get; set; }
        public string Status { get; set; }
        public string StatusGroup { get; set; }


    }
}
