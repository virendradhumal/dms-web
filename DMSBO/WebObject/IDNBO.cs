﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class IDNBO
    {
    public int IDNId { get; set; }
    public string Path { get; set; }
    public string IDNNo{get;set;}
    public DateTime IDNDate { get; set; }
    public string GRRNo { get; set; }
    public string GRRDate { get; set; }
    public decimal IDNTotalAmount { get; set; }
    public int SourceDepotId { get; set; }
    public int DestinationDepotId { get; set; }
    public string SourceDepot{get;set;}
    public string DestinationDepot { get; set; }
    public string StatusGroup { get; set; } 
    public string Status { get; set; }
    public string DirectSupplierName { get; set; }
    }
}
