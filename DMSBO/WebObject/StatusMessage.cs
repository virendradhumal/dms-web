﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    /// <summary>
    /// Modal for function Status
    /// </summary>
    /// 
    [Serializable]
    [DataContract]
    public class StatusMessage
    {
        [DataMember]
        public bool Status { get; set; }
         [DataMember]
        public string Message { get; set; }
    }
}
