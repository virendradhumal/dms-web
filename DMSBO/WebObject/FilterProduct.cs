﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class FilterProduct
    {
        public string ProductCodeOrName { get; set; }
        public string ProductGroup { get; set; }
        public int? Active { get; set; }
        public IEnumerable<ProductBO> Productlist{get;set;}
    }
}
