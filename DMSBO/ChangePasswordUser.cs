﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class ChangePasswordUser
    {
        
        public string Username { get; set; }

        [Required(ErrorMessage="Current Password Required")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage="New Password Required")]

       [MaxLength(12,ErrorMessage="Maximum Length of Password is 12")]
       [MinLength(6, ErrorMessage = "Minimum Length of Password is 6")]
        public string NewPassword { get; set; }

       [Compare("NewPassword",ErrorMessage="New Password & Confirm Password Do not Match.")]
       public string NewConfirmPassword { get; set; }
    }
}
