﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DepotWisePriceExportBO
    {
      public  string DepotName { get; set; }
      public string SimpleCode { get; set; }
      public decimal SellingPrice { get; set; }
    }
}
