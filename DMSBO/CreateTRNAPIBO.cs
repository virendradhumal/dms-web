﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class CreateTRNAPIBO
    {
        public int DepotId {get;set;}
        public string Username {get;set;}
        public int BankId {get;set;}
        public string TRNNo {get;set;}
        public long TransactionDate { get; set; }
        public string DSRNo {get;set;}
        public decimal TransactionAmount { get; set; }
        public string Path { get; set; }
    }
}
