﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class UserExportBO
    {
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string Username { get; set; }
        public string RegionCode { get; set; }
        public string DepotName { get; set; }
        public string UserRoleCode { get; set; }
        public int Status { get; set; }
    }
}
