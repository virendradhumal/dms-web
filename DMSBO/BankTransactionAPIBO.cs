﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class BankTransactionAPIBO
    {
        public int PKTransactionId { get; set; }
        public Nullable<int> FKDepotId { get; set; }
        public String DepotName { get; set; }
        public Nullable<int> FKDSRId { get; set; }
        public string DSRNo { get; set; }
        public string TransactionDate { get; set; }
        public Nullable<decimal> TransactionAmount { get; set; }
        public Nullable<int> StatusId { get; set; }
        public string Status { get; set; }
        public string StatusGroup { get; set; }
        public string TransactionImage { get; set; }
        public string CreatedBy { get; set; }
        public string RegionCode { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public int BankId { get; set; }
    }
}
