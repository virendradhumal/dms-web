﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class GetGRRAPIBO
    {

        public int PKGRRId { get; set; }
        public string GRRImage { get; set; }
        public string IDNNo { get; set; }
        public string GRRType { get; set; }
        public string IDNDate { get; set; }
        public string DirectSupplierName { get; set; }
        public string GRRNo { get; set; }
        public string GRRDate { get; set; }
        public decimal GRRAmount { get; set; }
        public string SourceDepot { get; set; }
        public string StatusGroup { get; set; }
        public string Status { get; set; }
        public IEnumerable<ProductIDNAPIBO> GRRProducts { get; set; }

    }
}
