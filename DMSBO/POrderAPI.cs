﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class POrderAPI
    {
        public string OrderNo { get; set; }
        public string OrderDate { get; set; }
        public Nullable<decimal> OrderAmount { get; set; }
        public Nullable<decimal> OrderDiscAmount { get; set; }
        public Nullable<decimal> OrderNetAmount { get; set; }
        public string OrderStatus { get; set; }
    }
}
