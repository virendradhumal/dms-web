﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DirectSupplierExportBO
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public string OfficeAddress { get; set; }
        public int ActiveStatus { get; set; }

    }
}
