﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSRPCVReportBO
    {
        public string DSRNo { get; set; }
        public DateTime DSRDate { get; set; }
        public string RegionCode { get; set; }
        public string DepotName { get; set; }
        public int DepotId { get; set; }
        public string PCVNo{get;set;}
        public decimal PCVAmount { get;set;}
        public string DSRStatus { get; set; }
    }
}
