﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class UserBO
    {
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string Usercode { get; set; }
        public string Password { get; set; }
        public Nullable<int> DepotId { get; set; }
        public string RoleCode { get; set; }
        public Nullable<int> CanBeDeleted { get; set; }
        public Nullable<int> ActiveStatus { get; set; }
        public Nullable<int> DeleteStatus { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
