﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DirectSupplierBO
    {
        public int PKSupplierId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public string OfficeAddress { get; set; }
    }
}
