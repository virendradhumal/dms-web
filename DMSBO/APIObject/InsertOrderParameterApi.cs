﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class InsertOrderParameterApi
    {
        public int DepotId{get;set;}
        public string Username{get;set;}
        public int FKDSRId{get;set;}
        public decimal OrderAmount{get;set;}
        public decimal OrderDiscAmount{get;set;}
        public decimal OrderNetAmount{get;set;}
        public string OrderStatus{get;set;}
        public OrderAllDetailBO[] Products;
    }
}
