﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class CreateDSRParameter
    {
        public int DepotId {get;set;}
        public string Username {get;set;}
        public string DSRNo{get;set;}
        public long DSRDate {get;set;}
        public decimal DSRAmount {get;set;}
        public string PCVNo { get; set; }
        public string PCSNo { get; set; }
        public decimal PCSAmount { get; set; }
        public decimal DSRDiscAmount {get;set;}
        public decimal DSRNetAmount { get; set; }
        public string Path { get; set; }
    }
}
