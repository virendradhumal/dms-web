﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    [DataContract]
   public  class LoginResult
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Usercode { get; set; }
        [DataMember]
        public string UserRole { get; set; }
        [DataMember]
        public int DepotId { get; set; }
        [DataMember]
        public string DepotTitle { get; set;}
        
        [DataMember]
        public string RegionId { get; set; }
        [DataMember]
        public string EmailId { get; set; }
        [DataMember]
        public string mobileNo { get; set; }
    }
}
