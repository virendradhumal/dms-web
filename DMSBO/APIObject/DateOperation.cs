﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DateOperation
    {

        public static  DateTime FromMilliToDate(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
            return epoch.AddMilliseconds(unixTime);
        }


        public static string FromDateTOMilli(DateTime date)
        {
           string millisecond= date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString();
           return millisecond;
        }

        public static string FromDateTOMilliGRR(string date)
        {
            try
            {
                if (string.IsNullOrEmpty(date))
                    return "";
                DateTime cdate = Convert.ToDateTime(date);
                string millisecond = cdate.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString();
                return millisecond;
            }
            catch(Exception ex)
            {
                return "";
            }
        }
    }
}
