﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
  public class DSRListModelApi
  {

    public string DSRNo { get; set; }
    public string DSRDate { get; set; }
    public Decimal DSRAmount { get; set; }
    public Decimal DSRDiscAmount { get; set; }
    public decimal PCSAmount { get; set; }
    public decimal DSRNetAmount { get; set; }
    //public string DSRImage { get; set; }
    public decimal CurrentTransactionAmount { get; set; }
    public string DSRCurrentStatus { get; set; }
    public IEnumerable<DSRImageBO> Images { get; set; }
  }
}
