﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{

    [Serializable]
    [DataContract]
    public class GetOrderParameter
    {
       [DataMember]
        public int DepotId { get; set; }
        [DataMember]
        public int NoOfDays { get; set; }
    }
}
