﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class SpecificDSR
    {
        public int DSRId { get; set; }
        public string DSRNo { get; set; }
        public string DSRDate { get; set; }
        public Decimal DSRAmount { get; set; }
        public Decimal DSRDiscAmount { get; set; }
        public decimal DSRNetAmount { get; set; }
        public string DSRImage { get; set; }
        public string DSRCurrentStatus { get; set; }
        public string PCVNo { get; set; }
        public string PCSNo { get; set; }
        public decimal PCSAmount { get; set; }
        public IEnumerable<ProductAllDetailBO> Products { get; set; }
        public IEnumerable<DSRImageBO> Images { get; set; }
    }
}
