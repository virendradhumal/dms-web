﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DMSBO
{
  public class ProductDetailApiBO
  {
    [DataMember]
    public string ProductCode { get; set; }
    [DataMember]
    public string Description { get; set; }
    [DataMember]
    public string Description2 { get; set; }
    [DataMember]
    public string Group { get; set; }
    [DataMember]
    public Nullable<decimal> PackSize { get; set; }
    [DataMember]
    public string PackDescription { get; set; }
    [DataMember]
    public decimal SellingPrice { get; set; }
    [DataMember]
    public int StockQuantity { get; set; }

    public int DepotId { get; set; }

  }
}
