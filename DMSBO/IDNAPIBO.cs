﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class IDNAPIBO
    {
        public int PKIDNId { get; set; }
        public int FKPurchaseOrderId { get; set; }
        public string IDNImage { get; set; }
        public string IDNNo { get; set; }
        public string IDNDate { get; set; }
        public decimal IDNAmount { get; set; }
        public int SourceDepotId { get; set; }
        public int DestinationDepotId { get; set; }
        public string SourceDepot { get; set; }
        public string DestinationDepot { get; set; }
        public string StatusGroup { get; set; }
        public string Status { get; set; }
    }
}
