﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class CreateAPIIDNBO
    {
        public string IDNNo { get; set; }
        public int DepotId { get; set; }
        public int DestinationId { get; set; }
        public decimal IDNAmount { get; set; }
        public string IDNDate { get; set; }
        public string IDNImage { get; set; }
        public string Status { get; set; }
        public string Username { get; set; }
        public string VehicleNo { get; set; }
        public IEnumerable<ProductIDNAPIBO> IDNProducts { get; set; }
    }
}
