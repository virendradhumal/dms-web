﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSRReportFilter: IValidatableObject
    {
        [Required(ErrorMessage="Please Enter From Date")]
        public DateTime? FromDate { get; set; }
        [Required(ErrorMessage = "Please Enter To Date")]
        public DateTime? ToDate { get; set; }
        public string Status { get; set; }
        public string RegionCode { get; set; }
        public int? DepotId { get; set; }
        public string DSRNo { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (FromDate > ToDate)
            {
                yield return
                  new ValidationResult(errorMessage: "ToDate must be greater than FromDate",
                                       memberNames: new[] { "EndDate" });
            }
        }
    }
}
