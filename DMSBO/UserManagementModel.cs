﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class UserManagementModel
    {   
        public string Name { get; set; }
        public string Region { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string Usercode { get; set; }
        public string RoleCode { get; set; }
        public string DepotId { get; set; }
        public string DepoName { get; set; }
        public Nullable<int> ActiveStatus { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
