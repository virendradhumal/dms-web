﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class ProductIDNAPIBO
    {
        public int IDNDId { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public decimal Rate { get; set; }
        public int SourceQuantity { get; set; }
        public Decimal Total { get; set; }
        public int ShortageQuantity { get; set; }
        public int ReceivedQuantity { get; set; }
        public int ExcessQuantity { get; set; }
    }
}
