﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class GRRApproveAPI
    {
        public string Username { get; set; }
        public string GRRNo { get; set; }
        public string DSRStatus { get; set; }
        public string Message { get; set; }
        public long DateTime { get; set; }
    }
}
