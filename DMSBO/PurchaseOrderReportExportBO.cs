﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class PurchaseOrderReportExportBO
    {
        public string DepotName {get;set;}
        public int DepotId { get; set; }
        public string PONo {get;set;}
        public DateTime PODate {get;set;}
        public string ItemCode {get;set;}
        public string ItemDescription{get;set;}
        public int OrderQty {get;set;}
        public int Delayindays {get;set;}
        public string IBTNo{get;set;}
        public DateTime IBTDate{get;set;}
        public int QtySent {get;set;}
        public int  BalanceQty {get;set;}
        public string BranchName {get;set;}
        public int POCompletedInDays { get; set; }
        public string Status { get;set;}

    }
}
