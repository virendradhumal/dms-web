﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class UploadDSRImageBO
    {
 
        public int DepotId { get; set; }
        public string Username { get; set; }
        public string DSRNo { get; set; }
        public string ImageKey { get; set; }
        public string Path { get; set; }
    }
}
