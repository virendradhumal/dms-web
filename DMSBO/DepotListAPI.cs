﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DepotListAPI
    {
        public int DepotId { get; set; }
        public string DepotName { get; set; }
        public string RegionCode { get; set; }
    }
}
