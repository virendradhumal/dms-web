﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
  public class GetIDNAPIBO
  {

    public int PKIDNId { get; set; }
    public int FKPurchaseOrderId { get; set; }
    public string IDNImage { get; set; }
    public string IDNNo { get; set; }
    public string IDNDate { get; set; }
    public decimal IDNAmount { get; set; }
    public string DestinationDepot { get; set; }
    public string SourceDepot { get; set; }
    public string StatusGroup { get; set; }
    public string Status { get; set; }
    public int SourceId { get; set; }
    public int DestinationId { get; set; }
    public int DepotId { get; set; }
    public string VehicleNo { get; set; }

    public IEnumerable<ProductIDNAPIBO> IDNProducts { get; set; }
  }
}
