﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DepotExportBO
    {
        public string RegionCode { get; set; }
        public string DepotTitle { get; set; }
        public int Status { get; set; }
    }
}
