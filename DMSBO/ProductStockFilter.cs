﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class ProductStockFilter
    {
        public string RegionCode { get; set; }
        public int? DepotId { get; set; }   
        public string ProductCodeOrName { get; set; }
        public string  ProductGroup { get; set; }
        public int? Active { get; set; }
    }
}
