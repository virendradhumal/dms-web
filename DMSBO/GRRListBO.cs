﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class GRRListBO
    {
        public int PKGRRId { get; set; }
        public Nullable<int> FKIDNId { get; set; }
        public Nullable<int> FKDirectSupplierId { get; set; }
        public string GRRType { get; set; }
        public string GRRNo { get; set; }
        public Nullable<System.DateTime> GRRDate { get; set; }
        public Nullable<decimal> GRRAmount { get; set; }
        public string GRRImage { get; set; }
        public Nullable<int> StatusID { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> FKDepotId { get; set; }
    }
}
