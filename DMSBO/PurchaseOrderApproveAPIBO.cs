﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class PurchaseOrderApproveAPIBO
    {
        public string Username { get; set; }
        public string OrderNo { get; set; }
        public string OrderStatus { get; set; }
        public string Message { get; set; }

    }
}
