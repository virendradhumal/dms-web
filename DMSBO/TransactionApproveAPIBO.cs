﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class TransactionApproveAPIBO
    {
        public string Username { get; set; }
        public int BankTransactionId { get; set; }
        public string TransactionStatus { get; set; }
        public string Message { get; set; }
        public long DateTime { get; set; }

    }
}
