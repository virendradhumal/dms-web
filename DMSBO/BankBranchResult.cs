﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    [DataContract]
   public class BankBranchResult
    {
       
        [DataMember]
       public IEnumerable<BankBOAPI> BankList { get; set; }
    }
}
