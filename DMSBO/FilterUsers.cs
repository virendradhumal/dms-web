﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class FilterUsers
    {
        public string Name { get; set; }
        public string RoleCode { get; set; }
        public string Region { get; set; }
        public int? DepotId { get; set; }
        public int? ActiveStatus { get; set; }
    }
}
