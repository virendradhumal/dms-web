﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class IDNReceiveAPIBO
    {
        public int DepotId { get; set; }
        public int DirectSupplierId { get; set; }
        public string GRRType { get; set; }
        public string Username { get; set; }
        public string IDNNo { get; set; }
        public string GRRNo { get; set; }
        public long DateTime { get; set; }
        public string Grrimage { get; set; }
        public decimal GRRAmount { get; set; }
        public IEnumerable<ProductIDNAPIBO> GRRProducts { get; set; }
    }
}
