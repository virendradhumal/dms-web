﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    [DataContract]
   public class GetPOrderResult
    {
        [DataMember]
        public IEnumerable<POrderAPI> Orders { get; set; } 

    }
}
