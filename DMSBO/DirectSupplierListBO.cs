﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class DirectSupplierListBO
    {
        public string DirectSupplierCodeOrName { get; set; }
        public int? Active { get; set; }
       public IEnumerable<DirectSupplierAllBO> List { get; set; }
    }
}
