﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    [Serializable]
    [DataContract]
    public class BankBOAPI
    {
        [DataMember]
        public int BankId { get; set; }
         [DataMember]
        public string BankName { get; set; }
    }
}
