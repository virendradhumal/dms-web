﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class IDNApproveAPIBO
    {
        public string Username { get; set; }
        public string IDNNo { get; set; }
        public string IDNStatus { get; set; }
        public string Message { get; set; }
        public long DateTime { get; set; }
    }
}
