﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DMSBO
{
   public class CreateGRRBO
    {
        public string GRRNo { get; set; }
        public int IDNId { get; set; }
        public string Pathname { get; set; }
        public HttpPostedFileBase GRRPhoto { get; set; }
    }
}
