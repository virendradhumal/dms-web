﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class BankTransactionExportBO
    {
        public int DepotId { get; set; }
        public String DepotName { get; set; }
       public string RegionCode{get;set;}
        public string DSRNo { get; set; }
        public DateTime DSRDate { get; set; }
        public decimal DSRAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public decimal TransactionAmount { get; set; }
        public decimal ShortageAmount { get; set; }
        public int DelayInDays { get; set; }
        public string Status { get; set; }

    }
}
