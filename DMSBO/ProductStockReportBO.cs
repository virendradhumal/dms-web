﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
   public class ProductStockReportBO
    {
        public string RegionCode { get; set; }
        public string DepotName { get; set; }
        public int DepotId { get; set; }
        public string Itemcode{get;set;}
        public string itemdescription{get;set;}
        public string ItemGroup { get; set; }
        public Decimal UnitPrice{get;set;}
        public int AvailableQty{get;set;}
        public decimal TotalValue{get;set;}
        public decimal ReorderLevel{get;set;}
        public int MinimumQty{get;set;}
        public int ActiveStatus { get; set; }
    }
}
