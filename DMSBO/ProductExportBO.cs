﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class ProductExportBO
    {
        public string SimpleCode { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Group { get; set; }
        public string Item_Group_Description{get;set;}
        public string Pack_Size { get; set; }
        public string Pack_Description { get; set; }
        public string Tax_Type { get; set; }
        public int Status { get; set; }
    }
}
