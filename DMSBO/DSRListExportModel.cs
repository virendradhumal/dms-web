﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class DSRListExportModel
    
        {
        public int DSRId { get; set; }
        public string RegionCode { get; set; }
        public string DepotName { get; set; }
        public int DepotId { get; set; }
        public string DSRNo { get; set; }
        public DateTime DSRDate { get; set; }
        public decimal TotalValueInclusive { get; set; }
        public decimal TotalValueExclusive { get; set; }
        public string PCVNo { get; set; }
        public Decimal PCVAmount { get; set; }
        public string PCSNo { get; set; }
        public decimal PCSAmount { get; set; }
        public decimal VatAmount { get; set; }
        public decimal DSRNetTotal { get; set; }
        public string Status { get; set; }
        }
}
