﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DMSBO
{
    public class BankExportBO
    {
        
        public string BankName { get; set; }
    
        public string BankBranch { get; set; }

        public string AccountNumber { get; set; }
    
        public string RegionCode { get; set; }
        
        public int Status { get; set; }
       
    }
}
