﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
    public class DirectSupplierManagement
    {
        DMSDbCon db;

        public IEnumerable<DirectSupplierBO> GetList()
        {
            try
            {
                db = new DMSDbCon();


                IEnumerable<DirectSupplierBO> data = from ds in db.tbDirectSupplierMsts
                                                     select new DirectSupplierBO
                                                     {
                                                         ContactNo = ds.ContactNo,
                                                         EmailId = ds.EmailId,
                                                         OfficeAddress = ds.OfficeAddress,
                                                         PKSupplierId = ds.PKSupplierId,
                                                         SupplierCode = ds.SupplierCode,
                                                         SupplierName = ds.SupplierName

                                                     };
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DirectSupplierBO> data = null;
                return data;
            }
        }

        public IEnumerable<DirectSupplierAllBO> GetDList(FilterDirectSupplier filter)
        {
            try
            {
                db = new DMSDbCon();


                IEnumerable<DirectSupplierAllBO> data = from ds in db.tbDirectSupplierMsts
                                                        select new DirectSupplierAllBO
                                                     {
                                                         ContactNo = ds.ContactNo,
                                                         EmailId = ds.EmailId,
                                                         OfficeAddress = ds.OfficeAddress,
                                                         PKSupplierId = ds.PKSupplierId,
                                                         SupplierCode = ds.SupplierCode,
                                                         SupplierName = ds.SupplierName,
                                                         ActiveStatus = ds.ActiveStatus == null ? 0 : (int)ds.ActiveStatus
                                                     };
                var result = data.AsQueryable();

                if (filter != null)
                {
                    if (!string.IsNullOrEmpty(filter.DirectSupplierCodeOrName))
                        result = result.Where(x => x.SupplierName == filter.DirectSupplierCodeOrName.ToString() || x.SupplierCode == filter.DirectSupplierCodeOrName.ToString());

                    if (filter.Active.HasValue)
                        result = result.Where(x => x.ActiveStatus == filter.Active);
                }

                data = result as IEnumerable<DirectSupplierAllBO>;
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DirectSupplierAllBO> data = null;
                return data;
            }
        }


        public DataTable GetAllSupplierExport()
        {
            try
            {
                db = new DMSDbCon();


                IEnumerable<DirectSupplierExportBO> data = from ds in db.tbDirectSupplierMsts
                                                           select new DirectSupplierExportBO
                                                        {
                                                            ContactNo = ds.ContactNo,
                                                            EmailId = ds.EmailId,
                                                            OfficeAddress = ds.OfficeAddress,
                                                            SupplierCode = ds.SupplierCode,
                                                            SupplierName = ds.SupplierName,
                                                            ActiveStatus = ds.ActiveStatus == null ? 0 : (int)ds.ActiveStatus
                                                        };

                return ConvertData.ToDataTable<DirectSupplierExportBO>(data.ToList());
            }
            catch (Exception ex)
            {
                DataTable dt = null;
                return dt;

            }
        }


    }
}
