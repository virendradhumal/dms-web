﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using System.Data.OleDb;
using System.Drawing;
using System.Web.Configuration;
using DMSBO;
namespace DMSDAL
{
  public class KulimaExcelUpload
  {
    string sConnectionString = "";

    DataSet objDataset1 = new DataSet();

    static string consString = ConfigurationManager.ConnectionStrings["DMSDbCons"].ConnectionString;

    SqlConnection scon = new SqlConnection(consString);

    private String[] GetExcelSheetNames(string excelFile)
    {
      OleDbConnection objConn = null;
      System.Data.DataTable dt = null;
      try
      {
        String connString = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + excelFile + ";Extended Properties=Excel 12.0;";
        //MyConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + path + "';Extended Properties=Excel 12.0;");
        objConn = new OleDbConnection(connString);
        // Open connection with the database.
        objConn.Open();
        // Get the data table containg the schema guid.
        dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        if (dt == null)
        {
          return null;
        }
        String[] excelSheets = new String[dt.Rows.Count];
        int i = 0;
        // Add the sheet name to the string array.
        foreach (DataRow row in dt.Rows)
        {
          excelSheets[i] = row["TABLE_NAME"].ToString();
          i++;
        }
        return excelSheets;
      }
      catch (Exception ex)
      {
        //lblMsg.Text = ex.Message;
        //Response.Write("Error On No of sheets Count" + ex.Message);
        //lblInfoMsg.Visible = true;
        //lblInfoMsg.Text = "Error On No of sheets Count" + ex.Message;
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        return null;
      }
      finally
      {
        // Clean up.
        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }
        if (dt != null)
        {
          dt.Dispose();
        }
      }
    }

    public void UploadBankData(string fullpath, string filename, string Username)
    {
      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          //lblMsg.Text = "FileType Not Supported, Please Upload Proper File"; 
          return;
        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "BankData";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "BankName";
        objDataset1.Tables[0].Columns[1].ColumnName = "Branch";
        objDataset1.Tables[0].Columns[2].ColumnName = "AccountNo";
        objDataset1.Tables[0].Columns[3].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[4].ColumnName = "Status";
        objDataset1.AcceptChanges();


        SqlCommand comd = new SqlCommand("update tbBankAccountsMst set ActiveStatus = 0", scon);
        scon.Open();
        comd.ExecuteNonQuery();
        scon.Close();


        for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
        {
          DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];
          SqlCommand cmd = new SqlCommand("SP_INSERTBANKMASTER", scon);

          cmd.CommandType = CommandType.StoredProcedure;
          if (row[0] != null)
          {

            cmd.Parameters.Add("@BankName", SqlDbType.VarChar, 50).Value = row[0].ToString();

          }
          else
          {
            cmd.Parameters.Add("@BankName", SqlDbType.VarChar, 50).Value = "";
          }

          if (row[1] != null)
          {

            cmd.Parameters.Add("@Branch", SqlDbType.VarChar, 50).Value = row[1].ToString();

          }
          else
          {
            cmd.Parameters.Add("@Branch", SqlDbType.VarChar, 50).Value = "";
          }

          if (row[2] != null)
          {
            string value = row[2].ToString();

            cmd.Parameters.Add("@AccountNo", SqlDbType.VarChar, 20).Value = value;

          }
          else
          {
            cmd.Parameters.Add("@AccountNo", SqlDbType.VarChar, 20).Value = "";
          }
          if (row[3] != null)
          {

            cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 50).Value = row[3].ToString();

          }
          else
          {
            cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 50).Value = "";
          }

          if (row[4] != null)
          {

            cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Convert.ToInt32(row[4].ToString());

          }
          else
          {
            cmd.Parameters.Add("@Status", SqlDbType.Int).Value = 0;
          }

          cmd.Parameters.Add("@Username", SqlDbType.VarChar, 20).Value = Username;
          scon.Open();
          cmd.ExecuteNonQuery();
          scon.Close();

        }

      }
      catch (Exception ex)
      {
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

      }
      finally
      {
        // objDataset1 = null;
        //dtValidation = null;
        //dtDuplicates = null;
        // Clean up.
        objDataset1.Dispose();

        //dt.Dispose();
        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }
      }
    }

    public void UploadUsersData(string fullpath, string filename, string Username)
    {
      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      SqlTransaction sqlTran = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          //lblMsg.Text = "FileType Not Supported, Please Upload Proper File"; 
          return;
        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "Name";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "EmailId";
        objDataset1.Tables[0].Columns[1].ColumnName = "DepotTitle";
        objDataset1.Tables[0].Columns[2].ColumnName = "MobileNo";

        objDataset1.Tables[0].Columns[0].ColumnName = "Username";
        objDataset1.Tables[0].Columns[1].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[2].ColumnName = "DepotName";

        objDataset1.Tables[0].Columns[1].ColumnName = "UserRoleCode";
        objDataset1.Tables[0].Columns[2].ColumnName = "Status";
        objDataset1.AcceptChanges();

        scon.Open();
        sqlTran = scon.BeginTransaction();

        SqlCommand cmddel = new SqlCommand("UPDATE tbUsersMst SET ActiveStatus=0 where canbedeleted!=0", scon, sqlTran);
        cmddel.ExecuteNonQuery();


        for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
        {
          DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];
          SqlCommand cmd = new SqlCommand("SP_INSERTUSERSMASTER", scon, sqlTran);

          string TempPass = "";
          cmd.CommandType = CommandType.StoredProcedure;
          if (row[0] != null)
          {

            cmd.Parameters.Add("@Name", SqlDbType.VarChar, 15).Value = row[0].ToString();
            TempPass += row[0].ToString().Substring(0, 4).Trim();
          }
          else
          {
            cmd.Parameters.Add("@Name", SqlDbType.VarChar, 15).Value = "";

          }

          if (row[1] != null)
          {

            cmd.Parameters.Add("@EmailId", SqlDbType.VarChar, 50).Value = row[1].ToString();

          }
          else
          {
            cmd.Parameters.Add("@EmailId", SqlDbType.VarChar, 50).Value = "";
          }

          if (row[2] != null)
          {

            cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar, 50).Value = row[2].ToString();

          }
          else
          {
            cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar, 50).Value = "";
          }
          if (row[3] != null)
          {

            cmd.Parameters.Add("@Usercode", SqlDbType.VarChar, 50).Value = row[3].ToString();
            TempPass += row[3].ToString().Substring(0, 4).Trim();
          }
          else
          {
            cmd.Parameters.Add("@Usercode", SqlDbType.VarChar, 50).Value = "";
          }

          if (row[4] != null)
          {

            cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 50).Value = row[4].ToString();

          }
          else
          {
            cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 50).Value = "";
          }

          if (row[5] != null)
          {
            string Depotids = "";

            int depotcount = 0;

            string[] list = row[5].ToString().Split(',');
            foreach (var item in list)
            {
              if (depotcount != 0)
                Depotids = Depotids + ",";
              Depotids = Depotids + DepotManagement.GetDepotID(item);
              depotcount++;
            }

            cmd.Parameters.Add("@DepotCode", SqlDbType.VarChar, 500).Value = Depotids.ToString();

          }
          else
          {
            cmd.Parameters.Add("@DepotCode", SqlDbType.VarChar, 50).Value = "";
          }

          if (row[6] != null)
          {

            cmd.Parameters.Add("@Rolecode", SqlDbType.VarChar, 50).Value = row[6].ToString();

          }
          else
          {
            cmd.Parameters.Add("@Rolecode", SqlDbType.VarChar, 50).Value = "";
          }
          if (row[7] != null)
          {

            cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Convert.ToInt32(row[7].ToString());

          }
          else
          {
            cmd.Parameters.Add("@Status", SqlDbType.Int).Value = 0;
          }


          cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = TempPass;
          cmd.Parameters.Add("@Postedby", SqlDbType.VarChar).Value = Username;

          //scon.Open();
          cmd.ExecuteNonQuery();
          //scon.Close();


        }
        sqlTran.Commit();

      }
      catch (Exception ex)
      {
        sqlTran.Rollback();
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
      }
      finally
      {
        // objDataset1 = null;
        //dtValidation = null;
        //dtDuplicates = null;
        // Clean up.
        objDataset1.Dispose();
        if (scon.State == ConnectionState.Open)
        {
          scon.Close();
        }

        //dt.Dispose();
        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }
      }
    }

    public void UploadDepotData(string fullpath, string filename, string Username)
    {
      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      SqlTransaction sqlTran = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          //lblMsg.Text = "FileType Not Supported, Please Upload Proper File"; 
          return;
        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "DepotData";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[1].ColumnName = "DepotTitle";

        objDataset1.Tables[0].Columns[2].ColumnName = "Status";
        objDataset1.AcceptChanges();

        scon.Open();
        sqlTran = scon.BeginTransaction();

        SqlCommand cmddel = new SqlCommand("UPDATE tbDepotMst SET ActiveStatus=0 where regioncode!='HO'", scon, sqlTran);
        cmddel.ExecuteNonQuery();

        for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
        {
          DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];
          SqlCommand cmd = new SqlCommand("SP_INSERTDEPOTMASTER", scon, sqlTran);

          cmd.CommandType = CommandType.StoredProcedure;
          if (row[0] != null)
          {

            cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 15).Value = row[0].ToString();

          }
          else
          {
            cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 15).Value = "";

          }

          if (row[1] != null)
          {

            cmd.Parameters.Add("@DepotTitle", SqlDbType.VarChar, 50).Value = row[1].ToString();

          }
          else
          {
            cmd.Parameters.Add("@DepotTitle", SqlDbType.VarChar, 50).Value = "";
          }


          if (row[2] != null)
          {

            cmd.Parameters.Add("@ActiveStatus", SqlDbType.Int).Value = Convert.ToInt32(row[2].ToString());

          }
          else
          {
            cmd.Parameters.Add("@ActiveStatus", SqlDbType.Int).Value = 0;
          }

          cmd.Parameters.Add("@Postedby", SqlDbType.VarChar, 20).Value = Username;


          cmd.ExecuteNonQuery();


        }

        sqlTran.Commit();

      }
      catch (Exception ex)
      {
        sqlTran.Rollback();
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

      }
      finally
      {
        // objDataset1 = null;
        //dtValidation = null;
        //dtDuplicates = null;
        // Clean up.
        if (scon.State == ConnectionState.Open)
        {
          scon.Close();
        }
        objDataset1.Dispose();


        //dt.Dispose();
        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }
      }
    }

    public void UploadProductData(string fullpath, string filename, string Username)
    {
      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      SqlTransaction sqlTran = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          //lblMsg.Text = "FileType Not Supported, Please Upload Proper File"; 
          return;
        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "DepotData";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[1].ColumnName = "DepotTitle";

        objDataset1.Tables[0].Columns[2].ColumnName = "Status";
        objDataset1.AcceptChanges();

        scon.Open();
        sqlTran = scon.BeginTransaction();

        SqlCommand cmddel = new SqlCommand("UPDATE tbPRODUCTSMst SET ActiveStatus=0", scon, sqlTran);
        cmddel.ExecuteNonQuery();

        for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
        {
          DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];
          SqlCommand cmd = new SqlCommand("SP_INSERTPRODUCTMASTER", scon, sqlTran);

          cmd.CommandType = CommandType.StoredProcedure;
          if (row[0] != null)
          {

            cmd.Parameters.Add("@SimpleCode", SqlDbType.VarChar, 10).Value = row[0].ToString();

          }
          else
          {
            cmd.Parameters.Add("@SimpleCode", SqlDbType.VarChar, 10).Value = "";

          }

          if (row[1] != null)
          {

            cmd.Parameters.Add("@Description", SqlDbType.VarChar, 250).Value = row[1].ToString();

          }
          else
          {
            cmd.Parameters.Add("@Description", SqlDbType.VarChar, 250).Value = "";
          }


          if (row[2] != null)
          {

            cmd.Parameters.Add("@Description2", SqlDbType.VarChar, 50).Value = (row[2].ToString());

          }
          else
          {
            cmd.Parameters.Add("@Description2", SqlDbType.VarChar, 50).Value = "";
          }


          if (row[3] != null)
          {

            cmd.Parameters.Add("@Group", SqlDbType.VarChar, 50).Value = row[3].ToString();

          }
          else
          {
            cmd.Parameters.Add("@Group", SqlDbType.VarChar, 50).Value = "";
          }




          if (row[4] != null)
          {

            cmd.Parameters.Add("@ItemGroupDescription", SqlDbType.VarChar, 100).Value = (row[4].ToString());

          }
          else
          {
            cmd.Parameters.Add("@ItemGroupDescription", SqlDbType.VarChar, 100).Value = "";
          }



          if (row[5] != null)
          {

            cmd.Parameters.Add("@PackSize", SqlDbType.Decimal).Value = Convert.ToDecimal(row[5].ToString());

          }
          else
          {
            cmd.Parameters.Add("@PackSize", SqlDbType.Decimal).Value = 0;
          }


          if (row[6] != null)
          {
            cmd.Parameters.Add("@PackDescription", SqlDbType.VarChar, 10).Value = (row[6].ToString());
          }
          else
          {
            cmd.Parameters.Add("@PackDescription", SqlDbType.VarChar, 10).Value = "";
          }

          if (row[7] != null)
          {
            cmd.Parameters.Add("@TaxType", SqlDbType.Int).Value = Convert.ToInt32(row[7].ToString());
          }
          else
          {
            cmd.Parameters.Add("@TaxType", SqlDbType.Int, 10).Value = 0;
          }

          if (row[8] != null)
          {
            cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Convert.ToInt32(row[8].ToString());
          }
          else
          {
            cmd.Parameters.Add("@Status", SqlDbType.Int, 10).Value = 0;
          }


          cmd.Parameters.Add("@Postedby", SqlDbType.VarChar, 20).Value = Username;


          cmd.ExecuteNonQuery();


        }

        sqlTran.Commit();

      }
      catch (Exception ex)
      {
        sqlTran.Rollback();
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

      }
      finally
      {
        // objDataset1 = null;
        //dtValidation = null;
        //dtDuplicates = null;
        // Clean up.
        if (scon.State == ConnectionState.Open)
        {
          scon.Close();
        }
        objDataset1.Dispose();


        //dt.Dispose();
        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }
      }
    }

    public StatusMessage UploadDepotProductPriceData(string fullpath, string filename, string Username)
    {
      StatusMessage s = new StatusMessage();
      s.Status = false;

      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      SqlTransaction sqlTran = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          s.Message = "Chose correct excel file.";
          return s;
        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "DepotData";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[1].ColumnName = "DepotTitle";

        objDataset1.Tables[0].Columns[2].ColumnName = "Status";
        objDataset1.AcceptChanges();

        scon.Open();
        sqlTran = scon.BeginTransaction();

        DMSDATABASE.DMSDbCon db = new DMSDATABASE.DMSDbCon();

        string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();
        string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();

        if (RoleCode != "ACTNT" || Regionlist.ToLower() == "all")
        {
          for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
          {


            DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];
            SqlCommand cmd = new SqlCommand("SP_INSERTPRODUCTSTOCKS", scon, sqlTran);

            cmd.CommandType = CommandType.StoredProcedure;
            if (row[0] != null)
            {
              int Dpid = Convert.ToInt32(DepotManagement.GetDepotID(row[0].ToString()));
              cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = Dpid;
            }
            else
            {
              cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = 0;
            }
            if (row[1] != null)
            {
              cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = row[1].ToString();
            }
            else
            {
              cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = "";
            }
            if (row[2] != null)
            {
              cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = Convert.ToDecimal((row[2].ToString()));
            }
            else
            {
              cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = 0;
            }
            cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = 0;
            cmd.Parameters.Add("@Username", SqlDbType.VarChar, 20).Value = Username;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
          }
        }
        else
        {
          string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();

          if (!string.IsNullOrEmpty(Regionlist) && !string.IsNullOrEmpty(depotlist))
          {
            string[] arraydepotid = depotlist.Split(',');

            string[] arrayregionid = Regionlist.Split(',');

            IEnumerable<DepotBO> depos = from dp in db.tbDepotMsts
                                         where arrayregionid.Contains(dp.RegionCode)
                                         select new DepotBO
                                         {
                                           DepotId = dp.PKDepotId,
                                           DepotName = dp.DepotTitle
                                         };
            var result = depos.AsQueryable();

            if (depotlist != "all")
            {
              result = result.Where(x => arraydepotid.Contains(x.DepotId.ToString()));
            }

            depos = result.AsEnumerable();

            var list = result.ToList();

            for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
            {
              DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];

              int Dpid = Convert.ToInt32(DepotManagement.GetDepotID(row[0].ToString()));

              int count = depos.Where(x => x.DepotId == Dpid).Count();

              if (count != 0)
              {
                SqlCommand cmd = new SqlCommand("SP_INSERTPRODUCTSTOCKS", scon, sqlTran);

                cmd.CommandType = CommandType.StoredProcedure;
                if (row[0] != null)
                {
                  cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = Dpid;
                }
                else
                {
                  cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = 0;
                }

                if (row[1] != null)
                {
                  cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = row[1].ToString();
                }
                else
                {
                  cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = "";
                }
                if (row[2] != null)
                {
                  cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = Convert.ToDecimal((row[2].ToString()));
                }
                else
                {
                  cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = 0;
                }
                cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@Username", SqlDbType.VarChar, 20).Value = Username;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
              }//if ends

            }
            //end for loop
          }
        }
        sqlTran.Commit();
        s.Message = "Product prices changed successfully.";
        s.Status = true;
        return s;
      }
      catch (Exception ex)
      {
        sqlTran.Rollback();
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        s.Message = "Please try again.";
        return s;

      }
      finally
      {
        if (scon.State == ConnectionState.Open)
        {
          scon.Close();
        }
        objDataset1.Dispose();

        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }

      }
    }


    public StatusMessage UploadDepotProductStockData(string fullpath, string filename, string Username)
    {
      StatusMessage s = new StatusMessage();
      s.Status = false;

      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      SqlTransaction sqlTran = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          s.Message = "Chose correct excel file.";
          return s;
        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "DepotData";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[1].ColumnName = "DepotTitle";

        objDataset1.Tables[0].Columns[2].ColumnName = "Status";
        objDataset1.AcceptChanges();

        scon.Open();
        sqlTran = scon.BeginTransaction();

        DMSDATABASE.DMSDbCon db = new DMSDATABASE.DMSDbCon();

        string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();
        string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();

        if (RoleCode != "ACTNT" || Regionlist.ToLower() == "all")
        {
          for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
          {


            DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];
            SqlCommand cmd = new SqlCommand("SP_INSERTPRODUCTSTOCKSQUANTITY", scon, sqlTran);

            cmd.CommandType = CommandType.StoredProcedure;
            if (row[0] != null)
            {
              int Dpid = Convert.ToInt32(DepotManagement.GetDepotID(row[0].ToString()));

              cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = Dpid;

            }
            else
            {
              cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = 0;

            }

            if (row[1] != null)
            {

              cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = row[1].ToString();

            }
            else
            {
              cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = "";
            }


            if (row[2] != null)
            {

              cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = Convert.ToInt32((row[2].ToString()));

            }
            else
            {
              cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = 0;
            }


            //if (row[3] != null)
            //{

            //    cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = Convert.ToInt32(row[3].ToString());

            //}
            //else
            //{
            //    cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = 0;
            //}

            cmd.Parameters.Add("@Username", SqlDbType.VarChar, 20).Value = Username;


            cmd.ExecuteNonQuery();

          }

        }
        else
        {
          string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();

          if (!string.IsNullOrEmpty(Regionlist) && !string.IsNullOrEmpty(depotlist))
          {
            string[] arraydepotid = depotlist.Split(',');

            string[] arrayregionid = Regionlist.Split(',');

            IEnumerable<DepotBO> depos = from dp in db.tbDepotMsts
                                         where arrayregionid.Contains(dp.RegionCode)
                                         select new DepotBO
                                         {
                                           DepotId = dp.PKDepotId,
                                           DepotName = dp.DepotTitle
                                         };
            var result = depos.AsQueryable();

            if (depotlist != "all")
            {
              result = result.Where(x => arraydepotid.Contains(x.DepotId.ToString()));
            }

            depos = result.AsEnumerable();

            var list = result.ToList();

            for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
            {


              DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];

              int Dpid = Convert.ToInt32(DepotManagement.GetDepotID(row[0].ToString()));

              int count = depos.Where(x => x.DepotId == Dpid).Count();


              if (count != 0)
              {
                SqlCommand cmd = new SqlCommand("SP_INSERTPRODUCTSTOCKSQUANTITY", scon, sqlTran);

                cmd.CommandType = CommandType.StoredProcedure;
                if (row[0] != null)
                {

                  cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = Dpid;

                }
                else
                {
                  cmd.Parameters.Add("@DepotId", SqlDbType.Int).Value = 0;

                }

                if (row[1] != null)
                {

                  cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = row[1].ToString();

                }
                else
                {
                  cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = "";
                }


                if (row[2] != null)
                {

                  cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = Convert.ToInt32((row[2].ToString()));

                }
                else
                {
                  cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = 0;
                }




                cmd.Parameters.Add("@Username", SqlDbType.VarChar, 20).Value = Username;


                cmd.ExecuteNonQuery();
              }//if ends

            }
            //end for loop
          }
        }
        sqlTran.Commit();
        s.Message = "Product Quantity changed successfully.";
        s.Status = true;
        return s;
      }
      catch (Exception ex)
      {
        sqlTran.Rollback();
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        s.Message = "Please try again.";
        return s;

      }
      finally
      {
        if (scon.State == ConnectionState.Open)
        {
          scon.Close();
        }
        objDataset1.Dispose();

        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }

      }
    }



    public StatusMessage UploadRegionProductPriceData(string fullpath, string filename, string Username)
    {
      StatusMessage s = new StatusMessage()
      {
        Status = false
      };

      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      SqlTransaction sqlTran = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          s.Message = "choose correct excel file.";
          return s;

        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "DepotData";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[1].ColumnName = "DepotTitle";

        objDataset1.Tables[0].Columns[2].ColumnName = "Status";
        objDataset1.AcceptChanges();

        scon.Open();
        sqlTran = scon.BeginTransaction();

        DMSDATABASE.DMSDbCon db = new DMSDATABASE.DMSDbCon();

        string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();
        string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();

        if (RoleCode != "ACTNT" || Regionlist.ToLower() == "all")
        {
          for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
          {


            DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];


            SqlCommand cmd = new SqlCommand("SP_INSERTREGIONPRODUCTSTOCKS", scon, sqlTran);

            cmd.CommandType = CommandType.StoredProcedure;
            if (row[0] != null)
            {

              cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 15).Value = row[0].ToString();

            }
            else
            {
              cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 15).Value = "";

            }

            if (row[1] != null)
            {

              cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = row[1].ToString();

            }
            else
            {
              cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = "";
            }


            if (row[2] != null)
            {

              cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = Convert.ToDecimal((row[2].ToString()));

            }
            else
            {
              cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = 0;
            }


            cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = 0;


            cmd.Parameters.Add("@Username", SqlDbType.VarChar, 20).Value = Username;


            cmd.ExecuteNonQuery();

          }

        }
        else
        {
          string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();

          if (!string.IsNullOrEmpty(Regionlist) && !string.IsNullOrEmpty(depotlist) && depotlist.ToLower() == "all")
          {

            string[] arrayregionid = Regionlist.Split(',');


            for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
            {

              DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];


              bool count = arrayregionid.Contains(row[0].ToString());


              if (count)
              {
                SqlCommand cmd = new SqlCommand("SP_INSERTREGIONPRODUCTSTOCKS", scon, sqlTran);

                cmd.CommandType = CommandType.StoredProcedure;
                if (row[0] != null)
                {

                  cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 15).Value = row[0].ToString();

                }
                else
                {
                  cmd.Parameters.Add("@RegionCode", SqlDbType.VarChar, 15).Value = "";

                }

                if (row[1] != null)
                {

                  cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = row[1].ToString();

                }
                else
                {
                  cmd.Parameters.Add("@ProductCode", SqlDbType.VarChar, 10).Value = "";
                }


                if (row[2] != null)
                {

                  cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = Convert.ToDecimal((row[2].ToString()));

                }
                else
                {
                  cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = 0;
                }


                cmd.Parameters.Add("@Stock", SqlDbType.Int).Value = 0;


                cmd.Parameters.Add("@Username", SqlDbType.VarChar, 20).Value = Username;


                cmd.ExecuteNonQuery();
              }//if ends

            }//end for

          }
          else
          {
            s.Message = "You are not authorized to change price by Region Please use Depot Wise Price change.";
            return s;
          }
        }
        sqlTran.Commit();


        s.Message = "price changed successfully.";
        s.Status = true;
        return s;
      }
      catch (Exception ex)
      {
        sqlTran.Rollback();
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

        s.Message = "please try again.";
        return s;
      }
      finally
      {
        if (scon.State == ConnectionState.Open)
        {
          scon.Close();
        }
        objDataset1.Dispose();

        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }
      }
    }

    public void UploadDirectSupplierData(string fullpath, string filename, string Username)
    {
      string[] excelSheets = GetExcelSheetNames(fullpath);
      // Loop through all of the sheets if you want too...
      OleDbConnection objConn = null;
      SqlTransaction sqlTran = null;
      try
      {
        string strFileType = System.IO.Path.GetExtension(filename).ToString().ToLower();
        if (strFileType == ".xls")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=No;IMEX=1'", fullpath);
        }
        else if (strFileType == ".xlsx")
        {
          sConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;IMEX=1'", fullpath);
        }
        else
        {
          //lblMsg.Text = "FileType Not Supported, Please Upload Proper File"; 
          return;
        }

        objConn = new OleDbConnection(sConnectionString);
        objConn.Open();
        string strQuery = string.Format("SELECT * FROM [{0}]", excelSheets[0].ToString());
        OleDbCommand objCmdSelect = new OleDbCommand(strQuery, objConn);
        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter();
        // Pass the Select command to the adapter.
        objAdapter1.SelectCommand = objCmdSelect;
        // Create new DataSet to hold information from the worksheet.
        // Fill the DataSet with the information from the worksheet.
        objAdapter1.Fill(objDataset1, "XLData");

        objDataset1.Tables[0].TableName = "DepotData";
        //objDataset1.Tables[0].TableName = "EmployeeDemographic";
        objDataset1.BeginInit();
        objDataset1.Tables[0].Columns[0].ColumnName = "RegionCode";
        objDataset1.Tables[0].Columns[1].ColumnName = "DepotTitle";

        objDataset1.Tables[0].Columns[2].ColumnName = "Status";
        objDataset1.AcceptChanges();

        scon.Open();
        sqlTran = scon.BeginTransaction();

        SqlCommand cmddel = new SqlCommand("UPDATE tbDirectSupplierMst SET ActiveStatus=0", scon, sqlTran);
        cmddel.ExecuteNonQuery();

        for (int i = 1; i < objDataset1.Tables[0].Rows.Count; i = i + 1)
        {
          DataRow row = (DataRow)objDataset1.Tables[0].Rows[i];
          SqlCommand cmd = new SqlCommand("SP_INSERTDIRECTSUPPLIERMASTER", scon, sqlTran);

          cmd.CommandType = CommandType.StoredProcedure;
          if (row[0] != null)
          {

            cmd.Parameters.Add("@SupplierCode", SqlDbType.VarChar, 20).Value = row[0].ToString();

          }
          else
          {
            cmd.Parameters.Add("@SupplierCode", SqlDbType.VarChar, 20).Value = "";

          }


          if (row[1] != null)
          {

            cmd.Parameters.Add("@SupplierName", SqlDbType.VarChar, 100).Value = row[1].ToString();

          }
          else
          {
            cmd.Parameters.Add("@SupplierName", SqlDbType.VarChar, 100).Value = "";
          }




          if (row[2] != null)
          {

            cmd.Parameters.Add("@ContactNo", SqlDbType.VarChar, 20).Value = (row[2].ToString());

          }
          else
          {
            cmd.Parameters.Add("@ContactNo", SqlDbType.VarChar, 20).Value = "";
          }



          if (row[3] != null)
          {

            cmd.Parameters.Add("@EmailId", SqlDbType.VarChar, 50).Value = row[3].ToString();

          }
          else
          {
            cmd.Parameters.Add("@EmailId", SqlDbType.VarChar, 50).Value = "";
          }




          if (row[4] != null)
          {

            cmd.Parameters.Add("@OfficeAddress", SqlDbType.VarChar, 200).Value = (row[4].ToString());

          }
          else
          {
            cmd.Parameters.Add("@OfficeAddress", SqlDbType.VarChar, 200).Value = "";
          }



          if (row[5] != null)
          {
            cmd.Parameters.Add("@ActiveStatus", SqlDbType.Int).Value = Convert.ToInt32(row[5].ToString());
          }
          else
          {
            cmd.Parameters.Add("@ActiveStatus", SqlDbType.Int, 10).Value = 0;
          }


          cmd.Parameters.Add("@Postedby", SqlDbType.VarChar, 20).Value = Username;


          cmd.ExecuteNonQuery();


        }

        sqlTran.Commit();

      }
      catch (Exception ex)
      {
        sqlTran.Rollback();
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

      }
      finally
      {
        // objDataset1 = null;
        //dtValidation = null;
        //dtDuplicates = null;
        // Clean up.
        if (scon.State == ConnectionState.Open)
        {
          scon.Close();
        }
        objDataset1.Dispose();


        //dt.Dispose();
        if (objConn != null)
        {
          objConn.Close();
          objConn.Dispose();
        }
      }
    }


  }
}