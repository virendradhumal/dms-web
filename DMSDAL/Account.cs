﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMSBO;
using DMSDATABASE;
using System.Data.Entity;
namespace DMSDAL
{
    /// <summary>
    /// This class is used to insert update User , PasswordReset and  
    /// Otp table
    /// </summary>
    public class Account
    {
        DMSDbCon db;
        
        /// <summary>
        /// checks if username with perticular password  is present in database user table
        /// </summary>
        /// <param name="User"> User class contain username password and remember attibute</param>
        /// <returns></returns>
        
        public StatusMessage IsUserPresent(UserDetail User)
        {
            try
            {

                db = new DMSDbCon();
                int count = 0;
                string username = User.UserName;
                string password = User.Password;
                StatusMessage statusMessage = new StatusMessage();
                db.Database.Connection.Open();
                count = db.tbUsersMsts.Where(S => S.Usercode == username && S.Password == password && S.ActiveStatus==1 ).Count();

                if (count == 0)
                {
                    statusMessage.Message = "Invalid Username or Password !!";
                    statusMessage.Status = false;
                    return statusMessage;
                }
                else
                {
                    var data = (from u in db.tbUsersMsts
                                join rl in db.tbRolesMsts on u.RoleCode equals rl.RoleCode
                                where u.Usercode == User.UserName
                                select new { UserType = rl.LoginAccess}).SingleOrDefault();

                    if (data.UserType.ToLower() == "web")
                    {
                        statusMessage.Message = "Successfully Logged in !!";
                        statusMessage.Status = true;
                        return statusMessage;
                    }
                    else
                    {
                        statusMessage.Message = "Please login using Android Application!!";
                        statusMessage.Status = false;
                        return statusMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusMessage statusMessage = new StatusMessage();
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
                return statusMessage;
            }

        }



        public StatusMessage IsUserPresentApp(UserDetail User)
        {
            try
            {

                db = new DMSDbCon();
                int count = 0;
                string username = User.UserName;
                string password = User.Password;
                StatusMessage statusMessage = new StatusMessage();
                db.Database.Connection.Open();
                count = db.tbUsersMsts.Where(S => S.Usercode == username && S.Password == password && S.ActiveStatus == 1).Count();

                if (count == 0)
                {
                    statusMessage.Message = "Invalid Username or Password !!";
                    statusMessage.Status = false;
                    return statusMessage;
                }
                else
                {
                    var data = (from u in db.tbUsersMsts
                                join rl in db.tbRolesMsts on u.RoleCode equals rl.RoleCode
                                where u.Usercode == User.UserName
                                select new { UserType = rl.LoginAccess }).SingleOrDefault();

                    if (data.UserType.ToLower() == "app")
                    {
                        statusMessage.Message = "Successfully Logged in !!";
                        statusMessage.Status = true;
                        return statusMessage;
                    }
                    else
                    {
                        statusMessage.Message = "Please login using Website !!";
                        statusMessage.Status = false;
                        return statusMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusMessage statusMessage = new StatusMessage();
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
                return statusMessage;
            }

        }

        /// <summary>
        /// checks if username  is present in database user table
        /// </summary>
        /// <param name="username"> username  attibute</param>
        /// <returns></returns>
        
        public bool IsUsernamePresent(string username)
        {
            try
            {
                db = new DMSDbCon();
                int count = 0;
                count = db.tbUsersMsts.Where(S => S.Usercode == username).Count();

                if (count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        /// <summary>
        /// returns the email of a perticular username 
        /// </summary>
        /// <param name="username">username attribute</param>
        /// <returns>Email of user</returns>
        
        public string GetUserEmail(string username)
        {
            try
            {

                db = new DMSDbCon();
                string email;
                email = db.tbUsersMsts.Where(S => S.Usercode == username).Select(x => x.EmailId).SingleOrDefault();
                return email;
            }
            catch (Exception ex)
            {
                return "";
            }

        }
        
        /// <summary>
        /// update or insert the Reset code of a perticular username 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="hashkey"></param>
        /// <returns>true if updated successful</returns>
        
        public bool UpdateResetkey(string username, string hashkey)
        {
            try
            {
                db = new DMSDbCon();
               
                int count = db.tbPasswordResetMsts.Where(S => S.Usercode == username).Count();
                if (count == 0)
                {
                    tbPasswordResetMst passwordReset = new tbPasswordResetMst()
                    {
                        Usercode = username,
                        ResetCode = hashkey,
                        IsUsed = false,
                        CreatedOn = DateTime.Now
                    };

                    db.tbPasswordResetMsts.Add(passwordReset);
                    db.SaveChanges();

                }
                else
                {
                    tbPasswordResetMst passwordReset = db.tbPasswordResetMsts.First(x => x.Usercode == username);
                    passwordReset.ResetCode = hashkey;
                    passwordReset.IsUsed = false;
                    passwordReset.CreatedOn = DateTime.Now;
                    // passwordReset.UsedOn = null;
                    db.Entry(passwordReset).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// checks the username and haskey and update the password in database table
        /// </summary>
        /// <param name="changePassword">contains attribute password,username, hashkey</param>
        /// <returns> true if updated</returns>
        /// 
        public  StatusMessage UpdatePassword(ChangePassword changePassword)
        {
            db= new DMSDbCon();
            StatusMessage statusMessage = new StatusMessage()
            {
                Status=false
            };
            try
            {
                
                if(IsUsernamePresent(changePassword.Username))
                {
                    string username=changePassword.Username;
                    string hashkey= changePassword.key;
                    int count = db.tbPasswordResetMsts.Where(x=>x.Usercode==username && x.ResetCode==hashkey && x.IsUsed==false).Count();
                    if(count==0)
                    {
                        statusMessage.Message = "Invalid or link is expired.";
                        return statusMessage;
                    }
                    else
                    {
                        tbPasswordResetMst reset = db.tbPasswordResetMsts.First(x=> x.Usercode==username && x.ResetCode==hashkey);
                        reset.IsUsed=true;
                        reset.UsedOn=DateTime.Now;
                        db.Entry(reset).State=EntityState.Modified;
                        db.SaveChanges();

                        tbUsersMst users = db.tbUsersMsts.First(x => x.Usercode == username);
                        users.Password = changePassword.Password;
                        users.ModifiedOn = DateTime.Now;
                        db.Entry(users).State = EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "Password changed successfully please login.";
                        statusMessage.Status = true;
                    return statusMessage;
                    }

                }
                else
                {
                    statusMessage.Message = "Username not valid.";
                    return statusMessage;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again later.";
                return statusMessage;
            }
        }




        public StatusMessage RequestChangePassword(ChangePasswordUser changePassword)
        {
            db= new DMSDbCon();
            StatusMessage statusMessage = new StatusMessage()
            {
                Status=false
            };
            try
            {
                
                if(IsUsernamePresent(changePassword.Username))
                {
                    string username=changePassword.Username;
                    string password= changePassword.OldPassword;
                    int count = db.tbUsersMsts.Where(x=>x.Usercode==username && x.Password==password && x.ActiveStatus==1).Count();
                    if(count==0)
                    {
                        statusMessage.Message = "User/Password not valid.";
                        return statusMessage;
                    }
                    else
                    {
                        
                        tbUsersMst users = db.tbUsersMsts.First(x => x.Usercode == username);
                        users.Password = changePassword.NewPassword;
                        users.ModifiedOn = DateTime.Now;
                        db.Entry(users).State = EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "Password changed successfully.";
                        statusMessage.Status = true;
                    return statusMessage;
                    }

                }
                else
                {
                    statusMessage.Message = "Username not valid.";
                    return statusMessage;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again later.";
                return statusMessage;
            }
        }



        /// <summary>
        /// returns the role of user
        /// </summary>
        /// <param name="username"></param>
        /// <returns>role of user</returns>
        public string GetUserRole(string username)
        {
            try
            {
                db = new DMSDbCon();
                string userrole;
                userrole = db.tbUsersMsts.Where(x => x.Usercode == username).Select(x => x.RoleCode).SingleOrDefault();
                return userrole;
            }
            catch (Exception ex)
            {
                return "Please try again";
            }
        }

        /// <summary>
        /// update the newly genrated otp in database
        /// </summary>
        /// <param name="username"></param>
        /// <param name="hashotp"></param>
        /// <returns></returns>
        public bool UpdateOtp(string username, string hashotp)
        {
            try
            {
                db = new DMSDbCon();
             
                int count = db.tbUsersOTPMsts.Where(S => S.Usercode == username).Count();
                if (count == 0)
                {
                    tbUsersOTPMst userotp = new tbUsersOTPMst()
                    {
                         Usercode=username, CreatedOn=DateTime.Now, IsUsed =false, OTP=hashotp
                    };

                    db.tbUsersOTPMsts.Add(userotp);
                    db.SaveChanges();

                }
                else
                {
                    tbUsersOTPMst userotp = db.tbUsersOTPMsts.First(x => x.Usercode == username);
                    userotp.OTP = hashotp;
                    userotp.IsUsed = false;
                    userotp.CreatedOn = DateTime.Now;
                    userotp.UsedOn = null;

                    db.Entry(userotp).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// compare otp and username with database and return true if present
        /// </summary>
        /// <param name="username"></param>
        /// <param name="hashotp"></param>
        /// <returns></returns>
        public StatusMessage CheckOTP(string username, string hashotp)
        {
            db= new DMSDbCon();

            StatusMessage statusMessage = new StatusMessage()
                {
                     Status=false
                };
            try
            {
                if(IsUsernamePresent(username))
                {
                   
                    int count = db.tbUsersOTPMsts.Where(x=>x.Usercode==username && x.OTP==hashotp && x.IsUsed==false).Count();
                    if(count==0)
                    {
                        statusMessage.Message = "OTP does not match";
                        return statusMessage;
                    }
                    else
                    {
                        tbUsersOTPMst reset = db.tbUsersOTPMsts.First(x => x.Usercode == username && x.OTP == hashotp);
                        reset.IsUsed=true;
                        reset.UsedOn=DateTime.Now;
                        db.Entry(reset).State=EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Status = true;
                        statusMessage.Message = "You are successfully verified.";
                    return statusMessage;
                    }

                }
                else
                {
                    statusMessage.Message = username+"named username not present.";
                    return statusMessage;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message ="some problem occured try again.";
                return statusMessage;
            }
        }


        public LoginResult AutheticateUserApi(LoginParameter loginParameter)
        {
            try
            {
                db = new DMSDbCon();

                LoginResult loginResult = (from us in db.tbUsersMsts
                                           join dp in db.tbDepotMsts on us.DepotId equals dp.PKDepotId.ToString()
                                           join rg in db.tbRegionsMsts on dp.RegionCode equals rg.RegionCode
                                           where us.Usercode == loginParameter.Username
                                           select new LoginResult
                                           {
                                                Name = us.Name,
                                               DepotId = dp.PKDepotId,
                                               EmailId = us.EmailId,
                                               mobileNo = us.MobileNo,
                                               Usercode = us.Usercode,
                                               UserRole = us.RoleCode,
                                               DepotTitle=dp.DepotTitle,
                                               RegionId=rg.RegionCode
                                           }).SingleOrDefault();
                return loginResult;
            }
            catch (Exception ex)
            {
                LoginResult loginResult = new LoginResult();
                return loginResult;
            }

        }

        public string UserNameRole(string name)
        {
            db=new DMSDbCon();
            var data = (from us in db.tbUsersMsts
                       where us.Usercode == name
                       select new
                       {
                          name=us.Name+"("+us.RoleCode+")"
                       }).SingleOrDefault();
            string result = data.name;
            return result;
        }
       
    }
}
