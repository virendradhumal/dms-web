﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
  public class OrderManagement
  {
    DMSDbCon db;
    /// <summary>
    /// returns all the orders
    /// </summary>
    /// <param name="searchModel"></param>
    /// <returns></returns>
    public IEnumerable<OrderManageModal> GetAllOrder(FilterOrders searchModel)
    {
      try
      {
        db = new DMSDbCon();

        IEnumerable<OrderManageModal> orderManageModal = from orm in db.tbOrdersMsts
                                                         join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                                         select new OrderManageModal
                                                             {

                                                               OrderId = orm.PKOrderId,
                                                               OrderNo = orm.OrderNo,
                                                               DepotId = dpo.PKDepotId,
                                                               DepoName = dpo.DepotTitle,
                                                               OrderAmount = (decimal)orm.OrderAmount,
                                                               OrderDate = (DateTime)orm.OrderDate,
                                                               Status = "Delivered"
                                                             };
        var result = orderManageModal.AsQueryable();

        if (searchModel != null)
        {
          if (!string.IsNullOrEmpty(searchModel.OrderNo))
            result = result.Where(x => x.OrderNo == searchModel.OrderNo.ToString());
          if (searchModel.DepotId.HasValue)
            result = result.Where(x => x.DepotId == searchModel.DepotId);
          if (searchModel.FromDate.HasValue)
            result = result.Where(x => DbFunctions.TruncateTime(x.OrderDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
          if (searchModel.ToDate.HasValue)
            result = result.Where(x => DbFunctions.TruncateTime(x.OrderDate) <= DbFunctions.TruncateTime(searchModel.ToDate));
        }

        var data = result.AsEnumerable();
        return data;
      }
      catch (Exception ex)
      {
        IEnumerable<OrderManageModal> orderManageModal = null;
        return orderManageModal;
      }
    }


    /// <summary>
    /// returns specific order
    /// </summary>
    /// <param name="OrderId"></param>
    /// <returns></returns>
    public OrderManageModal GetSpecificOrder(int OrderId)
    {
      try
      {
        db = new DMSDbCon();

        IEnumerable<OrderManageModal> orderManageModal = from orm in db.tbOrdersMsts
                                                         join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                                         select new OrderManageModal
                                                         {

                                                           OrderId = orm.PKOrderId,
                                                           OrderNo = orm.OrderNo,
                                                           DepotId = dpo.PKDepotId,
                                                           DepoName = dpo.DepotTitle,
                                                           OrderAmount = (decimal)orm.OrderAmount,
                                                           OrderDate = (DateTime)orm.OrderDate,
                                                           Status = "Delivered"
                                                         };
        var result = orderManageModal.AsQueryable();

        OrderManageModal data = result.Where(x => x.OrderId == OrderId).FirstOrDefault();

        return data;
      }
      catch (Exception ex)
      {
        OrderManageModal orderManageModal = null;
        return orderManageModal;
      }
    }

    /// <summary>
    /// return all the details of a specific order
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public IEnumerable<OrderDetailModal> GetOrderDetails(int id)
    {
      try
      {
        db = new DMSDbCon();
        var orderDetailModal = from odls in db.tbOrderDtls
                               join prod in db.tbProductsMsts on odls.ProductCode equals prod.ProductCode
                               where odls.FKORderId == id
                               select new OrderDetailModal
                               {
                                 OrderDetailId = odls.PKOrderDetailsMst,
                                 ProductCode = odls.ProductCode,
                                 ProductName = prod.ProductName,
                                 quantity = odls.Quantity == null ? 0 : (int)odls.Quantity,
                                 rate = odls.Rate == null ? 0 : (decimal)odls.Rate,
                                 taxamount = odls.TaxValue == null ? 0 : (decimal)odls.TaxValue,
                                 total = odls.Amount == null ? 0 : (int)odls.Amount,
                                 OrderId = odls.FKORderId == null ? 0 : (int)odls.FKORderId
                               };

        return orderDetailModal;
      }
      catch (Exception ex)
      {
        IEnumerable<OrderDetailModal> data = null;
        return data;
      }
    }

    public StatusMessage InsertOrder(InsertOrderParameterApi insertOrderParameterApi)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();

        using (var transaction = db.Database.BeginTransaction())
        {
          try
          {

            DateTime cdate = DateTime.Now;

            tbDSRMst cdata = new tbDSRMst();

            int? DSRID = db.tbDSRMsts.Where(x => x.FKDepotId == insertOrderParameterApi.DepotId && DbFunctions.TruncateTime(x.CreatedOn) == DbFunctions.TruncateTime(cdate) && x.DSRNo == null).Select(x => x.PKDSRID).SingleOrDefault();


            if (DSRID == 0)
            {

              cdata.FKDepotId = insertOrderParameterApi.DepotId;
              cdata.CreatedOn = DateTime.Now;
              db.tbDSRMsts.Add(cdata);
              db.SaveChanges();
              DSRID = db.tbDSRMsts.Where(x => x.FKDepotId == insertOrderParameterApi.DepotId && DbFunctions.TruncateTime(x.CreatedOn) == DbFunctions.TruncateTime(cdate) && x.DSRNo == null).Select(x => x.PKDSRID).SingleOrDefault();
              ;
            }

            OrderAllDetailBO[] AllorderAllDetailBO = insertOrderParameterApi.Products;
            tbOrdersMst odata = new tbOrdersMst();
            odata.CreatedBy = insertOrderParameterApi.Username;
            odata.FKDSRId = DSRID;
            odata.FKDepotId = insertOrderParameterApi.DepotId;
            odata.OrderAmount = insertOrderParameterApi.OrderAmount;
            odata.OrderDiscAmount = insertOrderParameterApi.OrderDiscAmount;
            odata.OrderNetAmount = insertOrderParameterApi.OrderNetAmount;
            odata.OrderStatus = insertOrderParameterApi.OrderStatus;
            odata.OrderDate = DateTime.Now;
            odata.CreatedOn = DateTime.Now;
            odata.ModifiedOn = DateTime.Now;
            db.tbOrdersMsts.Add(odata);
            db.SaveChanges();
            int orderid = odata.PKOrderId;
            odata.OrderNo = "ORD000" + orderid;
            db.Entry(odata).State = EntityState.Modified;
            db.SaveChanges();
            foreach (var orderAllDetailBO in AllorderAllDetailBO)
            {

              tbOrderDtl data = new tbOrderDtl();
              data.Amount = orderAllDetailBO.Amount;
              data.CreatedBy = orderAllDetailBO.CreatedBy;
              data.CreatedOn = DateTime.Now;
              data.DiscAmount = orderAllDetailBO.DiscAmount;
              data.DiscountType = orderAllDetailBO.DiscountType;
              data.DiscountValue = orderAllDetailBO.DiscountValue;
              data.FKORderId = orderid;
              data.ModifiedBy = orderAllDetailBO.ModifiedBy;
              data.ModifiedOn = DateTime.Now;
              data.NetAmount = orderAllDetailBO.NetAmount;
              data.PKOrderDetailsMst = orderAllDetailBO.PKOrderDetailsMst;
              data.ProductCode = orderAllDetailBO.ProductCode;
              data.Quantity = orderAllDetailBO.Quantity;
              data.Rate = orderAllDetailBO.Rate;
              data.TaxValue = orderAllDetailBO.TaxValue;
              db.tbOrderDtls.Add(data);
              statusMessage.Status = true;
              statusMessage.Message = "Order successfully Inserted";
              db.SaveChanges();

              tbProductStockMst stock = db.tbProductStockMsts.Where(x => x.FKDepotId == odata.FKDepotId && x.ProductCode == data.ProductCode).FirstOrDefault();
              stock.StockQuantity = stock.StockQuantity - data.Quantity;
              db.Entry(stock).State = EntityState.Modified;
              db.SaveChanges();

            }
            transaction.Commit();
            statusMessage.Status = true;
            statusMessage.Message = odata.OrderNo;

          }
          catch (Exception ex)
          {
            LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            statusMessage.Status = false;
            statusMessage.Message = "Order not Inserted try again";
            transaction.Rollback();
          }
        }
      }
      catch (Exception ex)
      {
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        statusMessage.Status = false;
        statusMessage.Message = "Order not Inserted try again";

      }
      return statusMessage;
    }

    public GetOrderResult GetOrderApi(GetOrderParameter getOrderParameter)
    {
      GetOrderResult getOrderResult = new GetOrderResult();
      try
      {

        db = new DMSDbCon();

        DateTime currentdate = DateTime.Now;
        int days = getOrderParameter.NoOfDays - 1;
        currentdate = currentdate.AddDays(-days);

        IEnumerable<OrderApi> orderApi = (from dsr in db.tbDSRMsts
                                          join orm in db.tbOrdersMsts
                                              on dsr.PKDSRID equals orm.FKDSRId
                                          join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                          where dpo.PKDepotId == getOrderParameter.DepotId && DbFunctions.TruncateTime(orm.OrderDate) >= DbFunctions.TruncateTime(currentdate)
                                          select new OrderApi
                                          {
                                            OrderNo = orm.OrderNo,
                                            OrderAmount = (decimal)orm.OrderAmount,
                                            OrderDate = orm.OrderDate.ToString(),
                                            OrderStatus = "Delivered",
                                            DSRNo = dsr.DSRNo,
                                            OrderDiscAmount = orm.OrderDiscAmount,
                                            OrderNetAmount = orm.OrderAmount
                                          });
        List<OrderApi> data = orderApi.ToList();

        data.ForEach(x => x.OrderDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(x.OrderDate)));
        getOrderResult.Orders = data as IEnumerable<OrderApi>;

      }
      catch (Exception ex)
      {
        getOrderResult.Orders = null;

      }


      return getOrderResult;
    }

    public IEnumerable<OrderManageModal> GetAllOrderByDSR(int DSRId)
    {
      try
      {
        db = new DMSDbCon();

        IEnumerable<OrderManageModal> orderManageModal = from orm in db.tbOrdersMsts
                                                         join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                                         where orm.FKDSRId == DSRId
                                                         select new OrderManageModal
                                                         {

                                                           OrderId = orm.PKOrderId,
                                                           OrderNo = orm.OrderNo,
                                                           DepotId = dpo.PKDepotId,
                                                           DepoName = dpo.DepotTitle,
                                                           OrderAmount = (decimal)orm.OrderAmount,
                                                           OrderDate = (DateTime)orm.OrderDate,
                                                           Status = "Delivered"
                                                         };

        return orderManageModal;
      }
      catch (Exception ex)
      {
        IEnumerable<OrderManageModal> orderManageModal = null;
        return orderManageModal;

      }
    }

    public StatusMessage UpdateOrderProduct(int? Odid, int? Quantity, string username)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        tbOrderDtl record = db.tbOrderDtls.Find(Odid);
        record.Quantity = Quantity;
        record.Amount = Quantity * record.Rate;
        record.NetAmount = record.Amount;
        record.ModifiedBy = username;
        record.ModifiedOn = DateTime.Now;
        db.Entry(record).State = EntityState.Modified;
        db.SaveChanges();
        statusMessage.Message = "Order detail updated successfuly";
        statusMessage.Status = true;
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Order detail not updated successfuly";
        statusMessage.Status = false;
      }

      return statusMessage;
    }

    public StatusMessage DeleteOrderProduct(int? Odid, string username)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        tbOrderDtl record = new tbOrderDtl();
        record = db.tbOrderDtls.Find(Odid);
        record.ModifiedBy = username;
        record.ModifiedOn = DateTime.Now;
        db.Entry(record).State = EntityState.Modified;
        db.SaveChanges();
        db.tbOrderDtls.Remove(record);
        db.SaveChanges();
        statusMessage.Message = "Order detail deleted successfuly";
        statusMessage.Status = true;
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Order detail not deleted successfuly";
        statusMessage.Status = false;
      }

      return statusMessage;
    }

    public StatusMessage AddOrderProduct(DSREditAddProduct product)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        tbOrderDtl record = new tbOrderDtl();
        record.CreatedBy = product.Username;
        record.CreatedOn = DateTime.Now;
        record.Amount = product.Rate * product.Quantity;
        record.DiscAmount = 0;
        record.DiscountType = "";
        record.DiscountValue = 0;
        record.FKORderId = product.OrderId;
        record.ModifiedBy = product.Username;
        record.ModifiedOn = DateTime.Now;
        record.NetAmount = record.Amount;
        record.ProductCode = "";
        record.Quantity = product.Quantity;
        record.Rate = product.Rate;
        record.TaxValue = 0;




      }
      catch (Exception ex)
      {

        statusMessage.Message = "Product not added try again.";
        statusMessage.Status = true;
      }



      return statusMessage;

    }


  }
}
