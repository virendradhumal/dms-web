﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
  public class ProductManagement
  {
    DMSDbCon db;

    public IEnumerable<ProductBO> GetAllProduct(DMSBO.FilterProduct searchModel)
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<ProductBO> ProductList = from pd in db.tbProductsMsts
                                             select new ProductBO
                                             {
                                               ProductCode = pd.ProductCode,
                                               ProductName = pd.ProductName,
                                               Description2 = pd.Description2,
                                               ProductGroupDescription = pd.ProductGroupDescription,
                                               PackDescription = pd.PackDescription,
                                               ProductGroup = pd.ProductGroup,
                                               PackSize = pd.PackSize,
                                               TaxType = pd.TaxType,
                                               ActiveStatus = pd.ActiveStatus
                                             };

        var result = ProductList.AsQueryable();

        if (searchModel != null)
        {
          if (!string.IsNullOrEmpty(searchModel.ProductCodeOrName))
            result = result.Where(x => x.ProductName == searchModel.ProductCodeOrName.ToString() || x.ProductCode == searchModel.ProductCodeOrName.ToString());

          if (!string.IsNullOrEmpty(searchModel.ProductGroup))
            result = result.Where(x => x.ProductGroup == searchModel.ProductGroup.ToString());

          if (searchModel.Active.HasValue)
            result = result.Where(x => x.ActiveStatus == searchModel.Active);
        }

        return result.AsEnumerable();
      }
      catch (Exception ex)
      {
        IEnumerable<ProductBO> ProductList = null;
        return ProductList;

      }
    }

    public DataTable GetAllProductExport()
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<ProductExportBO> ProductList = from pd in db.tbProductsMsts
                                                   select new ProductExportBO
                                                   {

                                                     SimpleCode = pd.ProductCode,
                                                     Description = pd.ProductName,
                                                     Description2 = pd.Description2,
                                                     Item_Group_Description = pd.ProductGroupDescription,
                                                     Pack_Description = pd.PackDescription,
                                                     Group = pd.ProductGroup,
                                                     Pack_Size = pd.PackSize.ToString(),
                                                     Tax_Type = pd.TaxType.ToString(),
                                                     Status = pd.ActiveStatus == null ? 0 : (int)pd.ActiveStatus
                                                   };

        var result = ProductList.AsQueryable();

        return ConvertData.ToDataTable<ProductExportBO>(result.ToList());
      }
      catch (Exception ex)
      {
        DataTable dt = null;
        return dt;

      }
    }

    public IEnumerable<ProductGroupBO> GetAllProductGroup()
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<ProductGroupBO> ProductList = from pd in db.tbProductsMsts
                                                  group pd by pd.ProductGroup into pdg
                                                  select new ProductGroupBO
                                             {
                                               ProductGroup = pdg.Key
                                             };

        return ProductList;
      }
      catch (Exception ex)
      {
        IEnumerable<ProductGroupBO> ProductList = null;
        return ProductList;
      }
    }

    public IEnumerable<ProductAllDetailBO> GetAllProductDetail()
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<ProductAllDetailBO> ProductList = from pd in db.tbProductsMsts
                                                      select new ProductAllDetailBO
                                             {
                                               ProductCode = pd.ProductCode,
                                               Description = pd.ProductName,
                                               PackDescription = pd.PackDescription,
                                               ProductGroup = pd.ProductGroup,
                                               PackSize = pd.PackSize,
                                               TaxType = pd.TaxType,
                                               ActiveStatus = pd.ActiveStatus,
                                               Description2 = pd.Description2,
                                               PKProductId = pd.PKProductId,
                                               ProductGroupDescription = pd.ProductGroupDescription
                                             };

        return ProductList;
      }
      catch (Exception ex)
      {
        IEnumerable<ProductAllDetailBO> ProductList = null;
        return ProductList;
      }
    }

    public IEnumerable<ProductAllDetailBOApi> GetAllProductDetailApi()
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<ProductAllDetailBOApi> ProductList = from pd in db.tbProductsMsts
                                                         join stock in db.tbProductStockMsts
                                                         on pd.ProductCode equals stock.ProductCode
                                                         select new ProductAllDetailBOApi
                                                         {
                                                           ProductCode = pd.ProductCode,
                                                           Description = pd.ProductName,
                                                           PackDescription = pd.PackDescription,
                                                           Group = pd.ProductGroup,
                                                           PackSize = pd.PackSize,
                                                           Description2 = pd.Description2,
                                                           StockQuantity = (int)stock.StockQuantity,
                                                           SellingPrice = (decimal)stock.SellingPrice,
                                                           DepotId = (int)stock.FKDepotId
                                                         };

        return ProductList;
      }
      catch (Exception ex)
      {
        IEnumerable<ProductAllDetailBOApi> ProductList = null;
        return ProductList;
      }
    }

    public OrderDetailResultApi GetAllProductDetail(OrderDetailParameter orderDetailParameter)
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<ProductAllDetailBO> ProductList = from od in db.tbOrdersMsts
                                                      join ods in db.tbOrderDtls on od.PKOrderId equals ods.FKORderId
                                                      join pd in db.tbProductsMsts on ods.ProductCode equals pd.ProductCode
                                                      where od.OrderNo == orderDetailParameter.OrderNo
                                                      select new ProductAllDetailBO
                                                      {
                                                        ProductCode = pd.ProductCode,
                                                        Description = pd.ProductName,
                                                        PackDescription = pd.PackDescription,
                                                        ProductGroup = pd.ProductGroup,
                                                        PackSize = pd.PackSize,
                                                        TaxType = pd.TaxType,
                                                        ActiveStatus = pd.ActiveStatus,
                                                        Description2 = pd.Description2,
                                                        PKProductId = pd.PKProductId,
                                                        ProductGroupDescription = pd.ProductGroupDescription,
                                                        Quantity = (ods.Quantity == null) ? 0 : (int)ods.Quantity,
                                                        Rate = (ods.Quantity == null) ? 0 : (decimal)ods.Rate,
                                                        DiscountType = ods.DiscountType,
                                                        Amount = (ods.Amount == null) ? 0 : (decimal)ods.Amount,
                                                        DiscAmount = (ods.DiscountValue == null) ? 0 : (decimal)ods.DiscAmount,
                                                        NetAmount = (ods.NetAmount == null) ? 0 : (decimal)ods.NetAmount,
                                                        DiscountValue = (ods.DiscountValue == null) ? 0 : (decimal)ods.DiscountValue,
                                                        TaxAmount = (ods.TaxValue == null) ? 0 : (decimal)ods.TaxValue
                                                      };
        OrderDetailResultApi orderDetailResultApi = (from dsr in db.tbDSRMsts
                                                     join od in db.tbOrdersMsts on dsr.PKDSRID equals od.FKDSRId
                                                     where od.OrderNo == orderDetailParameter.OrderNo
                                                     select new OrderDetailResultApi
                                                     {
                                                       DSRNo = dsr.DSRNo,
                                                       OrderAmount = od.OrderAmount,
                                                       OrderDate = od.OrderDate.ToString(),
                                                       OrderDiscAmount = od.OrderDiscAmount,
                                                       OrderNetAmount = od.OrderNetAmount,
                                                       OrderNo = od.OrderNo,
                                                       OrderStatus = od.OrderStatus,
                                                     }).SingleOrDefault();

        orderDetailResultApi.OrderDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(orderDetailResultApi.OrderDate));
        orderDetailResultApi.Products = ProductList;

        return orderDetailResultApi;
      }
      catch (Exception ex)
      {
        OrderDetailResultApi data = null;
        return data;
      }
    }

    public DSRProductRate GetProductRate(int Oid, string ProductName)
    {
      DSRProductRate dSRProductRate = new DSRProductRate();

      try
      {
        db = new DMSDbCon();

        dSRProductRate.ProductCode = db.tbProductsMsts.Where(x => x.ProductName == ProductName).Select(x => x.ProductCode).SingleOrDefault();
        dSRProductRate.Rate = db.tbProductStockMsts.Where(x => x.FKDepotId == Oid && x.ProductCode == dSRProductRate.ProductCode).Select(x => x.SellingPrice).SingleOrDefault();
        return dSRProductRate;
      }
      catch (Exception ex)
      {
        return dSRProductRate;
      }
    }

    public StatusMessage AddDSRProduct(DSREditAddProduct dSREditAddProduct)
    {
      StatusMessage statusMessage = new StatusMessage();

      try
      {
        db = new DMSDbCon();
        tbOrderDtl record = new tbOrderDtl();
        record.Amount = dSREditAddProduct.Rate * dSREditAddProduct.Quantity;
        record.CreatedBy = dSREditAddProduct.Username;
        record.CreatedOn = DateTime.Now;
        record.DiscAmount = 0;
        record.DiscountType = "";
        record.DiscountValue = 0;
        record.FKORderId = dSREditAddProduct.OrderId;
        record.ModifiedBy = dSREditAddProduct.Username;
        record.ModifiedOn = DateTime.Now;
        record.NetAmount = record.Amount;
        record.ProductCode = dSREditAddProduct.ProductCode;
        record.Quantity = dSREditAddProduct.Quantity;
        record.Rate = dSREditAddProduct.Rate;
        record.TaxValue = 0;
        db.tbOrderDtls.Add(record);
        db.SaveChanges();
        statusMessage.Status = true;
        statusMessage.Message = "Product successfully added";
      }
      catch (Exception ex)
      {
        statusMessage.Status = false;
        statusMessage.Message = "Product not added try again.";
      }

      return statusMessage;
    }

    public StatusMessage AddIDNProduct(DSREditAddProduct dSREditAddProduct)
    {
      StatusMessage statusMessage = new StatusMessage();

      try
      {
        db = new DMSDbCon();
        tbIDNDtl record = new tbIDNDtl();
        record.Rate = (dSREditAddProduct.Rate == null) ? 0 : dSREditAddProduct.Rate;
        record.SourceQuantity = (dSREditAddProduct.Quantity == null) ? 0 : dSREditAddProduct.Quantity;
        record.Amount = record.Rate * record.SourceQuantity;
        record.CreatedBy = dSREditAddProduct.Username;
        record.CreatedOn = DateTime.Now;
        record.FKIDNId = dSREditAddProduct.OrderId;
        record.ModifiedBy = dSREditAddProduct.Username;
        record.ModifiedOn = DateTime.Now;
        record.ProductCode = dSREditAddProduct.ProductCode;

        record.TaxValue = 0;
        db.tbIDNDtls.Add(record);

        tbIDNMst idnrecord = db.tbIDNMsts.Find(dSREditAddProduct.OrderId);
        if (idnrecord.IDNAmount == null)
        {

          idnrecord.IDNAmount = record.Amount;
        }
        else
        {
          idnrecord.IDNAmount += record.Amount;
        }
        db.Entry(idnrecord).State = System.Data.Entity.EntityState.Modified;
        db.SaveChanges();
        statusMessage.Status = true;
        statusMessage.Message = "Product successfully added";
      }
      catch (Exception ex)
      {
        statusMessage.Status = false;
        statusMessage.Message = "Product not added try again.";
      }

      return statusMessage;
    }

    public DataTable GetAllDepotWisePriceExport(string Username)
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<DepotWisePriceExportBO> ProductList;

        ProductList = from pd in db.tbProductStockMsts
                      join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                      select new DepotWisePriceExportBO
                      {

                        SimpleCode = pd.ProductCode,
                        DepotName = dp.DepotTitle,
                        SellingPrice = pd.SellingPrice == null ? 0 : (decimal)pd.SellingPrice

                      };

        string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();
        string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();

        if (RoleCode != "ACTNT" || Regionlist.ToLower() == "all")
        {
          ProductList = from pd in db.tbProductStockMsts
                        join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                        select new DepotWisePriceExportBO
                        {

                          SimpleCode = pd.ProductCode,
                          DepotName = dp.DepotTitle,
                          SellingPrice = pd.SellingPrice == null ? 0 : (decimal)pd.SellingPrice

                        };

        }
        else
        {
          string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();

          if (!string.IsNullOrEmpty(Regionlist) && !string.IsNullOrEmpty(depotlist))
          {


            string[] arraydepotid = depotlist.Split(',');

            string[] arrayregionid = Regionlist.Split(',');

            if (depotlist.ToLower() == "all")
            {
              ProductList = from pd in db.tbProductStockMsts
                            join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                            where arrayregionid.Contains(dp.RegionCode)
                            select new DepotWisePriceExportBO
                            {

                              SimpleCode = pd.ProductCode,
                              DepotName = dp.DepotTitle,
                              SellingPrice = pd.SellingPrice == null ? 0 : (decimal)pd.SellingPrice

                            };
            }
            else
            {
              ProductList = from pd in db.tbProductStockMsts
                            join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                            where arraydepotid.Contains(dp.PKDepotId.ToString())
                            select new DepotWisePriceExportBO
                            {

                              SimpleCode = pd.ProductCode,
                              DepotName = dp.DepotTitle,
                              SellingPrice = pd.SellingPrice == null ? 0 : (decimal)pd.SellingPrice

                            };
            }
          }
        }



        return ConvertData.ToDataTable<DepotWisePriceExportBO>(ProductList.ToList());
      }
      catch (Exception ex)
      {
        DataTable dt = null;
        return dt;

      }
    }


    public DataTable GetAllDepotWiseStockExport(string Username)
    {
      try
      {
        db = new DMSDbCon();
        IEnumerable<DepotWiseStockExport> ProductList;

        ProductList = from pd in db.tbProductStockMsts
                      join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                      select new DepotWiseStockExport
                      {

                        SimpleCode = pd.ProductCode,
                        DepotName = dp.DepotTitle,
                        StockQuantity = pd.StockQuantity == null ? 0 : (int)pd.StockQuantity

                      };

        string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();
        string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();

        if (RoleCode != "ACTNT" || Regionlist.ToLower() == "all")
        {
          ProductList = from pd in db.tbProductStockMsts
                        join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                        select new DepotWiseStockExport
                        {

                          SimpleCode = pd.ProductCode,
                          DepotName = dp.DepotTitle,
                          StockQuantity = pd.StockQuantity == null ? 0 : (int)pd.StockQuantity

                        };

        }
        else
        {
          string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();

          if (!string.IsNullOrEmpty(Regionlist) && !string.IsNullOrEmpty(depotlist))
          {


            string[] arraydepotid = depotlist.Split(',');

            string[] arrayregionid = Regionlist.Split(',');

            if (depotlist.ToLower() == "all")
            {
              ProductList = from pd in db.tbProductStockMsts
                            join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                            where arrayregionid.Contains(dp.RegionCode)
                            select new DepotWiseStockExport
                            {

                              SimpleCode = pd.ProductCode,
                              DepotName = dp.DepotTitle,
                              StockQuantity = pd.StockQuantity == null ? 0 : (int)pd.StockQuantity

                            };
            }
            else
            {
              ProductList = from pd in db.tbProductStockMsts
                            join dp in db.tbDepotMsts on pd.FKDepotId equals dp.PKDepotId
                            where arraydepotid.Contains(dp.PKDepotId.ToString())
                            select new DepotWiseStockExport
                            {

                              SimpleCode = pd.ProductCode,
                              DepotName = dp.DepotTitle,
                              StockQuantity = pd.StockQuantity == null ? 0 : (int)pd.StockQuantity

                            };
            }
          }
        }



        return ConvertData.ToDataTable<DepotWiseStockExport>(ProductList.ToList());
      }
      catch (Exception ex)
      {
        DataTable dt = null;
        return dt;

      }
    }

    public IEnumerable<ProductStockReportBO> GetAllProductStockReport(ProductStockFilter filter)
    {

      db = new DMSDbCon();
      IEnumerable<ProductStockReportBO> data = from pd in db.tbProductsMsts
                                               join pdst in db.tbProductStockMsts on pd.ProductCode equals pdst.ProductCode
                                               join dp in db.tbDepotMsts on pdst.FKDepotId equals dp.PKDepotId
                                               select new ProductStockReportBO
                                               {
                                                 AvailableQty = (int)pdst.StockQuantity,
                                                 DepotId = dp.PKDepotId,
                                                 DepotName = dp.DepotTitle,
                                                 Itemcode = pd.ProductCode,
                                                 itemdescription = pd.ProductName,
                                                 MinimumQty = 0,
                                                 RegionCode = dp.RegionCode,
                                                 ReorderLevel = 0,
                                                 TotalValue = (int)pdst.StockQuantity * (decimal)pdst.SellingPrice,
                                                 UnitPrice = (decimal)pdst.SellingPrice,
                                                 ActiveStatus = (int)pd.ActiveStatus,
                                                 ItemGroup = pd.ProductGroup
                                               };

      var result = data.AsQueryable();

      if (filter != null)
      {
        if (!string.IsNullOrEmpty(filter.ProductCodeOrName))
          result = result.Where(x => x.itemdescription == filter.ProductCodeOrName.ToString() || x.Itemcode == filter.ProductCodeOrName.ToString());

        if (!string.IsNullOrEmpty(filter.ProductGroup))
          result = result.Where(x => x.ItemGroup == filter.ProductGroup.ToString());

        if (filter.Active.HasValue)
          result = result.Where(x => x.ActiveStatus == filter.Active);
        if (filter.DepotId.HasValue)
          result = result.Where(x => x.DepotId == filter.DepotId);


        if (!string.IsNullOrEmpty(filter.RegionCode))
          result = result.Where(x => x.RegionCode == filter.RegionCode);
      }


      return result.AsEnumerable();
    }

    public ProductDetailApiBO GetProductDetailApi(int FkDepotId, string ProductCode)
    {
      try
      {
        db = new DMSDbCon();
        ProductDetailApiBO ProductDetail = new ProductDetailApiBO();

        int StockId = db.tbProductStockMsts.Where(x => x.ProductCode == ProductCode && x.FKDepotId == FkDepotId).Select(x => x.PKStockId).SingleOrDefault();

        ProductDetail = (from pd in db.tbProductsMsts
                         join stock in db.tbProductStockMsts
                              on pd.ProductCode equals stock.ProductCode
                         where stock.PKStockId == StockId
                         select new ProductDetailApiBO
                         {
                           ProductCode = pd.ProductCode,
                           Description = pd.ProductName,
                           PackDescription = pd.PackDescription,
                           Group = pd.ProductGroup,
                           PackSize = pd.PackSize,
                           Description2 = pd.Description2,
                           StockQuantity = (int)stock.StockQuantity,
                           SellingPrice = (decimal)stock.SellingPrice,
                           DepotId = (int)stock.FKDepotId
                         }).SingleOrDefault();

        return ProductDetail;
      }
      catch (Exception ex)
      {
        ProductDetailApiBO ProductDetail = null;
        return ProductDetail;
      }
    }
  }
}
