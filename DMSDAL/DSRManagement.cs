﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace DMSDAL
{
    public class DSRManagement
    {
        DMSDbCon db;

        public IEnumerable<ChartFormat> GetDsrGraph(string RegionCode, string DepotId)
        {
            try
            {
                db = new DMSDbCon();

                IEnumerable<DSRListModel> dSRList;

                dSRList = from dsr in db.tbDSRMsts
                          join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                          join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                          where dsr.DSRDate.Value.Year.ToString() == "2018"
                          select new DSRListModel
                          {
                              RegionCode = dp.RegionCode,
                              DepotName = dp.DepotTitle,
                              DepotId = dp.PKDepotId,
                              DSRDate = (DateTime)dsr.DSRDate,
                              DSRId = dsr.PKDSRID,
                              DSRDiscount = (decimal)dsr.DSRDiscAmount,
                              DSRNo = dsr.DSRNo,
                              DSRImage = dsr.DSRImage,
                              DSRTotal = (decimal)dsr.DSRAmount,
                              DSRTotalSale = (decimal)dsr.DSRNetAmount,
                              Status = st.StatusGroup,
                              StatusDescription = st.Status
                          };

                var result = dSRList.AsQueryable();
                if (!String.IsNullOrEmpty(DepotId))
                    result = result.Where(x => x.DepotId.ToString() == DepotId);

                if (!String.IsNullOrEmpty(RegionCode))
                {
                    result = result.Where(x => x.RegionCode.ToLower().Contains(RegionCode.ToLower()));
                }

                dSRList = result.AsEnumerable();

                IEnumerable<ChartFormat> dSRListModel = (from d in dSRList
                                                         group d by new
                 {
                     Month = d.DSRDate.ToString("MMMM"),

                 } into g
                                                         select new
                                                         {
                                                             Month = g.Key.Month,
                                                             Total = g.Sum(x => x.DSRTotalSale),
                                                         }
        ).AsEnumerable()
        .Select(g => new ChartFormat
        {
            Name = g.Month,
            Value = (decimal)g.Total,

        });
                var sample = dSRListModel.ToList();



                return dSRListModel;
            }
            catch (Exception ex)
            {
                IEnumerable<ChartFormat> data = null;
                return data;
            }
        }

        public IEnumerable<DSRListModel> GetAllDsr(DSRFilters searchModel, string Username)
        {
            try
            {
                db = new DMSDbCon();

                String path = WebConfigurationManager.AppSettings["Url"].ToString();
                string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();
                string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();
                string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();

                if (depotlist == null)
                {
                    depotlist = "";
                }
                if (Regionlist == null)
                {
                    Regionlist = "";
                }
                string[] arraydepotid = depotlist.Split(',');

                string[] arrayregionid = Regionlist.Split(',');
                IEnumerable<DSRListModel> dSRListModel;

                if (RoleCode != "ACCLK" && RoleCode != "ACTNT")
                {

                    dSRListModel = from dsr in db.tbDSRMsts
                                   join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                                   join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                                   select new DSRListModel
                                   {
                                       DepotName = dp.DepotTitle,
                                       DepotId = dp.PKDepotId,
                                       DSRDate = (DateTime)dsr.DSRDate,
                                       DSRId = dsr.PKDSRID,
                                       DSRDiscount = (decimal)dsr.DSRDiscAmount,
                                       DSRNo = dsr.DSRNo,
                                       DSRImage = path + dsr.DSRImage,
                                       DSRTotal = (decimal)dsr.DSRAmount,
                                       DSRTotalSale = (decimal)dsr.DSRNetAmount,
                                       Status = st.StatusGroup,
                                       StatusDescription = st.Status,
                                       Images = from im in db.tbDSRImages
                                                where im.FKDSRId == dsr.PKDSRID
                                                select new DSRImageBO
                                                {
                                                    DSRImage = path + im.DSRImage,
                                                    ImageKey = im.ImageKey
                                                }
                                   };
                }
                else
                {

                    if (Regionlist.ToLower().ToString() == "all")
                    {
                        dSRListModel = from dsr in db.tbDSRMsts
                                       join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                                       join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                                       select new DSRListModel
                                       {
                                           DepotName = dp.DepotTitle,
                                           DepotId = dp.PKDepotId,
                                           DSRDate = (DateTime)dsr.DSRDate,
                                           DSRId = dsr.PKDSRID,
                                           DSRDiscount = (decimal)dsr.DSRDiscAmount,
                                           DSRNo = dsr.DSRNo,
                                           DSRImage = path + dsr.DSRImage,
                                           DSRTotal = (decimal)dsr.DSRAmount,
                                           DSRTotalSale = (decimal)dsr.DSRNetAmount,
                                           Status = st.StatusGroup,
                                           StatusDescription = st.Status,
                                           Images = from im in db.tbDSRImages
                                                    where im.FKDSRId == dsr.PKDSRID
                                                    select new DSRImageBO
                                                    {
                                                        DSRImage = path + im.DSRImage,
                                                        ImageKey = im.ImageKey
                                                    }
                                       };
                    }
                    else
                    {

                        if (depotlist.ToLower().ToString() == "all")
                        {

                            dSRListModel = from dsr in db.tbDSRMsts
                                           join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                                           join rg in db.tbRegionsMsts on dp.RegionCode equals rg.RegionCode
                                           join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                                           where arrayregionid.Contains(dp.RegionCode.ToString())
                                           select new DSRListModel
                                           {
                                               DepotName = dp.DepotTitle,
                                               DepotId = dp.PKDepotId,
                                               DSRDate = (DateTime)dsr.DSRDate,
                                               DSRId = dsr.PKDSRID,
                                               DSRDiscount = (decimal)dsr.DSRDiscAmount,
                                               DSRNo = dsr.DSRNo,
                                               DSRImage = path + dsr.DSRImage,
                                               DSRTotal = (decimal)dsr.DSRAmount,
                                               DSRTotalSale = (decimal)dsr.DSRNetAmount,
                                               Status = st.StatusGroup,
                                               StatusDescription = st.Status,
                                               Images = from im in db.tbDSRImages
                                                        where im.FKDSRId == dsr.PKDSRID
                                                        select new DSRImageBO
                                                        {
                                                            DSRImage = path + im.DSRImage,
                                                            ImageKey = im.ImageKey
                                                        }
                                           };
                        }
                        else
                        {

                            dSRListModel = from dsr in db.tbDSRMsts
                                           join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                                           join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                                           where arraydepotid.Contains(dp.PKDepotId.ToString())
                                           select new DSRListModel
                                           {
                                               DepotName = dp.DepotTitle,
                                               DepotId = dp.PKDepotId,
                                               DSRDate = (DateTime)dsr.DSRDate,
                                               DSRId = dsr.PKDSRID,
                                               DSRDiscount = (decimal)dsr.DSRDiscAmount,
                                               DSRNo = dsr.DSRNo,
                                               DSRImage = path + dsr.DSRImage,
                                               DSRTotal = (decimal)dsr.DSRAmount,
                                               DSRTotalSale = (decimal)dsr.DSRNetAmount,
                                               Status = st.StatusGroup,
                                               StatusDescription = st.Status,
                                               Images = from im in db.tbDSRImages
                                                        where im.FKDSRId == dsr.PKDSRID
                                                        select new DSRImageBO
                                                        {
                                                            DSRImage = path + im.DSRImage,
                                                            ImageKey = im.ImageKey
                                                        }
                                           };
                        }
                    }

                }
                var result = dSRListModel.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) <= DbFunctions.TruncateTime(searchModel.ToDate));
                    if (!String.IsNullOrEmpty(searchModel.Status))
                    {
                        result = result.Where(x => x.Status.ToLower() == searchModel.Status.ToLower());
                    }
                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DSRListModel> data = null;
                return data;
            }
        }

        public IEnumerable<DSRListModel> GetAllDsrReport(DSRReportFilter searchModel)
        {
            try
            {
                db = new DMSDbCon();

                String path = WebConfigurationManager.AppSettings["Url"].ToString();

                IEnumerable<DSRListModel> dSRListModel;

                dSRListModel = from dsr in db.tbDSRMsts
                               join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                               join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                               select new DSRListModel
                               {
                                   DepotName = dp.DepotTitle,
                                   DepotId = dp.PKDepotId,
                                   DSRDate = (DateTime)dsr.DSRDate,
                                   DSRId = dsr.PKDSRID,
                                   RegionCode = dp.RegionCode,
                                   DSRDiscount = (decimal)dsr.DSRDiscAmount,
                                   DSRNo = dsr.DSRNo,
                                   DSRImage = path + dsr.DSRImage,
                                   DSRTotal = (decimal)dsr.DSRAmount,
                                   DSRTotalSale = (decimal)dsr.DSRNetAmount,
                                   Status = st.StatusGroup,
                                   StatusDescription = st.Status,
                                   DSRNetValue = (decimal)dsr.DSRNetAmount,
                                   PCVAmount = (decimal)dsr.DSRDiscAmount,
                                   PCVNo = dsr.PCVNo,
                                   PCSNo = dsr.PCSNo,
                                   TotalValueExclusive = (decimal)dsr.DSRAmount,
                                   PCSAmount = (decimal)dsr.PCSAmount,
                                   TotalValueInclusive = (decimal)dsr.DSRAmount,
                                   VatAmount = (decimal)0
                               };


                var result = dSRListModel.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) >= DbFunctions.TruncateTime(searchModel.FromDate));

                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.DSRNo))
                        result = result.Where(x => x.DSRNo.Contains(searchModel.DSRNo));

                    if (!String.IsNullOrEmpty(searchModel.Status))
                        result = result.Where(x => x.Status.ToLower() == searchModel.Status);


                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }

                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DSRListModel> data = null;
                return data;
            }
        }

        public IEnumerable<DSRListExportModel> GetAllDsrExportReport(DSRReportFilter searchModel)
        {
            try
            {
                db = new DMSDbCon();

                String path = WebConfigurationManager.AppSettings["Url"].ToString();

                IEnumerable<DSRListExportModel> dSRListModel;

                dSRListModel = from dsr in db.tbDSRMsts
                               join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                               join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                               select new DSRListExportModel
                               {
                                   DepotName = dp.DepotTitle,
                                   DepotId = dp.PKDepotId,
                                   DSRDate = (DateTime)dsr.DSRDate,
                                   DSRId = dsr.PKDSRID,
                                   DSRNo = dsr.DSRNo,
                                   DSRNetTotal = (decimal)dsr.DSRNetAmount,
                                   RegionCode = dp.RegionCode,
                                   PCSAmount = (decimal)dsr.PCSAmount,
                                   PCSNo = dsr.PCSNo,
                                   PCVAmount = (decimal)dsr.DSRDiscAmount,
                                   PCVNo = dsr.PCVNo,
                                   TotalValueExclusive = (decimal)dsr.DSRAmount,
                                   TotalValueInclusive = (decimal)dsr.DSRAmount,
                                   VatAmount = (decimal)0,
                                   Status = st.StatusGroup,

                               };


                var result = dSRListModel.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }


                    if (!String.IsNullOrEmpty(searchModel.Status))
                    {
                        result = result.Where(x => x.Status.ToLower().Contains(searchModel.Status.ToLower()));
                    }

                    if (!String.IsNullOrEmpty(searchModel.DSRNo))
                    {
                        result = result.Where(x => x.DSRNo.ToLower().Contains(searchModel.DSRNo.ToLower()));
                    }

                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DSRListExportModel> data = null;
                return data;
            }
        }


        public IEnumerable<DSRProductReport> GetAllDsrProductReport(DSRReportFilter searchModel)
        {
            try
            {
                db = new DMSDbCon();

                String path = WebConfigurationManager.AppSettings["Url"].ToString();

                IEnumerable<DSRProductReport> dSRListModel;

                dSRListModel = from dsr in db.tbDSRMsts
                               join Od in db.tbOrdersMsts on dsr.PKDSRID equals Od.FKDSRId
                               join Odd in db.tbOrderDtls on Od.PKOrderId equals Odd.FKORderId
                               join pd in db.tbProductsMsts on Odd.ProductCode equals pd.ProductCode
                               join tx in db.tbTaxMsts on pd.TaxType equals tx.PKTaxTypeId
                               join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                               join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                               select new DSRProductReport
                               {
                                   DepotName = dp.DepotTitle,
                                   DSRNo = dsr.DSRNo,
                                   DSRStatus = st.StatusGroup,
                                   ItemCode = Odd.ProductCode,
                                   ItemDescription = pd.ProductName,
                                   UnitPriceExclusive = ((decimal)Odd.Rate * 100) / (100 + (decimal)tx.TaxValue),
                                   UnitPriceInclusive = (decimal)Odd.Rate,
                                   SalePriceInclusive = (decimal)Odd.NetAmount,
                                   SalePriceExclusive = ((decimal)Odd.NetAmount * 100) / (100 + (decimal)tx.TaxValue),
                                   SaleQuantity = (int)Odd.Quantity,
                                   VATAmount = ((decimal)Odd.NetAmount) - (((decimal)Odd.NetAmount * 100) / (100 + (decimal)tx.TaxValue)),
                                   DepotId = dp.PKDepotId,
                                   DSRDate = (DateTime)dsr.DSRDate,
                                   RegionCode = dp.RegionCode
                               };


                var result = dSRListModel.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) >= DbFunctions.TruncateTime(searchModel.FromDate));

                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.DSRNo))
                        result = result.Where(x => x.DSRNo.Contains(searchModel.DSRNo));

                    if (!String.IsNullOrEmpty(searchModel.Status))
                        result = result.Where(x => x.DSRStatus.ToLower() == searchModel.Status);


                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }

                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DSRProductReport> data = null;
                return data;
            }
        }

        public IEnumerable<DSRPCVReportBO> GetAllDsrPCVReport(DSRReportFilter searchModel)
        {
            try
            {
                db = new DMSDbCon();

                String path = WebConfigurationManager.AppSettings["Url"].ToString();

                IEnumerable<DSRPCVReportBO> dSRListModel;

                dSRListModel = from dsr in db.tbDSRMsts
                               join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                               join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                               select new DSRPCVReportBO
                               {
                                   DepotId = dp.PKDepotId,
                                   DepotName = dp.DepotTitle,
                                   DSRDate = (DateTime)dsr.DSRDate,
                                   DSRNo = dsr.DSRNo,
                                   DSRStatus = st.StatusGroup,
                                   PCVAmount = (decimal)dsr.DSRDiscAmount,
                                   PCVNo = dsr.PCVNo,
                                   RegionCode = dp.RegionCode

                               };


                var result = dSRListModel.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) >= DbFunctions.TruncateTime(searchModel.FromDate));

                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.DSRNo))
                        result = result.Where(x => x.DSRNo.Contains(searchModel.DSRNo));

                    if (!String.IsNullOrEmpty(searchModel.Status))
                        result = result.Where(x => x.DSRStatus.ToLower() == searchModel.Status);


                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }

                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DSRPCVReportBO> data = null;
                return data;
            }
        }

        public IEnumerable<DSRPCSReportBO> GetAllDsrPCSReport(DSRReportFilter searchModel)
        {
            try
            {
                db = new DMSDbCon();

                String path = WebConfigurationManager.AppSettings["Url"].ToString();

                IEnumerable<DSRPCSReportBO> dSRListModel;

                dSRListModel = from dsr in db.tbDSRMsts
                               join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                               join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                               select new DSRPCSReportBO
                               {
                                   DepotId = dp.PKDepotId,
                                   DepotName = dp.DepotTitle,
                                   DSRDate = (DateTime)dsr.DSRDate,
                                   DSRNo = dsr.DSRNo,
                                   DSRStatus = st.StatusGroup,
                                   PCSAmount = (decimal)dsr.PCSAmount,
                                   PCSNo = dsr.PCSNo,
                                   RegionCode = dp.RegionCode

                               };


                var result = dSRListModel.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) >= DbFunctions.TruncateTime(searchModel.FromDate));

                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.DSRDate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.DSRNo))
                        result = result.Where(x => x.DSRNo.Contains(searchModel.DSRNo));

                    if (!String.IsNullOrEmpty(searchModel.Status))
                        result = result.Where(x => x.DSRStatus.ToLower() == searchModel.Status);


                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }

                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<DSRPCSReportBO> data = null;
                return data;
            }
        }

        public DSRListResult GetAllDsrApi(DSRListParameter dSRListParameter)
        {
            try
            {
                db = new DMSDbCon();

                DSRListResult result = new DSRListResult();
                DateTime currentdate = DateTime.Now;
                int days = dSRListParameter.NoOfDays - 1;
                currentdate = currentdate.AddDays(-days);
                String path = WebConfigurationManager.AppSettings["Url"].ToString();
                result.DSRs = from dsr in db.tbDSRMsts
                              join bt in db.tbBankTransactions.GroupBy(x => x.FKDSRId).Select(trn => new { Id = trn.Key, Sum = trn.Sum(x => x.TransactionAmount) })
                                on dsr.PKDSRID equals bt.Id into tranInfo
                              from transaction in tranInfo.DefaultIfEmpty()
                              join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                              join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId

                              where dsr.FKDepotId == dSRListParameter.DepotId && DbFunctions.TruncateTime(dsr.DSRDate) >= DbFunctions.TruncateTime(currentdate)
                              select new DSRListModelApi
                                {

                                    DSRDate = dsr.DSRDate.ToString(),
                                    DSRDiscAmount = (decimal)dsr.DSRDiscAmount,
                                    DSRNo = dsr.DSRNo,
                                    DSRAmount = (decimal)dsr.DSRAmount,
                                    PCSAmount = (decimal)dsr.PCSAmount,
                                    DSRNetAmount = (decimal)dsr.DSRNetAmount,
                                    DSRCurrentStatus = st.Status,
                                    CurrentTransactionAmount = transaction.Sum == null ? 0 : (decimal)transaction.Sum,
                                    Images = from im in db.tbDSRImages
                                             where im.FKDSRId == dsr.PKDSRID
                                             select new DSRImageBO
                                             {
                                                 DSRImage = im.DSRImage,
                                                 ImageKey = im.ImageKey
                                             }
                                };

                List<DSRListModelApi> data = result.DSRs.ToList();
                data.ForEach(x => x.DSRDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(x.DSRDate)));
                result.DSRs = data as IEnumerable<DSRListModelApi>;

                return result;
            }
            catch (Exception ex)
            {
                DSRListResult result = null;
                return result;
            }
        }

        public StatusMessage CreateDSRApi(CreateDSRParameter createDSRParameter)
        {
            StatusMessage statusMessage = new StatusMessage();

            try
            {
                DateTime Dsrdate = DateOperation.FromMilliToDate(createDSRParameter.DSRDate);
                db = new DMSDbCon();
                int DSRID = db.tbDSRMsts.Where(x => DbFunctions.TruncateTime(x.CreatedOn) == DbFunctions.TruncateTime(Dsrdate) && x.FKDepotId == createDSRParameter.DepotId && x.DSRNo == null).Select(x => x.PKDSRID).SingleOrDefault();
                int count = db.tbDSRMsts.Where(x => x.DSRNo == createDSRParameter.DSRNo).Count();
                if (count > 0)
                {
                    statusMessage.Status = false;
                    statusMessage.Message = "Trying to create dublicate DSR with same DSR number.";
                    return statusMessage;
                }
                tbDSRMst tbdSRMst = db.tbDSRMsts.Find(DSRID);
                tbdSRMst.DSRDate = Dsrdate;
                tbdSRMst.CreatedBy = createDSRParameter.Username;
                tbdSRMst.DSRAmount = createDSRParameter.DSRAmount;
                tbdSRMst.DSRDiscAmount = createDSRParameter.DSRDiscAmount;
                tbdSRMst.DSRNetAmount = createDSRParameter.DSRNetAmount;
                tbdSRMst.DSRNo = createDSRParameter.DSRNo;
                tbdSRMst.DSRImage = createDSRParameter.Path;
                tbdSRMst.PCSNo = createDSRParameter.PCSNo;
                tbdSRMst.PCVNo = createDSRParameter.PCVNo;
                tbdSRMst.PCSAmount = createDSRParameter.PCSAmount;
                int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();
                tbdSRMst.FKStatusId = sid;
                db.Entry(tbdSRMst).State = EntityState.Modified;
                db.SaveChanges();

                statusMessage.Status = true;
            }
            catch (Exception ex)
            {
                statusMessage.Status = false;
                statusMessage.Message = ex.Message;
            }

            return statusMessage;

        }

        public StatusMessage UploadDSRImageApi(UploadDSRImageBO uploadDSRImageBO)
        {
            StatusMessage statusMessage = new StatusMessage();

            try
            {
                db = new DMSDbCon();
                int DSRID = db.tbDSRMsts.Where(x => x.DSRNo == uploadDSRImageBO.DSRNo).Select(x => x.PKDSRID).SingleOrDefault();

                if (DSRID == 0)
                {
                    statusMessage.Status = false;
                    statusMessage.Message = "No such DSR number Present.";
                    return statusMessage;
                }
                tbDSRImage tbdSRImage = new tbDSRImage();
                tbdSRImage.CreatedBy = uploadDSRImageBO.Username;
                tbdSRImage.CreatedOn = DateTime.Now;
                tbdSRImage.DSRImage = uploadDSRImageBO.Path;
                tbdSRImage.FKDSRId = DSRID;
                tbdSRImage.ImageKey = uploadDSRImageBO.ImageKey;
                db.tbDSRImages.Add(tbdSRImage);
                db.SaveChanges();

                statusMessage.Status = true;
            }
            catch (Exception ex)
            {
                statusMessage.Status = false;
                statusMessage.Message = ex.Message;
            }

            return statusMessage;

        }

        public DSRTodayOrderApiResult GetTodaysOrderApi(DSRListParameter dSRListParameter)
        {
            try
            {
                db = new DMSDbCon();

                DateTime currentdate = DateOperation.FromMilliToDate(dSRListParameter.DSRDate);



                DSRTodayOrderApiResult dSRTodayOrderApiResult = new DSRTodayOrderApiResult();

                dSRTodayOrderApiResult.Products = from dsr in db.tbDSRMsts
                                                  join od in db.tbOrdersMsts on dsr.PKDSRID equals od.FKDSRId
                                                  join ods in db.tbOrderDtls on od.PKOrderId equals ods.FKORderId
                                                  join pd in db.tbProductsMsts on ods.ProductCode equals pd.ProductCode
                                                  where od.FKDepotId == dSRListParameter.DepotId && DbFunctions.TruncateTime(od.OrderDate) == DbFunctions.TruncateTime(currentdate)
                                                  && dsr.DSRNo == null
                                                  select new ProductAllDetailBO
                                                  {
                                                      ProductCode = pd.ProductCode,
                                                      Description = pd.ProductName,
                                                      PackDescription = pd.PackDescription,
                                                      ProductGroup = pd.ProductGroup,
                                                      PackSize = pd.PackSize,
                                                      TaxType = pd.TaxType,
                                                      ActiveStatus = pd.ActiveStatus,
                                                      Description2 = pd.Description2,
                                                      PKProductId = pd.PKProductId,
                                                      ProductGroupDescription = pd.ProductGroupDescription,
                                                      Amount = (ods.Amount == null) ? 0 : (decimal)ods.Amount,
                                                      Rate = (ods.Rate == null) ? 0 : (decimal)ods.Rate,
                                                      DiscAmount = (ods.DiscAmount == null) ? 0 : (decimal)ods.DiscAmount,
                                                      DiscountType = ods.DiscountType,
                                                      DiscountValue = (ods.DiscountValue == null) ? 0 : (decimal)ods.DiscountValue,
                                                      NetAmount = (ods.NetAmount == null) ? 0 : (decimal)ods.NetAmount,
                                                      Quantity = (ods.Quantity == null) ? 0 : (int)ods.Quantity,
                                                      TaxAmount = (ods.TaxValue == null) ? 0 : (decimal)ods.TaxValue

                                                  };

                dSRTodayOrderApiResult.Products = dSRTodayOrderApiResult.Products.GroupBy(x => new
                {
                    x.ProductCode,
                    x.Rate
                }).Select(xl => new ProductAllDetailBO
                {
                    ActiveStatus = xl.First().ActiveStatus,
                    Amount = xl.Sum(z => z.Amount),
                    Description2 = xl.First().Description2,
                    DiscAmount = xl.Sum(z => z.DiscAmount),
                    DiscountType = xl.First().DiscountType,
                    DiscountValue = xl.Sum(z => z.DiscountValue),
                    NetAmount = xl.Sum(z => z.NetAmount),
                    PackDescription = xl.First().PackDescription,
                    PackSize = xl.First().PackSize,
                    PKProductId = xl.First().PKProductId,
                    ProductCode = xl.First().ProductCode,
                    ProductGroup = xl.First().ProductGroup,
                    ProductGroupDescription = xl.First().ProductGroupDescription,
                    Description = xl.First().Description,
                    Quantity = xl.Sum(z => z.Quantity),
                    Rate = xl.First().Rate,
                    TaxAmount = xl.Sum(z => z.TaxAmount),
                    TaxType = xl.First().TaxType

                }).ToList();
                return dSRTodayOrderApiResult;
            }
            catch (Exception ex)
            {
                DSRTodayOrderApiResult dSRTodayOrderApiResult = new DSRTodayOrderApiResult();

                return dSRTodayOrderApiResult;
            }

        }

        public StatusMessage ApproveDsr(int DsrId, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);
                var query = (from dsr in db.tbDSRMsts
                             join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                             where dsr.PKDSRID == DsrId
                             select new { st.StatusGroup }).SingleOrDefault();

                string status = query.StatusGroup;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "approved").Select(x => x.StatusId).SingleOrDefault();
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "confirm" || status.ToLower() == "rejected")
                    {
                        tbDSRDtl tbdSRDtl = new tbDSRDtl();
                        tbdSRDtl.Comments = "";
                        tbdSRDtl.CreatedBy = username;
                        tbdSRDtl.CreatedOn = DateTime.Now;
                        tbdSRDtl.FKStatusId = sid;
                        tbdSRDtl.FKDSRID = DsrId;
                        db.tbDSRDtls.Add(tbdSRDtl);
                        tbDSRMst tbdSRMst = db.tbDSRMsts.Find(DsrId);
                        tbdSRMst.FKStatusId = sid;
                        db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The DSR is approved sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is not confirmed by Depo Manager/Depo Supervisor";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid DSR ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage RejectDsr(int DsrId, string message, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);
                var query = (from dsr in db.tbDSRMsts
                             join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                             where dsr.PKDSRID == DsrId
                             select new { st.StatusGroup }).SingleOrDefault();
                string status = query.StatusGroup;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "confirm" || status.ToLower() == "rejected")
                    {
                        tbDSRDtl tbdSRDtl = new tbDSRDtl();
                        tbdSRDtl.Comments = message;
                        tbdSRDtl.CreatedBy = username;
                        tbdSRDtl.CreatedOn = DateTime.Now;
                        tbdSRDtl.FKStatusId = sid;
                        tbdSRDtl.FKDSRID = DsrId;
                        db.tbDSRDtls.Add(tbdSRDtl);
                        tbDSRMst tbdSRMst = db.tbDSRMsts.Find(DsrId);
                        tbdSRMst.FKStatusId = sid;
                        db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The DSR is Rejected sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is not confirmed by Depo Manager/Depo Supervisor";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid DSR ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public DSREditBO GetSpecificDSR(int DSRId)
        {
            try
            {
                db = new DMSDbCon();
                String path = WebConfigurationManager.AppSettings["Url"].ToString();
                DSREditBO dSREditBO = (from dsr in db.tbDSRMsts
                                       join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                                       join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                                       where dsr.PKDSRID == DSRId
                                       select new DSREditBO
                                                           {
                                                               DSRId = dsr.PKDSRID,
                                                               DepotName = dp.DepotTitle,
                                                               DepotId = dp.PKDepotId,
                                                               DSRDate = (DateTime)dsr.DSRDate,
                                                               DSRDiscount = (decimal)dsr.DSRDiscAmount,
                                                               DSRNo = dsr.DSRNo,
                                                               DSRImage = path + dsr.DSRImage,
                                                               PCVNo = dsr.PCVNo,
                                                               PCSNo = dsr.PCSNo,
                                                               PCSAmount = dsr.PCSAmount == null ? 0 : (decimal)dsr.PCSAmount,
                                                               DSRTotal = (decimal)dsr.DSRAmount,
                                                               DSRTotalSale = (decimal)dsr.DSRNetAmount,
                                                               Status = st.Status,
                                                               StatusGroup = st.StatusGroup,
                                                               Images = from im in db.tbDSRImages
                                                                        where im.FKDSRId == dsr.PKDSRID
                                                                        select new DSRImageBO
                                                                        {
                                                                            DSRImage = path + im.DSRImage,
                                                                            ImageKey = im.ImageKey
                                                                        }
                                                           }).SingleOrDefault();
                OrderManagement orderManagement = new OrderManagement();
                dSREditBO.orderManageModal = orderManagement.GetAllOrderByDSR(DSRId);
                return dSREditBO;
            }
            catch (Exception ex)
            {
                DSREditBO dSREditBO = null;
                return dSREditBO;
            }
        }

        public StatusMessage UpdateSpecificDSR(DSREditBO dSREditBO, string username)
        {
            try
            {
                db = new DMSDbCon();
                StatusMessage statusMessage = new StatusMessage();

                statusMessage.Message = "DSR Updated Successfully";
                statusMessage.Status = true;
                tbDSRMst record = db.tbDSRMsts.Find(dSREditBO.DSRId);
                record.CreatedBy = username;
                record.DSRAmount = dSREditBO.DSRTotal;
                record.DSRDiscAmount = dSREditBO.DSRDiscount;
                record.DSRNetAmount = dSREditBO.DSRTotalSale;
                db.Entry(record).State = EntityState.Modified;
                db.SaveChanges();
                return statusMessage;
            }
            catch
            {
                StatusMessage statusMessage = new StatusMessage();
                statusMessage.Message = "DSR Updated Failed";
                statusMessage.Status = false;
                return statusMessage;
            }



        }

        public SpecificDSR GetSpecificDSR(string DSRNo)
        {
            SpecificDSR specificDSR = new SpecificDSR();
            String path = WebConfigurationManager.AppSettings["Url"].ToString();
            try
            {
                db = new DMSDbCon();
                specificDSR = (from dsr in db.tbDSRMsts
                               join dp in db.tbDepotMsts on dsr.FKDepotId equals dp.PKDepotId
                               join st in db.tbStatusMsts on dsr.FKStatusId equals st.StatusId
                               where dsr.DSRNo == DSRNo
                               select new SpecificDSR
                               {
                                   DSRId = dsr.PKDSRID,
                                   DSRDate = dsr.DSRDate.ToString(),
                                   DSRDiscAmount = (decimal)dsr.DSRDiscAmount,
                                   DSRNo = dsr.DSRNo,
                                   DSRImage = path + dsr.DSRImage,
                                   DSRAmount = (decimal)dsr.DSRAmount,
                                   DSRNetAmount = (decimal)dsr.DSRNetAmount,
                                   DSRCurrentStatus = st.Status,
                                   PCSAmount = dsr.PCSAmount == null ? 0 : (decimal)dsr.PCSAmount,
                                   PCSNo = dsr.PCSNo,
                                   PCVNo = dsr.PCVNo
                               }).SingleOrDefault();

                specificDSR.DSRDate = (specificDSR.DSRDate == "") ? "" : DateOperation.FromDateTOMilli(Convert.ToDateTime(specificDSR.DSRDate));

                specificDSR.Products = from od in db.tbOrdersMsts
                                       join ods in db.tbOrderDtls on od.PKOrderId equals ods.FKORderId
                                       join pd in db.tbProductsMsts on ods.ProductCode equals pd.ProductCode

                                       where od.FKDSRId == specificDSR.DSRId
                                       select new ProductAllDetailBO
                                       {
                                           ProductCode = pd.ProductCode,
                                           Description = pd.ProductName,
                                           PackDescription = pd.PackDescription,
                                           ProductGroup = pd.ProductGroup,
                                           PackSize = pd.PackSize,
                                           TaxType = pd.TaxType,
                                           ActiveStatus = pd.ActiveStatus,
                                           Description2 = pd.Description2,
                                           PKProductId = pd.PKProductId,
                                           ProductGroupDescription = pd.ProductGroupDescription,
                                           Quantity = (ods.Quantity == null) ? 0 : (int)ods.Quantity,
                                           Rate = (ods.Quantity == null) ? 0 : (decimal)ods.Rate,
                                           DiscountType = ods.DiscountType,
                                           Amount = (ods.Amount == null) ? 0 : (decimal)ods.Amount,
                                           DiscAmount = (ods.DiscountValue == null) ? 0 : (decimal)ods.DiscAmount,
                                           NetAmount = (ods.NetAmount == null) ? 0 : (decimal)ods.NetAmount,
                                           DiscountValue = (ods.DiscountValue == null) ? 0 : (decimal)ods.DiscountValue,
                                           TaxAmount = (ods.TaxValue == null) ? 0 : (decimal)ods.TaxValue
                                       };
                specificDSR.Images = from im in db.tbDSRImages
                                     where im.FKDSRId == specificDSR.DSRId
                                     select new DSRImageBO
                                     {
                                         DSRImage = im.DSRImage,
                                         ImageKey = im.ImageKey
                                     };

            }
            catch (Exception ex)
            {
                specificDSR = null;
            }

            return specificDSR;
        }

        public StatusMessage ApproveDsr(DSRApproveAPIBO para)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(para.Username);
                var query = (from dsr in db.tbDSRMsts
                             where dsr.DSRNo == para.DSRNo
                             select new { dsr.PKDSRID }).SingleOrDefault();

                int DsrId = query.PKDSRID;
                DateTime cdate = DateOperation.FromMilliToDate(para.DateTime);
                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "confirm").Select(x => x.StatusId).SingleOrDefault();

                tbDSRDtl tbdSRDtl = new tbDSRDtl();
                tbdSRDtl.Comments = "";
                tbdSRDtl.CreatedBy = para.Username;
                tbdSRDtl.CreatedOn = cdate;
                tbdSRDtl.FKStatusId = sid;
                tbdSRDtl.FKDSRID = DsrId;
                db.tbDSRDtls.Add(tbdSRDtl);
                tbDSRMst tbdSRMst = db.tbDSRMsts.Find(DsrId);
                tbdSRMst.FKStatusId = sid;
                db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();


                var query2 = (from dsr in db.tbDSRMsts
                              where dsr.DSRNo == para.DSRNo
                              select new { dsr.DSRDiscAmount }).SingleOrDefault();

                decimal DiscAmount = (query2.DSRDiscAmount == null) ? 0 : (decimal)query2.DSRDiscAmount;

                if (role == "DPSPR" && DiscAmount > 50000)
                {

                    sid = db.tbStatusMsts.Where(x => x.Role == "DOMGR" && x.StatusGroup.ToLower() == "pending").Select(x => x.StatusId).SingleOrDefault();

                    tbdSRDtl = new tbDSRDtl();
                    tbdSRDtl.Comments = "";
                    tbdSRDtl.CreatedBy = para.Username;
                    tbdSRDtl.CreatedOn = DateTime.Now;
                    tbdSRDtl.FKStatusId = sid;
                    tbdSRDtl.FKDSRID = DsrId;
                    db.tbDSRDtls.Add(tbdSRDtl);
                    tbdSRMst = db.tbDSRMsts.Find(DsrId);
                    tbdSRMst.FKStatusId = sid;
                    db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                }

                statusMessage.Message = "The DSR is approved sucessfully.";
                statusMessage.Status = true;
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage RejectDsr(DSRApproveAPIBO para)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(para.Username);
                var query = (from dsr in db.tbDSRMsts
                             where dsr.DSRNo == para.DSRNo
                             select new { dsr.PKDSRID }).SingleOrDefault();

                int DsrId = query.PKDSRID;

                DateTime cdate = DateOperation.FromMilliToDate(para.DateTime);

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();

                tbDSRDtl tbdSRDtl = new tbDSRDtl();
                tbdSRDtl.Comments = para.Message;
                tbdSRDtl.CreatedBy = para.Username;
                tbdSRDtl.CreatedOn = cdate;
                tbdSRDtl.FKStatusId = sid;
                tbdSRDtl.FKDSRID = DsrId;
                db.tbDSRDtls.Add(tbdSRDtl);
                tbDSRMst tbdSRMst = db.tbDSRMsts.Find(DsrId);
                tbdSRMst.FKStatusId = sid;
                db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                statusMessage.Message = "The DSR is Rejected sucessfully.";
                statusMessage.Status = true;

            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }


    }
}
