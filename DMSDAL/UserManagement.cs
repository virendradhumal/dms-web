﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
    public class UserManagement
    {
        DMSDbCon db;

        public string ConvertDepotIdToDepotName(string value)
        {
            try
            {
                db = new DMSDbCon();
                if (value.ToLower() == "all")
                {
                    return "All";
                }
                int i = 0;
                var data = value.Split(',');

                StringBuilder sb = new StringBuilder("");

                foreach (string item in data)
                {
                    if (i != 0 && i != data.Length)
                    {
                        sb.Append(",");
                    }
                    sb.Append(db.tbDepotMsts.Where(x => x.PKDepotId.ToString() == item).Select(x => x.DepotTitle).SingleOrDefault().ToString());
                    i++;
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        
        public string ConvertRegionCodeToRegionName(string value)
        {
            try
            {
                db = new DMSDbCon();
                if (value.ToLower() == "all")
                {
                    return "All";
                }
                int i = 0;
                var data = value.Split(',');

                StringBuilder sb = new StringBuilder("");

                foreach (string item in data)
                {
                    if (i != 0 && i != data.Length - 1)
                    {
                        sb.Append(",");
                    }
                    sb.Append(db.tbRegionsMsts.Where(x => x.RegionCode.ToString() == item).Select(x => x.RegionName).SingleOrDefault().ToString());
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public IEnumerable<UserManagementModel> GetAllUser(FilterUsers searchModel)
        {
            db = new DMSDbCon();


            IEnumerable<UserManagementModel> userManagementModel = from urm in db.tbUsersMsts
                                                                    select new UserManagementModel
                                                                   {                                                   
                                                                       Name = urm.Name,
                                                                       EmailId = urm.EmailId,
                                                                       MobileNo = urm.MobileNo,
                                                                       DepoName = urm.DepotId,
                                                                       DepotId = urm.DepotId,
                                                                       Usercode = urm.Usercode,
                                                                       RoleCode = urm.RoleCode,
                                                                       ActiveStatus = urm.ActiveStatus,
                                                                       CreatedOn = urm.CreatedOn,
                                                                        Region=urm.RegionCode
                                                                      
                                                                   };
            var result = userManagementModel.AsQueryable();

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.Name))
                {
                    result = result.Where(x => x.Name.Contains(searchModel.Name.ToString()) || x.EmailId.Contains(searchModel.Name.ToString())||x.MobileNo.Contains(searchModel.Name.ToString()));

                }

                if (!string.IsNullOrEmpty(searchModel.Region))
                {
                    result = result.Where(x => x.Region.Split(',').Contains(searchModel.Region.ToString()));
                }
                if (!string.IsNullOrEmpty(searchModel.RoleCode))
                {
                    result = result.Where(x => x.RoleCode.Contains(searchModel.RoleCode.ToString()));
                   
                }

                if (searchModel.DepotId.HasValue)
                {
                    result = result.Where(x => x.DepotId.Contains(searchModel.DepotId.ToString()));
                }

            
                if (searchModel.ActiveStatus.HasValue)
                    result = result.Where(x => x.ActiveStatus == searchModel.ActiveStatus);
               
            }
            var data = result.AsEnumerable();

            var DataList = data.ToList();
            DataList.ForEach(x => x.DepoName = ConvertDepotIdToDepotName(x.DepotId));
            DataList.ForEach(x => x.Region = ConvertRegionCodeToRegionName(x.Region));

            data = DataList as IEnumerable<UserManagementModel>;


            return data;
        }

        public IEnumerable<UserManagementModel> GetUserForDetails(string Uid)
        {
            db = new DMSDbCon();
            IEnumerable<UserManagementModel> userManagementModel = from urm in db.tbUsersMsts
                                                                   where urm.Usercode == Uid
                                                                   select new UserManagementModel
                                                                   {


                                                                       Name = urm.Name,
                                                                       EmailId = urm.EmailId,
                                                                       MobileNo = urm.MobileNo,
                                                                       DepotId = urm.DepotId,
                                                                       DepoName = urm.DepotId,
                                                                       Usercode = urm.Usercode,
                                                                       RoleCode = urm.RoleCode,
                                                                       ActiveStatus = urm.ActiveStatus,
                                                                       CreatedOn = urm.CreatedOn,
                                                                       //CanBeDeleted = urm.CanBeDeleted,
                                                                       //DeleteStatus = urm.DeleteStatus,
                                                                       //CreatedBy = urm.CreatedBy,
                                                                       //ModifiedOn = urm.ModifiedOn,
                                                                       //ModifiedBy = urm.ModifiedBy
                                                                   };
            var result = userManagementModel.AsQueryable();

            
            var data = result.AsEnumerable();
            var DataList = data.ToList();
            DataList.ForEach(x => x.DepoName = ConvertDepotIdToDepotName(x.DepotId));
            
            data = DataList as IEnumerable<UserManagementModel>;

            return data;
        }
       
        public DataTable GetAllUser()
        {
            db = new DMSDbCon();
            IEnumerable<UserExportBO> userManagementModel = from urm in db.tbUsersMsts
                                                                   select new UserExportBO
                                                                   {
                                                                       Name = urm.Name,
                                                                       EmailId = urm.EmailId,
                                                                       MobileNo = urm.MobileNo,
                                                                       DepotName = urm.DepotId,
                                                                       Username = urm.Usercode,
                                                                       UserRoleCode = urm.RoleCode,
                                                                       Status = urm.ActiveStatus == null ? 0 : (int)urm.ActiveStatus,
                                                                       RegionCode = urm.RegionCode

                                                                   };
            var result = userManagementModel.AsQueryable();

            var data = result.AsEnumerable();
            var DataList = data.ToList();
            DataList.ForEach(x => x.DepotName = ConvertDepotIdToDepotName(x.DepotName));

            data = DataList as IEnumerable<UserExportBO>;

            
            
            
            return ConvertData.ToDataTable<UserExportBO>(data.ToList());
        }

    }
}
