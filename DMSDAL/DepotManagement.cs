﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
    public class DepotManagement
    {
        /// <summary>
        /// Fetches all the records from depot table
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DepotBO> GetAllDepot()
        {
            try
            {
                DMSDbCon db = new DMSDbCon();
                var Data = from dp in db.tbDepotMsts
                           where dp.ActiveStatus==1 && dp.RegionCode!="HO"
                           select new DepotBO
                           {

                               DepotId = dp.PKDepotId,
                               DepotName = dp.DepotTitle
                           };
                return Data;
            }
            catch (Exception ex)
            {
                IEnumerable<DepotBO> data = null;
                return data;
            }
        }

        /// <summary>
        /// Fetches all the records from depot table
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DepotBO> GetAllDepot(string Username)
        {
            try
            {
                
                DMSDbCon db = new DMSDbCon();
                string Region = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault().ToString();
                string DepotList = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault().ToString();
                var Regionarray = Region.Split(',');
                if (Region.ToLower() == "all")
                {
                    return GetAllDepot();
                }
                else
                {
                    if (DepotList.ToLower() == "all")
                    {
                        var Data = from dp in db.tbDepotMsts
                                   where dp.ActiveStatus == 1 && Regionarray.Contains(dp.RegionCode)
                                   select new DepotBO
                                   {

                                       DepotId = dp.PKDepotId,
                                       DepotName = dp.DepotTitle
                                   };

                        return Data;

                    }
                    else
                    {
                     
                        var Data = from dp in db.tbDepotMsts
                                   where dp.ActiveStatus == 1 && Regionarray.Contains(dp.RegionCode) 
                                   select new DepotBO
                                   {
                                       DepotId = dp.PKDepotId,
                                       DepotName = dp.DepotTitle
                                   };

                        var search= Data.AsQueryable();
                        var result = DepotList.Split(',');
                        var list =new List<DepotBO>();
                        foreach (var item in result)
                        {
                          list.AddRange(search.Where(x => x.DepotId.ToString() == item).ToList());
                        }

                        return list as IEnumerable<DepotBO>;
                        
                    }

                }
                
            }
            catch (Exception ex)
            {
                IEnumerable<DepotBO> data = null;
                return data;
            }
        }

        public IEnumerable<DepotBO> GetAllDepots()
        {
            try
            {
                DMSDbCon db = new DMSDbCon();
                var Data = from dp in db.tbDepotMsts
                           where dp.ActiveStatus == 1 
                           select new DepotBO
                           {

                               DepotId = dp.PKDepotId,
                               DepotName = dp.DepotTitle
                           };
                return Data;
            }
            catch (Exception ex)
            {
                IEnumerable<DepotBO> data = null;
                return data;
            }
        }

        public IEnumerable<DepotList> GetAllDepotList(FilterDepots searchModel)
        {
            try
            {
                DMSDbCon db = new DMSDbCon();
                var Data = from dp in db.tbDepotMsts
                           join rg in db.tbRegionsMsts on dp.RegionCode equals rg.RegionCode
                           where  dp.RegionCode != "HO"
                           select new DepotList
                           {

                               DepotId = dp.PKDepotId,
                               DepotName = dp.DepotTitle,
                               Active = (int)dp.ActiveStatus,
                               Region = rg.RegionName,
                               RegionCode = rg.RegionCode
                           };

                var result = Data.AsQueryable();

                if (searchModel != null)
                {
                    if (!string.IsNullOrEmpty(searchModel.DepotName))
                        result = result.Where(x => x.DepotName == searchModel.DepotName.ToString());
                    if (searchModel.Active.HasValue)
                        result = result.Where(x => x.Active == searchModel.Active);
                }

                return result.AsEnumerable();
            }
            catch (Exception ex)
            {
                IEnumerable<DepotList> data = null;
                return data;
            }
        }

        public IEnumerable<DepotListAPI> GetAllDepotList(DepotParameter searchModel)
        {
            try
            {
                DMSDbCon db = new DMSDbCon();
                var Data = from dp in db.tbDepotMsts
                           join rg in db.tbRegionsMsts on dp.RegionCode equals rg.RegionCode
                           where dp.ActiveStatus == 1 && dp.RegionCode != "HO"
                           select new DepotListAPI
                           {
                               DepotName = dp.DepotTitle,
                               RegionCode = rg.RegionCode,
                                DepotId=dp.PKDepotId
                           };

                var result = Data.AsQueryable();

                if (searchModel != null)
                {
                    if (!string.IsNullOrEmpty(searchModel.RegionCode))
                        result = result.Where(x => x.RegionCode == searchModel.RegionCode.ToString());
                   
                }

                return result.AsEnumerable();
            }
            catch (Exception ex)
            {
                IEnumerable<DepotListAPI> data = null;
                return data;
            }
        }

        public DataTable ExportDepotList()
        {
            try
            {
                DMSDbCon db = new DMSDbCon();
                var Data = from dp in db.tbDepotMsts
                           join rg in db.tbRegionsMsts on dp.RegionCode equals rg.RegionCode
                           where dp.ActiveStatus == 1 && dp.RegionCode != "HO"
                           select new DepotExportBO
                           {
                               DepotTitle = dp.DepotTitle,
                               Status = (int)dp.ActiveStatus,
                               RegionCode = rg.RegionCode
                           };

                var result = Data.AsQueryable();


                return ConvertData.ToDataTable<DepotExportBO>(result.ToList());
            }
            catch (Exception ex)
            {
                DataTable data = null;
                return data;
            }
        }


        public static string GetDepotID(string depotname)
        {
            DMSDbCon db= new DMSDbCon();

            return db.tbDepotMsts.Where(x => x.DepotTitle == depotname).Select(x => x.PKDepotId).SingleOrDefault().ToString();
        }
 

    
    }
}
