﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
    public class   BankManagement
    {
        DMSDbCon db;

        public IEnumerable<BankBO> GellAllBankDetail(FilterBank searchModel)
        {
            try
            {
                db = new DMSDbCon();

                IEnumerable<BankBO> BankList = from bk in db.tbBankAccountsMsts
                                               select new BankBO
                                               {
                                                   BankName = bk.BankName,
                                                   AccountNumber = bk.AccountNumber,
                                                   BankBranch = bk.BankBranch,
                                                   RegionCodes = bk.RegionCodes,
                                                   ActiveStatus = bk.ActiveStatus
                                               };

                var result = BankList.AsQueryable();

                if (searchModel != null)
                {
                    if (!string.IsNullOrEmpty(searchModel.BankName))
                        result = result.Where(x => x.BankName == searchModel.BankName.ToString());

                    if (!string.IsNullOrEmpty(searchModel.AccountNo))
                        result = result.Where(x => x.AccountNumber == searchModel.AccountNo.ToString());

                    if (searchModel.Active.HasValue)
                        result = result.Where(x => x.ActiveStatus == searchModel.Active);
                }

                return result.AsEnumerable();
            }
            catch (Exception ex)
            {
                IEnumerable<BankBO> BankList= null;
                return BankList;
            }
        }

        public IEnumerable<BankNameBO> GellAllBankName()
        {

            try
            {
                db = new DMSDbCon();
                IEnumerable<BankNameBO> BankList = from bk in db.tbBankAccountsMsts
                                                   select new BankNameBO
                                               {
                                                   BankName = bk.BankName,
                                                   BankId = bk.PKBankId
                                               };
                return BankList;
            }
            catch (Exception ex)
            {
                IEnumerable<BankNameBO> BankList=null;

                return BankList;

            }
        }

        public IEnumerable<BankAccountBO> GellAllBankAcc()
        {
            try
            {
                db = new DMSDbCon();
                IEnumerable<BankAccountBO> BankList = from bk in db.tbBankAccountsMsts
                                                      select new BankAccountBO
                                                   {
                                                       AccountNo = bk.AccountNumber
                                                   };
                return BankList;
            }
            catch (Exception ex)
            {
                IEnumerable<BankAccountBO> BankList = null;
                return BankList;
            }
        }

        public DataTable GetBankExport()
        {
            try
            {
                db = new DMSDbCon();

                IEnumerable<BankExportBO> BankList = from bk in db.tbBankAccountsMsts
                                                     select new BankExportBO
                                               {
                                                   BankName = bk.BankName,
                                                   AccountNumber = bk.AccountNumber,
                                                   BankBranch = bk.BankBranch,
                                                   RegionCode = bk.RegionCodes,
                                                   Status = (bk.ActiveStatus == null) ? 0 : (int)bk.ActiveStatus
                                               };

                var result = BankList.AsQueryable();

                //if (searchModel != null)
                //{
                //    if (!string.IsNullOrEmpty(searchModel.BankName))
                //        result = result.Where(x => x.BankName == searchModel.BankName.ToString());

                //    if (!string.IsNullOrEmpty(searchModel.AccountNo))
                //        result = result.Where(x => x.AccountNumber == searchModel.AccountNo.ToString());

                //    if (searchModel.Active.HasValue)
                //        result = result.Where(x => x.Status == searchModel.Active);
                //}

                BankList = result.AsEnumerable();

                return ConvertData.ToDataTable<BankExportBO>(BankList.ToList());
            }
            catch (Exception ex)
            {
                
                return new DataTable();
            }
        }

    
    }

  
}
