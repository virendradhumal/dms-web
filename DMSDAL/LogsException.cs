﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
  public class LogsException
  {
    public static void LogError(Exception ex, string serverpath)
    {
      string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
      message += Environment.NewLine;
      message += "-----------------------------------------------------------";
      message += Environment.NewLine;
      message += string.Format("Message: {0}", ex.Message);
      message += Environment.NewLine;
      message += string.Format("StackTrace: {0}", ex.StackTrace);
      message += Environment.NewLine;
      message += string.Format("Source: {0}", ex.Source);
      message += Environment.NewLine;
      message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
      message += Environment.NewLine;
      message += "-----------------------------------------------------------";
      message += Environment.NewLine;

      if (ex.InnerException != null)
      {
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex.InnerException.Message);
        message += Environment.NewLine;
        message += string.Format("StackTrace: {0}", ex.InnerException.StackTrace);
        message += Environment.NewLine;
        message += string.Format("Source: {0}", ex.InnerException.Source);
        message += Environment.NewLine;
        message += string.Format("TargetSite: {0}", ex.InnerException.TargetSite.ToString());
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;

        if (ex.InnerException.InnerException != null)
        {
          message += Environment.NewLine;
          message += "-----------------------------------------------------------";
          message += Environment.NewLine;
          message += string.Format("Message: {0}", ex.InnerException.InnerException.Message);
          message += Environment.NewLine;
          message += string.Format("StackTrace: {0}", ex.InnerException.InnerException.StackTrace);
          message += Environment.NewLine;
          message += string.Format("Source: {0}", ex.InnerException.InnerException.Source);
          message += Environment.NewLine;
          message += string.Format("TargetSite: {0}", ex.InnerException.InnerException.TargetSite.ToString());
          message += Environment.NewLine;
          message += "-----------------------------------------------------------";
          message += Environment.NewLine;
        }
      }      

      string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/ErrorLog.txt";
      using (StreamWriter writer = new StreamWriter(path, true))
      {
        writer.WriteLine(message);
        writer.Close();
      }
    }
  }
}
