﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace DMSDAL
{
    public class IDNManagement
    {

        
        DMSDbCon db;

        public StatusMessage CreateIDN(CreateIDNBO createIDNBO, string Username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {

                db = new DMSDbCon();
                int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
                int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();
                int IDNExists = db.tbIDNMsts.Where(x => x.IDNNo == createIDNBO.IDNNo).Count();

                if (IDNExists > 0)
                {
                    statusMessage.Message = "You are trying to create duplicate IDN.";
                    statusMessage.Status = false;
                    return statusMessage;
                }


                string imagename = String.Format("Data\\IDN\\{0}-{1}-{2}", HOID, createIDNBO.IDNNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(createIDNBO.IDNPhoto.FileName);
                tbIDNMst record = new tbIDNMst();
                record.SourceId = HOID;
                record.DestinationId = createIDNBO.DepotId;
                record.IDNDate = DateTime.Now;
                record.IDNNo = createIDNBO.IDNNo;
                record.IDNImage = imagename;
                record.StatusID = sid;
                record.CreatedBy = Username;
                record.CreatedOn = DateTime.Now;
                db.tbIDNMsts.Add(record);
                db.SaveChanges();

                string savepath = createIDNBO.Pathname + imagename;
                createIDNBO.IDNPhoto.SaveAs(savepath);

                statusMessage.Message = record.PKIDNId.ToString();
                statusMessage.Status = true;

            }
            catch (Exception ex)
            {

                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;

            }
            return statusMessage;
        }

        public StatusMessage UpdateIDNPhoto(CreateIDNBO createIDNBO, string Username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {

                db = new DMSDbCon();
               
                tbIDNMst record = db.tbIDNMsts.Find(createIDNBO.IDNId);
                string imagename = String.Format("Data\\IDN\\{0}-{1}-{2}", record.SourceId, createIDNBO.IDNNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(createIDNBO.IDNPhoto.FileName);
                record.IDNImage = imagename;
                db.Entry(record).State=EntityState.Modified;
                db.SaveChanges();
                string savepath = createIDNBO.Pathname + imagename;
                createIDNBO.IDNPhoto.SaveAs(savepath);
                statusMessage.Message = record.PKIDNId.ToString();
                statusMessage.Status = true;
                UpdateStatus(createIDNBO.IDNId, Username);
            }
            catch (Exception ex)
            {

                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;

            }
            return statusMessage;
        }

        public StatusMessage CreateIDNOnPurchaseOrder(CreateIDNBO createIDNBO, string Username)
        {
            StatusMessage statusMessage = new StatusMessage();
            db = new DMSDbCon();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
                    int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();
                    int IDNExists = db.tbIDNMsts.Where(x => x.IDNNo == createIDNBO.IDNNo).Count();

                    if (IDNExists > 0)
                    {
                        statusMessage.Message = "You are trying to create duplicate IDN.";
                        statusMessage.Status = false;
                        return statusMessage;
                    }
                    
                    string imagename = String.Format("Data\\IDN\\{0}-{1}-{2}", HOID, createIDNBO.IDNNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(createIDNBO.IDNPhoto.FileName);

                    PurchaseOrders pobj = new PurchaseOrders();
                    var item = pobj.GetSpecificOrder(createIDNBO.OrderId);
                    var item2 = pobj.GetOrderDetails(createIDNBO.OrderId);


                    tbIDNMst record = new tbIDNMst();
                    record.SourceId = HOID;
                    record.DestinationId = item.DepotId;
                    record.IDNDate = DateTime.Now;
                    record.IDNNo = createIDNBO.IDNNo;
                    record.IDNImage = imagename;
                    record.StatusID = sid;
                    record.IDNAmount = item.OrderAmount;
                    record.CreatedBy = Username;
                    record.CreatedOn = DateTime.Now;
                    record.FKPurchaseOrderId = createIDNBO.OrderId;
                    db.tbIDNMsts.Add(record);
                    db.SaveChanges();


                    string savepath = createIDNBO.Pathname + imagename;
                    createIDNBO.IDNPhoto.SaveAs(savepath);


                    foreach (var order in item2)
                    {
                        tbIDNDtl dtlrecord = new tbIDNDtl();
                        dtlrecord.Amount = order.total;
                        dtlrecord.CreatedBy = Username;
                        dtlrecord.CreatedOn = DateTime.Now;
                        dtlrecord.SourceQuantity = order.quantity;
                        dtlrecord.FKIDNId = record.PKIDNId;
                        dtlrecord.ProductCode = order.ProductCode;
                        dtlrecord.Rate = order.rate;
                        dtlrecord.TaxValue = order.taxamount;
                        db.tbIDNDtls.Add(dtlrecord);
                        db.SaveChanges();
                    }


                    int POId = createIDNBO.OrderId;
                    Account account = new Account();
                    string role = account.GetUserRole(Username);
                    sid = db.tbStatusMsts.Where(x => x.StatusGroup.ToLower() == "processing").Select(x => x.StatusId).SingleOrDefault();
                    tbPurchaseOrderStatusDtl tbdSRDtl = new tbPurchaseOrderStatusDtl();
                    tbdSRDtl.Comments = "";
                    tbdSRDtl.CreatedBy = Username;
                    tbdSRDtl.CreatedOn = DateTime.Now;
                    tbdSRDtl.FKStatusId = sid;
                    tbdSRDtl.FKPurchaseOrderID = POId;
                    db.tbPurchaseOrderStatusDtls.Add(tbdSRDtl);
                    tbPurchaseOrdersMst tbdSRMst = db.tbPurchaseOrdersMsts.Find(POId);
                    tbdSRMst.FKStatusId = sid;
                    db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    transaction.Commit();
                    statusMessage.Message = record.PKIDNId.ToString();
                    statusMessage.Status = true;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    statusMessage.Message = "Please try again.";
                    statusMessage.Status = false;

                }
            }
            return statusMessage;
        }

        public IDNAddProductBO GetIDNData(int IDNId)
        {
            try
            {
                db = new DMSDbCon();

                IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();

                string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();
                iDNAddProductBO = (from idm in db.tbIDNMsts
                                   join dp in db.tbDepotMsts on idm.DestinationId equals dp.PKDepotId
                                   join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                                   where idm.PKIDNId == IDNId
                                   select new IDNAddProductBO
                                       {
                                           IDNId = idm.PKIDNId,
                                           IDNNo = idm.IDNNo,
                                           Path = urlpath + idm.IDNImage,
                                           DepotName = dp.DepotTitle,
                                           DepotId = dp.PKDepotId,
                                            Status=st.StatusGroup,
                                           TotalIDNAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                                           
                                       }).SingleOrDefault();
                if (iDNAddProductBO == null)
                {
                    iDNAddProductBO = new IDNAddProductBO();
                }



                iDNAddProductBO.productIDNBO =
                                               from idm in db.tbIDNMsts
                                               join idd in db.tbIDNDtls on idm.PKIDNId equals idd.FKIDNId
                                               join dp in db.tbDepotMsts on idm.SourceId equals dp.PKDepotId
                                               join pMst in db.tbProductsMsts on idd.ProductCode equals pMst.ProductCode
                                               where idm.PKIDNId == IDNId
                                               select new ProductIDNBO
                                                   {
                                                       IDNDetailId=idd.PKIDNDetailsId,
                                                       IDNDId=(int)idd.FKIDNId,
                                                       DepotName = dp.DepotTitle,
                                                       ProductCode = idd.ProductCode,
                                                       ProductName = pMst.ProductName,
                                                       Quantity = (idd.SourceQuantity == null) ? 0 : (int)idd.SourceQuantity,
                                                       Rate = (idd.Rate == null) ? 0 : (int)idd.Rate,
                                                       Total = (idd.Amount == null) ? 0 : (decimal)idd.Amount
                                                   };




                return iDNAddProductBO;

            }
            catch (Exception ex)
            {

                IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();
                iDNAddProductBO.productIDNBO = null;
                return iDNAddProductBO;
            }


        }

        public IEnumerable<IDNBO> GetAllIDNData(FilterIDN searchModel, string Username)
        {
            db = new DMSDbCon();

            IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();

            string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();


            string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();
            string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();
            string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();

            int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
            searchModel.SourceDepotId = HOID;


            if (depotlist == null)
            {
                depotlist = "";
            }
            if (Regionlist == null)
            {
                Regionlist = "";
            }
            string[] arraydepotid = depotlist.Split(',');
            string[] arrayregionid = Regionlist.Split(',');

            IEnumerable<IDNBO> data;
            if (RoleCode != "DOCLK" && RoleCode != "DOMGR")
            {
                data = (from idm in db.tbIDNMsts
                        join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                        from goods in grrInfo.DefaultIfEmpty()
                        join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                        join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                        join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                        select new IDNBO
                        {
                            IDNId = idm.PKIDNId,
                            IDNNo = idm.IDNNo,
                            Path = urlpath + idm.IDNImage,
                            SourceDepot = sdp.DepotTitle,
                            DestinationDepot = ddp.DepotTitle,
                            GRRNo = goods.GRRNo,
                            GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                            SourceDepotId = sdp.PKDepotId,
                            DestinationDepotId = ddp.PKDepotId,
                            IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                            IDNDate = (DateTime)idm.IDNDate,
                            Status = st.Status,
                            StatusGroup = st.StatusGroup
                        });

            }
            else
            {

                if (Regionlist.ToLower().ToString() == "all")
                {
                    data = (from idm in db.tbIDNMsts
                            join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                            from goods in grrInfo.DefaultIfEmpty()
                            join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                            join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                            join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                            select new IDNBO
                            {
                                IDNId = idm.PKIDNId,
                                IDNNo = idm.IDNNo,
                                Path = urlpath + idm.IDNImage,
                                SourceDepot = sdp.DepotTitle,
                                DestinationDepot = ddp.DepotTitle,
                                GRRNo = goods.GRRNo,
                                GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                                SourceDepotId = sdp.PKDepotId,
                                DestinationDepotId = ddp.PKDepotId,
                                IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                                IDNDate = (DateTime)idm.IDNDate,
                                Status = st.Status,
                                StatusGroup = st.StatusGroup
                            });

                }
                else
                {

                    if (depotlist.ToLower().ToString() == "all")
                    {
                        data = (from idm in db.tbIDNMsts
                                join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                                from goods in grrInfo.DefaultIfEmpty()
                                join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                                join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                                join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                                where arrayregionid.Contains(sdp.RegionCode.ToString()) || sdp.RegionCode.ToLower() == "ho"
                                select new IDNBO
                                {
                                    IDNId = idm.PKIDNId,
                                    IDNNo = idm.IDNNo,
                                    Path = urlpath + idm.IDNImage,
                                    SourceDepot = sdp.DepotTitle,
                                    DestinationDepot = ddp.DepotTitle,
                                    GRRNo = goods.GRRNo,
                                    GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                                    SourceDepotId = sdp.PKDepotId,
                                    DestinationDepotId = ddp.PKDepotId,
                                    IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                                    IDNDate = (DateTime)idm.IDNDate,
                                    Status = st.Status,
                                    StatusGroup = st.StatusGroup
                                });

                    }
                    else
                    {
                        data = (from idm in db.tbIDNMsts
                                join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                                from goods in grrInfo.DefaultIfEmpty()
                                join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                                join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                                join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                                where arraydepotid.Contains(sdp.PKDepotId.ToString()) || sdp.RegionCode.ToLower() == "ho"
                                select new IDNBO
                                {
                                    IDNId = idm.PKIDNId,
                                    IDNNo = idm.IDNNo,
                                    Path = urlpath + idm.IDNImage,
                                    SourceDepot = sdp.DepotTitle,
                                    DestinationDepot = ddp.DepotTitle,
                                    GRRNo = goods.GRRNo,
                                    GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                                    SourceDepotId = sdp.PKDepotId,
                                    DestinationDepotId = ddp.PKDepotId,
                                    IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                                    IDNDate = (DateTime)idm.IDNDate,
                                    Status = st.Status,
                                    StatusGroup = st.StatusGroup
                                });

                    }
                }

            }
            var result = data.AsQueryable();

            if (searchModel != null)
            {

                if (searchModel.SourceDepotId.HasValue)
                    result = result.Where(x => x.SourceDepotId == searchModel.SourceDepotId);
                if (searchModel.DestinationDepotId.HasValue)
                    result = result.Where(x => x.DestinationDepotId == searchModel.DestinationDepotId);
                if (searchModel.FromDate.HasValue)
                    result = result.Where(x => DbFunctions.TruncateTime(x.IDNDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                if (searchModel.ToDate.HasValue)
                    result = result.Where(x => DbFunctions.TruncateTime(x.IDNDate) <= DbFunctions.TruncateTime(searchModel.ToDate));
                if (!String.IsNullOrEmpty(searchModel.Status))
                {
                    if (searchModel.Status.ToLower() == "completed")
                    {
                        result = result.Where(x => x.StatusGroup.ToLower() == "approved");
                    }
                    else
                    {

                        result = result.Where(x => x.StatusGroup.ToLower() == searchModel.Status.ToLower());
                    }
                }
            }

            var check = data.ToList();
            data = result.AsEnumerable();
            return data;
        }

        public ListIDNAPI GetAllIDNData(DSRListParameter dSRListParameter)
        {

            ListIDNAPI result = new ListIDNAPI();
            try
            {
                db = new DMSDbCon();

                IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();

                DateTime currentdate = DateTime.Now;
                //dSRListParameter.NoOfDays = dSRListParameter.NoOfDays - 1;
                //currentdate = currentdate.AddDays(-dSRListParameter.NoOfDays);


                string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();
                IEnumerable<IDNAPIBO> data = (from idm in db.tbIDNMsts
                                              join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                                              join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                                              join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                                              where idm.SourceId == dSRListParameter.DepotId //&& DbFunctions.TruncateTime(idm.IDNDate) >= DbFunctions.TruncateTime(currentdate)
                                              select new IDNAPIBO
                                           {
                                               PKIDNId = idm.PKIDNId,
                                               FKPurchaseOrderId=idm.FKPurchaseOrderId==null?0:(int)idm.FKPurchaseOrderId,
                                               IDNNo = idm.IDNNo,   
                                               IDNImage = urlpath + idm.IDNImage,
                                               SourceDepot = sdp.DepotTitle,
                                               DestinationDepot = ddp.DepotTitle,
                                               SourceDepotId = sdp.PKDepotId,
                                               DestinationDepotId = ddp.PKDepotId,
                                               IDNAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                                               IDNDate = idm.IDNDate.ToString(),
                                               Status = st.Status,
                                               StatusGroup = st.StatusGroup
                                           });
                var listdata = data.ToList();

                listdata.ForEach(x => x.IDNDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(x.IDNDate)));
                data = listdata as IEnumerable<IDNAPIBO>;
                result.IDNList = data;
            }
            catch (Exception ex)
            {
                result = null;
            }

            return result;
        }

        public GetIDNAPIBO GetIDNData(string IDNNo)
        {
            try
            {
                db = new DMSDbCon();

                GetIDNAPIBO getIDNAPIBO = new GetIDNAPIBO();

                string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();

                getIDNAPIBO = (from idm in db.tbIDNMsts
                               join dp in db.tbDepotMsts on idm.DestinationId equals dp.PKDepotId
                               join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                               where idm.IDNNo == IDNNo
                               select new GetIDNAPIBO
                               {
                                   PKIDNId = idm.PKIDNId,
                                   FKPurchaseOrderId=idm.FKPurchaseOrderId==null?0:(int)idm.FKPurchaseOrderId,
                                   IDNNo = idm.IDNNo,
                                   IDNImage = urlpath + idm.IDNImage,
                                   DestinationDepot = dp.DepotTitle,
                                   IDNDate = idm.IDNDate == null ? "" : idm.IDNDate.ToString(),
                                   Status = st.Status,
                                   DepotId=idm.DestinationId==null?0:(int)idm.DestinationId,
                                   SourceId=idm.SourceId==null?0:(int)idm.SourceId,
                                   DestinationId=idm.DestinationId==null?0:(int)idm.DestinationId,
                                   VehicleNo=idm.VehicleNo,
                                   StatusGroup = st.StatusGroup,
                                   IDNAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount
                               }).SingleOrDefault();



                if (getIDNAPIBO == null)
                {
                    getIDNAPIBO = new GetIDNAPIBO();

                }
                else
                {
                   if (getIDNAPIBO.IDNDate != "")
                        getIDNAPIBO.IDNDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(getIDNAPIBO.IDNDate));
                }
                getIDNAPIBO.IDNProducts = from idm in db.tbIDNMsts
                                          join idd in db.tbIDNDtls on idm.PKIDNId equals idd.FKIDNId
                                          join pMst in db.tbProductsMsts on idd.ProductCode equals pMst.ProductCode
                                          join dp in db.tbDepotMsts on idm.SourceId equals dp.PKDepotId
                                          where idm.PKIDNId == getIDNAPIBO.PKIDNId

                                          select new ProductIDNAPIBO
                                         {

                                             ProductCode = idd.ProductCode,
                                             Description = pMst.ProductName,
                                             ExcessQuantity = 0,
                                             SourceQuantity = (idd.SourceQuantity == null) ? 0 : (int)idd.SourceQuantity,
                                             ShortageQuantity = 0,
                                             ReceivedQuantity = 0,
                                             Rate = (idd.Rate == null) ? 0 : (int)idd.Rate,
                                             Total = (idd.Amount == null) ? 0 : (decimal)idd.Amount
                                         };



                return getIDNAPIBO;

            }
            catch (Exception ex)
            {

                GetIDNAPIBO getIDNAPIBO = null;
                return getIDNAPIBO;
            }


        }

        public StatusMessage CreateIDN(CreateAPIIDNBO createIDNBO)
        {
            StatusMessage statusMessage = new StatusMessage();

            db = new DMSDbCon();

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();
                    int IDNExists = db.tbIDNMsts.Where(x => x.IDNNo == createIDNBO.IDNNo).Count();

                    if (IDNExists > 0)
                    {
                        statusMessage.Message = "You are trying to create duplicate IDN.";
                        statusMessage.Status = false;
                        return statusMessage;
                    }

                    tbIDNMst record = new tbIDNMst();
                    record.SourceId = createIDNBO.DepotId;
                    record.DestinationId = createIDNBO.DestinationId;
                    record.IDNDate = DateTime.Now;
                    record.IDNNo = createIDNBO.IDNNo;
                    record.IDNImage = createIDNBO.IDNImage;
                    record.IDNAmount = createIDNBO.IDNAmount;
                    record.CreatedBy = createIDNBO.Username;
                    record.CreatedOn = DateTime.Now;
                    record.VehicleNo = createIDNBO.VehicleNo;
                    record.StatusID = sid;
                    record.FKPurchaseOrderId =0;
                    db.tbIDNMsts.Add(record);
                    db.SaveChanges();

                    statusMessage.Message = record.PKIDNId.ToString();
                    statusMessage.Status = true;

                    foreach (var item in createIDNBO.IDNProducts)
                    {

                        db = new DMSDbCon();
                        tbIDNDtl records = new tbIDNDtl();
                        records.Amount = item.Total;
                        records.CreatedBy = createIDNBO.Username;
                        records.CreatedOn = DateTime.Now;
                        records.FKIDNId = record.PKIDNId;
                        records.ModifiedBy = createIDNBO.Username;
                        records.ModifiedOn = DateTime.Now;
                        records.ProductCode = item.ProductCode;
                        records.SourceQuantity = item.SourceQuantity;
                        records.Rate = item.Rate;
                        records.TaxValue = 0;
                        db.tbIDNDtls.Add(records);
                        db.SaveChanges();


                        tbProductStockMst stock = db.tbProductStockMsts.Where(x => x.FKDepotId == record.SourceId && x.ProductCode == records.ProductCode).FirstOrDefault();
                        stock.StockQuantity = stock.StockQuantity - records.SourceQuantity;
                        db.Entry(stock).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    statusMessage.Status = true;
                    statusMessage.Message = "Product successfully added";
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    statusMessage.Message = "Please try again.";
                    statusMessage.Status = false;
                    transaction.Rollback();
                }
            }
            return statusMessage;
        }

        public StatusMessage EditIDNAPI(CreateAPIIDNBO createIDNBO)
        {
            StatusMessage statusMessage = new StatusMessage();

            db = new DMSDbCon();

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    int sid = db.tbStatusMsts.Where(x => x.Role=="DPCLK" && x.StatusGroup == "Edited").Select(x => x.StatusId).SingleOrDefault();
                    int IDNExists = db.tbIDNMsts.Where(x => x.IDNNo == createIDNBO.IDNNo).Count();

                    if (IDNExists == 0)
                    {
                        statusMessage.Message = "Wrong IDN Number.";
                        statusMessage.Status = false;
                        return statusMessage;
                    }

                    int idnid = db.tbIDNMsts.Where(x => x.IDNNo == createIDNBO.IDNNo).Select(x => x.PKIDNId).SingleOrDefault();

                    tbIDNMst record = db.tbIDNMsts.Find(idnid);
                    record.SourceId = createIDNBO.DepotId;
                    record.DestinationId = createIDNBO.DestinationId;
                    record.IDNDate = DateTime.Now;
                    record.IDNImage = createIDNBO.IDNImage;
                    record.IDNAmount = createIDNBO.IDNAmount;
                    record.CreatedBy = createIDNBO.Username;
                    record.CreatedOn = DateTime.Now;
                    record.VehicleNo = createIDNBO.VehicleNo;
                    record.StatusID = sid;
                    record.FKPurchaseOrderId = 0;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();

                    statusMessage.Message = record.PKIDNId.ToString();
                    statusMessage.Status = true;
                    IEnumerable<tbIDNDtl> todelete = db.tbIDNDtls.Where(x => x.FKIDNId == idnid);

                    foreach (var item in todelete)
                    {
                        tbProductStockMst stock = db.tbProductStockMsts.Where(x => x.FKDepotId == record.SourceId && x.ProductCode == item.ProductCode).FirstOrDefault();
                        stock.StockQuantity = stock.StockQuantity +item.SourceQuantity;
                        db.Entry(stock).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    db.tbIDNDtls.RemoveRange(todelete);
                    db.SaveChanges();


                    foreach (var item in createIDNBO.IDNProducts)
                    {

                    //    db = new DMSDbCon();
                        tbIDNDtl records = new tbIDNDtl();
                        records.Amount = item.Total;
                        records.CreatedBy = createIDNBO.Username;
                        records.CreatedOn = DateTime.Now;
                        records.FKIDNId = record.PKIDNId;
                        records.ModifiedBy = createIDNBO.Username;
                        records.ModifiedOn = DateTime.Now;
                        records.ProductCode = item.ProductCode;
                        records.SourceQuantity = item.SourceQuantity;
                        records.Rate = item.Rate;
                        records.TaxValue = 0;
                        db.tbIDNDtls.Add(records);
                        db.SaveChanges();


                        tbProductStockMst stock = db.tbProductStockMsts.Where(x => x.FKDepotId == record.SourceId && x.ProductCode == records.ProductCode).FirstOrDefault();
                        stock.StockQuantity = stock.StockQuantity - records.SourceQuantity;
                        db.Entry(stock).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    statusMessage.Status = true;
                    statusMessage.Message = "Product successfully added";
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    statusMessage.Message = "Please try again.";
                    statusMessage.Status = false;
                    transaction.Rollback();
                }
            }
            return statusMessage;
        }

        public StatusMessage DeleteIDNProduct(int? IDNDetailId, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                
                tbIDNDtl record = db.tbIDNDtls.Find(IDNDetailId);
                record.ModifiedOn= DateTime.Now;
                record.ModifiedBy=username;
                db.Entry(record).State = EntityState.Modified;
                db.SaveChanges();
                
                tbIDNMst record1 =db.tbIDNMsts.Find(record.FKIDNId);
                record1.IDNAmount-=record.Amount;
                db.SaveChanges();

                db.tbIDNDtls.Remove(record);
                db.SaveChanges();

                statusMessage.Message = "Product Deleted Successfully";
                statusMessage.Status = true;
                UpdateStatus(record.FKIDNId, username);
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Product not deleted successfuly";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage UpdateIDNProduct(int? Odid, int? Quantity, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
               
                tbIDNDtl record = db.tbIDNDtls.Find(Odid);
                tbIDNMst record1 = db.tbIDNMsts.Find(record.FKIDNId);
                record.SourceQuantity = Quantity;
                record1.IDNAmount -= record.Amount;
                record.Amount = Quantity * record.Rate;
                record1.IDNAmount += record.Amount;
                record.ModifiedBy = username;
                record.ModifiedOn = DateTime.Now;
                db.Entry(record).State = EntityState.Modified;

                db.Entry(record1).State = EntityState.Modified;
                db.SaveChanges();

                statusMessage.Message = "Product detail updated successfuly";
                statusMessage.Status = true;
                UpdateStatus(record.FKIDNId, username);
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Product detail not updated successfuly";
                statusMessage.Status = false;
            }

            return statusMessage;
        }


        public StatusMessage UpdateStatus(int? IDNId, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                  Account account = new Account();
                string role = account.GetUserRole(username);
                int sid = db.tbStatusMsts.Where(x => x.Status.ToLower() == "edited by " + role.ToLower()).Select(x => x.StatusId).SingleOrDefault();
                
                tbIDNMst record = db.tbIDNMsts.Find(IDNId);

                if (record.StatusID != sid)
                {
                    record.StatusID = sid;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                }
                statusMessage.Message = "IDN status Changed Succefully.";
                statusMessage.Status = true;
            }
            catch (Exception ex)
            {
                statusMessage.Message = "IDN status not changed";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage ApproveIDN(int IDNId, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);

                var query = (from dsr in db.tbIDNMsts
                             join st in db.tbStatusMsts on dsr.StatusID equals st.StatusId
                             where dsr.PKIDNId == IDNId
                             select new { st.StatusGroup }).SingleOrDefault();

                string status = query.StatusGroup;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "confirm").Select(x => x.StatusId).SingleOrDefault();
                
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "edited" || status.ToLower() == "rejected")
                    {
                    //    tbDSRDtl tbdSRDtl = new tbDSRDtl();
                    //    tbdSRDtl.Comments = "";
                    //    tbdSRDtl.CreatedBy = username;
                    //    tbdSRDtl.CreatedOn = DateTime.Now;
                    //    tbdSRDtl.FKStatusId = sid;
                    //    tbdSRDtl.FKDSRID = DsrId;
                    //    db.tbDSRDtls.Add(tbdSRDtl);
                      
                        tbIDNMst tbiDNMst = db.tbIDNMsts.Find(IDNId);
                        tbiDNMst.StatusID = sid;
                        db.Entry(tbiDNMst).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The IDN is approved sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is not needed approvel";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid IDN ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage RejectIDN(int IDNId, string message, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);
                var query = (from dsr in db.tbIDNMsts
                             join st in db.tbStatusMsts on dsr.StatusID equals st.StatusId
                             where dsr.PKIDNId == IDNId
                             select new { st.StatusGroup }).SingleOrDefault();
                
                string status = query.StatusGroup;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "edited" || status.ToLower() == "rejected")
                    {
                        //tbDSRDtl tbdSRDtl = new tbDSRDtl();
                        //tbdSRDtl.Comments = message;
                        //tbdSRDtl.CreatedBy = username;
                        //tbdSRDtl.CreatedOn = DateTime.Now;
                        //tbdSRDtl.FKStatusId = sid;
                        //tbdSRDtl.FKDSRID = DsrId;
                        //db.tbDSRDtls.Add(tbdSRDtl);
                        tbIDNMst tbiDNMst = db.tbIDNMsts.Find(IDNId);
                        tbiDNMst.StatusID = sid;
                        db.Entry(tbiDNMst).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The IDN is Rejected sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is cannot be rejected";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid IDN ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }



        public StatusMessage ApproveIDN(string IDNNo, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);

                var query = (from dsr in db.tbIDNMsts
                             join st in db.tbStatusMsts on dsr.StatusID equals st.StatusId
                             where dsr.IDNNo == IDNNo
                             select new { st.StatusGroup }).SingleOrDefault();


                var query2 = (from dsr in db.tbIDNMsts
                              where dsr.IDNNo == IDNNo
                             select new { dsr.PKIDNId }).SingleOrDefault();

                
                string status = query.StatusGroup;


                int IDNId = query2.PKIDNId;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "confirm").Select(x => x.StatusId).SingleOrDefault();

                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "edited" || status.ToLower() == "rejected")
                    {
                        //    tbDSRDtl tbdSRDtl = new tbDSRDtl();
                        //    tbdSRDtl.Comments = "";
                        //    tbdSRDtl.CreatedBy = username;
                        //    tbdSRDtl.CreatedOn = DateTime.Now;
                        //    tbdSRDtl.FKStatusId = sid;
                        //    tbdSRDtl.FKDSRID = DsrId;
                        //    db.tbDSRDtls.Add(tbdSRDtl);

                        tbIDNMst tbiDNMst = db.tbIDNMsts.Find(IDNId);
                        tbiDNMst.StatusID = sid;
                        db.Entry(tbiDNMst).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The IDN is approved sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is not needed approvel";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid IDN ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage RejectIDN(string IDNNo, string message, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);

                var query = (from dsr in db.tbIDNMsts
                             join st in db.tbStatusMsts on dsr.StatusID equals st.StatusId
                             where dsr.IDNNo == IDNNo
                             select new { st.StatusGroup }).SingleOrDefault();


                var query2 = (from dsr in db.tbIDNMsts
                              where dsr.IDNNo == IDNNo
                              select new { dsr.PKIDNId }).SingleOrDefault();


                string status = query.StatusGroup;


                int IDNId = query2.PKIDNId;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "edited" || status.ToLower() == "rejected")
                    {
                        //tbDSRDtl tbdSRDtl = new tbDSRDtl();
                        //tbdSRDtl.Comments = message;
                        //tbdSRDtl.CreatedBy = username;
                        //tbdSRDtl.CreatedOn = DateTime.Now;
                        //tbdSRDtl.FKStatusId = sid;
                        //tbdSRDtl.FKDSRID = DsrId;
                        //db.tbDSRDtls.Add(tbdSRDtl);
                        tbIDNMst tbiDNMst = db.tbIDNMsts.Find(IDNId);
                        tbiDNMst.StatusID = sid;
                        db.Entry(tbiDNMst).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The IDN is Rejected sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is cannot be rejected";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid IDN ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }



    }
}