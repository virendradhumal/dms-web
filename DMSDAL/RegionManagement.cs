﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{

    public class RegionManagement
    {
        DMSDbCon db;


        public IEnumerable<RegionBO> GetAllRegion()
        {
            try
            {
                db = new DMSDbCon();

                IEnumerable<RegionBO> regions = from rg in db.tbRegionsMsts
                                                where rg.ActiveStatus==1 && rg.RegionCode!="HO"
                                                select new RegionBO
                                                {
                                                    RegionName = rg.RegionName,
                                                    RegionCode = rg.RegionCode
                                                };
                return regions;
            }
            catch (Exception ex)
            {
                IEnumerable<RegionBO> regions = null;
                return regions;

            }

        }


        public IEnumerable<RegionBO> GetAllRegion(string Username)
        {
            try
            {
                db = new DMSDbCon();

                string Region = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault().ToString();
                string []Regionarray = Region.Split(',');
                if (Region.ToLower() == "all")
                {
                    IEnumerable<RegionBO> regions = from rg in db.tbRegionsMsts
                                                    where rg.ActiveStatus == 1 && rg.RegionCode != "HO"
                                                    select new RegionBO
                                                    {
                                                        RegionName = rg.RegionName,
                                                        RegionCode = rg.RegionCode
                                                    };
                    return regions;
                }
                else
                   {
                        IEnumerable<RegionBO> regions = from rg in db.tbRegionsMsts
                                                        where rg.ActiveStatus == 1 && Regionarray.Contains(rg.RegionCode) && rg.RegionCode != "HO"
                                                        select new RegionBO
                                                        {
                                                            RegionName = rg.RegionName,
                                                            RegionCode = rg.RegionCode
                                                        };
                        return regions;
                    }
            }
            catch (Exception ex)
            {
                IEnumerable<RegionBO> regions = null;
                return regions;

            }

        }


    }
}
