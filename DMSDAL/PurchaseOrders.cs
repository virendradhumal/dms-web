﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSDAL
{
    public class PurchaseOrders
    {
        DMSDbCon db;

        public IEnumerable<OrderManageModal> GetAllOrder(FilterPOrder searchModel, string Username)
        {
            try
            {
                db = new DMSDbCon();

                IEnumerable<OrderManageModal> orderManageModal;
                string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();
                string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();
                string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();

                if (depotlist == null)
                {
                    depotlist = "";
                }
                if (Regionlist == null)
                {
                    Regionlist = "";
                }
                string[] arraydepotid = depotlist.Split(',');
                string[] arrayregionid = Regionlist.Split(',');


                if (RoleCode != "DOCLK" && RoleCode != "DOMGR")
                {
                    orderManageModal = from orm in db.tbPurchaseOrdersMsts
                                       join st in db.tbStatusMsts on orm.FKStatusId equals st.StatusId
                                       join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                       select new OrderManageModal
                                       {

                                           OrderId = orm.PKPurchaseOrderId,
                                           OrderNo = orm.OrderNo,
                                           DepotId = dpo.PKDepotId,
                                           DepoName = dpo.DepotTitle,
                                           OrderAmount = (decimal)orm.OrderAmount,
                                           OrderDate = (DateTime)orm.OrderDate,
                                           Status = st.Status,
                                           StatusGroup = st.StatusGroup

                                       };

                }
                else
                {

                    if (Regionlist.ToLower().ToString() == "all")
                    {
                        orderManageModal = from orm in db.tbPurchaseOrdersMsts
                                           join st in db.tbStatusMsts on orm.FKStatusId equals st.StatusId
                                           join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                           select new OrderManageModal
                                           {

                                               OrderId = orm.PKPurchaseOrderId,
                                               OrderNo = orm.OrderNo,
                                               DepotId = dpo.PKDepotId,
                                               DepoName = dpo.DepotTitle,
                                               OrderAmount = (decimal)orm.OrderAmount,
                                               OrderDate = (DateTime)orm.OrderDate,
                                               Status = st.Status,
                                               StatusGroup = st.StatusGroup

                                           };

                    }
                    else
                    {

                        if (depotlist.ToLower().ToString() == "all")
                        {
                            orderManageModal = from orm in db.tbPurchaseOrdersMsts
                                               join st in db.tbStatusMsts on orm.FKStatusId equals st.StatusId
                                               join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                               join rg in db.tbRegionsMsts on dpo.RegionCode equals rg.RegionCode
                                               where arrayregionid.Contains(rg.RegionCode.ToString())
                                               select new OrderManageModal
                                               {

                                                   OrderId = orm.PKPurchaseOrderId,
                                                   OrderNo = orm.OrderNo,
                                                   DepotId = dpo.PKDepotId,
                                                   DepoName = dpo.DepotTitle,
                                                   OrderAmount = (decimal)orm.OrderAmount,
                                                   OrderDate = (DateTime)orm.OrderDate,
                                                   Status = st.Status,
                                                   StatusGroup = st.StatusGroup

                                               };



                        }
                        else
                        {

                            orderManageModal = from orm in db.tbPurchaseOrdersMsts
                                               join st in db.tbStatusMsts on orm.FKStatusId equals st.StatusId
                                               join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                               join rg in db.tbRegionsMsts on dpo.RegionCode equals rg.RegionCode
                                               where arraydepotid.Contains(dpo.PKDepotId.ToString())
                                               select new OrderManageModal
                                               {

                                                   OrderId = orm.PKPurchaseOrderId,
                                                   OrderNo = orm.OrderNo,
                                                   DepotId = dpo.PKDepotId,
                                                   DepoName = dpo.DepotTitle,
                                                   OrderAmount = (decimal)orm.OrderAmount,
                                                   OrderDate = (DateTime)orm.OrderDate,
                                                   Status = st.Status,
                                                   StatusGroup = st.StatusGroup

                                               };

                        }
                    }

                }


                var result = orderManageModal.AsQueryable();

                if (searchModel != null)
                {
                    if (!string.IsNullOrEmpty(searchModel.OrderNo))
                        result = result.Where(x => x.OrderNo == searchModel.OrderNo.ToString());
                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.OrderDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.OrderDate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.Status))
                    {
                        if (searchModel.Status.ToLower() == "approved")
                        {
                            result = result.Where(x => x.StatusGroup.ToLower() == "confirm");
                        }
                        else
                        {
                            result = result.Where(x => x.StatusGroup.ToLower() == searchModel.Status.ToLower());
                        }
                    }
                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<OrderManageModal> orderManageModal = null;
                return orderManageModal;
            }
        }

        /// <summary>
        /// returns specific order
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public OrderManageModal GetSpecificOrder(int OrderId)
        {
            try
            {
                db = new DMSDbCon();

                IEnumerable<OrderManageModal> orderManageModal = from orm in db.tbPurchaseOrdersMsts
                                                                 join st in db.tbStatusMsts on orm.FKStatusId equals st.StatusId
                                                                 join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                                                 select new OrderManageModal
                                                                 {

                                                                     OrderId = orm.PKPurchaseOrderId,
                                                                     OrderNo = orm.OrderNo,
                                                                     DepotId = dpo.PKDepotId,
                                                                     DepoName = dpo.DepotTitle,
                                                                     OrderAmount = (decimal)orm.OrderAmount,
                                                                     OrderDate = (DateTime)orm.OrderDate,
                                                                     Status = st.Status
                                                                 };
                var result = orderManageModal.AsQueryable();

                OrderManageModal data = result.Where(x => x.OrderId == OrderId).FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                OrderManageModal orderManageModal = null;
                return orderManageModal;
            }
        }

        /// <summary>
        /// return all the details of a specific order
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<OrderDetailModal> GetOrderDetails(int id)
        {
            try
            {
                db = new DMSDbCon();
                var orderDetailModal = from odls in db.tbPurchaseOrderDtls
                                       join prod in db.tbProductsMsts on odls.ProductCode equals prod.ProductCode
                                       where odls.FKPurchaseOrderId == id
                                       select new OrderDetailModal
                                       {
                                           OrderDetailId = odls.PKOrderDetailsMst,
                                           ProductCode = odls.ProductCode,
                                           ProductName = prod.ProductName,
                                           quantity = odls.Quantity == null ? 0 : (int)odls.Quantity,
                                           rate = odls.Rate == null ? 0 : (decimal)odls.Rate,
                                           taxamount = odls.TaxValue == null ? 0 : (decimal)odls.TaxValue,
                                           total = odls.Amount == null ? 0 : (int)odls.Amount,
                                           OrderId = odls.FKPurchaseOrderId == null ? 0 : (int)odls.FKPurchaseOrderId
                                       };

                return orderDetailModal;
            }
            catch (Exception ex)
            {
                IEnumerable<OrderDetailModal> data = null;
                return data;
            }
        }

        public GetPOrderResult GetOrderApi(GetOrderParameter getOrderParameter)
        {
            GetPOrderResult getOrderResult = new GetPOrderResult();
            try
            {

                db = new DMSDbCon();
                int sid = db.tbStatusMsts.Where(x => x.StatusGroup == "Completed").Select(x => x.StatusId).SingleOrDefault();


                IEnumerable<POrderAPI> orderApi = (from orm in db.tbPurchaseOrdersMsts
                                                   join st in db.tbStatusMsts on orm.FKStatusId equals st.StatusId
                                                   join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                                   where dpo.PKDepotId == getOrderParameter.DepotId && orm.FKStatusId != sid
                                                   select new POrderAPI
                                                   {
                                                       OrderNo = orm.OrderNo,
                                                       OrderAmount = (decimal)orm.OrderAmount,
                                                       OrderDate = orm.OrderDate.ToString(),
                                                       OrderStatus = st.Status,
                                                       OrderDiscAmount = orm.OrderDiscAmount,
                                                       OrderNetAmount = orm.OrderAmount
                                                   });
                List<POrderAPI> data = orderApi.ToList();

                data.ForEach(x => x.OrderDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(x.OrderDate)));
                getOrderResult.Orders = data as IEnumerable<POrderAPI>;

            }
            catch (Exception ex)
            {
                getOrderResult.Orders = null;

            }


            return getOrderResult;
        }

        public POrderDetailResultApi GetAllProductDetail(OrderDetailParameter orderDetailParameter)
        {
            try
            {
                db = new DMSDbCon();
                IEnumerable<ProductAllDetailBO> ProductList = from od in db.tbPurchaseOrdersMsts
                                                              join ods in db.tbPurchaseOrderDtls on od.PKPurchaseOrderId equals ods.FKPurchaseOrderId
                                                              join pd in db.tbProductsMsts on ods.ProductCode equals pd.ProductCode
                                                              where od.OrderNo == orderDetailParameter.OrderNo
                                                              select new ProductAllDetailBO
                                                              {
                                                                  ProductCode = pd.ProductCode,
                                                                  Description = pd.ProductName,
                                                                  PackDescription = pd.PackDescription,
                                                                  ProductGroup = pd.ProductGroup,
                                                                  PackSize = pd.PackSize,
                                                                  TaxType = pd.TaxType,
                                                                  ActiveStatus = pd.ActiveStatus,
                                                                  Description2 = pd.Description2,
                                                                  PKProductId = pd.PKProductId,
                                                                  ProductGroupDescription = pd.ProductGroupDescription,
                                                                  Quantity = (ods.Quantity == null) ? 0 : (int)ods.Quantity,
                                                                  Rate = (ods.Quantity == null) ? 0 : (decimal)ods.Rate,
                                                                  DiscountType = ods.DiscountType,
                                                                  Amount = (ods.Amount == null) ? 0 : (decimal)ods.Amount,
                                                                  DiscAmount = (ods.DiscountValue == null) ? 0 : (decimal)ods.DiscAmount,
                                                                  NetAmount = (ods.NetAmount == null) ? 0 : (decimal)ods.NetAmount,
                                                                  DiscountValue = (ods.DiscountValue == null) ? 0 : (decimal)ods.DiscountValue,
                                                                  TaxAmount = (ods.TaxValue == null) ? 0 : (decimal)ods.TaxValue
                                                              };
                POrderDetailResultApi orderDetailResultApi = (from od in db.tbPurchaseOrdersMsts
                                                              join st in db.tbStatusMsts on od.FKStatusId equals st.StatusId
                                                              where od.OrderNo == orderDetailParameter.OrderNo

                                                              select new POrderDetailResultApi
                                                              {
                                                                  OrderAmount = od.OrderAmount,
                                                                  OrderDate = od.OrderDate.ToString(),
                                                                  OrderDiscAmount = od.OrderDiscAmount,
                                                                  OrderNetAmount = od.OrderNetAmount,
                                                                  OrderNo = od.OrderNo,
                                                                  OrderStatus = st.Status,
                                                              }).SingleOrDefault();

                orderDetailResultApi.OrderDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(orderDetailResultApi.OrderDate));
                orderDetailResultApi.Products = ProductList;

                return orderDetailResultApi;
            }
            catch (Exception ex)
            {
                POrderDetailResultApi data = null;
                return data;
            }
        }

        public StatusMessage InsertOrder(InsertOrderParameterApi insertOrderParameterApi)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();

                        OrderAllDetailBO[] AllorderAllDetailBO = insertOrderParameterApi.Products;
                        tbPurchaseOrdersMst odata = new tbPurchaseOrdersMst();
                        odata.CreatedBy = insertOrderParameterApi.Username;
                        odata.FKDepotId = insertOrderParameterApi.DepotId;
                        odata.OrderAmount = insertOrderParameterApi.OrderAmount;
                        odata.OrderDiscAmount = insertOrderParameterApi.OrderDiscAmount;
                        odata.OrderNetAmount = insertOrderParameterApi.OrderNetAmount;
                        odata.FKStatusId = sid;
                        odata.OrderDate = DateTime.Now;
                        odata.CreatedOn = DateTime.Now;
                        odata.ModifiedOn = DateTime.Now;
                        db.tbPurchaseOrdersMsts.Add(odata);
                        db.SaveChanges();
                        int orderid = odata.PKPurchaseOrderId;
                        odata.OrderNo = "PORD000" + orderid;
                        db.Entry(odata).State = EntityState.Modified;
                        db.SaveChanges();
                        foreach (var orderAllDetailBO in AllorderAllDetailBO)
                        {

                            tbPurchaseOrderDtl data = new tbPurchaseOrderDtl();
                            data.Amount = orderAllDetailBO.Amount;
                            data.CreatedBy = orderAllDetailBO.CreatedBy;
                            data.CreatedOn = DateTime.Now;
                            data.DiscAmount = orderAllDetailBO.DiscAmount;
                            data.DiscountType = orderAllDetailBO.DiscountType;
                            data.DiscountValue = orderAllDetailBO.DiscountValue;
                            data.FKPurchaseOrderId = orderid;
                            data.ModifiedBy = orderAllDetailBO.ModifiedBy;
                            data.ModifiedOn = DateTime.Now;
                            data.NetAmount = orderAllDetailBO.NetAmount;
                            data.PKOrderDetailsMst = orderAllDetailBO.PKOrderDetailsMst;
                            data.ProductCode = orderAllDetailBO.ProductCode;
                            data.Quantity = orderAllDetailBO.Quantity;
                            data.Rate = orderAllDetailBO.Rate;
                            data.TaxValue = orderAllDetailBO.TaxValue;
                            db.tbPurchaseOrderDtls.Add(data);
                            statusMessage.Status = true;
                            statusMessage.Message = "Order successfully Inserted";
                            db.SaveChanges();


                        }
                        transaction.Commit();
                        statusMessage.Status = true;
                        statusMessage.Message = odata.OrderNo;

                    }
                    catch (Exception ex)
                    {
                        statusMessage.Status = false;
                        statusMessage.Message = "Order not Inserted try again";
                        transaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                statusMessage.Status = false;
                statusMessage.Message = "Order not Inserted try again";

            }
            return statusMessage;
        }


        public StatusMessage ApproveOrder(PurchaseOrderApproveAPIBO para)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(para.Username);
                var query = (from ord in db.tbPurchaseOrdersMsts
                             where ord.OrderNo == para.OrderNo
                             select new { ord.PKPurchaseOrderId }).SingleOrDefault();
                int POId = query.PKPurchaseOrderId;
                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "confirm").Select(x => x.StatusId).SingleOrDefault();
                tbPurchaseOrderStatusDtl tbdSRDtl = new tbPurchaseOrderStatusDtl();
                tbdSRDtl.Comments = para.Message;
                tbdSRDtl.CreatedBy = para.Username;
                tbdSRDtl.CreatedOn = DateTime.Now;
                tbdSRDtl.FKStatusId = sid;
                tbdSRDtl.FKPurchaseOrderID = POId;
                db.tbPurchaseOrderStatusDtls.Add(tbdSRDtl);
                tbPurchaseOrdersMst tbdSRMst = db.tbPurchaseOrdersMsts.Find(POId);
                tbdSRMst.FKStatusId = sid;
                db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                statusMessage.Message = "The Purchase is approved sucessfully.";
                statusMessage.Status = true;
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage RejectOrder(PurchaseOrderApproveAPIBO para)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(para.Username);

                var query = (from ord in db.tbPurchaseOrdersMsts
                             where ord.OrderNo == para.OrderNo
                             select new { ord.PKPurchaseOrderId }).SingleOrDefault();
                int POId = query.PKPurchaseOrderId;


                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();

                tbPurchaseOrderStatusDtl tbdSRDtl = new tbPurchaseOrderStatusDtl();
                tbdSRDtl.Comments = para.Message;
                tbdSRDtl.CreatedBy = para.Username;
                tbdSRDtl.CreatedOn = DateTime.Now;
                tbdSRDtl.FKStatusId = sid;
                tbdSRDtl.FKPurchaseOrderID = POId;
                db.tbPurchaseOrderStatusDtls.Add(tbdSRDtl);
                tbPurchaseOrdersMst tbdSRMst = db.tbPurchaseOrdersMsts.Find(POId);
                tbdSRMst.FKStatusId = sid;
                db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                statusMessage.Message = "The Purchase order is Rejected sucessfully.";
                statusMessage.Status = true;

            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public IEnumerable<PurchaseOrderReportExportBO> GetAllReportPurchaseOrder(FilterPOrder searchModel)
        {
            try
            {
                db = new DMSDbCon();

                IEnumerable<PurchaseOrderReportExportBO> orderManageModal;

                orderManageModal = from orm in db.tbPurchaseOrdersMsts
                                   join ormd in db.tbPurchaseOrderDtls on orm.PKPurchaseOrderId equals ormd.FKPurchaseOrderId
                                   join idn in db.tbIDNMsts on orm.PKPurchaseOrderId equals idn.FKPurchaseOrderId
                                   join idnd in db.tbIDNDtls on idn.PKIDNId equals idnd.FKIDNId
                                   join st in db.tbStatusMsts on orm.FKStatusId equals st.StatusId
                                   join dpo in db.tbDepotMsts on orm.FKDepotId equals dpo.PKDepotId
                                   join pd in db.tbProductsMsts on ormd.ProductCode equals pd.ProductCode
                                   select new PurchaseOrderReportExportBO
                                   {
                                       ItemCode = ormd.ProductCode,
                                       BranchName = "",
                                       Delayindays = 0,
                                       DepotName = dpo.DepotTitle,
                                       PONo = orm.OrderNo,
                                       IBTNo = idn.IDNNo,
                                       IBTDate = (DateTime)idn.IDNDate,
                                       ItemDescription = pd.ProductName,
                                       OrderQty = (int)ormd.Quantity,
                                       PODate = (DateTime)orm.OrderDate,
                                       QtySent = (int)idnd.SourceQuantity,
                                       BalanceQty = (int)ormd.Quantity - (int)idnd.SourceQuantity,
                                       POCompletedInDays=(int)EntityFunctions.DiffDays( idn.IDNDate.Value ,orm.OrderDate),
                                       DepotId=dpo.PKDepotId,
                                       Status=st.StatusGroup
                                   };



                var result = orderManageModal.AsQueryable();

                if (searchModel != null)
                {
                    if (!string.IsNullOrEmpty(searchModel.OrderNo))
                        result = result.Where(x => x.PONo == searchModel.OrderNo.ToString());
                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.PODate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.PODate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.Status))
                    {
                        if (searchModel.Status.ToLower() == "approved")
                        {
                            result = result.Where(x => x.Status.ToLower() == "confirm");
                        }
                        else
                        {
                            result = result.Where(x => x.Status.ToLower() == searchModel.Status.ToLower());
                        }
                    }
                }

                var data = result.AsEnumerable();
                return data;
            }
            catch (Exception ex)
            {
                IEnumerable<PurchaseOrderReportExportBO> orderManageModal = null;
                return orderManageModal;
            }
        }




    }
}
