﻿﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace DMSDAL
{
  public class GRRManagement
  {
    DMSDbCon db;

    public StatusMessage CreateGRR(CreateGRRBO createIDNBO, string Username)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {

        db = new DMSDbCon();
        int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
        int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();
        int GRRExists = db.tbGRRMsts.Where(x => x.GRRNo == createIDNBO.GRRNo).Count();

        if (GRRExists > 0)
        {
          statusMessage.Message = "You are trying to create duplicate GRR.";
          statusMessage.Status = false;
          return statusMessage;
        }


        string imagename = String.Format("Data\\GRR\\{0}-{1}-{2}", HOID, createIDNBO.GRRNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(createIDNBO.GRRPhoto.FileName);
        tbGRRMst record = new tbGRRMst();
        record.FKDepotId = HOID;
        record.FKDirectSupplierId = 0;
        record.CreatedBy = Username;
        record.CreatedOn = DateTime.Now;
        record.GRRNo = createIDNBO.GRRNo;
        record.GRRImage = imagename;
        record.StatusID = sid;
        record.FKIDNId = createIDNBO.IDNId;
        record.GRRDate = DateTime.Now;
        db.tbGRRMsts.Add(record);
        db.SaveChanges();

        string savepath = createIDNBO.Pathname + imagename;
        createIDNBO.GRRPhoto.SaveAs(savepath);

        statusMessage.Message = record.FKIDNId.ToString();
        statusMessage.Status = true;

      }
      catch (Exception ex)
      {

        statusMessage.Message = "Please try again.";
        statusMessage.Status = false;

      }
      return statusMessage;
    }

    public IDNAddProductBO GetIDNData(int IDNId)
    {
      try
      {
        db = new DMSDbCon();

        IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();

        string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();
        iDNAddProductBO = (from idm in db.tbIDNMsts
                           join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                           from goods in grrInfo.DefaultIfEmpty()
                           join dp in db.tbDepotMsts on idm.SourceId equals dp.PKDepotId
                           join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                           where idm.PKIDNId == IDNId
                           select new IDNAddProductBO
                           {
                             IDNId = idm.PKIDNId,
                             IDNNo = idm.IDNNo,
                             IDNDate = (DateTime)idm.IDNDate,
                             Path = urlpath + goods.GRRImage,
                             DepotName = dp.DepotTitle,
                             DepotId = dp.PKDepotId,
                             TotalIDNAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                             Status = st.Status,
                             StatusGroup = st.StatusGroup,
                             GRRNo = goods.GRRNo
                           }).SingleOrDefault();
        if (iDNAddProductBO == null)
        {
          iDNAddProductBO = new IDNAddProductBO();
        }
        int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();


        int isgrr = db.tbGRRMsts.Where(x => x.FKIDNId == IDNId && x.StatusID != sid).Count();

        if (isgrr == 0)
        {
          iDNAddProductBO.productIDNBO =
                                        from idm in db.tbIDNMsts
                                        join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                                        from goods in grrInfo.DefaultIfEmpty()
                                        join idmd in db.tbIDNDtls on idm.PKIDNId equals idmd.FKIDNId into idnInfo
                                        from note in idnInfo.DefaultIfEmpty()
                                        join grrd in db.tbGRRDtls on goods.PKGRRId equals grrd.FKGRRId into grrdinfo
                                        from goodsd in grrdinfo.DefaultIfEmpty()
                                        join dp in db.tbDepotMsts on idm.SourceId equals dp.PKDepotId
                                        join pMst in db.tbProductsMsts on note.ProductCode equals pMst.ProductCode
                                        where idm.PKIDNId == IDNId
                                        select new ProductIDNBO
                                        {

                                          DepotName = dp.DepotTitle,
                                          ProductCode = note.ProductCode,
                                          ProductName = pMst.ProductName,
                                          Quantity = (note.SourceQuantity == null) ? 0 : (int)note.SourceQuantity,
                                          SQuantity = (goodsd.ShortageQuantity == null) ? 0 : (int)goodsd.ShortageQuantity,
                                          EQuantity = (goodsd.ExcessQuantity == null) ? 0 : (int)goodsd.ExcessQuantity,
                                          RQuantity = (goodsd.ReceivedQuantity == null) ? 0 : (int)goodsd.ReceivedQuantity,
                                          Rate = (note.Rate == null) ? 0 : (int)note.Rate,
                                          Total = (note.Amount == null) ? 0 : (decimal)note.Amount
                                        };
        }

        else if (isgrr == 1)
        {
          iDNAddProductBO.productIDNBO =
                                        from idm in db.tbIDNMsts
                                        join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                                        from goods in grrInfo.DefaultIfEmpty()
                                        join grrd in db.tbGRRDtls on goods.PKGRRId equals grrd.FKGRRId into grrdinfo
                                        from goodsd in grrdinfo.DefaultIfEmpty()
                                        join dp in db.tbDepotMsts on idm.SourceId equals dp.PKDepotId
                                        join pMst in db.tbProductsMsts on goodsd.ProductCode equals pMst.ProductCode
                                        where idm.PKIDNId == IDNId
                                        select new ProductIDNBO
                                        {

                                          DepotName = dp.DepotTitle,
                                          ProductCode = goodsd.ProductCode,
                                          ProductName = pMst.ProductName,
                                          Quantity = ((goodsd.SourceQuantity == null) ? 0 : (int)goodsd.SourceQuantity),
                                          SQuantity = (goodsd.ShortageQuantity == null) ? 0 : (int)goodsd.ShortageQuantity,
                                          EQuantity = (goodsd.ExcessQuantity == null) ? 0 : (int)goodsd.ExcessQuantity,
                                          RQuantity = (goodsd.ReceivedQuantity == null) ? 0 : (int)goodsd.ReceivedQuantity
                                        };
        }

        return iDNAddProductBO;

      }
      catch (Exception ex)
      {

        IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();
        iDNAddProductBO.productIDNBO = null;
        return iDNAddProductBO;
      }


    }

    public IEnumerable<IDNBO> GetAllIDNData(FilterIDN searchModel, String Username)
    {
      db = new DMSDbCon();

      IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();
      int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
      searchModel.DestinationDepotId = HOID;



      string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();


      string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();
      string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();
      string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();

      if (depotlist == null)
      {
        depotlist = "";
      }
      if (Regionlist == null)
      {
        Regionlist = "";
      }

      string[] arraydepotid = depotlist.Split(',');
      string[] arrayregionid = Regionlist.Split(',');

      IEnumerable<IDNBO> data;

      if (RoleCode != "DOCLK" && RoleCode != "DOMGR")
      {

        data = (from idm in db.tbIDNMsts
                join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                from goods in grrInfo.DefaultIfEmpty()
                join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                select new IDNBO
                {
                  IDNId = idm.PKIDNId,
                  IDNNo = idm.IDNNo,
                  Path = urlpath + goods.GRRImage,
                  SourceDepot = sdp.DepotTitle,
                  GRRNo = goods.GRRNo,
                  GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                  DestinationDepot = ddp.DepotTitle,
                  SourceDepotId = sdp.PKDepotId,
                  DestinationDepotId = ddp.PKDepotId,
                  IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                  IDNDate = (DateTime)idm.IDNDate,
                  Status = st.Status,
                  StatusGroup = st.StatusGroup
                });

      }
      else
      {

        if (Regionlist.ToLower().ToString() == "all")
        {
          data = (from idm in db.tbIDNMsts
                  join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                  from goods in grrInfo.DefaultIfEmpty()
                  join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                  join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                  join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                  select new IDNBO
                  {
                    IDNId = idm.PKIDNId,
                    IDNNo = idm.IDNNo,
                    Path = urlpath + goods.GRRImage,
                    SourceDepot = sdp.DepotTitle,
                    GRRNo = goods.GRRNo,
                    GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                    DestinationDepot = ddp.DepotTitle,
                    SourceDepotId = sdp.PKDepotId,
                    DestinationDepotId = ddp.PKDepotId,
                    IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                    IDNDate = (DateTime)idm.IDNDate,
                    Status = st.Status,
                    StatusGroup = st.StatusGroup
                  });
        }
        else
        {
          if (depotlist.ToLower().ToString() == "all")
          {

            data = (from idm in db.tbIDNMsts
                    join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                    from goods in grrInfo.DefaultIfEmpty()
                    join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                    join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                    join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                    where arrayregionid.Contains(ddp.RegionCode.ToString()) || ddp.RegionCode.ToLower() == "ho"
                    select new IDNBO
                    {
                      IDNId = idm.PKIDNId,
                      IDNNo = idm.IDNNo,
                      Path = urlpath + goods.GRRImage,
                      SourceDepot = sdp.DepotTitle,
                      GRRNo = goods.GRRNo,
                      GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                      DestinationDepot = ddp.DepotTitle,
                      SourceDepotId = sdp.PKDepotId,
                      DestinationDepotId = ddp.PKDepotId,
                      IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                      IDNDate = (DateTime)idm.IDNDate,
                      Status = st.Status,
                      StatusGroup = st.StatusGroup
                    });
          }
          else
          {

            data = (from idm in db.tbIDNMsts
                    join grr in db.tbGRRMsts on idm.PKIDNId equals grr.FKIDNId into grrInfo
                    from goods in grrInfo.DefaultIfEmpty()
                    join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                    join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                    join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                    where arraydepotid.Contains(ddp.PKDepotId.ToString()) || ddp.RegionCode.ToLower() == "ho"
                    select new IDNBO
                    {
                      IDNId = idm.PKIDNId,
                      IDNNo = idm.IDNNo,
                      Path = urlpath + goods.GRRImage,
                      SourceDepot = sdp.DepotTitle,
                      GRRNo = goods.GRRNo,
                      GRRDate = goods.GRRDate == null ? "" : goods.GRRDate.ToString(),
                      DestinationDepot = ddp.DepotTitle,
                      SourceDepotId = sdp.PKDepotId,
                      DestinationDepotId = ddp.PKDepotId,
                      IDNTotalAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                      IDNDate = (DateTime)idm.IDNDate,
                      Status = st.Status,
                      StatusGroup = st.StatusGroup
                    });
          }

        }

      }


      var result = data.AsQueryable();

      if (searchModel != null)
      {

        if (searchModel.SourceDepotId.HasValue)
          result = result.Where(x => x.SourceDepotId == searchModel.SourceDepotId);
        if (searchModel.DestinationDepotId.HasValue)
          result = result.Where(x => x.DestinationDepotId == searchModel.DestinationDepotId);
        if (searchModel.FromDate.HasValue)
          result = result.Where(x => DbFunctions.TruncateTime(x.IDNDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
        if (searchModel.ToDate.HasValue)
          result = result.Where(x => DbFunctions.TruncateTime(x.IDNDate) <= DbFunctions.TruncateTime(searchModel.ToDate));
        if (!String.IsNullOrEmpty(searchModel.Status))
        {
          if (searchModel.Status.ToLower() == "completed")
          {
            result = result.Where(x => x.StatusGroup.ToLower() == "approved");
          }
          else
          {

            result = result.Where(x => x.StatusGroup.ToLower() == searchModel.Status.ToLower());
          }

        }
      }

      data = result.AsEnumerable();
      return data;
    }


    public IEnumerable<GRRCreateIDNList> GetAllIDNDropDown(String Username)
    {
      db = new DMSDbCon();

      IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();
      int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
      int DestinationDepotId = HOID;



      string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();


      string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();
      string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();
      string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();

      if (depotlist == null)
      {
        depotlist = "";
      }
      if (Regionlist == null)
      {
        Regionlist = "";
      }

      string[] arraydepotid = depotlist.Split(',');
      string[] arrayregionid = Regionlist.Split(',');

      IEnumerable<GRRCreateIDNList> data;

      if (RoleCode != "DOCLK" && RoleCode != "DOMGR")
      {

        data = (from idm in db.tbIDNMsts
                join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                where idm.DestinationId == HOID && (st.StatusGroup.ToLower() == "pending" || st.StatusGroup.ToLower() == "confirm")
                select new GRRCreateIDNList
                {
                  IDNId = idm.PKIDNId,
                  IDNNo = idm.IDNNo,

                });

      }
      else
      {

        if (Regionlist.ToLower().ToString() == "all")
        {
          data = (from idm in db.tbIDNMsts
                  join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                  join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                  join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                  where idm.DestinationId == HOID && (st.StatusGroup.ToLower() == "pending" || st.StatusGroup.ToLower() == "confirm")
                  select new GRRCreateIDNList
                  {
                    IDNId = idm.PKIDNId,
                    IDNNo = idm.IDNNo,
                  });
        }
        else
        {
          if (depotlist.ToLower().ToString() == "all")
          {

            data = (from idm in db.tbIDNMsts
                    join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                    join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                    join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                    where arrayregionid.Contains(ddp.RegionCode.ToString()) || ddp.RegionCode.ToLower() == "ho"
                    && DestinationDepotId == HOID && (st.StatusGroup.ToLower() == "pending" || st.StatusGroup.ToLower() == "confirm")
                    select new GRRCreateIDNList
                    {
                      IDNId = idm.PKIDNId,
                      IDNNo = idm.IDNNo,
                    });
          }
          else
          {

            data = (from idm in db.tbIDNMsts
                    join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                    join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                    join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                    where arraydepotid.Contains(ddp.PKDepotId.ToString()) || ddp.RegionCode.ToLower() == "ho"
                    where DestinationDepotId == HOID && (st.StatusGroup.ToLower() == "pending" || st.StatusGroup.ToLower() == "confirm")
                    select new GRRCreateIDNList
                    {
                      IDNId = idm.PKIDNId,
                      IDNNo = idm.IDNNo,

                    });
          }

        }

      }


      var result = data.AsQueryable();

      data = result.AsEnumerable();
      return data;
    }


    public IEnumerable<IDNBO> GetAllGRRData(FilterIDN searchModel, String Username)
    {
      db = new DMSDbCon();

      IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();
      int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
      searchModel.DestinationDepotId = HOID;



      string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();


      string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();
      string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();
      string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();

      if (depotlist == null)
      {
        depotlist = "";
      }
      if (Regionlist == null)
      {
        Regionlist = "";
      }

      string[] arraydepotid = depotlist.Split(',');
      string[] arrayregionid = Regionlist.Split(',');

      IEnumerable<IDNBO> data;

      if (RoleCode != "DOCLK" && RoleCode != "DOMGR")
      {

        data = (from grr in db.tbGRRMsts
                join idm in db.tbIDNMsts on grr.FKIDNId equals idm.PKIDNId into grrInfo
                from goods in grrInfo.DefaultIfEmpty()
                join ddp in db.tbDepotMsts on grr.FKDepotId equals ddp.PKDepotId
                join sdp in db.tbDepotMsts on goods.SourceId equals sdp.PKDepotId
                join st in db.tbStatusMsts on grr.StatusID equals st.StatusId
                select new IDNBO
                {
                  IDNId = goods.PKIDNId,
                  IDNNo = goods.IDNNo,
                  Path = urlpath + grr.GRRImage,
                  GRRNo = grr.GRRNo,
                  GRRDate = grr.GRRDate == null ? "" : grr.GRRDate.ToString(),
                  SourceDepot = sdp.DepotTitle,
                  DestinationDepot = ddp.DepotTitle,
                  SourceDepotId = sdp.PKDepotId,
                  DestinationDepotId = ddp.PKDepotId,
                  IDNTotalAmount = (grr.GRRAmount == null) ? 0 : (int)grr.GRRAmount,
                  IDNDate = (DateTime)goods.IDNDate,
                  Status = st.Status,
                  StatusGroup = st.StatusGroup
                });

      }
      else
      {

        if (Regionlist.ToLower().ToString() == "all")
        {
          data = (from grr in db.tbGRRMsts
                  join idm in db.tbIDNMsts on grr.FKIDNId equals idm.PKIDNId into grrInfo
                  from goods in grrInfo.DefaultIfEmpty()
                  join ddp in db.tbDepotMsts on grr.FKDepotId equals ddp.PKDepotId
                  join sdp in db.tbDepotMsts on goods.SourceId equals sdp.PKDepotId
                  join st in db.tbStatusMsts on grr.StatusID equals st.StatusId
                  select new IDNBO
                  {
                    IDNId = goods.PKIDNId,
                    IDNNo = goods.IDNNo,
                    Path = urlpath + grr.GRRImage,
                    GRRNo = grr.GRRNo,
                    GRRDate = grr.GRRDate == null ? "" : grr.GRRDate.ToString(),
                    SourceDepot = sdp.DepotTitle,
                    DestinationDepot = ddp.DepotTitle,
                    SourceDepotId = sdp.PKDepotId,
                    DestinationDepotId = ddp.PKDepotId,
                    IDNTotalAmount = (grr.GRRAmount == null) ? 0 : (int)grr.GRRAmount,
                    IDNDate = (DateTime)goods.IDNDate,
                    Status = st.Status,
                    StatusGroup = st.StatusGroup

                  });
        }
        else
        {
          if (depotlist.ToLower().ToString() == "all")
          {

            data = (from grr in db.tbGRRMsts
                    join idm in db.tbIDNMsts on grr.FKIDNId equals idm.PKIDNId into grrInfo
                    from goods in grrInfo.DefaultIfEmpty()
                    join ddp in db.tbDepotMsts on grr.FKDepotId equals ddp.PKDepotId
                    join sdp in db.tbDepotMsts on goods.SourceId equals sdp.PKDepotId
                    join st in db.tbStatusMsts on grr.StatusID equals st.StatusId
                    where arrayregionid.Contains(ddp.RegionCode.ToString()) || ddp.RegionCode.ToLower() == "ho"
                    select new IDNBO
                    {
                      IDNId = goods.PKIDNId,
                      IDNNo = goods.IDNNo,
                      Path = urlpath + grr.GRRImage,

                      GRRNo = grr.GRRNo,
                      GRRDate = grr.GRRDate == null ? "" : grr.GRRDate.ToString(),
                      SourceDepot = sdp.DepotTitle,
                      DestinationDepot = ddp.DepotTitle,
                      SourceDepotId = sdp.PKDepotId,
                      DestinationDepotId = ddp.PKDepotId,
                      IDNTotalAmount = (goods.IDNAmount == null) ? 0 : (int)goods.IDNAmount,
                      IDNDate = (DateTime)goods.IDNDate,
                      Status = st.Status,
                      StatusGroup = st.StatusGroup
                    });
          }
          else
          {

            data = (from grr in db.tbGRRMsts
                    join idm in db.tbIDNMsts on grr.FKIDNId equals idm.PKIDNId into grrInfo
                    from goods in grrInfo.DefaultIfEmpty()
                    join ddp in db.tbDepotMsts on grr.FKDepotId equals ddp.PKDepotId
                    join sdp in db.tbDepotMsts on goods.SourceId equals sdp.PKDepotId
                    join st in db.tbStatusMsts on grr.StatusID equals st.StatusId
                    where arraydepotid.Contains(ddp.PKDepotId.ToString()) || ddp.RegionCode.ToLower() == "ho"
                    select new IDNBO
                    {
                      IDNId = goods.PKIDNId,
                      IDNNo = goods.IDNNo,
                      Path = urlpath + grr.GRRImage,
                      SourceDepot = sdp.DepotTitle,
                      GRRNo = grr.GRRNo,
                      GRRDate = grr.GRRDate == null ? "" : grr.GRRDate.ToString(),
                      DestinationDepot = ddp.DepotTitle,
                      SourceDepotId = sdp.PKDepotId,
                      DestinationDepotId = ddp.PKDepotId,
                      IDNTotalAmount = (goods.IDNAmount == null) ? 0 : (int)goods.IDNAmount,
                      IDNDate = (DateTime)goods.IDNDate,
                      Status = st.Status,
                      StatusGroup = st.StatusGroup
                    });
          }

        }

      }


      var result = data.AsQueryable();

      if (searchModel != null)
      {

        if (searchModel.SourceDepotId.HasValue)
          result = result.Where(x => x.SourceDepotId == searchModel.SourceDepotId);
        if (searchModel.DestinationDepotId.HasValue)
          result = result.Where(x => x.DestinationDepotId == searchModel.DestinationDepotId);
        if (searchModel.FromDate.HasValue)
          result = result.Where(x => DbFunctions.TruncateTime(x.IDNDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
        if (searchModel.ToDate.HasValue)
          result = result.Where(x => DbFunctions.TruncateTime(x.IDNDate) <= DbFunctions.TruncateTime(searchModel.ToDate));
        if (!String.IsNullOrEmpty(searchModel.Status))
        {
          if (searchModel.Status.ToLower() == "completed")
          {
            result = result.Where(x => x.StatusGroup.ToLower() == "approved");
          }
          else
          {

            result = result.Where(x => x.StatusGroup.ToLower() == searchModel.Status.ToLower());
          }

        }
      }

      data = result.AsEnumerable();
      return data;
    }


    public StatusMessage UpdateToIDNAsGRR(IDNAddProductBO iDNAddProductBO, string username)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        using (var transaction = db.Database.BeginTransaction())
        {
          try
          {


            Account account = new Account();
            string role = account.GetUserRole(username);
            int HOID = db.tbDepotMsts.Where(x => x.RegionCode == "HO").Select(x => x.PKDepotId).SingleOrDefault();
            int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "Received").Select(x => x.StatusId).SingleOrDefault();


            tbIDNMst record = db.tbIDNMsts.Find(iDNAddProductBO.IDNId);
            record.StatusID = sid;
            db.Entry(record).State = EntityState.Modified;
            db.SaveChanges();

            tbGRRMst record2 = db.tbGRRMsts.Where(x => x.GRRNo == iDNAddProductBO.GRRNo).SingleOrDefault();
            record2.StatusID = sid;
            db.Entry(record2).State = EntityState.Modified;
            db.SaveChanges();

            //tbIDNGRRStatusDtl record1 = new tbIDNGRRStatusDtl();
            //record1.Comments = "";
            //record1.CreatedBy = username;
            //record1.CreatedOn = DateTime.Now;
            //record1.FKIDNGRRID = iDNAddProductBO.IDNId;
            //record1.FKStatusId = sid;
            //db.tbIDNGRRStatusDtls.Add(record1);
            //db.SaveChanges();


            foreach (var item in iDNAddProductBO.productIDNBO)
            {
              tbGRRDtl tbiDNGRRDtl = new tbGRRDtl();
              tbiDNGRRDtl.Amount = item.Total;
              tbiDNGRRDtl.CreatedBy = username;
              tbiDNGRRDtl.CreatedOn = DateTime.Now;
              tbiDNGRRDtl.FKGRRId = record2.PKGRRId;
              tbiDNGRRDtl.ProductCode = item.ProductCode;
              tbiDNGRRDtl.Rate = item.Rate;
              tbiDNGRRDtl.SourceQuantity = item.Quantity;
              tbiDNGRRDtl.ShortageQuantity = item.SQuantity;
              tbiDNGRRDtl.ExcessQuantity = item.EQuantity;
              tbiDNGRRDtl.ReceivedQuantity = item.RQuantity;
              db.tbGRRDtls.Add(tbiDNGRRDtl);
              db.SaveChanges();
            }

            statusMessage.Message = "GRR Successflly generated.";
            statusMessage.Status = true;
            transaction.Commit();
          }
          catch (Exception ex)
          {
            statusMessage.Message = "Please try again.";
            statusMessage.Status = false;
            transaction.Rollback();
          }
        }
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Please try again.";
        statusMessage.Status = false;

      }
      return statusMessage;
    }

    public StatusMessage CreateGRR(IDNReceiveAPIBO para)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        using (var transaction = db.Database.BeginTransaction())
        {
          try
          {

            Account account = new Account();
            string role = account.GetUserRole(para.Username);
            int IDNId = db.tbIDNMsts.Where(x => x.IDNNo == para.IDNNo).Select(x => x.PKIDNId).SingleOrDefault();
            int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "Received").Select(x => x.StatusId).SingleOrDefault();
            int GRRExists = db.tbGRRMsts.Where(x => x.GRRNo == para.GRRNo).Count();

            if (GRRExists > 0)
            {
              statusMessage.Message = "You are trying to create duplicate GRR.";
              statusMessage.Status = false;
              return statusMessage;
            }


            tbGRRMst record = new tbGRRMst();
            record.CreatedBy = para.Username;
            record.CreatedOn = DateTime.Now;
            record.FKDepotId = para.DepotId;
            record.FKDirectSupplierId = para.DirectSupplierId;
            record.GRRType = para.GRRType;
            record.FKIDNId = IDNId;
            record.GRRAmount = para.GRRAmount;
            record.GRRDate = DateTime.Now;
            record.GRRImage = para.Grrimage;
            record.GRRNo = para.GRRNo;
            record.StatusID = sid;
            db.tbGRRMsts.Add(record);
            db.SaveChanges();

            tbIDNStatusDtl record1 = new tbIDNStatusDtl();
            record1.Comments = "";
            record1.CreatedBy = para.Username;
            record1.CreatedOn = DateTime.Now;
            record1.FKIDNID = IDNId;
            record1.FKStatusId = sid;
            db.tbIDNStatusDtls.Add(record1);
            db.SaveChanges();



            tbGRRStatusDtl record2 = new tbGRRStatusDtl();
            record2.Comments = "";
            record2.CreatedBy = para.Username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = record.PKGRRId;
            record2.FKStatusId = sid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();





            if (IDNId != 0)
            {
              tbIDNMst irecord = db.tbIDNMsts.Find(IDNId);
              irecord.StatusID = sid;
              db.Entry(irecord).State = EntityState.Modified;
              db.SaveChanges();
            }

            foreach (var item in para.GRRProducts)
            {
              tbGRRDtl tbiDNGRRDtl = new tbGRRDtl();
              tbiDNGRRDtl.Amount = item.Total;
              tbiDNGRRDtl.CreatedBy = para.Username;
              tbiDNGRRDtl.CreatedOn = DateTime.Now;
              tbiDNGRRDtl.FKGRRId = record.PKGRRId;
              tbiDNGRRDtl.ProductCode = item.ProductCode;
              tbiDNGRRDtl.Rate = item.Rate;
              tbiDNGRRDtl.TaxValue = 0;
              tbiDNGRRDtl.SourceQuantity = item.SourceQuantity;
              tbiDNGRRDtl.ShortageQuantity = item.ShortageQuantity;
              tbiDNGRRDtl.ExcessQuantity = item.ExcessQuantity;
              tbiDNGRRDtl.ReceivedQuantity = item.ReceivedQuantity;
              db.tbGRRDtls.Add(tbiDNGRRDtl);
              db.SaveChanges();


              tbProductStockMst stock = db.tbProductStockMsts.Where(x => x.FKDepotId == para.DepotId && x.ProductCode == item.ProductCode).FirstOrDefault();
              stock.StockQuantity = stock.StockQuantity + item.SourceQuantity;
              db.Entry(stock).State = EntityState.Modified;
              db.SaveChanges();

            }

            statusMessage.Message = "GRR Successflly generated.";
            statusMessage.Status = true;
            transaction.Commit();
          }
          catch (Exception ex)
          {
            statusMessage.Message = "Please try again.";
            statusMessage.Status = false;
            transaction.Rollback();
          }
        }
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Please try again.";
        statusMessage.Status = false;

      }
      return statusMessage;
    }

    public StatusMessage ApproveGRR(int Id, string username)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        Account account = new Account();
        string role = account.GetUserRole(username);
        var query = (from idn in db.tbGRRMsts
                     join st in db.tbStatusMsts on idn.StatusID equals st.StatusId
                     where idn.FKIDNId == Id
                     select new { st.StatusGroup }).SingleOrDefault();

        string status = query.StatusGroup;

        int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "approved").Select(x => x.StatusId).SingleOrDefault();
        int csid = db.tbStatusMsts.Where(x => x.StatusGroup.ToLower() == "Completed").Select(x => x.StatusId).SingleOrDefault();
        if (!string.IsNullOrEmpty(status))
        {
          if (status.ToLower() == "received" || status.ToLower() == "rejected")
          {
            tbIDNStatusDtl record1 = new tbIDNStatusDtl();
            record1.Comments = "";
            record1.CreatedBy = username;
            record1.CreatedOn = DateTime.Now;
            record1.FKIDNID = Id;
            record1.FKStatusId = sid;
            db.tbIDNStatusDtls.Add(record1);
            db.SaveChanges();

            tbIDNMst tbIDNMst = db.tbIDNMsts.Find(Id);
            tbIDNMst.StatusID = sid;
            db.Entry(tbIDNMst).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            tbIDNMst = db.tbIDNMsts.Find(Id);
            tbIDNMst.StatusID = csid;
            db.Entry(tbIDNMst).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            record1 = new tbIDNStatusDtl();
            record1.Comments = "";
            record1.CreatedBy = username;
            record1.CreatedOn = DateTime.Now;
            record1.FKIDNID = Id;
            record1.FKStatusId = csid;
            db.tbIDNStatusDtls.Add(record1);
            db.SaveChanges();


            int grrid = db.tbGRRMsts.Where(x => x.FKIDNId == Id).Select(x => x.PKGRRId).SingleOrDefault();

            tbGRRStatusDtl record2 = new tbGRRStatusDtl();
            record2.Comments = "";
            record2.CreatedBy = username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = grrid;
            record2.FKStatusId = sid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();


            tbGRRMst tbGRRMst = db.tbGRRMsts.Find(grrid);
            tbGRRMst.StatusID = csid;
            db.Entry(tbGRRMst).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            ;

            record2 = new tbGRRStatusDtl();


            record2 = new tbGRRStatusDtl();
            record2.Comments = "";
            record2.CreatedBy = username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = grrid;
            record2.FKStatusId = sid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();

            record2 = new tbGRRStatusDtl();
            record2.Comments = "";
            record2.CreatedBy = username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = grrid;
            record2.FKStatusId = csid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();

            if (tbIDNMst.FKPurchaseOrderId != null || tbIDNMst.FKPurchaseOrderId != 0)
            {
              sid = db.tbStatusMsts.Where(x => x.StatusGroup.ToLower() == "completed").Select(x => x.StatusId).SingleOrDefault();


              tbPurchaseOrderStatusDtl tbdSRDtl = new tbPurchaseOrderStatusDtl();
              tbdSRDtl.Comments = "";
              tbdSRDtl.CreatedBy = username;
              tbdSRDtl.CreatedOn = DateTime.Now;
              tbdSRDtl.FKStatusId = sid;
              tbdSRDtl.FKPurchaseOrderID = tbIDNMst.FKPurchaseOrderId;
              db.tbPurchaseOrderStatusDtls.Add(tbdSRDtl);
              tbPurchaseOrdersMst tbdSRMst = db.tbPurchaseOrdersMsts.Find(tbIDNMst.FKPurchaseOrderId);
              tbdSRMst.FKStatusId = sid;
              db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
              db.SaveChanges();

            }
            statusMessage.Message = "The GRR is approved sucessfully.";
            statusMessage.Status = true;
          }
          else
          {
            statusMessage.Message = "The GRR is not Recevid  by Depo Manager/Depo Supervisor";
            statusMessage.Status = false;
          }
        }
        else
        {
          statusMessage.Message = "Invalid GRR ID";
          statusMessage.Status = false;
        }
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Please try again.";
        statusMessage.Status = false;
      }

      return statusMessage;
    }

    public StatusMessage RejectGRR(int Id, string message, string username)
    {

      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        Account account = new Account();
        string role = account.GetUserRole(username);
        var query = (from idn in db.tbGRRMsts
                     join st in db.tbStatusMsts on idn.StatusID equals st.StatusId
                     where idn.PKGRRId == Id
                     select new { st.StatusGroup }).SingleOrDefault();

        string status = query.StatusGroup;

        int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();
        if (!string.IsNullOrEmpty(status))
        {
          if (status.ToLower() == "received" || status.ToLower() == "rejected")
          {
            tbIDNStatusDtl record1 = new tbIDNStatusDtl();
            record1.Comments = message;
            record1.CreatedBy = username;
            record1.CreatedOn = DateTime.Now;
            record1.FKIDNID = Id;
            record1.FKStatusId = sid;
            db.tbIDNStatusDtls.Add(record1);
            db.SaveChanges();




            tbIDNMst tbIDNMst = db.tbIDNMsts.Find(Id);
            tbIDNMst.StatusID = sid;
            db.Entry(tbIDNMst).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            int grrid = db.tbGRRMsts.Where(x => x.FKIDNId == Id).Select(x => x.PKGRRId).SingleOrDefault();

            tbGRRStatusDtl record2 = new tbGRRStatusDtl();
            record2.Comments = message;
            record2.CreatedBy = username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = grrid;
            record2.FKStatusId = sid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();


            tbGRRMst tbGRRMst = db.tbGRRMsts.Find(Id);
            tbGRRMst.StatusID = sid;
            db.Entry(tbIDNMst).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();


            statusMessage.Message = "The GRR is Rejected sucessfully.";
            statusMessage.Status = true;
          }
          else
          {
            statusMessage.Message = "The GRR is not Recevid  by Depo Manager/Depo Supervisor";
            statusMessage.Status = false;
          }
        }
        else
        {
          statusMessage.Message = "Invalid GRR ID";
          statusMessage.Status = false;
        }
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Please try again.";
        statusMessage.Status = false;
      }
      return statusMessage;
    }

    public StatusMessage ApproveGRR(GRRApproveBO para)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        Account account = new Account();
        string role = account.GetUserRole(para.Username);

        var query = (from idn in db.tbGRRMsts
                     join st in db.tbStatusMsts on idn.StatusID equals st.StatusId
                     where idn.GRRNo == para.GRRNo
                     select new { st.StatusGroup }).SingleOrDefault();

        string status = query.StatusGroup;

        int Id = (int)db.tbGRRMsts.Where(x => x.GRRNo == para.GRRNo).Select(x => x.FKIDNId).SingleOrDefault();

        int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "confirm").Select(x => x.StatusId).SingleOrDefault();

        if (!string.IsNullOrEmpty(status))
        {
          if (status.ToLower() == "received" || status.ToLower() == "rejected")
          {
            int csid = db.tbStatusMsts.Where(x => x.StatusGroup.ToLower() == "completed").Select(x => x.StatusId).SingleOrDefault();

            tbIDNStatusDtl record1 = new tbIDNStatusDtl();
            record1.Comments = para.Message;
            record1.CreatedBy = para.Username;
            record1.CreatedOn = DateTime.Now;
            record1.FKIDNID = Id;
            record1.FKStatusId = sid;
            db.tbIDNStatusDtls.Add(record1);
            db.SaveChanges();



            record1 = new tbIDNStatusDtl();
            record1.Comments = para.Message;
            record1.CreatedBy = para.Username;
            record1.CreatedOn = DateTime.Now;
            record1.FKIDNID = Id;
            record1.FKStatusId = csid;
            db.tbIDNStatusDtls.Add(record1);
            db.SaveChanges();

            if (Id != 0)
            {
              tbIDNMst idnmst = db.tbIDNMsts.Find(Id);
              idnmst.StatusID = csid;
              db.Entry(idnmst).State = EntityState.Modified;
              db.SaveChanges();

              tbIDNMst tbIDNMst = db.tbIDNMsts.Find(Id);
              tbIDNMst.StatusID = sid;
              db.Entry(tbIDNMst).State = System.Data.Entity.EntityState.Modified;
              db.SaveChanges();

              if (tbIDNMst.FKPurchaseOrderId != null && tbIDNMst.FKPurchaseOrderId != 0)
              {

                tbPurchaseOrderStatusDtl tbdSRDtl = new tbPurchaseOrderStatusDtl();
                tbdSRDtl.Comments = "";
                tbdSRDtl.CreatedBy = para.Username;
                tbdSRDtl.CreatedOn = DateTime.Now;
                tbdSRDtl.FKStatusId = sid;
                tbdSRDtl.FKPurchaseOrderID = tbIDNMst.FKPurchaseOrderId;
                db.tbPurchaseOrderStatusDtls.Add(tbdSRDtl);
                tbPurchaseOrdersMst tbdSRMst = db.tbPurchaseOrdersMsts.Find(tbIDNMst.FKPurchaseOrderId);
                tbdSRMst.FKStatusId = sid;
                db.Entry(tbdSRMst).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
              }
            }
            int grrid = db.tbGRRMsts.Where(x => x.GRRNo == para.GRRNo).Select(x => x.PKGRRId).SingleOrDefault();

            tbGRRStatusDtl record2 = new tbGRRStatusDtl();
            record2.Comments = para.Message;
            record2.CreatedBy = para.Username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = grrid;
            record2.FKStatusId = sid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();

            record2 = new tbGRRStatusDtl();
            record2.Comments = para.Message;
            record2.CreatedBy = para.Username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = grrid;
            record2.FKStatusId = csid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();


            tbGRRMst tbGRRMst = db.tbGRRMsts.Find(grrid);
            tbGRRMst.StatusID = csid;
            db.Entry(tbGRRMst).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();



            record2 = new tbGRRStatusDtl();
            record2.Comments = "";
            record2.CreatedBy = para.Username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = grrid;
            record2.FKStatusId = csid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();


            statusMessage.Message = "The GRR is approved sucessfully.";
            statusMessage.Status = true;
          }
          else
          {
            statusMessage.Message = "The GRR is not Recevid  by Depo Manager/Depo Supervisor";
            statusMessage.Status = false;
          }
        }
        else
        {
          statusMessage.Message = "Invalid GRR ID";
          statusMessage.Status = false;
        }
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Please try again.";
        statusMessage.Status = false;
      }

      return statusMessage;
    }

    public StatusMessage RejectGRR(GRRApproveBO para)
    {
      StatusMessage statusMessage = new StatusMessage();
      try
      {
        db = new DMSDbCon();
        Account account = new Account();
        string role = account.GetUserRole(para.Username);
        int Id = db.tbGRRMsts.Where(x => x.GRRNo == para.GRRNo).Select(x => x.PKGRRId).SingleOrDefault();

        var query = (from idn in db.tbGRRMsts
                     join st in db.tbStatusMsts on idn.StatusID equals st.StatusId
                     where idn.PKGRRId == Id
                     select new { st.StatusGroup }).SingleOrDefault();

        string status = query.StatusGroup;

        int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();
        if (!string.IsNullOrEmpty(status))
        {
          if (status.ToLower() == "received" || status.ToLower() == "rejected")
          {
            tbIDNStatusDtl record1 = new tbIDNStatusDtl();
            record1.Comments = para.Message;
            record1.CreatedBy = para.Username;
            record1.CreatedOn = DateTime.Now;
            record1.FKIDNID = Id;
            record1.FKStatusId = sid;
            db.tbIDNStatusDtls.Add(record1);
            db.SaveChanges();

            if (Id != 0)
            {
              tbIDNMst tbIDNMst = db.tbIDNMsts.Find(Id);
              tbIDNMst.StatusID = sid;
              db.Entry(tbIDNMst).State = System.Data.Entity.EntityState.Modified;
              db.SaveChanges();
            }
            int grrid = db.tbGRRMsts.Where(x => x.FKIDNId == Id).Select(x => x.PKGRRId).SingleOrDefault();

            tbGRRStatusDtl record2 = new tbGRRStatusDtl();
            record2.Comments = para.Message;
            record2.CreatedBy = para.Username;
            record2.CreatedOn = DateTime.Now;
            record2.FKGRRID = Id;
            record2.FKStatusId = sid;
            db.tbGRRStatusDtls.Add(record2);
            db.SaveChanges();

            tbGRRMst tbGRRMst = db.tbGRRMsts.Find(Id);
            tbGRRMst.StatusID = sid;
            db.Entry(tbGRRMst).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();


            statusMessage.Message = "The GRR is Rejected sucessfully.";
            statusMessage.Status = true;
          }
          else
          {
            statusMessage.Message = "The GRR is not Recevid  by Depo Manager/Depo Supervisor";
            statusMessage.Status = false;
          }
        }
        else
        {
          statusMessage.Message = "Invalid GRR ID";
          statusMessage.Status = false;
        }
      }
      catch (Exception ex)
      {
        statusMessage.Message = "Please try again.";
        statusMessage.Status = false;
      }
      return statusMessage;
    }

    public ListGRRAPI GetAllIDNData(DSRListParameter dSRListParameter)
    {

      ListGRRAPI result = new ListGRRAPI();
      try
      {
        db = new DMSDbCon();

        IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();

        DateTime currentdate = DateTime.Now;
        dSRListParameter.NoOfDays = dSRListParameter.NoOfDays - 1;
        currentdate = currentdate.AddDays(-dSRListParameter.NoOfDays);


        string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();
        IEnumerable<GRRAPIBO> data = (from idm in db.tbGRRMsts
                                      join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                                      where idm.FKDepotId == dSRListParameter.DepotId && DbFunctions.TruncateTime(idm.GRRDate) >= DbFunctions.TruncateTime(currentdate)
                                      select new GRRAPIBO
                                      {
                                        PKGRRId = idm.PKGRRId,
                                        GRRType = idm.GRRType,
                                        GRRNo = idm.GRRNo,
                                        GRRImage = urlpath + idm.GRRImage,
                                        Status = st.Status,
                                        GRRAmount = idm.GRRAmount == null ? 0 : (decimal)idm.GRRAmount,
                                        GRRDate = idm.GRRDate == null ? "" : idm.GRRDate.ToString(),
                                        StatusGroup = st.StatusGroup
                                      });
        var listdata = data.ToList();

        listdata.ForEach(x => x.GRRDate = DateOperation.FromDateTOMilliGRR((x.GRRDate)));

        data = listdata as IEnumerable<GRRAPIBO>;
        result.GRRList = data;

      }
      catch (Exception ex)
      {
        result = null;
      }

      return result;
    }

    public GetGRRAPIBO GetIDNData(string GRRNo)
    {
      try
      {
        db = new DMSDbCon();

        GetGRRAPIBO getIDNAPIBO = new GetGRRAPIBO();

        string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();

        GRRAPIBO ObjGRRAPIBO = new GRRAPIBO();

        ObjGRRAPIBO = (from tbgrr in db.tbGRRMsts
                       where tbgrr.GRRNo == GRRNo
                       select new GRRAPIBO
                       {
                         GRRType = tbgrr.GRRType
                       }
                       ).SingleOrDefault();

        if (ObjGRRAPIBO.GRRType.ToLower().Contains("idn"))
        {
          getIDNAPIBO = (from idm in db.tbGRRMsts
                         join idn in db.tbIDNMsts on idm.FKIDNId equals idn.PKIDNId into idns
                         from intdispatch in idns.DefaultIfEmpty()
                         join dcs in db.tbDirectSupplierMsts on idm.FKDirectSupplierId equals dcs.PKSupplierId into dsp
                         from direct in dsp.DefaultIfEmpty()
                         join dp in db.tbDepotMsts on intdispatch.SourceId equals dp.PKDepotId
                         join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                         where idm.GRRNo == GRRNo
                         select new GetGRRAPIBO
                         {
                           DirectSupplierName = direct.SupplierName,
                           GRRAmount = idm.GRRAmount == null ? 0 : (decimal)idm.GRRAmount,
                           GRRDate = idm.GRRDate.ToString(),
                           GRRImage = urlpath + idm.GRRImage,
                           GRRNo = idm.GRRNo,
                           GRRType = idm.GRRType,
                           IDNDate = intdispatch.IDNDate.ToString(),
                           IDNNo = intdispatch.IDNNo,
                           PKGRRId = idm.PKGRRId,
                           SourceDepot = dp.DepotTitle,
                           Status = st.Status,
                           StatusGroup = st.StatusGroup
                         }).SingleOrDefault();


        }
        else if (ObjGRRAPIBO.GRRType.ToLower().Contains("supplier"))
        {
          getIDNAPIBO = (from idm in db.tbGRRMsts
                         join dcs in db.tbDirectSupplierMsts on idm.FKDirectSupplierId equals dcs.PKSupplierId into dsp
                         from direct in dsp.DefaultIfEmpty()
                         join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                         where idm.GRRNo == GRRNo
                         select new GetGRRAPIBO
                         {
                           DirectSupplierName = direct.SupplierName,
                           GRRAmount = idm.GRRAmount == null ? 0 : (decimal)idm.GRRAmount,
                           GRRDate = idm.GRRDate.ToString(),
                           GRRImage = urlpath + idm.GRRImage,
                           GRRNo = idm.GRRNo,
                           GRRType = idm.GRRType,
                           IDNDate = idm.GRRDate.ToString(),
                           IDNNo = null,
                           PKGRRId = idm.PKGRRId,
                           SourceDepot = "",
                           Status = st.Status,
                           StatusGroup = st.StatusGroup
                         }).SingleOrDefault();
        }

        if (getIDNAPIBO == null)
        {
          getIDNAPIBO = new GetGRRAPIBO();

        }
        else
        {
          if (getIDNAPIBO.GRRDate != "")
            getIDNAPIBO.GRRDate = DateOperation.FromDateTOMilliGRR((getIDNAPIBO.GRRDate));
          if (getIDNAPIBO.IDNDate != "")
            getIDNAPIBO.IDNDate = DateOperation.FromDateTOMilliGRR((getIDNAPIBO.IDNDate));
        }
        getIDNAPIBO.GRRProducts = from idm in db.tbGRRMsts
                                  join idd in db.tbGRRDtls on idm.PKGRRId equals idd.FKGRRId
                                  join pMst in db.tbProductsMsts on idd.ProductCode equals pMst.ProductCode
                                  where idm.PKGRRId == getIDNAPIBO.PKGRRId

                                  select new ProductIDNAPIBO
                                  {

                                    ProductCode = idd.ProductCode,
                                    Description = pMst.ProductName,
                                    ExcessQuantity = (idd.ExcessQuantity == null) ? 0 : (int)idd.ExcessQuantity,
                                    SourceQuantity = (idd.SourceQuantity == null) ? 0 : (int)idd.SourceQuantity,
                                    ShortageQuantity = (idd.ShortageQuantity == null) ? 0 : (int)idd.ShortageQuantity,
                                    ReceivedQuantity = (idd.ReceivedQuantity == null) ? 0 : (int)idd.ReceivedQuantity,
                                    Rate = (idd.Rate == null) ? 0 : (int)idd.Rate,
                                    Total = (idd.Amount == null) ? 0 : (decimal)idd.Amount
                                  };



        return getIDNAPIBO;

      }
      catch (Exception ex)
      {

        GetGRRAPIBO getGRRAPIBO = null;
        return getGRRAPIBO;
      }
    }

    public ListIDNAPI GetAllIDNGRRData(DSRListParameter dSRListParameter)
    {

      ListIDNAPI result = new ListIDNAPI();
      try
      {
        db = new DMSDbCon();

        IDNAddProductBO iDNAddProductBO = new IDNAddProductBO();

        DateTime currentdate = DateTime.Now;


        string urlpath = WebConfigurationManager.AppSettings["Url"].ToString();
        IEnumerable<IDNAPIBO> data = (from idm in db.tbIDNMsts
                                      join ddp in db.tbDepotMsts on idm.DestinationId equals ddp.PKDepotId
                                      join sdp in db.tbDepotMsts on idm.SourceId equals sdp.PKDepotId
                                      join st in db.tbStatusMsts on idm.StatusID equals st.StatusId
                                      where idm.DestinationId == dSRListParameter.DepotId && (st.StatusGroup.ToLower() == "pending" || st.StatusGroup.ToLower() == "confirm")
                                      select new IDNAPIBO
                                      {
                                        PKIDNId = idm.PKIDNId,
                                        FKPurchaseOrderId = idm.FKPurchaseOrderId == null ? 0 : (int)idm.FKPurchaseOrderId,
                                        IDNNo = idm.IDNNo,
                                        IDNImage = urlpath + idm.IDNImage,
                                        SourceDepot = sdp.DepotTitle,
                                        DestinationDepot = ddp.DepotTitle,
                                        SourceDepotId = sdp.PKDepotId,
                                        DestinationDepotId = ddp.PKDepotId,
                                        IDNAmount = (idm.IDNAmount == null) ? 0 : (int)idm.IDNAmount,
                                        IDNDate = idm.IDNDate.ToString(),
                                        Status = st.Status,
                                        StatusGroup = st.StatusGroup
                                      });
        var listdata = data.ToList();

        listdata.ForEach(x => x.IDNDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(x.IDNDate)));
        data = listdata as IEnumerable<IDNAPIBO>;
        result.IDNList = data;
      }
      catch (Exception ex)
      {
        result = null;
      }

      return result;
    }


  }
}