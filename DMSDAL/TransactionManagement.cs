﻿using DMSBO;
using DMSDATABASE;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace DMSDAL
{
    public class TransactionManagement
    {

        DMSDbCon db;

        public IEnumerable<BankTransactionBO> GetAllTransaction(FilterTransaction searchModel, string Username)
        {
            try
            {
                db = new DMSDbCon();
                String path = WebConfigurationManager.AppSettings["Url"].ToString();

                string depotlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.DepotId).SingleOrDefault();
                string Regionlist = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RegionCode).SingleOrDefault();
                string RoleCode = db.tbUsersMsts.Where(x => x.Usercode == Username).Select(x => x.RoleCode).SingleOrDefault();
                if (depotlist == null)
                {
                    depotlist = "";
                }
                if (Regionlist == null)
                {
                    Regionlist = "";
                }
                string[] arraydepotid = depotlist.Split(',');
                string[] arrayregionid = Regionlist.Split(',');
                IEnumerable<BankTransactionBO> Transcation;
                if (RoleCode != "ACCLK" && RoleCode != "ACTNT")
                {

                    Transcation = from bk in db.tbBankTransactions
                                  join dp in db.tbDepotMsts
                                  on bk.FKDepotId equals dp.PKDepotId
                                  join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                                  join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                                  join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                                  select new BankTransactionBO
                                  {
                                      CreatedBy = bk.CreatedBy,
                                      CreatedOn = bk.CreatedOn,
                                      DepotName = dp.DepotTitle,
                                      FKDepotId = dp.PKDepotId,
                                      FKDSRId = bk.FKDSRId,
                                      PKTransactionId = (int)bk.PKTransactionId,
                                      Status = st.Status,
                                      StatusId = st.StatusId,
                                      TransactionAmount = bk.TransactionAmount,
                                      TransactionDate = bk.TransactionDate,
                                      TransactionImage = path + bk.TransactionImage,
                                      DSRNo = dsr.DSRNo,
                                      StatusGroup = st.StatusGroup,
                                      RegionCode = dp.RegionCode,
                                      BankName = bkn.BankName,
                                      BankBranch = bkn.BankBranch
                                  };

                }
                else
                {

                    if (Regionlist.ToLower().ToString() == "all")
                    {
                        Transcation = from bk in db.tbBankTransactions
                                      join dp in db.tbDepotMsts
                                      on bk.FKDepotId equals dp.PKDepotId
                                      join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                                      join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                                      join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                                      select new BankTransactionBO
                                      {
                                          CreatedBy = bk.CreatedBy,
                                          CreatedOn = bk.CreatedOn,
                                          DepotName = dp.DepotTitle,
                                          FKDepotId = dp.PKDepotId,
                                          FKDSRId = bk.FKDSRId,
                                          PKTransactionId = (int)bk.PKTransactionId,
                                          Status = st.Status,
                                          StatusId = st.StatusId,
                                          TransactionAmount = bk.TransactionAmount,
                                          TransactionDate = bk.TransactionDate,
                                          TransactionImage = path + bk.TransactionImage,
                                          DSRNo = dsr.DSRNo,
                                          StatusGroup = st.StatusGroup,
                                          RegionCode = dp.RegionCode,
                                          BankName = bkn.BankName,
                                          BankBranch = bkn.BankBranch
                                      };

                    }
                    else
                    {

                        if (depotlist.ToLower().ToString() == "all")
                        {
                            Transcation = from bk in db.tbBankTransactions
                                          join dp in db.tbDepotMsts
                                          on bk.FKDepotId equals dp.PKDepotId
                                          join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                                          join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                                          join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                                          where arrayregionid.Contains(dp.RegionCode.ToString())
                                          select new BankTransactionBO
                                          {
                                              CreatedBy = bk.CreatedBy,
                                              CreatedOn = bk.CreatedOn,
                                              DepotName = dp.DepotTitle,
                                              FKDepotId = dp.PKDepotId,
                                              FKDSRId = bk.FKDSRId,
                                              PKTransactionId = (int)bk.PKTransactionId,
                                              Status = st.Status,
                                              StatusId = st.StatusId,
                                              TransactionAmount = bk.TransactionAmount,
                                              TransactionDate = bk.TransactionDate,
                                              TransactionImage = path + bk.TransactionImage,
                                              DSRNo = dsr.DSRNo,
                                              StatusGroup = st.StatusGroup,
                                              RegionCode = dp.RegionCode,
                                              BankName = bkn.BankName,
                                              BankBranch = bkn.BankBranch
                                          };
                        }
                        else
                        {
                            Transcation = from bk in db.tbBankTransactions
                                          join dp in db.tbDepotMsts
                                          on bk.FKDepotId equals dp.PKDepotId
                                          join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                                          join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                                          join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                                          where arraydepotid.Contains(dp.PKDepotId.ToString())
                                          select new BankTransactionBO
                                          {
                                              CreatedBy = bk.CreatedBy,
                                              CreatedOn = bk.CreatedOn,
                                              DepotName = dp.DepotTitle,
                                              FKDepotId = dp.PKDepotId,
                                              FKDSRId = bk.FKDSRId,
                                              PKTransactionId = (int)bk.PKTransactionId,
                                              Status = st.Status,
                                              StatusId = st.StatusId,
                                              TransactionAmount = bk.TransactionAmount,
                                              TransactionDate = bk.TransactionDate,
                                              TransactionImage = path + bk.TransactionImage,
                                              DSRNo = dsr.DSRNo,
                                              StatusGroup = st.StatusGroup,
                                              RegionCode = dp.RegionCode,
                                              BankName = bkn.BankName,
                                              BankBranch = bkn.BankBranch
                                          };
                        }
                    }
                }

                var result = Transcation.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.FKDepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.TransactionDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.TransactionDate) <= DbFunctions.TruncateTime(searchModel.ToDate));
                    if (!String.IsNullOrEmpty(searchModel.Status))
                    {
                        result = result.Where(x => x.StatusGroup.ToLower() == searchModel.Status.ToLower());
                    }
                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }
                }

                var data = result.AsEnumerable();
                return data;



            }
            catch (Exception ex)
            {
                IEnumerable<BankTransactionBO> Transcation = null;

                return Transcation;
            }


        }


        public IEnumerable<BankTransactionBO> GetAllTransaction(FilterTransaction searchModel)
        {
            try
            {
                db = new DMSDbCon();
                String path = WebConfigurationManager.AppSettings["Url"].ToString();


                IEnumerable<BankTransactionBO> Transcation;


                Transcation = from bk in db.tbBankTransactions
                              join dp in db.tbDepotMsts
                              on bk.FKDepotId equals dp.PKDepotId
                              join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                              join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                              join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                              where st.StatusGroup.ToLower() == "Approved".ToLower()
                              select new BankTransactionBO
                              {
                                  CreatedBy = bk.CreatedBy,
                                  CreatedOn = bk.CreatedOn,
                                  DepotName = dp.DepotTitle,
                                  FKDepotId = dp.PKDepotId,
                                  FKDSRId = bk.FKDSRId,
                                  PKTransactionId = (int)bk.PKTransactionId,
                                  Status = st.Status,
                                  StatusId = st.StatusId,
                                  TransactionAmount = bk.TransactionAmount,
                                  TransactionDate = bk.TransactionDate,
                                  TransactionImage = path + bk.TransactionImage,
                                  DSRNo = dsr.DSRNo,
                                  StatusGroup = st.StatusGroup,
                                  RegionCode = dp.RegionCode,
                                  BankName = bkn.BankName,
                                  BankBranch = bkn.BankBranch,
                                  DelayInDays = 0,
                                  DSRAmount = (decimal)dsr.DSRNetAmount,
                                  DSRDate = (DateTime)dsr.DSRDate


                              };



                var result = Transcation.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.FKDepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.TransactionDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.TransactionDate) <= DbFunctions.TruncateTime(searchModel.ToDate));

                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }
                }

                var data = result.AsEnumerable();
                return data;



            }
            catch (Exception ex)
            {
                IEnumerable<BankTransactionBO> Transcation = null;

                return Transcation;
            }


        }

        public IEnumerable<BankTransactionExportBO> GetAllExportTransaction(FilterTransaction searchModel)
        {
            try
            {
                db = new DMSDbCon();
                String path = WebConfigurationManager.AppSettings["Url"].ToString();


                IEnumerable<BankTransactionExportBO> Transcation;


                Transcation = from bk in db.tbBankTransactions
                              join dp in db.tbDepotMsts
                              on bk.FKDepotId equals dp.PKDepotId
                              join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                              join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                              join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                              where st.StatusGroup.ToLower() == "approved"
                              select new BankTransactionExportBO
                              {
                                  DepotId = dp.PKDepotId,
                                  DepotName = dp.DepotTitle,
                                  TransactionAmount = bk.TransactionAmount == null ? 0 : (decimal)bk.TransactionAmount,
                                  TransactionDate = bk.TransactionDate == null ? DateTime.Now : (DateTime)bk.TransactionDate,
                                  DSRNo = dsr.DSRNo,
                                  BankName = bkn.BankName,
                                  BankBranch = bkn.BankBranch,
                                  RegionCode = dp.RegionCode,
                                  DelayInDays = 0,
                                  DSRAmount = (decimal)dsr.DSRAmount,
                                  DSRDate = (DateTime)dsr.DSRDate,
                                  Status = st.StatusGroup,
                                  ShortageAmount=(decimal)dsr.DSRAmount-(decimal)bk.TransactionAmount

                              };



                var result = Transcation.AsQueryable();

                if (searchModel != null)
                {

                    if (searchModel.DepotId.HasValue)
                        result = result.Where(x => x.DepotId == searchModel.DepotId);
                    if (searchModel.FromDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.TransactionDate) >= DbFunctions.TruncateTime(searchModel.FromDate));
                    if (searchModel.ToDate.HasValue)
                        result = result.Where(x => DbFunctions.TruncateTime(x.TransactionDate) <= DbFunctions.TruncateTime(searchModel.ToDate));


                    if (!String.IsNullOrEmpty(searchModel.RegionCode))
                    {
                        result = result.Where(x => x.RegionCode.ToLower().Contains(searchModel.RegionCode.ToLower()));
                    }
                }

                var data = result.AsEnumerable();
                return data;



            }
            catch (Exception ex)
            {
                IEnumerable<BankTransactionExportBO> Transcation = null;

                return Transcation;
            }


        }


        public StatusMessage ApproveTransaction(int TID, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);
                var query = (from bkt in db.tbBankTransactions
                             join st in db.tbStatusMsts on bkt.TransactionStatusId equals st.StatusId
                             where bkt.PKTransactionId == TID
                             select new { st.StatusGroup }).SingleOrDefault();

                string status = query.StatusGroup;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "approved").Select(x => x.StatusId).SingleOrDefault();
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "confirm" || status.ToLower() == "rejected")
                    {
                        tbBankTransactionDtl tbbankTransactionDtl = new tbBankTransactionDtl();

                        tbbankTransactionDtl.Comments = "";
                        tbbankTransactionDtl.CreatedBy = username;
                        tbbankTransactionDtl.CreatedOn = DateTime.Now;
                        tbbankTransactionDtl.StatusId = sid;
                        tbbankTransactionDtl.FKTransactionId = TID;
                        db.tbBankTransactionDtls.Add(tbbankTransactionDtl);
                        tbBankTransaction tbbankTransaction = db.tbBankTransactions.Find(TID);
                        tbbankTransaction.TransactionStatusId = sid;
                        db.Entry(tbbankTransaction).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The bank Transaction is approved sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is not confirmed by Depo Manager/Depo Supervisor";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid Bank Transaction ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage RejectTransaction(int TID, string message, string username)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(username);
                var query = (from bkt in db.tbBankTransactions
                             join st in db.tbStatusMsts on bkt.TransactionStatusId equals st.StatusId
                             where bkt.PKTransactionId == TID
                             select new { st.StatusGroup }).SingleOrDefault();
                string status = query.StatusGroup;

                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower() == "confirm" || status.ToLower() == "rejected")
                    {
                        tbBankTransactionDtl tbbankTransactionDtl = new tbBankTransactionDtl();

                        tbbankTransactionDtl.Comments = message;
                        tbbankTransactionDtl.CreatedBy = username;
                        tbbankTransactionDtl.CreatedOn = DateTime.Now;
                        tbbankTransactionDtl.StatusId = sid;
                        tbbankTransactionDtl.FKTransactionId = TID;
                        db.tbBankTransactionDtls.Add(tbbankTransactionDtl);
                        tbBankTransaction tbbankTransaction = db.tbBankTransactions.Find(TID);
                        tbbankTransaction.TransactionStatusId = sid;
                        db.Entry(tbbankTransaction).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        statusMessage.Message = "The bank Transaction is Rejected sucessfully.";
                        statusMessage.Status = true;
                    }
                    else
                    {
                        statusMessage.Message = "The id is not confirmed by Depo Manager/Depo Supervisor";
                        statusMessage.Status = false;
                    }
                }
                else
                {
                    statusMessage.Message = "Invalid Transaction ID";
                    statusMessage.Status = false;
                }
            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage ApproveTransaction(TransactionApproveAPIBO para)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(para.Username);


                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "confirm").Select(x => x.StatusId).SingleOrDefault();

                DateTime cdate = DateOperation.FromMilliToDate(para.DateTime);

                tbBankTransactionDtl tbbankTransactionDtl = new tbBankTransactionDtl();

                tbbankTransactionDtl.Comments = "";
                tbbankTransactionDtl.CreatedBy = para.Username;
                tbbankTransactionDtl.CreatedOn = DateTime.Now;
                tbbankTransactionDtl.StatusId = sid;
                tbbankTransactionDtl.FKTransactionId = para.BankTransactionId;
                db.tbBankTransactionDtls.Add(tbbankTransactionDtl);
                tbBankTransaction tbbankTransaction = db.tbBankTransactions.Find(para.BankTransactionId);
                tbbankTransaction.TransactionStatusId = sid;
                db.Entry(tbbankTransaction).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                statusMessage.Message = "The bank Transaction is approved sucessfully.";
                statusMessage.Status = true;

            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public StatusMessage RejectTransaction(TransactionApproveAPIBO para)
        {
            StatusMessage statusMessage = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                Account account = new Account();
                string role = account.GetUserRole(para.Username);


                int sid = db.tbStatusMsts.Where(x => x.Role == role && x.StatusGroup.ToLower() == "rejected").Select(x => x.StatusId).SingleOrDefault();

                tbBankTransactionDtl tbbankTransactionDtl = new tbBankTransactionDtl();
                DateTime cdate = DateOperation.FromMilliToDate(para.DateTime);
                tbbankTransactionDtl.Comments = para.Message;
                tbbankTransactionDtl.CreatedBy = para.Username;
                tbbankTransactionDtl.CreatedOn = cdate;
                tbbankTransactionDtl.StatusId = sid;
                tbbankTransactionDtl.FKTransactionId = para.BankTransactionId;
                db.tbBankTransactionDtls.Add(tbbankTransactionDtl);
                tbBankTransaction tbbankTransaction = db.tbBankTransactions.Find(para.BankTransactionId);
                tbbankTransaction.TransactionStatusId = sid;
                db.Entry(tbbankTransaction).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                statusMessage.Message = "The bank Transaction is Rejected sucessfully.";
                statusMessage.Status = true;

            }
            catch (Exception ex)
            {
                statusMessage.Message = "Please try again.";
                statusMessage.Status = false;
            }

            return statusMessage;
        }

        public BankTransactionBO GetSpecificTransaction(int TID)
        {
            try
            {
                db = new DMSDbCon();
                String path = WebConfigurationManager.AppSettings["Url"].ToString();
                BankTransactionBO Transcation = (from bk in db.tbBankTransactions
                                                 join dp in db.tbDepotMsts
                                                     on bk.FKDepotId equals dp.PKDepotId
                                                 join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                                                 join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                                                 where bk.PKTransactionId == TID
                                                 select new BankTransactionBO
                                                 {
                                                     CreatedBy = bk.CreatedBy,
                                                     CreatedOn = bk.CreatedOn,
                                                     DepotName = dp.DepotTitle,
                                                     FKDepotId = dp.PKDepotId,
                                                     FKDSRId = bk.FKDSRId,
                                                     PKTransactionId = (int)bk.PKTransactionId,
                                                     Status = st.Status,
                                                     StatusId = st.StatusId,
                                                     TransactionAmount = bk.TransactionAmount,
                                                     TransactionDate = bk.TransactionDate,
                                                     TransactionImage = path + bk.TransactionImage,
                                                     DSRNo = dsr.DSRNo,
                                                     StatusGroup = st.StatusGroup,
                                                     RegionCode = dp.RegionCode
                                                 }).SingleOrDefault();

                return Transcation;



            }
            catch (Exception ex)
            {
                BankTransactionBO Transcation = null;

                return Transcation;
            }


        }

        public StatusMessage UpdateSpecifcTransaction(BankTransactionBO bankTransactionBO, string Username)
        {
            StatusMessage statusMessage = new StatusMessage();

            try
            {
                db = new DMSDbCon();
                tbBankTransaction record = db.tbBankTransactions.Find(bankTransactionBO.PKTransactionId);
                record.TransactionAmount = bankTransactionBO.TransactionAmount;
                record.CreatedBy = Username;
                record.CreatedOn = DateTime.Now;
                db.Entry(record).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                statusMessage.Status = true;
                statusMessage.Message = "Bank transaction successfully updated.";
            }
            catch (Exception ex)
            {
                statusMessage.Status = false;
                statusMessage.Message = "Please try again";
            }

            return statusMessage;
        }

        public BankTransListAPI List(DSRListParameter dSRListParameter)
        {
            BankTransListAPI BankTransListAPI = new BankTransListAPI();
            try
            {
                db = new DMSDbCon();
                DateTime currentdate = DateTime.Now;
                int days = dSRListParameter.NoOfDays - 1;
                currentdate = currentdate.AddDays(-days);
                String path = WebConfigurationManager.AppSettings["Url"].ToString();
                BankTransListAPI.TrnList = from bk in db.tbBankTransactions
                                           join dp in db.tbDepotMsts
                                               on bk.FKDepotId equals dp.PKDepotId
                                           join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                                           join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                                           join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                                           where bk.FKDepotId == dSRListParameter.DepotId && DbFunctions.TruncateTime(bk.TransactionDate) >= DbFunctions.TruncateTime(currentdate)
                                           select new BankTransactionAPIBO
                                           {
                                               CreatedBy = bk.CreatedBy,
                                               DepotName = dp.DepotTitle,
                                               FKDepotId = dp.PKDepotId,
                                               FKDSRId = bk.FKDSRId,
                                               PKTransactionId = (int)bk.PKTransactionId,
                                               Status = st.Status,
                                               StatusId = st.StatusId,
                                               TransactionAmount = bk.TransactionAmount,
                                               TransactionDate = bk.TransactionDate.ToString(),
                                               TransactionImage = path + bk.TransactionImage,
                                               DSRNo = dsr.DSRNo,
                                               StatusGroup = st.StatusGroup,
                                               RegionCode = dp.RegionCode,
                                               BankName = bkn.BankName,
                                               BankBranch = bkn.BankBranch
                                           };
                List<BankTransactionAPIBO> data = BankTransListAPI.TrnList.ToList();
                data.ForEach(x => x.TransactionDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(x.TransactionDate)));
                BankTransListAPI.TrnList = data as IEnumerable<BankTransactionAPIBO>;


            }
            catch (Exception ex)
            {
                BankTransListAPI = null;
            }

            return BankTransListAPI;
        }

        public SpecificTransBO SpecificTransactionAPI(int TRNNo)
        {
            SpecificTransBO SpecificTransBO = new SpecificTransBO();
            try
            {
                db = new DMSDbCon();

                String path = WebConfigurationManager.AppSettings["Url"].ToString();
                SpecificTransBO = (from bk in db.tbBankTransactions
                                   join dp in db.tbDepotMsts
                                       on bk.FKDepotId equals dp.PKDepotId
                                   join st in db.tbStatusMsts on bk.TransactionStatusId equals st.StatusId
                                   join dsr in db.tbDSRMsts on bk.FKDSRId equals dsr.PKDSRID
                                   join bkn in db.tbBankAccountsMsts on bk.FKBankId equals bkn.PKBankId
                                   where bk.PKTransactionId == TRNNo
                                   select new SpecificTransBO
                                            {
                                                CreatedBy = bk.CreatedBy,
                                                CreatedOn = bk.CreatedOn,
                                                DepotName = dp.DepotTitle,
                                                FKDepotId = dp.PKDepotId,
                                                FKDSRId = bk.FKDSRId,
                                                PKTransactionId = (int)bk.PKTransactionId,
                                                Status = st.Status,
                                                StatusId = st.StatusId,
                                                TransactionAmount = bk.TransactionAmount,
                                                TransactionDate = bk.TransactionDate.ToString(),
                                                TransactionImage = path + bk.TransactionImage,
                                                DSRNo = dsr.DSRNo,
                                                StatusGroup = st.StatusGroup,
                                                RegionCode = dp.RegionCode,
                                                BankName = bkn.BankName,
                                                BankBranch = bkn.BankBranch
                                            }).SingleOrDefault();
                SpecificTransBO.TransactionDate = DateOperation.FromDateTOMilli(Convert.ToDateTime(SpecificTransBO.TransactionDate));

            }
            catch (Exception ex)
            {
                SpecificTransBO = null;
            }

            return SpecificTransBO;



        }

        public StatusMessage CreateTRNApi(CreateTRNAPIBO bk)
        {
            tbBankTransaction record = new tbBankTransaction();
            DateTime Cdate = DateOperation.FromMilliToDate(bk.TransactionDate);
            StatusMessage status = new StatusMessage();
            try
            {
                db = new DMSDbCon();
                int sid = db.tbStatusMsts.Where(x => x.Role == "New").Select(x => x.StatusId).SingleOrDefault();
                int DSRId = db.tbDSRMsts.Where(x => x.DSRNo == bk.DSRNo).Select(x => x.PKDSRID).SingleOrDefault();
                if (DSRId == 0)
                {

                    status.Message = "Please enter valid DSR number.";
                    status.Status = false;
                    return status;
                }
                record.CreatedBy = bk.Username;
                record.CreatedOn = DateTime.Now;
                record.FKDepotId = bk.DepotId;
                record.FKDSRId = DSRId;
                record.TransactionStatusId = sid;
                record.TransactionAmount = bk.TransactionAmount;
                record.TransactionDate = Cdate;
                record.TransactionImage = bk.Path;
                record.FKBankId = bk.BankId;
                db.tbBankTransactions.Add(record);
                db.SaveChanges();
                status.Message = record.PKTransactionId.ToString();
                status.Status = true;
            }
            catch (Exception ex)
            {

                status.Message = ex.Message;
                status.Status = false;
            }

            return status;



        }

        public BankBranchResult BankBranch(BankBranchAPIBO bankBOAPI)
        {

            BankBranchResult data = new BankBranchResult();

            try
            {
                db = new DMSDbCon();
                string RegionCode = db.tbDepotMsts.Where(x => x.PKDepotId == bankBOAPI.DepotId).Select(x => x.RegionCode).SingleOrDefault();

                IEnumerable<tbBankAccountsMst> bank = db.tbBankAccountsMsts;


                data.BankList = bank.Where(x => x.ActiveStatus == 1).Select(
                       x => new BankBOAPI
                                         {
                                             BankId = x.PKBankId,
                                             BankName = x.BankName + '-' + x.BankBranch + '-' + x.AccountNumber
                                         });


                List<BankBOAPI> RE = data.BankList.ToList();


            }
            catch (Exception ex)
            {
                data = null;
            }

            return data;
        }

    }
}
