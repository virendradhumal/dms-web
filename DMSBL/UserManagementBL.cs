﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public class UserManagementBL
    {
        
        public IEnumerable<UserManagementModel> GetAllUser(FilterUsers filterUsers)
        {
            UserManagement userManagement = new UserManagement();
            return userManagement.GetAllUser(filterUsers);
        }


        public IEnumerable<UserManagementModel> GetUserForDetails(string Uid)
        {
            UserManagement userManagement = new UserManagement();


            return userManagement.GetUserForDetails(Uid);
        }

        public DataTable GetAllUser()
        {
            UserManagement userManagement = new UserManagement();
            return userManagement.GetAllUser();
        }
    }
}
