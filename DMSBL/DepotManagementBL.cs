﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMSDAL;
using DMSBO;
using System.Data;

namespace DMSBL
{
    public class DepotManagementBL
    {
        /// <summary>
        /// Returns all the list of Depots
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DepotBO> GetAllDepot()
        {
            DepotManagement depotManagement = new DepotManagement();
            return depotManagement.GetAllDepot();
        }

        public IEnumerable<DepotBO> GetAllDepot(string Username)
        {
            DepotManagement depotManagement = new DepotManagement();
            return depotManagement.GetAllDepot(Username);
        }
        public IEnumerable<DepotBO> GetAllDepots()
        {
            DepotManagement depotManagement = new DepotManagement();
            return depotManagement.GetAllDepots();
        }
        public IEnumerable<DepotList> GetAllDepotList(FilterDepots filterDepots)
        {

            DepotManagement depotManagement = new DepotManagement();
            return depotManagement.GetAllDepotList(filterDepots);
        }
        public IEnumerable<DepotListAPI> GetAllDepotList(DepotParameter filterDepots)
        {

            DepotManagement depotManagement = new DepotManagement();
            return depotManagement.GetAllDepotList(filterDepots);
        }

        public DataTable ExportDepotList()
        {
            DepotManagement depotManagement = new DepotManagement();
            return depotManagement.ExportDepotList(); 
        }
    }
}
