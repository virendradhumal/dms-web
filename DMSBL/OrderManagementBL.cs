﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
  public class OrderManagementBL
  {
    /// <summary>
    /// Returns all the orders depending upon filters applied to it
    /// </summary>
    /// <param name="filterOrders"></param>
    /// <returns></returns>
    public IEnumerable<OrderManageModal> GetAllOrder(FilterOrders filterOrders)
    {
      OrderManagement orderManagement = new OrderManagement();
      return orderManagement.GetAllOrder(filterOrders);
    }

    /// <summary>
    /// Get all the details of a specific order   
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public OrderAndDetail GetOrderAndOrderDetail(int id)
    {
      OrderManagement orderManagement = new OrderManagement();
      OrderAndDetail orderAndDetail = new OrderAndDetail();
      orderAndDetail.orderManageModal = orderManagement.GetSpecificOrder(id);
      orderAndDetail.orderDetailModal = orderManagement.GetOrderDetails(id);
      return orderAndDetail;
    }

    public StatusMessage InsertOrder(InsertOrderParameterApi insertOrderParameterApi)
    {
      StatusMessage statusMessage = new StatusMessage();
      OrderManagement orderManagement;
      try
      {

        orderManagement = new OrderManagement();
        statusMessage = orderManagement.InsertOrder(insertOrderParameterApi);

      }
      catch (Exception ex)
      {
        LogsException.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        statusMessage.Status = false;
        statusMessage.Message = "Order not Inserted try again";
      }
      return statusMessage;
    }

    public GetOrderResult GetOrderApi(GetOrderParameter getOrderParameter)
    {
      OrderManagement orderManagement = new OrderManagement();
      GetOrderResult getOrderResult = orderManagement.GetOrderApi(getOrderParameter);
      return getOrderResult;
    }


    public OrderDetailResultApi GetAllProductDetail(OrderDetailParameter orderDetailParameter)
    {
      ProductManagement productManagement = new ProductManagement();
      OrderDetailResultApi orderDetailResultApi = productManagement.GetAllProductDetail(orderDetailParameter);
      return orderDetailResultApi;
    }

    public StatusMessage UpdateOrderProduct(int? Odid, int? Quantity, string username)
    {

      OrderManagement orderManagement = new OrderManagement();

      return orderManagement.UpdateOrderProduct(Odid, Quantity, username);
    }

    public StatusMessage DeleteOrderProduct(int? Odid, string username)
    {
      OrderManagement orderManagement = new OrderManagement();
      return orderManagement.DeleteOrderProduct(Odid, username);
    }


  }
}
