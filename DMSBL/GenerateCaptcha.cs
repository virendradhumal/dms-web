﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
   public class GenerateCaptcha
    {
       /// <summary>
       /// Generates Random text for Captcha
       /// </summary>
       /// <returns></returns>
        public string GetRandomText()
        {
            StringBuilder randomText = new StringBuilder();
            string alphabets = "012345679";
            Random r = new Random();
            for (int j = 0; j <= 5; j++)
            {
                randomText.Append(alphabets[r.Next(alphabets.Length)]);
            }
            return randomText.ToString();
        }

    }
}
