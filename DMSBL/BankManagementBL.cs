﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public class BankManagementBL
    {
        public IEnumerable<BankBO> GetAllBankDetail(FilterBank filterBank)
        {
            BankManagement bankManagement= new BankManagement();
            IEnumerable<BankBO> BankList = bankManagement.GellAllBankDetail(filterBank); 
            return BankList;
        }

        public IEnumerable<BankNameBO> GetAllBankName()
        {
            BankManagement bankManagement = new BankManagement();
            IEnumerable<BankNameBO> BankList = bankManagement.GellAllBankName();
            return BankList;
        }

        public IEnumerable<BankAccountBO> GetAllBankAcc()
        {
            BankManagement bankManagement = new BankManagement();
            IEnumerable<BankAccountBO> BankList = bankManagement.GellAllBankAcc();
            return BankList;
        }

        public DataTable GetBankExport()
        {
            BankManagement bankManagement= new BankManagement();
            return bankManagement.GetBankExport(); 
           
        }

    }
}
