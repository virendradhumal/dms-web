﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public class IDNManagementBL
    {
        public StatusMessage CreateIDN(CreateIDNBO createIDNBO,string Username)
        {
            
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.CreateIDN(createIDNBO,Username);
        }


        public StatusMessage UpdateIDNPhoto(CreateIDNBO createIDNBO, string Username)
        {

            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.UpdateIDNPhoto(createIDNBO, Username);
        }

        public StatusMessage EditIDNAPI(CreateAPIIDNBO createIDNBO)
        {
            IDNManagement iDNManagement = new IDNManagement();
             return iDNManagement.EditIDNAPI(createIDNBO);
        }

        public StatusMessage CreateIDNOnPurchaseOrder(CreateIDNBO createIDNBO,string Username)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.CreateIDNOnPurchaseOrder(createIDNBO,Username);
        }


        public IDNAddProductBO GetIDNData(int IDNId)
        {
       
                IDNManagement iDNManagement = new IDNManagement();

                IDNAddProductBO iDNAddProductBO = iDNManagement.GetIDNData(IDNId);
                return iDNAddProductBO;
            
        }

        public IEnumerable<IDNBO> GetAllIDNData(FilterIDN filterIDN,string Username)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.GetAllIDNData(filterIDN, Username);
        }


        public ListIDNAPI GetAllIDNData(DSRListParameter dSRListParameter)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.GetAllIDNData(dSRListParameter);
        }

        public GetIDNAPIBO GetIDNData(string IDNNo)
        {

            IDNManagement iDNManagement = new IDNManagement();

            GetIDNAPIBO iDNAddProductBO = iDNManagement.GetIDNData(IDNNo);
            return iDNAddProductBO;

        }

        public StatusMessage CreateIDN(CreateAPIIDNBO createIDNBO)
        {

            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.CreateIDN(createIDNBO);
        }

        public StatusMessage DeleteIDNProduct(int? IDNDetailId, string username)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.DeleteIDNProduct(IDNDetailId, username);

        }


        public StatusMessage UpdateIDNProduct(int? Odid, int? Quantity, string username)
        {

            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.UpdateIDNProduct(Odid, Quantity, username);
        }

        public StatusMessage ApproveIDN(int Id, string Username)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.ApproveIDN(Id,Username);
        }



        public StatusMessage RejectIDN(int Id, string Message, string Username)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.RejectIDN(Id, Message, Username);
        }


        public StatusMessage ApproveIDN(string IDNNo, string Username)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.ApproveIDN(IDNNo, Username);
        }



        public StatusMessage RejectIDN(string IDNNo, string Message, string Username)
        {
            IDNManagement iDNManagement = new IDNManagement();
            return iDNManagement.RejectIDN(IDNNo, Message, Username);
        }


    }
}
