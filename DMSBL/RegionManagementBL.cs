﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public class RegionManagementBL
    {
        public IEnumerable<RegionBO> GetAllRegion()
        {

            RegionManagement regionManagement = new RegionManagement();
            return regionManagement.GetAllRegion();
        }

        public IEnumerable<RegionBO> GetAllRegion(string Username)
        {

            RegionManagement regionManagement = new RegionManagement();
            return regionManagement.GetAllRegion(Username);
        }
    }
}
