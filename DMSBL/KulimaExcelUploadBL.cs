﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public class KulimaExcelUploadBL
    {
        public void UploadBankData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
            kulimaExcelUpload.UploadBankData(fullpath,filename,Username);
        }
        public void UploadUserData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
            kulimaExcelUpload.UploadUsersData(fullpath, filename, Username);
        }
        public void UploadDepotData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
            kulimaExcelUpload.UploadDepotData(fullpath, filename, Username);
        }

        public void UploadProductData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
            kulimaExcelUpload.UploadProductData(fullpath, filename, Username);
        }

        public StatusMessage UploadDepotProductPriceData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
           return kulimaExcelUpload.UploadDepotProductPriceData(fullpath, filename, Username);
        }

        public StatusMessage UploadRegionProductPriceData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
          return  kulimaExcelUpload.UploadRegionProductPriceData(fullpath, filename, Username);
        }


        public void UploadDirectSupplierData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
            kulimaExcelUpload.UploadDirectSupplierData(fullpath, filename, Username);
        }

        public StatusMessage UploadDepotProductStockData(string fullpath, string filename, string Username)
        {
            KulimaExcelUpload kulimaExcelUpload = new KulimaExcelUpload();
            return kulimaExcelUpload.UploadDepotProductStockData(fullpath, filename, Username);
        }



        }
}
