﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace DMSBL
{
  /// <summary>
  /// This class is used to send the email to the user
  /// </summary>
  public class SendEmail
  {
    /// <summary>
    /// This Function send the email
    /// </summary>
    /// <param name="Subject">Actual Email Subject</param>
    /// <param name="Message">Actual Email Message</param>
    /// <param name="EmailTo">Email Sending To</param>
    public void Send(string Subject, string Message, String EmailTo)
    {

      using (MailMessage Mail = new MailMessage("Kulimagold DMS" + "<nihilent2017@gmail.com>", EmailTo))
      {
        Mail.Subject = Subject;
        Mail.Body = Message;
        Mail.IsBodyHtml = true;
        SmtpClient smtpClient = new SmtpClient();
        smtpClient.Send(Mail);
      }

    }

  }
}