﻿
using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public class DSRManagementBL
    {
        public IEnumerable<DSRListModel> GetAllDsr(DSRFilters searchModel, string username)
        {

            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.GetAllDsr(searchModel,username);

        }


        public StatusMessage UploadDSRImageApi(UploadDSRImageBO uploadDSRImageBO)
        {
            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.UploadDSRImageApi(uploadDSRImageBO);
        }


        public IEnumerable<ChartFormat> GetDsrGraph(string RegionCode, string DepotId)
        {
            
            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.GetDsrGraph(RegionCode, DepotId);
        }


        public IEnumerable<DSRListModel> GetAllDsrReport(DSRReportFilter searchModel)
        {
            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.GetAllDsrReport(searchModel);
        }



        public IEnumerable<DSRProductReport> GetAllDsrProductReport(DSRReportFilter searchModel)
        {
            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.GetAllDsrProductReport(searchModel);
        }


        public IEnumerable<DSRPCVReportBO> GetAllDsrPCVReport(DSRReportFilter searchModel)
        {
            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.GetAllDsrPCVReport(searchModel);
        }

        public IEnumerable<DSRPCSReportBO> GetAllDsrPCSReport(DSRReportFilter searchModel)
        {
            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.GetAllDsrPCSReport(searchModel);
        }


        public IEnumerable<DSRListExportModel> GetAllDsrExportReport(DSRReportFilter searchModel)
        {
            DSRManagement DSRManagement = new DSRManagement();
            return DSRManagement.GetAllDsrExportReport(searchModel);
        }

        public DSRListResult GetAllDsrApi(DSRListParameter dSRListParameter)
        {

            DSRManagement dSRManagement = new DSRManagement();
           return dSRManagement.GetAllDsrApi(dSRListParameter);

        }   


        public StatusMessage CreateDSRApi(CreateDSRParameter createDSRParameter)
        {
            DSRManagement dSRManagement = new DSRManagement();
            return dSRManagement.CreateDSRApi(createDSRParameter);
        }

        public DSRTodayOrderApiResult GetTodaysOrderApi(DSRListParameter dSRListParameter)
        {
            DSRManagement dSRManagement = new DSRManagement();
            return dSRManagement.GetTodaysOrderApi(dSRListParameter);
        }

        public StatusMessage ApproveDsr(int DsrId, string username)
        {
            DSRManagement dSRManagement = new DSRManagement();
            StatusMessage statusMessage = dSRManagement.ApproveDsr(DsrId, username);
            return statusMessage;
        }
        public StatusMessage RejectDsr(int DsrId, string message, string username)
        {
            DSRManagement dSRManagement = new DSRManagement();
            StatusMessage statusMessage = dSRManagement.RejectDsr(DsrId, message,username);
            return statusMessage;
        }

        public StatusMessage ApproveDsr(DSRApproveAPIBO para)
        {
            DSRManagement dSRManagement = new DSRManagement();
            StatusMessage statusMessage = dSRManagement.ApproveDsr(para);
            return statusMessage;
        }
        public StatusMessage RejectDsr(DSRApproveAPIBO para)
        {
            DSRManagement dSRManagement = new DSRManagement();
            StatusMessage statusMessage = dSRManagement.RejectDsr(para);
            return statusMessage;
        }
        
        public DSREditBO GetSpecificDSR(int DSRId)
        {
            DSRManagement dSRManagement = new DSRManagement();
            return dSRManagement.GetSpecificDSR(DSRId);
        }

        public StatusMessage UpdateSpecificDSR(DSREditBO dSREditBO, string username)
        {
            DSRManagement dSRManagement = new DSRManagement();
            return dSRManagement.UpdateSpecificDSR(dSREditBO, username);
        }

        public SpecificDSR GetSpecificDSR(string DSRNo)
        {
            DSRManagement dSRManagement = new DSRManagement();
            return dSRManagement.GetSpecificDSR(DSRNo);
        }
    }
}
