﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
   public class DirectSupplierManagementBL
    {
        public IEnumerable<DirectSupplierBO> GetList()
        {
            DirectSupplierManagement directSupplierManagementBL = new DirectSupplierManagement();
            return directSupplierManagementBL.GetList();
        }

        public IEnumerable<DirectSupplierAllBO> GetDList(FilterDirectSupplier filter)
        {
            DirectSupplierManagement directSupplierManagementBL = new DirectSupplierManagement();
            return directSupplierManagementBL.GetDList(filter);
        }

        public DataTable GetAllSupplierExport()
        {
            DirectSupplierManagement directSupplierManagementBL = new DirectSupplierManagement();
            return directSupplierManagementBL.GetAllSupplierExport();
        }

      
    }
}
