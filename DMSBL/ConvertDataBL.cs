﻿using DMSDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public static class ConvertDataBL
    {
        public static DataTable ToDataTable<T>(this List<T> items)
        {
           return ConvertData.ToDataTable<T>(items);
        }

    }
}
