﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
   public class GRRManagementBL
    {
        public IDNAddProductBO GetIDNData(int IDNId)
        {

            GRRManagement gRRManagement = new GRRManagement();

            IDNAddProductBO iDNAddProductBO = gRRManagement.GetIDNData(IDNId);
            return iDNAddProductBO;

        }

        public StatusMessage CreateGRR(CreateGRRBO createIDNBO, string Username)
        {


            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.CreateGRR(createIDNBO, Username);
        }



        public IEnumerable<IDNBO> GetAllIDNData(FilterIDN filterIDN,string Username)
        {
            GRRManagement gRRManagement = new GRRManagement();
       //     return gRRManagement.GetAllIDNData(filterIDN, Username);
            return gRRManagement.GetAllGRRData(filterIDN, Username);
        
        }


        public IEnumerable<GRRCreateIDNList> GetAllIDNDropDown(String Username)
        {
            GRRManagement gRRManagement = new GRRManagement();

           return  gRRManagement.GetAllIDNDropDown(Username);

        }
       
       public ListIDNAPI GetAllIDNGRRData(DSRListParameter dSRListParameter)
        {
            GRRManagement iDNManagement = new GRRManagement();
            return iDNManagement.GetAllIDNGRRData(dSRListParameter);
        }


        public StatusMessage UpdateToIDNAsGRR(IDNAddProductBO iDNAddProductBO, string username)
        {
            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.UpdateToIDNAsGRR(iDNAddProductBO, username);
        }

        public StatusMessage CreateGRR(IDNReceiveAPIBO para)
        {
            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.CreateGRR(para);
        }

        public StatusMessage ApproveGRR(int Id, string username)
        {
            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.ApproveGRR(Id, username);
        }

        public StatusMessage RejectGRR(int TID, string message, string username)
        {
            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.RejectGRR(TID, message, username);
        }

        public StatusMessage ApproveGRR(GRRApproveBO para)
        {
            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.ApproveGRR(para);
        }

        public StatusMessage RejectGRR(GRRApproveBO para)
        {
            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.RejectGRR(para);
        }



        public ListGRRAPI GetAllIDNData(DSRListParameter dSRListParameter)
        {
            GRRManagement gRRManagement = new GRRManagement();
            return gRRManagement.GetAllIDNData(dSRListParameter);
        }


        public GetGRRAPIBO GetIDNData(string IDNNo)
        {
            GRRManagement iDNManagement = new GRRManagement();
            GetGRRAPIBO iDNAddProductBO = iDNManagement.GetIDNData(IDNNo);
            return iDNAddProductBO;
        }

    }
}
