﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMSBO;
using DMSDAL;
using System.IO;
using System.Web.Security;
using System.Web.Configuration;
namespace DMSBL
{
    /// <summary>
    /// Bussiness Logic realated to User Operation like Authentication , Reset Password
    /// and Send OTP
    /// </summary>
    public class AuthenticateUser
    {
        /// <summary>
        /// Equates username , password entered by the users with database record
        /// </summary>
        /// <param name="username">usercode of user</param>
        /// <param name="password">password of user</param>
        /// <returns> true if user present</returns>
        public StatusMessage ValidateUser(string username,string password)
        {
            UserDetail u = new UserDetail();
            u.UserName = username;
            u.Password = password;
            Account account = new Account();
            return account.IsUserPresent(u);   
        }



        /// <summary>
        /// Equates username , password entered by the users with database record
        /// </summary>
        /// <param name="username">usercode of user</param>
        /// <param name="password">password of user</param>
        /// <returns> true if user present</returns>
        public StatusMessage ValidateUserApp(string username, string password)
        {
            UserDetail u = new UserDetail();
            u.UserName = username;
            u.Password = password;
            Account account = new Account();
            return account.IsUserPresentApp(u);
        }


        /// <summary>
        /// verify username and generate haskey and mail the key to the user on email and 
        /// </summary>
        /// <param name="username"></param>
        /// <returns>returs true when record updated</returns>
        public StatusMessage ForgotUser(string username)
        {
            Account account = new Account();
            StatusMessage statusMessage = new StatusMessage()
            {
                Status=false
            };
            if (account.IsUsernamePresent(username))
            {
                string Mail = account.GetUserEmail(username);
                Random r = new Random();
                int num =r.Next(10000, 99999);
                String Message;
                var appDomain = System.AppDomain.CurrentDomain;
                var basePath = appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
               // Path.Combine(basePath, "App_Data", "Welcome2.html");

                using (var sr = new StreamReader(Path.Combine(basePath,"..", "App_Data", "Welcome2.html")))
                {
                    Message = sr.ReadToEnd();
                }
                
                string hashotp = FormsAuthentication.HashPasswordForStoringInConfigFile(num.ToString(), "SHA1");
                string link = WebConfigurationManager.AppSettings["Url"].ToString()+"/Account/ResetPassword?key="+hashotp+"&username="+username;
                Message = string.Format(Message,username, username,Mail,link,link);
                string Subject = "Your password reset link for Depot Management System";
                SendEmail Email = new SendEmail();
                Email.Send(Subject, Message, Mail);
                account.UpdateResetkey(username, hashotp);
                statusMessage.Message = "Check your email for password reset instructions or contact your Super Admin for assistance.";
                statusMessage.Status = true;
                return statusMessage;
            }
            else
            {
                //username not present
                statusMessage.Message = "Not a valid Username.";
                return statusMessage;
            }

            
        }

        /// <summary>
        /// verify username, hashkey entered and update the new password
        /// </summary>
        /// <param name="changePassword"> holds the data related to reset code, username and new password </param>
        /// <returns> returns true after successful operation</returns>
        public StatusMessage ResetUserPassword(ChangePassword changePassword)
        {
            Account account = new Account();
            StatusMessage statusMessage= account.UpdatePassword(changePassword);
            if (statusMessage.Status)
            {
                return statusMessage;
            }
            else
            {
                //username not present
                return statusMessage;
            }


        }

        /// <summary>
        /// verify username and genrete otp and mail the otp to user on email and
        /// save the otp hashed in database
        /// </summary>
        /// <param name="username"></param>
        /// <returns> true with successful operation</returns>
        public StatusMessage UserOtp(string username)
        {
            Account account = new Account();
            StatusMessage statusMessage = new StatusMessage();
             
            if (account.IsUsernamePresent(username))
            {
                string Mail = account.GetUserEmail(username);
                Random r = new Random();
                int num = r.Next(10000, 99999);
                String Message;
                var appDomain = System.AppDomain.CurrentDomain;
                var basePath = appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
                // Path.Combine(basePath, "App_Data", "Welcome2.html");

                using (var sr = new StreamReader(Path.Combine(basePath, "..", "App_Data", "Welcome3.html")))
                {
                    Message = sr.ReadToEnd();
                }
                string hashotp = FormsAuthentication.HashPasswordForStoringInConfigFile(num.ToString(), "SHA1");
                Message = string.Format(Message, username, username, Mail, num.ToString());
                string Subject = "OTP for superadmin to login into Depot Management System";
                SendEmail Email = new SendEmail();
                Email.Send(Subject, Message, Mail);
                account.UpdateOtp(username, hashotp);
                statusMessage.Message = "An OTP has been sent on you email id.";
                statusMessage.Status = true;
                return statusMessage;
            }
            else
            {
                //username not present
                statusMessage.Message = "Please try again later.";
                statusMessage.Status = true;
                return statusMessage;
            }


        }

        /// <summary>
        /// Verify the username and the otp in the database
        /// </summary>
        /// <param name="username"></param>
        /// <param name="otp"></param>
        /// <returns>true if user is authentic</returns>
        public StatusMessage IsValidOTP(string username, string otp)
        {
            Account account = new Account();
          string hashotp=  FormsAuthentication.HashPasswordForStoringInConfigFile(otp,"SHA1");
            StatusMessage statusMessage= account.CheckOTP(username,hashotp);

            if (statusMessage.Status)
            {
                return statusMessage;
            }
            else
            {
                //username not present
                return statusMessage;
            }

        }

        /// <summary>
        /// checks whether entered username is admin or not
        /// </summary>
        /// <param name="username"></param>
        /// <returns>true if admin</returns>
        public bool isAdmin(string username)
        {
            Account account = new Account();
            if (account.GetUserRole(username) == "SPADM")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetUser(string username)
        {
            Account account = new Account();
            
            return account.GetUserRole(username);

        }

        public LoginResult AutheticateUserApi(LoginParameter loginParameter)
        {
            Account account = new Account();

            return account.AutheticateUserApi(loginParameter);

        }

        public StatusMessage RequestChangePassword(ChangePasswordUser data)
        {
            Account account = new Account();

            return account.RequestChangePassword(data);
        }

        
        public static string UserNameRole(string name)
        {
            Account account = new Account();
            return account.UserNameRole(name);
        }

    }
}
