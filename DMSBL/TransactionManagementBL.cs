﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
   public  class TransactionManagementBL
    {


         public IEnumerable<BankTransactionBO> GetAllTransaction(FilterTransaction filterTransaction,string Username)
        {
            try
            {

                TransactionManagement transactionManagement = new TransactionManagement();
                return transactionManagement.GetAllTransaction(filterTransaction, Username);


            }
            catch (Exception ex)
            {
                IEnumerable<BankTransactionBO> Transcation = null;

                return Transcation;
            }


        }

         public IEnumerable<BankTransactionBO> GetAllTransaction(FilterTransaction filterTransaction)
         {
             try
             {

                 TransactionManagement transactionManagement = new TransactionManagement();
                 return transactionManagement.GetAllTransaction(filterTransaction);


             }
             catch (Exception ex)
             {
                 IEnumerable<BankTransactionBO> Transcation = null;

                 return Transcation;
             }


         }

         public IEnumerable<BankTransactionExportBO> GetAllExportTransaction(FilterTransaction filterTransaction)
         {
             try
             {

                 TransactionManagement transactionManagement = new TransactionManagement();
                 return transactionManagement.GetAllExportTransaction(filterTransaction);


             }
             catch (Exception ex)
             {
                 IEnumerable<BankTransactionExportBO> Transcation = null;

                 return Transcation;
             }


         }

         public StatusMessage ApproveTransaction(int TID, string username)
       {
           TransactionManagement Transaction= new TransactionManagement();
           return Transaction.ApproveTransaction(TID, username);
       }

         public StatusMessage RejectTransaction(int TID, string message, string username)
       {
           TransactionManagement Transaction = new TransactionManagement();
           return Transaction.RejectTransaction( TID,  message,  username);
       }


         public StatusMessage ApproveTransaction(TransactionApproveAPIBO para)
         {
             TransactionManagement Transaction = new TransactionManagement();
             return Transaction.ApproveTransaction(para);
         }

         public StatusMessage RejectTransaction(TransactionApproveAPIBO para)
         {
             TransactionManagement Transaction = new TransactionManagement();
             return Transaction.RejectTransaction(para);
         }


         public BankTransactionBO GetSpecificTransaction(int TID)
         {
             TransactionManagement transactionManagement = new TransactionManagement();

             return transactionManagement.GetSpecificTransaction(TID);
         }

         public StatusMessage UpdateSpecifcTransaction(BankTransactionBO bankTransactionBO, string Username)
         {
             TransactionManagement transactionManagement = new TransactionManagement();

             return transactionManagement.UpdateSpecifcTransaction(bankTransactionBO, Username);
         }

         public BankTransListAPI List(DSRListParameter dSRListParameter)
         {

             TransactionManagement transactionManagement = new TransactionManagement();
             return transactionManagement.List(dSRListParameter);
         }

         public SpecificTransBO SpecificTransactionAPI(int TRNNo)
         {
             TransactionManagement transactionManagement = new TransactionManagement();
             return transactionManagement.SpecificTransactionAPI(TRNNo);

         }

         public StatusMessage CreateTRNApi(CreateTRNAPIBO BankTransactionBO)
         {
             TransactionManagement transactionManagement = new TransactionManagement();
             return transactionManagement.CreateTRNApi(BankTransactionBO);
         }

         public BankBranchResult BankBranch(BankBranchAPIBO bankBOAPI)
         {
             TransactionManagement transactionManagement = new TransactionManagement();
             return transactionManagement.BankBranch(bankBOAPI);

         }
    
   }


}
