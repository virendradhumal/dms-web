﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
    public class PurchaseOrdersBL
    {

        public IEnumerable<OrderManageModal> GetAllOrder(FilterPOrder searchModel,string Username)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return purchaseOrders.GetAllOrder(searchModel,Username);
        }

        public GetPOrderResult GetOrderApi(GetOrderParameter getOrderParameter)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return purchaseOrders.GetOrderApi(getOrderParameter);
        }

        public POrderDetailResultApi GetAllProductDetail(OrderDetailParameter orderDetailParameter)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return purchaseOrders.GetAllProductDetail(orderDetailParameter);
        }

        public StatusMessage InsertOrder(InsertOrderParameterApi insertOrderParameterApi)
        {

            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return purchaseOrders.InsertOrder(insertOrderParameterApi);
       
        }

        public StatusMessage ApproveOrder(PurchaseOrderApproveAPIBO para)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return purchaseOrders.ApproveOrder(para);
       
        }

        public StatusMessage RejectOrder(PurchaseOrderApproveAPIBO para)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return purchaseOrders.RejectOrder(para);
       
            
        }

        public OrderAndDetail GetOrderAndOrderDetail(int id)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
           
            OrderAndDetail orderAndDetail = new OrderAndDetail();
            orderAndDetail.orderManageModal = purchaseOrders.GetSpecificOrder(id);
            orderAndDetail.orderDetailModal = purchaseOrders.GetOrderDetails(id);
            return orderAndDetail;
        }

        public IEnumerable<PurchaseOrderReportExportBO> GetAllReportPurchaseOrder(FilterPOrder searchModel)
        {
            PurchaseOrders purchaseOrders = new PurchaseOrders();
            return purchaseOrders.GetAllReportPurchaseOrder(searchModel);
        }

    }
}
