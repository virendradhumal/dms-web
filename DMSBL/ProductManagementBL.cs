﻿using DMSBO;
using DMSDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSBL
{
  public class ProductManagementBL
  {
    public IEnumerable<ProductBO> GetAllProduct(FilterProduct filterProduct)
    {
      ProductManagement productManagement = new ProductManagement();
      IEnumerable<ProductBO> ProductList = productManagement.GetAllProduct(filterProduct);
      return ProductList;
    }

    public DataTable GetAllProductExport()
    {
      ProductManagement productManagement = new ProductManagement();
      return productManagement.GetAllProductExport();

    }


    public DataTable GetAllDepotWisePriceExport(String Username)
    {
      ProductManagement productManagement = new ProductManagement();
      return productManagement.GetAllDepotWisePriceExport(Username);
    }

    public DataTable GetAllDepotWiseStockExport(String Username)
    {
      ProductManagement productManagement = new ProductManagement();
      return productManagement.GetAllDepotWiseStockExport(Username);
    }



    public IEnumerable<ProductAllDetailBO> GetAllProductDetail()
    {
      ProductManagement productManagement = new ProductManagement();
      IEnumerable<ProductAllDetailBO> ProductList = productManagement.GetAllProductDetail();
      return ProductList;
    }

    public IEnumerable<ProductAllDetailBOApi> GetAllProductDetailApi()
    {
      ProductManagement productManagement = new ProductManagement();
      IEnumerable<ProductAllDetailBOApi> ProductList = productManagement.GetAllProductDetailApi();
      return ProductList;
    }

    public IEnumerable<ProductGroupBO> GetAllProductGroup()
    {
      ProductManagement productManagement = new ProductManagement();
      IEnumerable<ProductGroupBO> ProductList = productManagement.GetAllProductGroup();
      return ProductList;
    }

    public DSRProductRate GetProductRate(int Oid, string ProductName)
    {
      ProductManagement productManagement = new ProductManagement();
      return productManagement.GetProductRate(Oid, ProductName);
    }


    public StatusMessage AddDSRProduct(DSREditAddProduct dSREditAddProduct)
    {
      ProductManagement productManagement = new ProductManagement();
      return productManagement.AddDSRProduct(dSREditAddProduct);
    }

    public StatusMessage AddIDNProduct(DSREditAddProduct dSREditAddProduct)
    {
      ProductManagement productManagement = new ProductManagement();
      return productManagement.AddIDNProduct(dSREditAddProduct);
    }

    public StatusMessage AddEditIDNProduct(DSREditAddProduct dSREditAddProduct)
    {
      ProductManagement productManagement = new ProductManagement();
      var result = productManagement.AddIDNProduct(dSREditAddProduct);

      IDNManagement iDNManagement = new IDNManagement();
      var result2 = iDNManagement.UpdateStatus(dSREditAddProduct.OrderId, dSREditAddProduct.Username);

      if (result2.Status)
      {
        return result;
      }
      else
      {
        return result2;
      }

    }

    public IEnumerable<ProductStockReportBO> GetAllProductStockReport(ProductStockFilter filter)
    {
      ProductManagement obj = new ProductManagement();
      return obj.GetAllProductStockReport(filter);

    }

    public ProductDetailApiBO GetProductDetailApi(int FkDepotId, string ProductCode)
    {
      ProductManagement productManagement = new ProductManagement();
      ProductDetailApiBO ProductDetail = productManagement.GetProductDetailApi(FkDepotId, ProductCode);
      return ProductDetail;
    }
  }
}
