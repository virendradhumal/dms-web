﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DMS.Models
{
    [Serializable]
    [DataContract]
    public class ErrorStatus
    {
         [DataMember]
        public string ErrorCode { get; set; }
    }
}