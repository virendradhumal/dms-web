﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DMS.Models
{[Serializable]
  [DataContract]
    public class ProductParameter
    {
    [DataMember]
        public string SearchParameter { get; set; }
    [DataMember]
    public int DepotId { get; set; }
    }
}