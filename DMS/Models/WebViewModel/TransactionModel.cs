﻿using DMSBO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class TransactionModel
    {
        public int? DepotId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Status { get; set; }
        public string RegionCode { get; set; }
        public int NoOfTransaction { get; set; }
        public decimal? NetAmount { get; set; }
        public IPagedList<BankTransactionBO> bankTransactionBO { get; set; }
    }
}