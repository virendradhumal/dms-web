﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class OrderModel
    {
        public IPagedList<DMSBO.OrderManageModal> orderManageModal{get;set;}
        public int ?DepotId { get; set; }
        public string OrderNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
    }
}