﻿using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class ProductModel
    {
        public string ProductCodeOrName { get; set; }
        public string ProductGroup { get; set; }
        public int? Active { get; set; }
        public IEnumerable<ProductBO> ProductList { get; set; }
    }
}