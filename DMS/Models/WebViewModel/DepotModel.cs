﻿using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class DepotModel
    {
        public IEnumerable<DepotList> depotList { get; set; }
        public string DepotName { get; set; }

        public int? Active { get; set; }
    }
}