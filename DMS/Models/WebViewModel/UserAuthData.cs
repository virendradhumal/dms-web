﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class UserAuthData
    {
        [Required(ErrorMessage = "Please enter Username.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please enter Password.")]
        public string Password { get; set; }
         [Required(ErrorMessage="Please enter Security Code.")]
        public string CapthaValue { get; set; }
        [Required]
        public bool Remember { get; set; }
    }
}