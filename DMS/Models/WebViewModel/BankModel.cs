﻿using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class BankModel
    {
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public int? Active { get; set; }
        public IEnumerable<BankBO> BankList { get; set; }
    }
}