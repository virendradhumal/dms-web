﻿using DMSBO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class IDNModel
    {
        public int? SourceDepotId { get; set; }
        public int? DestinationDepotId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Status { get; set; }
        public IPagedList<IDNBO> iDNBO { get; set; }
    }
}