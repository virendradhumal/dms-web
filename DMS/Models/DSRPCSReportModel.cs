﻿using DMSBO;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class DSRPCSReportModel
    {
        public int? DepotId { get; set; }
        public string DSRNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Status { get; set; }
        public IPagedList<DSRPCSReportBO> dSRListModel { get; set; }
        public int? PageCount { get; set; }
        public int? PageNumber { get; set; }
        public int? NoofProducts { get; set; }
        public Decimal? TotalAmount { get; set; }
        public Decimal? TotalDiscountAmount { get; set; }
        public Decimal? TotalNetAmount { get; set; }
        public string RegionCode { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (FromDate > ToDate)
            {
                yield return
                  new ValidationResult(errorMessage: "ToDate must be greater than FromDate",
                                       memberNames: new[] { "EndDate" });
            }
        }
   
    }
}