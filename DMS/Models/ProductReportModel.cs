﻿using DMSBO;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DMS.Models
{
    public class ProductReportModel
    {
        public string RegionCode { get; set; }
        public int? DepotId { get; set; }
        public string ProductCodeOrName { get; set; }
        public string ProductGroup { get; set; }
        public int? Active { get; set; }
        public IPagedList<ProductStockReportBO> dSRListModel { get; set; }
        public int? PageCount { get; set; }
        public int? PageNumber { get; set; }
       
    }
}