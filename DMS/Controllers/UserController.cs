﻿using ClosedXML.Excel;
using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Threading;

namespace DMS.Controllers
{
  public class UserController : Controller
  {

    public ActionResult Dashboard()
    {

      DepotManagementBL depotManagementBL = new DepotManagementBL();

      ViewBag.Depots = depotManagementBL.GetAllDepot();
      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();
      FilterTransaction f = new FilterTransaction();
      return View(f);
    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult OrderList(FilterOrders filterOrders, int? page)
    {
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      OrderManagementBL orderManageModal = new OrderManagementBL();
      IEnumerable<OrderManageModal> orderManageModals = orderManageModal.GetAllOrder(filterOrders);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      OrderModel model = new OrderModel();

      model.orderManageModal = orderManageModals.OrderByDescending(x => x.OrderDate).ToPagedList(pageIndex, pageSize); ;

      if (filterOrders.DepotId.HasValue)
        model.DepotId = (int)filterOrders.DepotId;
      model.FromDate = filterOrders.FromDate;
      model.ToDate = filterOrders.ToDate;
      model.OrderNo = filterOrders.OrderNo;
      return View(model);
    }


    public ActionResult OrderDetails(string Oid)
    {
      int key = Convert.ToInt32(EncryptOperation.Decode(Oid));
      OrderManagementBL orderManagementBL = new OrderManagementBL();
      return View(orderManagementBL.GetOrderAndOrderDetail(key));
    }


    [Authorize(Roles = "SPADM,HOSPR,DOMGR,HOCLK,DOCLK")]
    public ActionResult PurchaseOrderList(FilterPOrder filterOrders, int? page)
    {
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      PurchaseOrdersBL orderManageModal = new PurchaseOrdersBL();
      IEnumerable<OrderManageModal> orderManageModals = orderManageModal.GetAllOrder(filterOrders, User.Identity.Name);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();

      if (User.IsInRole("DOMGR") || User.IsInRole("DOCLK"))
      {
        ViewBag.Depots = depotManagementBL.GetAllDepot(User.Identity.Name);
      }

      OrderModel model = new OrderModel();

      model.orderManageModal = orderManageModals.OrderByDescending(x => x.OrderDate).ToPagedList(pageIndex, pageSize); ;

      if (filterOrders.DepotId.HasValue)
        model.DepotId = (int)filterOrders.DepotId;
      model.FromDate = filterOrders.FromDate;
      model.ToDate = filterOrders.ToDate;
      model.OrderNo = filterOrders.OrderNo;

      ViewBag.StatusStages = from PurchaseOrderStatus e in Enum.GetValues(typeof(PurchaseOrderStatus))
                             select new { Id = (int)e, Name = e.ToString() };
      if (!String.IsNullOrEmpty(filterOrders.Status))
      {
        model.Status = filterOrders.Status;
      }

      return View(model);
    }


    public ActionResult PurchaseOrderDetails(string Oid)
    {
      int key = Convert.ToInt32(EncryptOperation.Decode(Oid));
      PurchaseOrdersBL purchaseOrdersBL = new PurchaseOrdersBL();
      return View(purchaseOrdersBL.GetOrderAndOrderDetail(key));
    }

    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult CreateIDNOnParchaseOrder(string Oid)
    {
      int key = Convert.ToInt32(EncryptOperation.Decode(Oid));
      PurchaseOrdersBL purchaseOrdersBL = new PurchaseOrdersBL();
      return View(purchaseOrdersBL.GetOrderAndOrderDetail(key));
    }


    [HttpPost]
    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult CreateIDNOnParchaseOrder(CreateIDNBO create)
    {
      StatusMessage statusMessage = new StatusMessage();

      IDNManagementBL iDNManagementBL = new IDNManagementBL();
      create.Pathname = Server.MapPath("~");
      statusMessage = iDNManagementBL.CreateIDNOnPurchaseOrder(create, User.Identity.Name);
      if (statusMessage.Status)
      {
        int IDNID = Convert.ToInt32(statusMessage.Message);
        statusMessage.Message = "IDN Created Successfully";
        Session["StatusMessage"] = statusMessage;
        return RedirectToAction("IDNList", "User");
      }
      else
      {
        Session["StatusMessage"] = statusMessage;
        PurchaseOrdersBL purchaseOrdersBL = new PurchaseOrdersBL();
        string key = EncryptOperation.Encode(create.OrderId.ToString());
        return RedirectToAction("CreateIDNOnParchaseOrder", "User", new { key = key });
      }
    }



    [Authorize(Roles = "SPADM")]
    public ActionResult ProductList(FilterProduct filterProduct)
    {
      ProductManagementBL ProductManagementBL = new ProductManagementBL();
      ProductModel productModel = new ProductModel();
      productModel.ProductList = ProductManagementBL.GetAllProduct(filterProduct);
      productModel.ProductCodeOrName = filterProduct.ProductCodeOrName;
      if (!string.IsNullOrEmpty(filterProduct.ProductGroup))
        productModel.ProductGroup = filterProduct.ProductGroup;
      if (filterProduct.Active.HasValue)
        productModel.Active = filterProduct.Active;


      ViewBag.ProductGroupList = ProductManagementBL.GetAllProductGroup();
      ViewBag.ItemStatus = from ItemStatus e in Enum.GetValues(typeof(ItemStatus))
                           select new { Id = (int)e, Name = e.ToString() };

      return View(productModel);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM")]
    public ActionResult ExportProductData(string ActionPost, HttpPostedFileBase ProductFile)
    {
      ProductManagementBL productManagementBL = new ProductManagementBL();

      if (ActionPost == "Export")
      {
        DataTable dt = productManagementBL.GetAllProductExport();

        using (XLWorkbook wb = new XLWorkbook())
        {
          wb.Worksheets.Add(dt, "ProductMaster");
          wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
          wb.Style.Font.Bold = true;
          Response.Clear();
          Response.Buffer = true;
          Response.Charset = "";
          Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
          Response.AddHeader("content-disposition", "attachment;filename= ProductMaster.xlsx");

          using (MemoryStream MyMemoryStream = new MemoryStream())
          {
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
          }
        }
      }
      if (ActionPost == "Upload")
      {
        string xlsname = String.Format("ProductMaster-{0}", DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(ProductFile.FileName);
        string Path = Server.MapPath("~") + "Data\\ProductMaster\\" + xlsname;
        ProductFile.SaveAs(Path);
        KulimaExcelUploadBL kulimaExcelUploadBL = new KulimaExcelUploadBL();
        kulimaExcelUploadBL.UploadProductData(Path, xlsname, User.Identity.Name);
      }

      return RedirectToAction("ProductList", "User");
    }

    [Authorize(Roles = "SPADM")]
    public ActionResult DirectSupplierList(FilterDirectSupplier filter)
    {
      ViewBag.ItemStatus = from ItemStatus e in Enum.GetValues(typeof(ItemStatus))
                           select new { Id = (int)e, Name = e.ToString() };


      DirectSupplierManagementBL obj = new DirectSupplierManagementBL();


      DirectSupplierListBO listobj = new DirectSupplierListBO();
      listobj.List = obj.GetDList(filter);

      if (filter.Active.HasValue)
      {
        listobj.Active = filter.Active;
      }

      if (!string.IsNullOrEmpty(filter.DirectSupplierCodeOrName))
      {
        listobj.DirectSupplierCodeOrName = filter.DirectSupplierCodeOrName;
      }
      return View(listobj);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM")]
    public ActionResult ExportDirectSupplierData(string ActionPost, HttpPostedFileBase ProductFile)
    {
      DirectSupplierManagementBL obj = new DirectSupplierManagementBL();

      if (ActionPost == "Export")
      {
        DataTable dt = obj.GetAllSupplierExport();

        using (XLWorkbook wb = new XLWorkbook())
        {
          wb.Worksheets.Add(dt, "DirectSupplierMaster");
          wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
          wb.Style.Font.Bold = true;
          Response.Clear();
          Response.Buffer = true;
          Response.Charset = "";
          Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
          Response.AddHeader("content-disposition", "attachment;filename= DirectSupplierMaster.xlsx");

          using (MemoryStream MyMemoryStream = new MemoryStream())
          {
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
          }
        }
      }
      if (ActionPost == "Upload")
      {
        string xlsname = String.Format("DirectSupplierMaster-{0}", DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(ProductFile.FileName);
        string Path = Server.MapPath("~") + "Data\\DirectSupplierMaster\\" + xlsname;
        ProductFile.SaveAs(Path);
        KulimaExcelUploadBL kulimaExcelUploadBL = new KulimaExcelUploadBL();
        kulimaExcelUploadBL.UploadDirectSupplierData(Path, xlsname, User.Identity.Name);
      }

      return RedirectToAction("DirectSupplierList", "User");
    }

    [HttpPost]
    public ActionResult DirectCNjson(String Prefix)
    {
      List<DirectSupplierAllBO> ObjList;
      DirectSupplierManagementBL obj = new DirectSupplierManagementBL();
      FilterDirectSupplier filter = new FilterDirectSupplier();
      var ObjLists = obj.GetDList(filter);
      //Searching records from list using LINQ query  
      ObjList = ObjLists.ToList<DirectSupplierAllBO>();
      var List = (from N in ObjList
                  where N.SupplierCode.ToUpper().Contains(Prefix.ToUpper()) || N.SupplierName.ToUpper().Contains(Prefix.ToUpper())
                  select new { N.SupplierName });
      return Json(List, JsonRequestBehavior.AllowGet);
    }


    [Authorize(Roles = "SPADM")]
    public ActionResult UploadProductPriceList()
    {
      return View();
    }

    [HttpPost]
    public ActionResult ProductCNjson(String Prefix)
    {
      List<ProductBO> ObjList;
      ProductManagementBL ProductManagementBL = new ProductManagementBL();
      FilterProduct filterProduct = new FilterProduct();
      var ObjLists = ProductManagementBL.GetAllProduct(filterProduct);
      //Searching records from list using LINQ query  
      ObjList = ObjLists.ToList<ProductBO>();
      var ProductList = (from N in ObjList
                         where N.ProductCode.ToUpper().Contains(Prefix.ToUpper()) || N.ProductName.ToUpper().Contains(Prefix.ToUpper())
                         select new { N.ProductName });
      return Json(ProductList, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult DsrReportjson(string rcode, string dpid)
    {
      DSRManagementBL d = new DSRManagementBL();
      return Json(d.GetDsrGraph(rcode, dpid), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult Productratejson(String name, int Oid)
    {
      ProductManagementBL productManagementBL = new ProductManagementBL();
      DSRProductRate data = productManagementBL.GetProductRate(Oid, name);
      return Json(data, JsonRequestBehavior.AllowGet);
    }

    [Authorize(Roles = "SPADM")]
    public ActionResult DepotList(FilterDepots filterDepots)
    {
      DepotModel depotModel = new DepotModel();
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      depotModel.depotList = depotManagementBL.GetAllDepotList(filterDepots);
      if (filterDepots.Active.HasValue)
        depotModel.Active = filterDepots.Active;

      depotModel.DepotName = filterDepots.DepotName;

      ViewBag.ItemStatus = from ItemStatus e in Enum.GetValues(typeof(ItemStatus))
                           select new { Id = (int)e, Name = e.ToString() };

      return View(depotModel);
    }

    [Authorize(Roles = "SPADM")]
    public ActionResult ExportDepotData(string ActionPost, HttpPostedFileBase DepotFile)
    {
      DepotManagementBL depotManagementBL = new DepotManagementBL();

      if (ActionPost == "Export")
      {
        DataTable dt = depotManagementBL.ExportDepotList();

        using (XLWorkbook wb = new XLWorkbook())
        {
          wb.Worksheets.Add(dt, "DepotMaster");
          wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
          wb.Style.Font.Bold = true;
          Response.Clear();
          Response.Buffer = true;
          Response.Charset = "";
          Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
          Response.AddHeader("content-disposition", "attachment;filename= DepotMaster.xlsx");

          using (MemoryStream MyMemoryStream = new MemoryStream())
          {
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
          }
        }
      }
      if (ActionPost == "Upload")
      {
        string xlsname = String.Format("DepotMaster-{0}", DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(DepotFile.FileName);
        string Path = Server.MapPath("~") + "Data\\DepotMaster\\" + xlsname;
        DepotFile.SaveAs(Path);
        KulimaExcelUploadBL kulimaExcelUploadBL = new KulimaExcelUploadBL();
        kulimaExcelUploadBL.UploadDepotData(Path, xlsname, User.Identity.Name);
      }

      return RedirectToAction("DepotList", "User");
    }

    public ActionResult Depotjson(String Prefix)
    {
      List<DepotBO> ObjList;
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      var ObjLists = depotManagementBL.GetAllDepot();
      //Searching records from list using LINQ query  
      ObjList = ObjLists.ToList<DepotBO>();
      var DepotList = (from N in ObjList
                       where N.DepotName.Contains(Prefix.ToUpper())
                       select new { N.DepotName });
      return Json(DepotList, JsonRequestBehavior.AllowGet);
    }

    [Authorize(Roles = "SPADM")]
    public ActionResult BankList(FilterBank filterBank, string ActionPost)
    {
      BankManagementBL bankManagementBL = new BankManagementBL();
      BankModel bankModel = new BankModel();
      bankModel.BankList = bankManagementBL.GetAllBankDetail(filterBank);

      bankModel.AccountNo = filterBank.AccountNo;
      bankModel.BankName = filterBank.BankName;
      if (filterBank.Active.HasValue)
        bankModel.Active = filterBank.Active;

      ViewBag.ItemStatus = from ItemStatus e in Enum.GetValues(typeof(ItemStatus))
                           select new { Id = (int)e, Name = e.ToString() };

      return View(bankModel);
    }

    [Authorize(Roles = "SPADM")]
    public ActionResult UploadBankAccount(string ActionPost, HttpPostedFileBase BankFile)
    {
      BankManagementBL bankManagementBL = new BankManagementBL();

      if (ActionPost == "Export")
      {
        DataTable dt = bankManagementBL.GetBankExport();

        using (XLWorkbook wb = new XLWorkbook())
        {
          wb.Worksheets.Add(dt, "BankMaster");
          wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
          wb.Style.Font.Bold = true;

          Response.Clear();
          Response.Buffer = true;
          Response.Charset = "";
          Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
          Response.AddHeader("content-disposition", "attachment;filename= BankMaster.xlsx");

          using (MemoryStream MyMemoryStream = new MemoryStream())
          {
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
          }
        }
      }
      if (ActionPost == "Upload")
      {
        string xlsname = String.Format("BankMaster-{0}", DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(BankFile.FileName);
        string Path = Server.MapPath("~") + "Data\\BankMaster\\" + xlsname;
        BankFile.SaveAs(Path);
        KulimaExcelUploadBL kulimaExcelUploadBL = new KulimaExcelUploadBL();
        kulimaExcelUploadBL.UploadBankData(Path, xlsname, User.Identity.Name);
      }

      return RedirectToAction("BankList", "User");
    }

    [HttpPost]
    public ActionResult bankjson(String Prefix)
    {
      List<BankNameBO> ObjList;
      BankManagementBL bankManagementBL = new BankManagementBL();
      var ObjLists = bankManagementBL.GetAllBankName();
      //Searching records from list using LINQ query  
      ObjList = ObjLists.ToList<BankNameBO>();
      var BankList = (from N in ObjList
                      where N.BankName.ToUpper().Contains(Prefix.ToUpper())
                      select new { N.BankName });
      return Json(BankList, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult bankaccjson(String Prefix)
    {
      List<BankAccountBO> ObjList;
      BankManagementBL bankManagementBL = new BankManagementBL();
      var ObjLists = bankManagementBL.GetAllBankAcc();
      //Searching records from list using LINQ query  
      ObjList = ObjLists.ToList<BankAccountBO>();
      var BankList = (from N in ObjList
                      where N.AccountNo.ToUpper().Contains(Prefix.ToUpper())
                      select new { N.AccountNo });
      return Json(BankList, JsonRequestBehavior.AllowGet);
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT,ACCLK")]
    public ActionResult DSRList(DSRFilters dSRFilters, int? page)
    {

      DSRModel dSRModel = new DSRModel();

      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      DSRManagementBL DSRManagementBL = new DSRManagementBL();

      var data = DSRManagementBL.GetAllDsr(dSRFilters, User.Identity.Name);

      if (!ModelState.IsValid)
      {
        DSRFilters d = new DSRFilters();
        data = DSRManagementBL.GetAllDsr(d, User.Identity.Name);
      }


      dSRModel.dSRListModel = data.OrderByDescending(x => x.DSRDate).ToPagedList(pageIndex, pageSize);

      DepotManagementBL depotManagementBL = new DepotManagementBL();

      ViewBag.Depots = depotManagementBL.GetAllDepot();



      if (User.IsInRole("ACTNT") || User.IsInRole("ACCLK"))
      {
        ViewBag.Depots = depotManagementBL.GetAllDepot(User.Identity.Name);
      }

      ViewBag.StatusStages = from StatusStages e in Enum.GetValues(typeof(StatusStages))
                             select new { Id = (int)e, Name = e.ToString() };
      if (dSRFilters.DepotId.HasValue)
      {
        dSRModel.DepotId = (int)dSRFilters.DepotId;
      }


      if (!String.IsNullOrEmpty(dSRFilters.Status))
      {
        dSRModel.Status = dSRFilters.Status;
      }
      if (dSRFilters.FromDate.HasValue)
        dSRModel.FromDate = dSRFilters.FromDate;
      if (dSRFilters.ToDate.HasValue)
        dSRModel.ToDate = dSRFilters.ToDate;


      return View(dSRModel);

    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult DSRReport(DSRReportFilter dSRFilters, int? page)
    {

      Session["DSRRFilter"] = dSRFilters;

      DSRModel dSRModel = new DSRModel();
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      DSRManagementBL DSRManagementBL = new DSRManagementBL();

      var data = DSRManagementBL.GetAllDsrReport(dSRFilters);
      var list = data.ToList();

      dSRModel.NoofDSR = list.Count;
      dSRModel.TotalAmount = (list.Sum(x => x.DSRTotal));
      dSRModel.TotalDiscountAmount = (list.Sum(x => x.DSRDiscount));
      dSRModel.TotalNetAmount = (list.Sum(x => x.DSRTotalSale));

      dSRModel.dSRListModel = data.OrderByDescending(x => x.DSRDate).ToPagedList(pageIndex, pageSize);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();



      ViewBag.StatusStages = from StatusStages e in Enum.GetValues(typeof(StatusStages))
                             select new { Id = (int)e, Name = e.ToString() };

      if (dSRFilters.DepotId.HasValue)
      {
        dSRModel.DepotId = (int)dSRFilters.DepotId;
      }


      if (!String.IsNullOrEmpty(dSRFilters.Status))
      {
        dSRModel.Status = dSRFilters.Status;
      }

      if (!String.IsNullOrEmpty(dSRFilters.DSRNo))
      {
        dSRModel.DSRNo = dSRFilters.DSRNo;
      }


      if (dSRFilters.FromDate.HasValue)
        dSRModel.FromDate = dSRFilters.FromDate;

      if (dSRFilters.ToDate.HasValue)
        dSRModel.ToDate = dSRFilters.ToDate;


      return View(dSRModel);

    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult DSRRProductReport(DSRReportFilter dSRFilters, int? page)
    {

      Session["DSRRFilter"] = dSRFilters;

      DSRReportListModel dSRModel = new DSRReportListModel();
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      DSRManagementBL DSRManagementBL = new DSRManagementBL();

      var data = DSRManagementBL.GetAllDsrProductReport(dSRFilters);
      var list = data.ToList();

      dSRModel.NoofProducts = list.Count;
      dSRModel.TotalAmount = (list.Sum(x => x.SalePriceInclusive));

      dSRModel.dSRListModel = data.OrderByDescending(x => x.DSRDate).ToPagedList(pageIndex, pageSize);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();



      ViewBag.StatusStages = from StatusStages e in Enum.GetValues(typeof(StatusStages))
                             select new { Id = (int)e, Name = e.ToString() };

      if (dSRFilters.DepotId.HasValue)
      {
        dSRModel.DepotId = (int)dSRFilters.DepotId;
      }


      if (!String.IsNullOrEmpty(dSRFilters.Status))
      {
        dSRModel.Status = dSRFilters.Status;
      }

      if (!String.IsNullOrEmpty(dSRFilters.DSRNo))
      {
        dSRModel.DSRNo = dSRFilters.DSRNo;
      }


      if (dSRFilters.FromDate.HasValue)
        dSRModel.FromDate = dSRFilters.FromDate;

      if (dSRFilters.ToDate.HasValue)
        dSRModel.ToDate = dSRFilters.ToDate;


      return View(dSRModel);

    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult DSRPCVReport(DSRReportFilter dSRFilters, int? page)
    {

      Session["DSRRFilter"] = dSRFilters;

      DSRPCVReportModel dSRModel = new DSRPCVReportModel();
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      DSRManagementBL DSRManagementBL = new DSRManagementBL();

      var data = DSRManagementBL.GetAllDsrPCVReport(dSRFilters);
      var list = data.ToList();

      dSRModel.NoofProducts = list.Count;
      dSRModel.TotalAmount = (list.Sum(x => x.PCVAmount));

      dSRModel.dSRListModel = data.OrderByDescending(x => x.DSRDate).ToPagedList(pageIndex, pageSize);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();



      ViewBag.StatusStages = from StatusStages e in Enum.GetValues(typeof(StatusStages))
                             select new { Id = (int)e, Name = e.ToString() };

      if (dSRFilters.DepotId.HasValue)
      {
        dSRModel.DepotId = (int)dSRFilters.DepotId;
      }


      if (!String.IsNullOrEmpty(dSRFilters.Status))
      {
        dSRModel.Status = dSRFilters.Status;
      }

      if (!String.IsNullOrEmpty(dSRFilters.DSRNo))
      {
        dSRModel.DSRNo = dSRFilters.DSRNo;
      }


      if (dSRFilters.FromDate.HasValue)
        dSRModel.FromDate = dSRFilters.FromDate;

      if (dSRFilters.ToDate.HasValue)
        dSRModel.ToDate = dSRFilters.ToDate;


      return View(dSRModel);

    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult ExportDSRPCVReportData()
    {
      DSRReportFilter dSRFilters = new DSRReportFilter();
      if (Session["DSRRFilter"] != null)
      {
        dSRFilters = (DSRReportFilter)Session["DSRRFilter"];
      }

      DSRManagementBL DSRManagementBL = new DSRManagementBL();
      var data = DSRManagementBL.GetAllDsrPCVReport(dSRFilters);
      var list = data.ToList();

      DataTable dt = ConvertDataBL.ToDataTable<DSRPCVReportBO>(list);



      DataRow InsertRow = dt.NewRow();
      InsertRow["PCVNo"] = "Total";
      InsertRow["PCVAmount"] = (list.Sum(x => x.PCVAmount));

      dt.Rows.Add(InsertRow);
      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "PCVReport");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= PCVReport.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("DSRReport", dSRFilters);
    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult DSRPCSReport(DSRReportFilter dSRFilters, int? page)
    {

      Session["DSRRFilter"] = dSRFilters;

      DSRPCSReportModel dSRModel = new DSRPCSReportModel();
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      DSRManagementBL DSRManagementBL = new DSRManagementBL();

      var data = DSRManagementBL.GetAllDsrPCSReport(dSRFilters);
      var list = data.ToList();

      dSRModel.NoofProducts = list.Count;
      dSRModel.TotalAmount = (list.Sum(x => x.PCSAmount));

      dSRModel.dSRListModel = data.OrderByDescending(x => x.DSRDate).ToPagedList(pageIndex, pageSize);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();



      ViewBag.StatusStages = from StatusStages e in Enum.GetValues(typeof(StatusStages))
                             select new { Id = (int)e, Name = e.ToString() };

      if (dSRFilters.DepotId.HasValue)
      {
        dSRModel.DepotId = (int)dSRFilters.DepotId;
      }


      if (!String.IsNullOrEmpty(dSRFilters.Status))
      {
        dSRModel.Status = dSRFilters.Status;
      }

      if (!String.IsNullOrEmpty(dSRFilters.DSRNo))
      {
        dSRModel.DSRNo = dSRFilters.DSRNo;
      }


      if (dSRFilters.FromDate.HasValue)
        dSRModel.FromDate = dSRFilters.FromDate;

      if (dSRFilters.ToDate.HasValue)
        dSRModel.ToDate = dSRFilters.ToDate;


      return View(dSRModel);

    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult ExportDSRPCSReportData()
    {
      DSRReportFilter dSRFilters = new DSRReportFilter();
      if (Session["DSRRFilter"] != null)
      {
        dSRFilters = (DSRReportFilter)Session["DSRRFilter"];
      }

      DSRManagementBL DSRManagementBL = new DSRManagementBL();
      var data = DSRManagementBL.GetAllDsrPCSReport(dSRFilters);
      var list = data.ToList();

      DataTable dt = ConvertDataBL.ToDataTable<DSRPCSReportBO>(list);



      DataRow InsertRow = dt.NewRow();
      InsertRow["PCSNo"] = "Total";
      InsertRow["PCSAmount"] = (list.Sum(x => x.PCSAmount));

      dt.Rows.Add(InsertRow);
      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "PCSReport");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= PCSReport.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("DSRReport", dSRFilters);
    }


    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult ProductStockReport(ProductStockFilter Filters, int? page)
    {
      //if (Session["ProductFilter"] != null)
      //Filters = (ProductStockFilter)Session["ProductFilter"];
      //else
      Session["ProductFilter"] = Filters;

      ProductReportModel dSRModel = new ProductReportModel();
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      ProductManagementBL productManagementBL = new ProductManagementBL();

      var data = productManagementBL.GetAllProductStockReport(Filters);
      var list = data.ToList();


      dSRModel.dSRListModel = data.OrderByDescending(x => x.DepotId).ToPagedList(pageIndex, pageSize);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();

      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();

      ProductManagementBL ProductManagementBL = new ProductManagementBL();
      ProductModel productModel = new ProductModel();

      productModel.ProductCodeOrName = Filters.ProductCodeOrName;
      if (!string.IsNullOrEmpty(Filters.ProductGroup))
        productModel.ProductGroup = Filters.ProductGroup;

      if (Filters.Active.HasValue)
        productModel.Active = Filters.Active;


      ViewBag.ProductGroupList = ProductManagementBL.GetAllProductGroup();
      ViewBag.ItemStatus = from ItemStatus e in Enum.GetValues(typeof(ItemStatus))
                           select new { Id = (int)e, Name = e.ToString() };




      if (Filters.DepotId.HasValue)
      {
        dSRModel.DepotId = (int)Filters.DepotId;
      }




      return View(dSRModel);

    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult ProductStockReportExport()
    {
      ProductStockFilter dSRFilters = new ProductStockFilter();
      if (Session["ProductFilter"] != null)
      {
        dSRFilters = (ProductStockFilter)Session["ProductFilter"];

      }

      ProductManagementBL obj = new ProductManagementBL();
      var data = obj.GetAllProductStockReport(dSRFilters);
      var list = data.ToList();

      DataTable dt = ConvertDataBL.ToDataTable<ProductStockReportBO>(list);




      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "StockValuationReport");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= StockValuationReport.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("ProductStockReport", dSRFilters);
    }



    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult PurchaseOrderReport(FilterPOrder filterOrders, int? page)
    {
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      Session["PurchaseFilter"] = filterOrders;
      PurchaseOrdersBL orderManageModal = new PurchaseOrdersBL();
      IEnumerable<PurchaseOrderReportExportBO> orderManageModals = orderManageModal.GetAllReportPurchaseOrder(filterOrders);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();


      PurchaseOrderReportModel model = new PurchaseOrderReportModel();

      model.orderManageModal = orderManageModals.OrderByDescending(x => x.PODate).ToPagedList(pageIndex, pageSize); ;

      if (filterOrders.DepotId.HasValue)
        model.DepotId = (int)filterOrders.DepotId;
      model.FromDate = filterOrders.FromDate;
      model.ToDate = filterOrders.ToDate;
      model.OrderNo = filterOrders.OrderNo;

      ViewBag.StatusStages = from PurchaseOrderStatus e in Enum.GetValues(typeof(PurchaseOrderStatus))
                             select new { Id = (int)e, Name = e.ToString() };
      if (!String.IsNullOrEmpty(filterOrders.Status))
      {
        model.Status = filterOrders.Status;
      }

      return View(model);
    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult PurchaseOrderReportExport()
    {
      FilterPOrder dSRFilters = new FilterPOrder();
      if (Session["PurchaseFilter"] != null)
      {
        dSRFilters = (FilterPOrder)Session["PurchaseFilter"];

      }

      PurchaseOrdersBL obj = new PurchaseOrdersBL();
      var data = obj.GetAllReportPurchaseOrder(dSRFilters);
      var list = data.ToList();

      DataTable dt = ConvertDataBL.ToDataTable<PurchaseOrderReportExportBO>(list);




      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "PurchaseOrderReport");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= PurchaseOrderReport.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("PurchaseOrderReport", dSRFilters);
    }



    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult BankTransactionReport(FilterTransaction filterTransaction, int? page)
    {
      Session["DSRRFilter"] = filterTransaction;

      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();



      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();


      TransactionManagementBL transactionManagementBL = new TransactionManagementBL();
      TransactionModel model = new TransactionModel();


      if (filterTransaction.DepotId.HasValue)
      {
        model.DepotId = (int)filterTransaction.DepotId;
      }


      if (!String.IsNullOrEmpty(filterTransaction.Status))
      {
        model.Status = filterTransaction.Status;
      }

      if (!String.IsNullOrEmpty(filterTransaction.RegionCode))
      {
        model.RegionCode = filterTransaction.RegionCode;
      }

      if (filterTransaction.FromDate.HasValue)
        model.FromDate = filterTransaction.FromDate;

      if (filterTransaction.ToDate.HasValue)
        model.ToDate = filterTransaction.ToDate;



      if (ModelState.IsValid)
      {
        model.bankTransactionBO = transactionManagementBL.GetAllTransaction(filterTransaction).OrderByDescending(x => x.TransactionDate).ToPagedList(pageIndex, pageSize);

      }
      else
      {
        FilterTransaction obj = new FilterTransaction();
        model.bankTransactionBO = transactionManagementBL.GetAllTransaction(obj).OrderByDescending(x => x.TransactionDate).ToPagedList(pageIndex, pageSize);

      }
      var list = model.bankTransactionBO.ToList();

      model.NoOfTransaction = list.Count;
      model.NetAmount = (list.Sum(x => x.TransactionAmount));


      return View(model);

    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult ExportDSRReportData()
    {
      DSRReportFilter dSRFilters = new DSRReportFilter();
      if (Session["DSRRFilter"] != null)
      {
        dSRFilters = (DSRReportFilter)Session["DSRRFilter"];
      }

      DSRManagementBL DSRManagementBL = new DSRManagementBL();
      var data = DSRManagementBL.GetAllDsrExportReport(dSRFilters);
      var list = data.ToList();

      DataTable dt = ConvertDataBL.ToDataTable<DSRListExportModel>(list);



      DataRow InsertRow = dt.NewRow();
      InsertRow["DSRNo"] = "Total";
      InsertRow["TotalValueInclusive"] = (list.Sum(x => x.TotalValueInclusive));
      InsertRow["PCVAmount"] = (list.Sum(x => x.PCVAmount));
      InsertRow["DSRNetTotal"] = (list.Sum(x => x.DSRNetTotal));

      dt.Rows.Add(InsertRow);
      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "DSRSummaryReport");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= DSRSummaryReport.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("DSRReport", dSRFilters);
    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult ExportDSRProductReportData()
    {
      DSRReportFilter dSRFilters = new DSRReportFilter();
      if (Session["DSRRFilter"] != null)
      {
        dSRFilters = (DSRReportFilter)Session["DSRRFilter"];
      }

      DSRManagementBL DSRManagementBL = new DSRManagementBL();
      var data = DSRManagementBL.GetAllDsrProductReport(dSRFilters);
      var list = data.ToList();

      DataTable dt = ConvertDataBL.ToDataTable<DSRProductReport>(list);



      DataRow InsertRow = dt.NewRow();
      InsertRow["ItemDescription"] = "Total";
      InsertRow["SalePriceInclusive"] = (list.Sum(x => x.SalePriceInclusive));

      dt.Rows.Add(InsertRow);
      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "DSRReport");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= DSRReport.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("DSRReport", dSRFilters);
    }

    [Authorize(Roles = "SPADM,RGMGR,ACTNT")]
    public ActionResult ExportTransactionReportData()
    {
      FilterTransaction filterTransaction = new FilterTransaction();
      if (Session["DSRRFilter"] != null)
      {
        filterTransaction = (FilterTransaction)Session["DSRRFilter"];
      }

      TransactionManagementBL transactionManagementBL = new TransactionManagementBL();
      var data = transactionManagementBL.GetAllExportTransaction(filterTransaction);
      var list = data.ToList();

      DataTable dt = ConvertDataBL.ToDataTable<BankTransactionExportBO>(list);



      DataRow InsertRow = dt.NewRow();
      InsertRow["BankBranch"] = "Total";
      InsertRow["TransactionAmount"] = (list.Sum(x => x.TransactionAmount));
      dt.Rows.Add(InsertRow);
      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "BankTransactionReport");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= BankTransactionReport.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("BankTransactionReport", filterTransaction);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT,ACCLK")]
    public ActionResult ApproveDSR(int Id)
    {
      DSRManagementBL DSRManagementBL = new DSRManagementBL();
      StatusMessage statusMessage = DSRManagementBL.ApproveDsr(Id, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return RedirectToAction("DSRList", "User");
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT,ACCLK")]
    public ActionResult RejectDSR(int Id, string comment)
    {
      DSRManagementBL DSRManagementBL = new DSRManagementBL();
      StatusMessage statusMessage = DSRManagementBL.RejectDsr(Id, comment, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return RedirectToAction("DSRList", "User");
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT")]
    public ActionResult EditDSR(string Key)
    {

      int id = Convert.ToInt32(EncryptOperation.Decode(Key));
      DSRManagementBL dSRManagementBL = new DSRManagementBL();
      return View(dSRManagementBL.GetSpecificDSR(id));
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT,ACCLK")]
    public ActionResult DSRDetails(string Key)
    {
      int id = Convert.ToInt32(EncryptOperation.Decode(Key));
      DSRManagementBL dSRManagementBL = new DSRManagementBL();
      return View(dSRManagementBL.GetSpecificDSR(id));
    }

    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult EditOrderDetails(string Oid)
    {
      int key = Convert.ToInt32(EncryptOperation.Decode(Oid));
      OrderManagementBL orderManagementBL = new OrderManagementBL();
      return View(orderManagementBL.GetOrderAndOrderDetail(key));
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult UpdateOrderProduct(int? Odid, int? Oid, int? Quantity)
    {
      OrderManagementBL orderManagementBL = new OrderManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      statusMessage = orderManagementBL.UpdateOrderProduct(Odid, Quantity, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      string key = EncryptOperation.Encode(Oid.ToString());
      return RedirectToAction("EditOrderDetails", "User", new { Oid = key });
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult DeleteOrderProduct(int? Id, int? Oid)
    {
      OrderManagementBL orderManagementBL = new OrderManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      statusMessage = orderManagementBL.DeleteOrderProduct(Id, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      string key = EncryptOperation.Encode(Oid.ToString());
      return RedirectToAction("EditOrderDetails", "User", new { Oid = key });
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult AddDSROrderProduct(DSREditAddProduct dSREditAddProduct)
    {
      ProductManagementBL productManagementBL = new ProductManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      statusMessage = productManagementBL.AddDSRProduct(dSREditAddProduct);
      Session["StatusMessage"] = statusMessage;
      string key = EncryptOperation.Encode(dSREditAddProduct.OrderId.ToString());
      return RedirectToAction("EditOrderDetails", "User", new { Oid = key });
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult EditDSR(DSREditBO dSREditBO, string Update, string UpdateConfirm)
    {
      DSRManagementBL dSRManagementBL = new DSRManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      statusMessage = dSRManagementBL.UpdateSpecificDSR(dSREditBO, User.Identity.Name);
      if (statusMessage.Status && !String.IsNullOrEmpty(UpdateConfirm))
      {
        statusMessage = dSRManagementBL.ApproveDsr((int)dSREditBO.DSRId, User.Identity.Name);
        Session["StatusMessage"] = statusMessage;
      }
      else
      {
        Session["StatusMessage"] = statusMessage;
      }
      return View(dSRManagementBL.GetSpecificDSR((int)dSREditBO.DSRId));
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT,ACCLK")]
    public ActionResult BankTransactionList(FilterTransaction filterTransaction, int? page)
    {
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();

      if (User.IsInRole("ACTNT") || User.IsInRole("ACCLK"))
      {
        ViewBag.Depots = depotManagementBL.GetAllDepot(User.Identity.Name);
      }


      RegionManagementBL regionManagementBL = new RegionManagementBL();
      ViewBag.Regions = regionManagementBL.GetAllRegion();

      if (User.IsInRole("ACTNT") || User.IsInRole("ACCLK"))
      {
        ViewBag.Regions = regionManagementBL.GetAllRegion(User.Identity.Name);
      }


      ViewBag.StatusStages = from StatusStages e in Enum.GetValues(typeof(StatusStages))
                             select new { Id = (int)e, Name = e.ToString() };

      TransactionManagementBL transactionManagementBL = new TransactionManagementBL();
      TransactionModel model = new TransactionModel();


      if (filterTransaction.DepotId.HasValue)
      {
        model.DepotId = (int)filterTransaction.DepotId;
      }


      if (!String.IsNullOrEmpty(filterTransaction.Status))
      {
        model.Status = filterTransaction.Status;
      }

      if (!String.IsNullOrEmpty(filterTransaction.RegionCode))
      {
        model.RegionCode = filterTransaction.RegionCode;
      }

      if (filterTransaction.FromDate.HasValue)
        model.FromDate = filterTransaction.FromDate;

      if (filterTransaction.ToDate.HasValue)
        model.ToDate = filterTransaction.ToDate;



      if (ModelState.IsValid)
      {
        model.bankTransactionBO = transactionManagementBL.GetAllTransaction(filterTransaction, User.Identity.Name).OrderByDescending(x => x.TransactionDate).ToPagedList(pageIndex, pageSize);

      }
      else
      {
        FilterTransaction obj = new FilterTransaction();
        model.bankTransactionBO = transactionManagementBL.GetAllTransaction(obj, User.Identity.Name).OrderByDescending(x => x.TransactionDate).ToPagedList(pageIndex, pageSize);

      }

      return View(model);

    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT,ACCLK")]
    public ActionResult ApproveTransaction(int Id)
    {
      TransactionManagementBL transactionManagement = new TransactionManagementBL();
      StatusMessage statusMessage = transactionManagement.ApproveTransaction(Id, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return RedirectToAction("BankTransactionList", "User");
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,HOCLK,ACTNT,ACCLK")]
    public ActionResult RejectTransaction(int Id, string comment)
    {
      TransactionManagementBL transactionManagement = new TransactionManagementBL();
      StatusMessage statusMessage = transactionManagement.RejectTransaction(Id, comment, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return RedirectToAction("BankTransactionList", "User");
    }

    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult EditTransaction(string Key)
    {
      int id = Convert.ToInt32(EncryptOperation.Decode(Key));
      TransactionManagementBL transactionManagement = new TransactionManagementBL();
      return View(transactionManagement.GetSpecificTransaction(id));
    }
    [HttpPost]

    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult EditTransaction(BankTransactionBO bankTransactionBO, string Update)
    {
      TransactionManagementBL transactionManagement = new TransactionManagementBL();
      StatusMessage statusMessage = transactionManagement.UpdateSpecifcTransaction(bankTransactionBO, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      if (Update.Equals("UpdateApprove"))
      {
        transactionManagement.ApproveTransaction(bankTransactionBO.PKTransactionId, User.Identity.Name);

      }

      string key = EncryptOperation.Encode(bankTransactionBO.PKTransactionId.ToString());
      return RedirectToAction("EditTransaction", "User", new { key = key });
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,DOMGR,DOCLK")]
    public ActionResult IDNList(FilterIDN filterIDN, int? page)
    {
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;


      IDNManagementBL iDNManagementBL = new IDNManagementBL();
      IDNModel iDNModel = new IDNModel();

      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepots();

      if (User.IsInRole("DOMGR") || User.IsInRole("DOCLK"))
      {
        ViewBag.Depots = depotManagementBL.GetAllDepot(User.Identity.Name);
      }
      ViewBag.StatusStages = from IDNStatusStages e in Enum.GetValues(typeof(IDNStatusStages))
                             select new { Id = (int)e, Name = e.ToString() };

      var datas = iDNManagementBL.GetAllIDNData(filterIDN, User.Identity.Name);

      iDNModel.iDNBO = datas.OrderByDescending(x => x.IDNDate).ToPagedList(pageIndex, pageSize);

      if (!ModelState.IsValid)
      {

        FilterIDN obj = new FilterIDN();
        iDNModel.iDNBO = iDNManagementBL.GetAllIDNData(obj, User.Identity.Name).OrderByDescending(x => x.IDNDate).ToPagedList(pageIndex, pageSize);

      }
      if (filterIDN.SourceDepotId.HasValue)
      {
        iDNModel.SourceDepotId = (int)filterIDN.SourceDepotId;
      }

      if (filterIDN.DestinationDepotId.HasValue)
      {
        iDNModel.DestinationDepotId = (int)filterIDN.DestinationDepotId;
      }

      if (!String.IsNullOrEmpty(filterIDN.Status))
      {
        iDNModel.Status = filterIDN.Status;
      }
      if (filterIDN.FromDate.HasValue)
        iDNModel.FromDate = filterIDN.FromDate;
      if (filterIDN.ToDate.HasValue)
        iDNModel.ToDate = filterIDN.ToDate;
      return View(iDNModel);
    }

    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult CompleteIDN()
    {
      StatusMessage s = new StatusMessage();
      s.Message = "IDN Created Successfully";
      s.Status = true;
      Session["StatusMessage"] = s;
      return RedirectToAction("IDNList", "User");
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,DOMGR,DOCLK")]
    public ActionResult IDNDetails(string Key)
    {
      int Id = Convert.ToInt32(EncryptOperation.Decode(Key));
      IDNManagementBL iDNManagement = new IDNManagementBL();
      IDNAddProductBO iDNAddProductBO = iDNManagement.GetIDNData(Id);
      return View(iDNAddProductBO);
    }


    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult CreateIDN()
    {
      CreateIDNBO create = new CreateIDNBO();
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      if (User.IsInRole("DOMGR") || User.IsInRole("DOCLK"))
      {
        ViewBag.Depots = depotManagementBL.GetAllDepot(User.Identity.Name);
      }

      return View(create);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult CreateIDN(CreateIDNBO create)
    {
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      ViewBag.Depots = depotManagementBL.GetAllDepot();

      if (User.IsInRole("DOMGR") || User.IsInRole("DOCLK"))
      {
        ViewBag.Depots = depotManagementBL.GetAllDepot(User.Identity.Name);
      }

      IDNManagementBL iDNManagementBL = new IDNManagementBL();
      create.Pathname = Server.MapPath("~");
      statusMessage = iDNManagementBL.CreateIDN(create, User.Identity.Name);
      if (statusMessage.Status)
      {
        int IDNID = Convert.ToInt32(statusMessage.Message);
        statusMessage.Message = "Please Add products to IDN.";
        Session["StatusMessage"] = statusMessage;
        string key = EncryptOperation.Encode(IDNID.ToString());
        return RedirectToAction("AddProductToIDN", "User", new { Key = key });
      }
      else
      {
        Session["StatusMessage"] = statusMessage;
        return View(create);

      }
    }


    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult AddProductToIDN(string Key)
    {
      int id = Convert.ToInt32(EncryptOperation.Decode(Key));
      IDNManagementBL iDNManagement = new IDNManagementBL();
      IDNAddProductBO iDNAddProductBO = iDNManagement.GetIDNData(id);
      return View(iDNAddProductBO);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult AddIDNProduct(DSREditAddProduct dSREditAddProduct)
    {
      ProductManagementBL productManagementBL = new ProductManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      dSREditAddProduct.Username = User.Identity.Name;
      statusMessage = productManagementBL.AddIDNProduct(dSREditAddProduct);
      Session["StatusMessage"] = statusMessage;
      string key = EncryptOperation.Encode(dSREditAddProduct.OrderId.ToString());
      return RedirectToAction("AddProductToIDN", "User", new { Key = key });
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,DOMGR,DOCLK")]
    public ActionResult GRRList(FilterIDN filterIDN, int? page)
    {
      int pageSize = 10;
      int pageIndex = 1;
      pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

      GRRManagementBL iDNManagementBL = new GRRManagementBL();
      IDNModel iDNModel = new IDNModel();

      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepots();
      if (User.IsInRole("DOMGR") || User.IsInRole("DOCLK"))
      {
        ViewBag.Depots = depotManagementBL.GetAllDepot(User.Identity.Name);
      }
      ViewBag.StatusStages = from IDNStatusStages e in Enum.GetValues(typeof(IDNStatusStages))
                             select new { Id = (int)e, Name = e.ToString() };

      if (ModelState.IsValid)
      {
        iDNModel.iDNBO = iDNManagementBL.GetAllIDNData(filterIDN, User.Identity.Name).OrderByDescending(x => x.IDNDate).ToPagedList(pageIndex, pageSize);
      }
      else
      {
        FilterIDN obj = new FilterIDN();
        iDNModel.iDNBO = iDNManagementBL.GetAllIDNData(obj, User.Identity.Name).OrderByDescending(x => x.IDNDate).ToPagedList(pageIndex, pageSize);
      }

      if (filterIDN.SourceDepotId.HasValue)
      {
        iDNModel.SourceDepotId = (int)filterIDN.SourceDepotId;
      }

      if (filterIDN.DestinationDepotId.HasValue)
      {
        iDNModel.DestinationDepotId = (int)filterIDN.DestinationDepotId;
      }

      if (!String.IsNullOrEmpty(filterIDN.Status))
      {
        iDNModel.Status = filterIDN.Status;
      }
      if (filterIDN.FromDate.HasValue)
        iDNModel.FromDate = filterIDN.FromDate;
      if (filterIDN.ToDate.HasValue)
        iDNModel.ToDate = filterIDN.ToDate;
      return View(iDNModel);
    }

    [Authorize(Roles = "SPADM,HOSPR,HOCLK,DOMGR,DOCLK")]
    public ActionResult GRRDetails(string Key)
    {
      int id = Convert.ToInt32(EncryptOperation.Decode(Key));
      GRRManagementBL iDNManagement = new GRRManagementBL();
      IDNAddProductBO iDNAddProductBO = iDNManagement.GetIDNData(id);
      return View(iDNAddProductBO);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,HOCLK,DOMGR,DOCLK")]
    public ActionResult GRRDetails(IDNAddProductBO Data, string PostAction)
    {
      GRRManagementBL gRRManagementBL = new GRRManagementBL();
      Data.PathName = Server.MapPath("~");

      StatusMessage statusMessage = new StatusMessage();

      if (PostAction.ToLower() == "received")
      {
        statusMessage = gRRManagementBL.UpdateToIDNAsGRR(Data, User.Identity.Name);
      }

      if (PostAction.ToLower() == "approved")
      {
        statusMessage = gRRManagementBL.ApproveGRR(Data.IDNId, User.Identity.Name);
      }
      Session["StatusMessage"] = statusMessage;

      string key = EncryptOperation.Encode(Data.IDNId.ToString());
      return RedirectToAction("GRRList", "User", new { Key = key });
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,HOCLK")]
    public ActionResult RejectGRR(int Id, string comment)
    {
      GRRManagementBL gRRManagement = new GRRManagementBL();
      StatusMessage statusMessage = gRRManagement.RejectGRR(Id, comment, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return RedirectToAction("GRRList", "User");
    }

    [Authorize(Roles = "SPADM")]
    public ActionResult UserList()
    {
      FilterUsers filterUsers = new FilterUsers();
      UserManagementBL userManagementModel = new UserManagementBL();
      IEnumerable<UserManagementModel> userManagementModels = userManagementModel.GetAllUser(filterUsers);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      UserModel model = new UserModel();
      model.userManagementModel = userManagementModels;

      return View(model);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM")]
    public ActionResult UserList(FilterUsers filterUsers)
    {
      //FilterUsers filterUsers = new FilterUsers();
      UserManagementBL userManagementModel = new UserManagementBL();
      IEnumerable<UserManagementModel> userManagementModels = userManagementModel.GetAllUser(filterUsers);
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      UserModel model = new UserModel();
      model.userManagementModel = userManagementModels;

      if (filterUsers.DepotId.HasValue)
        model.DepotId = (int)filterUsers.DepotId;

      model.Region = filterUsers.Region;
      model.RoleCode = filterUsers.RoleCode;
      model.Name = filterUsers.Name;

      return View(model);
    }

    [Authorize(Roles = "SPADM")]

    public ActionResult UserDetails(string Uid)
    {
      FilterUsers filterUsers = new FilterUsers();
      UserManagementBL userManagementModel = new UserManagementBL();
      IEnumerable<UserManagementModel> userManagementModels = userManagementModel.GetUserForDetails(EncryptOperation.Decode(Uid));
      DepotManagementBL depotManagementBL = new DepotManagementBL();
      ViewBag.Depots = depotManagementBL.GetAllDepot();
      UserModel model = new UserModel();
      model.userManagementModel = userManagementModels;

      return View(model);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM")]

    public ActionResult ExportUserData(string ActionPost, HttpPostedFileBase UserFile)
    {
      UserManagementBL um = new UserManagementBL();

      if (ActionPost == "Export")
      {
        DataTable dt = um.GetAllUser();

        using (XLWorkbook wb = new XLWorkbook())
        {
          wb.Worksheets.Add(dt, "UserMaster");
          wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
          wb.Style.Font.Bold = true;
          Response.Clear();
          Response.Buffer = true;
          Response.Charset = "";
          Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
          Response.AddHeader("content-disposition", "attachment;filename= UserMaster.xlsx");
          using (MemoryStream MyMemoryStream = new MemoryStream())
          {
            wb.SaveAs(MyMemoryStream);
            MyMemoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
          }
        }
      }
      else if (ActionPost == "Upload")
      {
        string xlsname = String.Format("UserMaster-{0}", DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(UserFile.FileName);
        string Path = Server.MapPath("~") + "Data\\UserMaster\\" + xlsname;
        UserFile.SaveAs(Path);
        KulimaExcelUploadBL kulimaExcelUploadBL = new KulimaExcelUploadBL();
        kulimaExcelUploadBL.UploadUserData(Path, xlsname, User.Identity.Name);

      }

      return RedirectToAction("UserList", "User");
    }


    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult UpdateStockPrice()
    {

      return View();
    }


    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult UpdateStockPrice(string Depot, HttpPostedFileBase UserFile)
    {
      string xlsname = String.Format("SellingPrice-{0}", DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(UserFile.FileName);
      string Path = Server.MapPath("~") + "Data\\SellingPrice\\" + xlsname;
      UserFile.SaveAs(Path);
      KulimaExcelUploadBL kulimaExcelUploadBL = new KulimaExcelUploadBL();
      StatusMessage statusMessage = new StatusMessage();
      if (Depot == "Depot")
      {
        new Thread(() =>
        {
          statusMessage = kulimaExcelUploadBL.UploadDepotProductPriceData(Path, xlsname, User.Identity.Name);
          //Session["StatusMessage"] = statusMessage;

        }).Start();

      }
      else if (!string.IsNullOrEmpty(Depot))
      {
        new Thread(() =>
        {
          statusMessage = kulimaExcelUploadBL.UploadRegionProductPriceData(Path, xlsname, User.Identity.Name);
          //Session["StatusMessage"] = statusMessage;

        }).Start();
      }
      //Session["StatusMessage"] = "Import has been started. It will take approx 30 min to complete.";
      return View();
    }


    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult ExportStockData()
    {
      ProductManagementBL um = new ProductManagementBL();


      DataTable dt = um.GetAllDepotWisePriceExport(User.Identity.Name);

      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "DepotWiseSellingPrice");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= DepotWiseSellingPrice.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("UpdateStockPrice", User);
    }


    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult UpdateStockQuantity()
    {

      return View();
    }


    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult UpdateStockQuantity(HttpPostedFileBase UserFile)
    {
      string xlsname = String.Format("StockMaster-{0}", DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-")) + System.IO.Path.GetExtension(UserFile.FileName);
      string Path = Server.MapPath("~") + "Data\\StockMaster\\" + xlsname;
      UserFile.SaveAs(Path);
      KulimaExcelUploadBL kulimaExcelUploadBL = new KulimaExcelUploadBL();
      StatusMessage statusMessage = new StatusMessage();
      statusMessage = kulimaExcelUploadBL.UploadDepotProductStockData(Path, xlsname, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return View();
    }


    [HttpPost]
    [Authorize(Roles = "SPADM,HOSPR,ACTNT")]
    public ActionResult ExportStockQuantityData()
    {
      ProductManagementBL um = new ProductManagementBL();


      DataTable dt = um.GetAllDepotWiseStockExport(User.Identity.Name);

      using (XLWorkbook wb = new XLWorkbook())
      {
        wb.Worksheets.Add(dt, "DepotWiseStock");
        wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
        wb.Style.Font.Bold = true;
        Response.Clear();
        Response.Buffer = true;
        Response.Charset = "";
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;filename= DepotWiseStock.xlsx");
        using (MemoryStream MyMemoryStream = new MemoryStream())
        {
          wb.SaveAs(MyMemoryStream);
          MyMemoryStream.WriteTo(Response.OutputStream);
          Response.Flush();
          Response.End();
        }
      }

      return RedirectToAction("UpdateStockQuantity", User);
    }


    [Authorize(Roles = "HOCLK,DOCLK")]
    public ActionResult EditIDN(string Key)
    {

      int Id = Convert.ToInt32(EncryptOperation.Decode(Key));
      IDNManagementBL iDNManagement = new IDNManagementBL();
      IDNAddProductBO iDNAddProductBO = iDNManagement.GetIDNData(Id);
      return View(iDNAddProductBO);
    }

    [HttpPost]
    [Authorize(Roles = "HOCLK,DOCLK")]
    public ActionResult AddEditIDNProduct(DSREditAddProduct iDNAddProductBo)
    {
      ProductManagementBL productManagementBL = new ProductManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      iDNAddProductBo.Username = User.Identity.Name;
      statusMessage = productManagementBL.AddEditIDNProduct(iDNAddProductBo);
      Session["StatusMessage"] = statusMessage;
      string key = EncryptOperation.Encode(iDNAddProductBo.OrderId.ToString());
      return RedirectToAction("EditIDN", "User", new { key = key });
    }

    [HttpPost]
    [Authorize(Roles = "HOCLK,DOCLK")]
    public ActionResult DeleteIDNProduct(int? Id, int? Oid)
    {
      IDNManagementBL orderManagementBL = new IDNManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      statusMessage = orderManagementBL.DeleteIDNProduct(Id, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      string key = EncryptOperation.Encode(Oid.ToString());
      return RedirectToAction("EditIDN", "User", new { Key = key });
    }

    [HttpPost]
    [Authorize(Roles = "HOCLK,DOCLK")]
    public ActionResult UpdateIDNProduct(int? Odid, int? Oid, int? Quantity)
    {
      IDNManagementBL iDNManagementBL = new IDNManagementBL();
      StatusMessage statusMessage = new StatusMessage();
      statusMessage = iDNManagementBL.UpdateIDNProduct(Odid, Quantity, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      string key = EncryptOperation.Encode(Oid.ToString());
      return RedirectToAction("EditIDN", "User", new { Key = key });
    }

    [HttpPost]
    [Authorize(Roles = "HOSPR,DOSPR")]
    public ActionResult ApproveIDN(int Id)
    {
      IDNManagementBL iDNManagementBL = new IDNManagementBL();
      StatusMessage statusMessage = iDNManagementBL.ApproveIDN(Id, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return RedirectToAction("IDNList", "User");
    }

    [HttpPost]
    [Authorize(Roles = "HOSPR,DOSPR")]
    public ActionResult RejectIDN(int Id, string comment)
    {
      IDNManagementBL iDNManagementBL = new IDNManagementBL();
      StatusMessage statusMessage = iDNManagementBL.RejectIDN(Id, comment, User.Identity.Name);
      Session["StatusMessage"] = statusMessage;
      return RedirectToAction("IDNList", "User");
    }

    [HttpPost]
    [Authorize(Roles = "HOCLK,DOCLK")]
    public ActionResult UpdateIDNPhoto(CreateIDNBO create)
    {
      StatusMessage statusMessage = new StatusMessage();

      IDNManagementBL iDNManagementBL = new IDNManagementBL();
      create.Pathname = Server.MapPath("~");
      statusMessage = iDNManagementBL.UpdateIDNPhoto(create, User.Identity.Name);
      string key = EncryptOperation.Encode(create.IDNId.ToString());
      if (statusMessage.Status)
      {
        statusMessage.Message = "Photo changed successfully.";
        Session["StatusMessage"] = statusMessage;
      }
      else
      {
        Session["StatusMessage"] = statusMessage;

      }


      return RedirectToAction("EditIDN", "User", new { Key = key });
    }

    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult CreateGRR()
    {
      GRRCreateIDNList create = new GRRCreateIDNList();
      GRRManagementBL gRRManagementBL = new GRRManagementBL();
      ViewBag.IdnList = gRRManagementBL.GetAllIDNDropDown(User.Identity.Name);
      return View(create);
    }

    [HttpPost]
    [Authorize(Roles = "SPADM,HOCLK,DOCLK")]
    public ActionResult CreateGRR(CreateGRRBO create)
    {
      StatusMessage statusMessage = new StatusMessage();

      GRRManagementBL gRRManagementBL = new GRRManagementBL();
      ViewBag.IdnList = gRRManagementBL.GetAllIDNDropDown(User.Identity.Name);

      IDNManagementBL iDNManagementBL = new IDNManagementBL();

      create.Pathname = Server.MapPath("~");

      statusMessage = gRRManagementBL.CreateGRR(create, User.Identity.Name);
      if (statusMessage.Status)
      {
        int GRRId = Convert.ToInt32(statusMessage.Message);
        statusMessage.Message = "Please Add Details to GRR.";
        Session["StatusMessage"] = statusMessage;
        string key = EncryptOperation.Encode(GRRId.ToString());
        return RedirectToAction("GRRDetails", "User", new { Key = key });
      }
      else
      {
        Session["StatusMessage"] = statusMessage;
        return View(create);

      }
    }
  }
}
