﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CaptchaMvc.HtmlHelpers;
using DMS.Models;
using System.Web.Security;
using System.Web.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using DMSBL;
using DMSBO;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;

namespace DMS.Controllers
{
  [AllowAnonymous]
  public class AccountController : Controller
  {
    // GET: /Account/   
    public ActionResult Login()
    {
      // ModelState.AddModelError("Message", "Not logge in");
      GenerateCaptcha generateCaptcha = new GenerateCaptcha();
      Session["CAPTCHA"] = generateCaptcha.GetRandomText();
      return View();
    }

    [HttpPost]
    public ActionResult Login(UserAuthData userAuthData)
    {
      if (Session["CAPTCHA"] != null)
      {
        string CaptchaGenerated = (string)Session["CAPTCHA"];
        string CaptchaEntered = userAuthData.CapthaValue;

        if (!CaptchaGenerated.Equals(CaptchaEntered))
        {
            if (!string.IsNullOrEmpty(CaptchaEntered))
            {
                ModelState.AddModelError("Error", "Enter correct security code.");
            }
            GenerateCaptcha generateCaptcha = new GenerateCaptcha();
          Session["CAPTCHA"] = generateCaptcha.GetRandomText();
          return View();
        }
        else
        {
          AuthenticateUser authenticateUser = new AuthenticateUser();

          //  string BaseUrl = WebConfigurationManager.AppSettings["ApiUrl"].ToString();

          //  HttpClient client = new HttpClient();

          //  client.BaseAddress = new Uri(BaseUrl);

          ////  string query = string.Format("api/values?username={0}&password={1}", "VD00001", "sample");

          //  string query = string.Format("api/APIAccount?username={0}&password={1}", userAuthData.Username, userAuthData.Password);
          //  // Add an Accept header for JSON format.
          //  client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue("application/json"));

          // HttpResponseMessage response = client.GetAsync(query).Result;
          //bool Result = response.Content.ReadAsAsync<bool>().Result;

          StatusMessage statusMessage = authenticateUser.ValidateUser(userAuthData.Username, userAuthData.Password);

          if (statusMessage.Status)
          {
            Session["StatusMessage"] = statusMessage;
            if (authenticateUser.isAdmin(userAuthData.Username))
            {
              Session["AdminAuth"] = userAuthData.Username;
              Session["StatusMessage"] = authenticateUser.UserOtp(userAuthData.Username);
              return RedirectToAction("OtpVerification", "Account", new {Remember=userAuthData.Remember});
            }
            else
            {
              FormsAuthentication.SetAuthCookie(userAuthData.Username, userAuthData.Remember);
              return RedirectToAction("Dashboard", "User");
            }
          }
          else
          {
            ModelState.AddModelError("Error", "Not a valid Username/Password");
            GenerateCaptcha generateCaptcha = new GenerateCaptcha();
            Session["CAPTCHA"] = generateCaptcha.GetRandomText();
            return View();
          }
        }
      }
      else
      {
        ModelState.AddModelError("Error", "Session timed out. Please login again");
        GenerateCaptcha generateCaptcha = new GenerateCaptcha();
        Session["CAPTCHA"] = generateCaptcha.GetRandomText();
        return View();
      }
    }

    public ActionResult ForgotPassword()
    {
      GenerateCaptcha generateCaptcha = new GenerateCaptcha();
      Session["CAPTCHA"] = generateCaptcha.GetRandomText();
      return View();
    }

    [HttpPost]
    public ActionResult ForgotPassword(string Username, string CapthaValue)
    {
      if (Session["CAPTCHA"] != null)
      {
        StatusMessage StatusMessage = new StatusMessage();
        string CaptchaGenerated = (string)Session["CAPTCHA"];
        string CaptchaEntered = CapthaValue;

        if (!CaptchaGenerated.Equals(CaptchaEntered))
        {
          GenerateCaptcha generateCaptcha = new GenerateCaptcha();
          Session["CAPTCHA"] = generateCaptcha.GetRandomText();
          ModelState.AddModelError("Error", "Captcha not mathced");
          return View();
        }
        else
        {
          AuthenticateUser authenticateUser = new AuthenticateUser();
          StatusMessage = authenticateUser.ForgotUser(Username);
          if (StatusMessage.Status)
          {
            Session["StatusMessage"] = StatusMessage;
            return RedirectToAction("Login", "Account");
          }
          else
          {
            ModelState.AddModelError("Error", StatusMessage.Message);
            GenerateCaptcha generateCaptcha = new GenerateCaptcha();
            Session["CAPTCHA"] = generateCaptcha.GetRandomText();
            return View();
          }
        }
      }
      else
      {
        ModelState.AddModelError("Error", "Session timed out. Please login again");
        GenerateCaptcha generateCaptcha = new GenerateCaptcha();
        Session["CAPTCHA"] = generateCaptcha.GetRandomText();
        return View();
      }
    }

    [HttpGet]
    public ActionResult ResetPassword(string key, string username)
    {
      ChangePassword resetpassword = new ChangePassword()
      {
        key = key,
        Username = username
      };

      GenerateCaptcha generateCaptcha = new GenerateCaptcha();
      Session["CAPTCHA"] = generateCaptcha.GetRandomText();

      return View(resetpassword);
    }

    [HttpPost]
    public ActionResult ResetPassword(ChangePassword changePassword)
    {
      if (Session["CAPTCHA"] != null)
      {
        string CaptchaGenerated = (string)Session["CAPTCHA"];
        string CaptchaEntered = changePassword.CapthaValue;

        if (ModelState.IsValid)
        {

          if (!CaptchaGenerated.Equals(CaptchaEntered))
          {

              if (!string.IsNullOrEmpty(CaptchaEntered))
              {
                  ModelState.AddModelError("Error", "Please enter correct security code.");
              } GenerateCaptcha generateCaptcha = new GenerateCaptcha();
            Session["CAPTCHA"] = generateCaptcha.GetRandomText();

            return View(changePassword);
          }

          AuthenticateUser authenticateUser = new AuthenticateUser();
          StatusMessage statusMessage = authenticateUser.ResetUserPassword(changePassword);
          Session["StatusMessage"] = statusMessage;
          return RedirectToAction("Login", "Account");
        }
        else
        {
            if (!CaptchaGenerated.Equals(CaptchaEntered))
            {

                if (!string.IsNullOrEmpty(CaptchaEntered))
                {

                    ModelState.AddModelError("Error", "Please enter correct security code.");
                }
            }
          GenerateCaptcha generateCaptcha = new GenerateCaptcha();
          Session["CAPTCHA"] = generateCaptcha.GetRandomText();
          return View(changePassword);
        }
      }
      else
      {
        ModelState.AddModelError("Error", "Session timed out. Please login again");
        GenerateCaptcha generateCaptcha = new GenerateCaptcha();
        Session["CAPTCHA"] = generateCaptcha.GetRandomText();
        return View();
      }
    }

    [HttpGet]
    public ActionResult OtpVerification(bool Remember)
    {
      GenerateCaptcha generateCaptcha = new GenerateCaptcha();
      Session["CAPTCHA"] = generateCaptcha.GetRandomText();
      ViewBag.Remember = Remember;
      return View();
    }

    [HttpPost]
    public ActionResult OtpVerification([Required]string OTP,bool Remember)
    {

      // string CaptchaGenerated = (string)Session["CAPTCHA"];
      //string CaptchaEntered = CapthaValue;


      if (ModelState.IsValid)
      {


        AuthenticateUser authenticateUser = new AuthenticateUser();

        StatusMessage statusMessage = authenticateUser.IsValidOTP((string)Session["AdminAuth"], OTP);
        if (statusMessage.Status)
        {
          FormsAuthentication.SetAuthCookie((string)Session["AdminAuth"], Remember);
          Session["StatusMessage"] = statusMessage;
          return RedirectToAction("Dashboard", "User");
        }
        else
        {
          GenerateCaptcha generateCaptcha = new GenerateCaptcha();

          ModelState.AddModelError("Error", statusMessage.Message);
          return View();
        }

      }
      else
      {

        GenerateCaptcha generateCaptcha = new GenerateCaptcha();

        return View();
      }
    }

    public ActionResult Logout()
    {
      FormsAuthentication.SignOut();
      return RedirectToAction("Login", "Account");
    }



    [HttpPost]
    public ActionResult RequestChangePassword(ChangePasswordUser data)
    {
      string message = "";

      StatusMessage statusMessage = new StatusMessage()
          {
            Status = false
          };


      if (ModelState.IsValid)
      {


        AuthenticateUser authenticateUser = new AuthenticateUser();

        statusMessage = authenticateUser.RequestChangePassword(data);
        if (statusMessage.Status)
        {
          Session["StatusMessage"] = statusMessage;
          return RedirectToAction("Dashboard", "User");
        }
        else
        {



          Session["StatusMessage"] = statusMessage;
          return RedirectToAction("Dashboard", "User");

        }

      }
      else
      {

        Session["Validation"] = ViewData.ModelState.Values;
        return RedirectToAction("Dashboard", "User");

      }
    }


    public FileResult GetCaptchaImage()
    {
      string text = Session["CAPTCHA"].ToString();

      //first, create a dummy bitmap just to get a graphics object
      System.Drawing.Image img = new Bitmap(1, 1);
      Graphics drawing = Graphics.FromImage(img);

      Font font = new Font("Arial", 15);
      //measure the string to see how big the image needs to be
      SizeF textSize = drawing.MeasureString(text, font);

      //free up the dummy image and old graphics object
      img.Dispose();
      drawing.Dispose();

      //create a new image of the right size
      img = new Bitmap((int)textSize.Width + 40, (int)textSize.Height + 20);
      drawing = Graphics.FromImage(img);

      Color backColor = Color.White;
      Color textColor = Color.Black;
      //paint the background
      drawing.Clear(backColor);

      //create a brush for the text
      Brush textBrush = new SolidBrush(textColor);

      drawing.DrawString(text, font, textBrush, 20, 10);

      drawing.Save();

      font.Dispose();
      textBrush.Dispose();
      drawing.Dispose();

      MemoryStream ms = new MemoryStream();
      img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
      img.Dispose();

      return File(ms.ToArray(), "image/png");
    }




  }
}
