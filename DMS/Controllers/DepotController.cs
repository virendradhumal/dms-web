﻿using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers
{
    public class DepotController : ApiController
    {

        [HttpPost]
        [ActionName("getDepotList")]
        public HttpResponseMessage FetchAll(DepotParameter depotParamenter)
        {
            try
            {
                DepotManagementBL depotManagementBL = new DepotManagementBL();
                DepotListResult result = new DepotListResult();
                result.DepotList= depotManagementBL.GetAllDepotList(depotParamenter);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                if (result.DepotList.Count() == 0)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

    }
}
