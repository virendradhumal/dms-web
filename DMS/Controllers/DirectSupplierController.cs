﻿using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers
{
    public class DirectSupplierController : ApiController
    {

        [HttpGet]
        [ActionName("getDirectSupplierList")]
        public HttpResponseMessage Fetch()
        {
            try
            {
                DirectSupplierManagementBL directSupplierManagementBL = new DMSBL.DirectSupplierManagementBL();
                IEnumerable<DirectSupplierBO> getOrderResult = directSupplierManagementBL.GetList();
                if (getOrderResult == null )
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, getOrderResult);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

    }
}
