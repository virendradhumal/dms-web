﻿using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DMS.Controllers.Api;
using System.Threading.Tasks;
using DMS.Models;
using System.Web;
using System.IO;
using System.Diagnostics;
//8975674964 Dms#1234
namespace DMS.Controllers
{
    public class BankTransactionsController : ApiController
    {
        [HttpPost]
        [Route("getBankBranchList")]
        public HttpResponseMessage getBankBranchList(BankBranchAPIBO bankBranchAPIBO)
        {
            BankBranchResult bankBranchResult = new BankBranchResult();
            try
            {
                TransactionManagementBL transactionManagementBL = new TransactionManagementBL();
                bankBranchResult = transactionManagementBL.BankBranch(bankBranchAPIBO);

                if (bankBranchResult == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                return Request.CreateResponse(HttpStatusCode.OK,bankBranchResult);
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
            }

        }

        [HttpPost]
        [ActionName("getBankTransactionList")]
        public HttpResponseMessage List(DSRListParameter dSRListParameter)
        {
            try
            {
                TransactionManagementBL transactionManagementBL = new TransactionManagementBL();
                var result = transactionManagementBL.List(dSRListParameter);
                if (result == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
            
        }
        
        [HttpPost]
        [ActionName("getBankTransactionDetails")]
        public HttpResponseMessage GetTrans(GetTransParaBO para)
        {
            try
            {
                TransactionManagementBL transactionManagement = new TransactionManagementBL();
                var Result = transactionManagement.SpecificTransactionAPI(para.PKTransactionId);
                if (Result == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                return Request.CreateResponse(HttpStatusCode.OK, Result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }


        }


        [HttpPost]
        [ActionName("createBankTransaction")]
        public async Task<HttpResponseMessage> Create()
        {
            TransactionManagementBL transactionManagement = new TransactionManagementBL();
            CreateTRNResultAPIBO output = new CreateTRNResultAPIBO();

            try
            {
                string root = HttpContext.Current.Server.MapPath("~/");

                Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                var result = await Request.Content.ReadAsMultipartAsync(provider);
                var model = result.FormData["Trndetail"];
                CreateTRNAPIBO data = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateTRNAPIBO>(model);
                string imagename = String.Format("Data\\TRN\\{0}-{1}",  data.DSRNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-"));

                foreach (var item in result.FileData)
                {
                    string filename = root + "\\" + imagename;
                    string name = item.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    filename += System.IO.Path.GetExtension(name);
                    data.Path = imagename + System.IO.Path.GetExtension(name);
                    Trace.WriteLine(item.Headers.ContentDisposition.FileName);
                    File.Move(item.LocalFileName, filename);
                    break;
                }

                StatusMessage Status = transactionManagement.CreateTRNApi(data);
                
                if (Status.Status)
                {
                    output.PKTransactionId =Convert.ToInt32( Status.Message);
                    return Request.CreateResponse(HttpStatusCode.Created, output);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,Status);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
            
        }

        [HttpPost]
        [ActionName("UpdateStatus")]
        public HttpResponseMessage Approve([FromBody]TransactionApproveAPIBO para)
        {
            try
            {
                TransactionManagementBL transactionManagementBL = new TransactionManagementBL();
                StatusMessage status = new StatusMessage();
                ErrorStatus ErrorCode = new ErrorStatus();
                if (para.TransactionStatus.ToLower() == "approved")
                {
                    status = transactionManagementBL.ApproveTransaction(para);
                }
                else
                {
                    if (para.TransactionStatus.ToLower() == "rejected")
                        status = transactionManagementBL.RejectTransaction(para);
                }


                if (status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,status);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
            }
        }

    }
}
