﻿
using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers
{
    public class ForgetPasswordController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage ForgotPassword(LoginParameter loginParameter)
        {
            try
            {
                ErrorStatus errorStatus = new ErrorStatus();
                AuthenticateUser authenticateUser = new AuthenticateUser();
                StatusMessage statusMessage = authenticateUser.ForgotUser(loginParameter.Username);

                if (statusMessage.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
            }
        }
    }
}
