﻿using DMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers.Api
{
    public class ServerDateController : ApiController
    {
        // GET api/serverdate
        public HttpResponseMessage Get()
        {
            DateApi DateApi = new DateApi();
            try
            {
                DateTime baseDate = new DateTime(1970, 1, 1);
                TimeSpan diff = DateTime.Now - baseDate;
            
                DateApi.DateMiliseconds = diff.TotalMilliseconds.ToString();

                return Request.CreateResponse(HttpStatusCode.OK, DateApi);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
            
        }

    }
}
