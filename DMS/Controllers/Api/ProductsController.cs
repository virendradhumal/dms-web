﻿using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers
{
  public class ProductsController : ApiController
  {
    [HttpPost]
    [ActionName("getProducts")]
    public HttpResponseMessage getProducts(ProductParameter productParameter)
    {
      try
      {
        ProductManagementBL productManagement = new ProductManagementBL();
        IEnumerable<ProductAllDetailBOApi> ProductList = productManagement.GetAllProductDetailApi();
        var Data = ProductList.AsQueryable();
        if (!string.IsNullOrEmpty(productParameter.SearchParameter))
          Data = Data.Where(x => x.Description.ToLower().Contains(productParameter.SearchParameter.ToLower()) || x.ProductCode.ToLower().Contains(productParameter.SearchParameter.ToLower()));
        Data = Data.Where(x => x.DepotId == productParameter.DepotId);
        ReturnProductBO result = new ReturnProductBO();
        result.Products = Data.AsEnumerable();
        return Request.CreateResponse(HttpStatusCode.OK, result);
      }
      catch (Exception ex)
      {
        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
      }
    }

    [HttpPost]
    [ActionName("getProductDetails")]
    public HttpResponseMessage getProductDetails(ProductParameter productParameter)
    {
      try
      {
        ProductManagementBL productManagement = new ProductManagementBL();
        var ProductDetails = productManagement.GetProductDetailApi(productParameter.DepotId, productParameter.SearchParameter);
        if (ProductDetails == null)
          return Request.CreateResponse(HttpStatusCode.BadRequest);

        return Request.CreateResponse(HttpStatusCode.OK, ProductDetails);
      }
      catch (Exception ex)
      {
        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
      }
    }
  }
}
