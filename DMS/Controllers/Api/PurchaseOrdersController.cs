﻿using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers
{
    public class PurchaseOrdersController : ApiController
    {

        [HttpPost]
        [ActionName("getOrders")]
        public HttpResponseMessage Fetch(GetOrderParameter getOrderParameter)
        {
            try
            {
                PurchaseOrdersBL purchaseOrdersBL = new DMSBL.PurchaseOrdersBL();
                GetPOrderResult getOrderResult = purchaseOrdersBL.GetOrderApi(getOrderParameter);
                if (getOrderResult.Orders == null )
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, getOrderResult);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }


        [HttpPost]
        [ActionName("getOrderDetails")]
        public HttpResponseMessage getOrderDetails(OrderDetailParameter orderDetailParameter)
        {
            try
            {
                POrderDetailResultApi orderDetailResultApi = new POrderDetailResultApi();
                PurchaseOrdersBL purchaseOrdersBL = new PurchaseOrdersBL();
                orderDetailResultApi = purchaseOrdersBL.GetAllProductDetail(orderDetailParameter);
                if (orderDetailResultApi == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                return Request.CreateResponse(HttpStatusCode.OK, orderDetailResultApi);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }

        }


        [HttpPost]
        [ActionName("insertOrder")]
        public HttpResponseMessage Insert([FromBody] InsertOrderParameterApi insertOrderParameterApi)
        {
            try
            {
                PurchaseOrdersBL purchaseOrdersBL = new PurchaseOrdersBL();
                StatusMessage statusMessage = purchaseOrdersBL.InsertOrder(insertOrderParameterApi);
                InsertOrderResultApi insertOrderResultApi = new InsertOrderResultApi();

                if (statusMessage.Status)
                {
                    insertOrderResultApi.OrderNo = statusMessage.Message;
                    return Request.CreateResponse(HttpStatusCode.Created, insertOrderResultApi);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }

        }



        [HttpPost]
        [ActionName("UpdateStatus")]
        public HttpResponseMessage Approve([FromBody]PurchaseOrderApproveAPIBO para)
        {
            try
            {
                PurchaseOrdersBL purchaseOrdersBL = new PurchaseOrdersBL();
                StatusMessage status = new StatusMessage();
                ErrorStatus ErrorCode = new ErrorStatus();
                if (para.OrderStatus.ToLower() == "approved")
                {
                    status = purchaseOrdersBL.ApproveOrder(para);
                }
                else
                {
                    if (para.OrderStatus.ToLower() == "rejected")
                        status = purchaseOrdersBL.RejectOrder(para);
                }


                if (status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, status);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

    
    }
}
