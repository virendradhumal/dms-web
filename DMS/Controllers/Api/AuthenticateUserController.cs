﻿using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers
{
    public class AuthenticateUserController : ApiController
    {
        
        [HttpPost]
        [ActionName("Authenticate")]
        public HttpResponseMessage Authenticate([FromBody]LoginParameter loginParameter)
        {
            try
            {
                AuthenticateUser authenticateUser = new AuthenticateUser();
                StatusMessage statusMessage = authenticateUser.ValidateUserApp(loginParameter.Username, loginParameter.Password);

                LoginResult loginResult = new LoginResult();
                if (statusMessage.Status)
                {
                    loginResult = authenticateUser.AutheticateUserApi(loginParameter);
                    return Request.CreateResponse(HttpStatusCode.OK, loginResult);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
            

        }
    }
}
