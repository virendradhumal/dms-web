﻿using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
namespace DMS.Controllers
{
    public class DSRController : ApiController
    {
        // GET api/dsr
        [HttpPost]
        [ActionName("getDSRList")]
        public HttpResponseMessage FetchAll(DSRListParameter dSRListParameter)
        {
            try
            {
                DSRManagementBL dSRManagementBL = new DSRManagementBL();
                DSRListResult result = dSRManagementBL.GetAllDsrApi(dSRListParameter);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [ActionName("CreateDSR")]
        public  HttpResponseMessage Create([FromBody]CreateDSRParameter data)
        {
            DSRManagementBL dSRManagementBL = new DSRManagementBL();
            
            try
            {
              
                StatusMessage Status = dSRManagementBL.CreateDSRApi(data);
                if (Status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, Status);
                }

            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
            
        }


        [HttpPost]
        [ActionName("UploadDSRImage")]
        public async Task<HttpResponseMessage> Create()
        {
            DSRManagementBL dSRManagementBL = new DSRManagementBL();
            ErrorStatus error = new ErrorStatus();

            try
            {
                string root = HttpContext.Current.Server.MapPath("~/");

                Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                var result = await Request.Content.ReadAsMultipartAsync(provider);
                var model = result.FormData["DSRImageDetail"];
                UploadDSRImageBO data = Newtonsoft.Json.JsonConvert.DeserializeObject<UploadDSRImageBO>(model);
                string imagename = String.Format("Data\\DSR\\{0}-{1}-{2}", data.DepotId, data.DSRNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-"));

                foreach (var item in result.FileData)
                {
                    string filename = root + "\\" + imagename;
                    string name = item.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    filename += System.IO.Path.GetExtension(name);
                    data.Path = imagename + System.IO.Path.GetExtension(name);
                    Trace.WriteLine(item.Headers.ContentDisposition.FileName);
                    File.Move(item.LocalFileName, filename);
                    break;
                }

                StatusMessage Status = dSRManagementBL.UploadDSRImageApi(data);
                if (Status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,Status);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }


        [HttpPost]
        [ActionName("getOrders")]
        public HttpResponseMessage TodaysOrder(DSRListParameter dSRListParameter)
        {
            try
            {
                DSRManagementBL dSRManagementBL = new DSRManagementBL();
                DSRTodayOrderApiResult data = dSRManagementBL.GetTodaysOrderApi(dSRListParameter);
                if (data.Products.Count() == 0)
                    return Request.CreateResponse(HttpStatusCode.NotFound);

                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
            }
        }


        [HttpPost]
        [ActionName("getDSRDetails")]
        public HttpResponseMessage GetByDSRNO([FromBody]SpecificDSRAPIBO specificDSRAPIBO)
        {
            try
            {
                DSRManagementBL DSRManagementBL = new DSRManagementBL();
                var result = DSRManagementBL.GetSpecificDSR(specificDSRAPIBO.DSRNo);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }

        }


        [HttpPost]
        [ActionName("UpdateStatus")]
        public HttpResponseMessage Approve([FromBody]DSRApproveAPIBO para)
        {
            try
            {
                DSRManagementBL DSRManagementBL = new DSRManagementBL();
                StatusMessage status = new StatusMessage();
                ErrorStatus ErrorCode = new ErrorStatus();
                if (para.DSRStatus.ToLower() == "approved")
                {
                    status = DSRManagementBL.ApproveDsr(para);
                }
                else
                {
                    if (para.DSRStatus.ToLower() == "rejected")
                        status = DSRManagementBL.RejectDsr(para);
                }


                if (status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest,status);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
            }
        }

    
    
    }
}
