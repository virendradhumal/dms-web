﻿using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;  

namespace DMS.Controllers
{
    public class GRRController : ApiController
    {
        [HttpPost]
        [ActionName("getGRRList")]
        public HttpResponseMessage List(DSRListParameter dSRListParameter)
        {
            try
            {
                GRRManagementBL iDNManagementBL = new GRRManagementBL();
                var result= iDNManagementBL.GetAllIDNData(dSRListParameter);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                return Request.CreateResponse(HttpStatusCode.OK,result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [ActionName("getIDNList")]
        public HttpResponseMessage IDNList(DSRListParameter dSRListParameter)
        {
            try
            {
                GRRManagementBL iDNManagementBL = new GRRManagementBL();
                var result = iDNManagementBL.GetAllIDNGRRData(dSRListParameter);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
     

        [HttpPost]
        [ActionName("getGRRDetails")]
        public HttpResponseMessage GetSpecificIDN(GetGRRPara Para)
        {

            try
            {
                GRRManagementBL iDNManagementBL = new GRRManagementBL();

                var result = iDNManagementBL.GetIDNData(Para.GRRNo);

                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [ActionName("CreateGRR")]
        public async Task<HttpResponseMessage> Create()
        {
            GRRManagementBL iDNManagementBL = new GRRManagementBL();
            
            try
            {
                string root = HttpContext.Current.Server.MapPath("~/");

                Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                var result = await Request.Content.ReadAsMultipartAsync(provider);
                var model = result.FormData["Grrdetail"];
                IDNReceiveAPIBO data = Newtonsoft.Json.JsonConvert.DeserializeObject<IDNReceiveAPIBO>(model);
                string imagename = String.Format("Data\\GRR\\{0}-{1}", data.GRRNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-"));

                foreach (var item in result.FileData)
                {
                    string filename = root + "\\" + imagename;
                    string name = item.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    filename += System.IO.Path.GetExtension(name);
                    data.Grrimage = imagename + System.IO.Path.GetExtension(name);
                    Trace.WriteLine(item.Headers.ContentDisposition.FileName);
                    File.Move(item.LocalFileName, filename);
                    break;
                }

                StatusMessage Status = iDNManagementBL.CreateGRR(data);
                if (Status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
            }
            
            
        }

        [HttpPost]
        [ActionName("UpdateStatus")]
        public HttpResponseMessage Approve([FromBody] GRRApproveBO para)
        {
            try
            {
                GRRManagementBL GRRManagementBL = new GRRManagementBL();
                StatusMessage status = new StatusMessage();
                if (para.GRRStatus.ToLower() == "approved")
                {
                    status = GRRManagementBL.ApproveGRR(para);
                }
                else
                {
                    if (para.GRRStatus.ToLower() == "rejected")
                        status = GRRManagementBL.RejectGRR(para);
                }


                if (status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
            }
           
        }

    }
}
