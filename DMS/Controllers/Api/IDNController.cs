﻿using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DMS.Controllers
{
    public class IDNController : ApiController
    {
        // GET api/idn
        [HttpPost]
        [ActionName("getIDNList")]
        public HttpResponseMessage List(DSRListParameter dSRListParameter)
        {
            try
            {
                IDNManagementBL iDNManagementBL = new IDNManagementBL();
                var result = iDNManagementBL.GetAllIDNData(dSRListParameter);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
     
        [HttpPost]
        [ActionName("getIDNDetails")]
        public HttpResponseMessage GetSpecificIDN(GetIDNParaAPI Para)
        {

            try
            {
                IDNManagementBL iDNManagementBL = new IDNManagementBL();

                var result = iDNManagementBL.GetIDNData(Para.IDNNo);

                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

         [HttpPost]
         [ActionName("CreateIDN")]
         public async Task<HttpResponseMessage> Create()
         {
             IDNManagementBL iDNManagementBL = new IDNManagementBL();
             ErrorStatus error = new ErrorStatus();

             try
             {
                 string root = HttpContext.Current.Server.MapPath("~/");

                 Directory.CreateDirectory(root);
                 var provider = new MultipartFormDataStreamProvider(root);
                 var result = await Request.Content.ReadAsMultipartAsync(provider);
                 var model = result.FormData["Idndetail"];
                 CreateAPIIDNBO data = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateAPIIDNBO>(model);
                 string imagename = String.Format("Data\\IDN\\{0}-{1}", data.IDNNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-"));

                 foreach (var item in result.FileData)
                 {
                     string filename = root + "\\" + imagename;
                     string name = item.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                     filename += System.IO.Path.GetExtension(name);
                     data.IDNImage = imagename + System.IO.Path.GetExtension(name);
                     Trace.WriteLine(item.Headers.ContentDisposition.FileName);
                     File.Move(item.LocalFileName, filename);
                     break;
                 }

                 StatusMessage Status = iDNManagementBL.CreateIDN(data);
                 if (Status.Status)
                 {
                     return Request.CreateResponse(HttpStatusCode.Created);
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest,Status);
                 }
             }
             catch (Exception ex)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,ex);
             }
            
         }
     
        
        [HttpPost]
        [ActionName("EditIDN")]
        public async Task<HttpResponseMessage> EditSpecificIDN()
        {
            IDNManagementBL iDNManagementBL = new IDNManagementBL();
            ErrorStatus error = new ErrorStatus();

            try
            {
                string root = HttpContext.Current.Server.MapPath("~/");

                Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                var result = await Request.Content.ReadAsMultipartAsync(provider);
                var model = result.FormData["Idndetail"];
                CreateAPIIDNBO data = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateAPIIDNBO>(model);
                string imagename = String.Format("Data\\IDN\\{0}-{1}", data.IDNNo, DateTime.Now.ToString().Replace("/", "-").Replace(":", "-").Replace(" ", "-"));

                foreach (var item in result.FileData)
                {
                    string filename = root + "\\" + imagename;
                    string name = item.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    filename += System.IO.Path.GetExtension(name);
                    data.IDNImage = imagename + System.IO.Path.GetExtension(name);
                    Trace.WriteLine(item.Headers.ContentDisposition.FileName);
                    File.Move(item.LocalFileName, filename);
                    break;
                }

                StatusMessage Status =  iDNManagementBL.EditIDNAPI(data);
                if (Status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, Status);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }

        }

        [HttpPost]
        [ActionName("UpdateStatus")]
        public HttpResponseMessage Approve([FromBody]ApproveIDNAPIBO para)
        {
            try
            {
                IDNManagementBL DSRManagementBL = new IDNManagementBL();
                StatusMessage status = new StatusMessage();
                ErrorStatus ErrorCode = new ErrorStatus();
                if (para.Status.ToLower() == "approved")
                {
                    status = DSRManagementBL.ApproveIDN(para.IDNNo,para.Username);
                }
                else
                {
                    if (para.Status.ToLower() == "rejected")
                        status = DSRManagementBL.RejectIDN(para.IDNNo,para.Message,para.Username);
                }


                if (status.Status)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, status);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }




    }
}
