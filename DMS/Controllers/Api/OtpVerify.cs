﻿using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DMS.Controllers
{
    public class OtpVerify
    {
        [HttpGet]
        public StatusMessage Checkotp(string username,string OTP)
        {
            AuthenticateUser authenticateUser = new AuthenticateUser();
            StatusMessage statusMessage = authenticateUser.IsValidOTP(username, OTP);
            return statusMessage;
        }
    }
}