﻿using DMS.Models;
using DMSBL;
using DMSBO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DMS.Controllers
{
  public class OrdersController : ApiController
  {
    [HttpPost]
    [ActionName("getOrders")]
    public HttpResponseMessage Fetch(GetOrderParameter getOrderParameter)
    {
      try
      {
        OrderManagementBL orderManagementBL = new DMSBL.OrderManagementBL();
        GetOrderResult getOrderResult = orderManagementBL.GetOrderApi(getOrderParameter);
        if (getOrderResult.Orders == null || getOrderResult.Orders.Count() == 0)
        {
          return Request.CreateResponse(HttpStatusCode.NotFound);
        }
        return Request.CreateResponse(HttpStatusCode.OK, getOrderResult);
      }
      catch (Exception ex)
      {
        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
      }
    }


    [HttpPost]
    [ActionName("getOrderDetails")]
    public HttpResponseMessage getOrderDetails(OrderDetailParameter orderDetailParameter)
    {
      try
      {
        OrderDetailResultApi orderDetailResultApi = new OrderDetailResultApi();
        OrderManagementBL orderManagementBL = new OrderManagementBL();
        orderDetailResultApi = orderManagementBL.GetAllProductDetail(orderDetailParameter);
        if (orderDetailResultApi == null)
        {
          return Request.CreateResponse(HttpStatusCode.NotFound);
        }
        return Request.CreateResponse(HttpStatusCode.OK, orderDetailResultApi);
      }
      catch (Exception ex)
      {
        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
      }

    }


    [HttpPost]
    [ActionName("insertOrder")]
    public HttpResponseMessage Insert([FromBody] InsertOrderParameterApi insertOrderParameterApi)
    {
      try
      {
        OrderManagementBL orderManagementBL = new OrderManagementBL();
        StatusMessage statusMessage = orderManagementBL.InsertOrder(insertOrderParameterApi);
        InsertOrderResultApi insertOrderResultApi = new InsertOrderResultApi();

        if (statusMessage.Status)
        {
          insertOrderResultApi.OrderNo = statusMessage.Message;
          return Request.CreateResponse(HttpStatusCode.Created, insertOrderResultApi);
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
      }
      catch (Exception ex)
      {
        LogsExceptionBL.LogError(ex, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
      }

    }


  }
}
